# # -*- coding: utf-8 -*-
# policy_interpreter.py
"""
This python module implements the HSPL/MSPL policy interpreter inside the ANASTACIA European Project,
extending the HSPL/MSPL language defined in secured project.
How to use:
	Run the server: gunicorn policy_interpreter:app
	Request for HSPL/MSPL translation: curl localhost:8000/h2mservice -d @[hspl_policy.json]
	Request for /Lower translation: curl localhost:8000/m2lservice -d @[mspl_policy.json]
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

# Let's get this party started!
import falcon
import json
import requests
from datetime import datetime
#from h2mrefiner import H2MRefiner
from m2ltranslator import M2LTranslator
#from mud2mtranslator import MUD2MTranslator
#from utils import timing
from settings import logging, remote_logger
logger = logging.getLogger(__name__)
import mspl
from conflict_detector import MSPLConflictDetector


class MCDTService(object):
    """Implementation of MSPL Conflict Detection service"""
       
    def to_json(self,conflicts_dependencies):
        """Parse the conflicts to json"""
        # TODO: Loop and build the json
        return json.dumps(conflicts_dependencies)

    #@timing("MCDTService")
    def on_post(self, req, resp):
        """Handles POST requests for MSPL conflict detection."""

        # Not empty data verification
        data = req.stream.read().decode("utf-8")
        if not data:
            raise falcon.HTTPInvalidParam('Empty request body',
                                        'A valid JSON/XML document is required.')


        logging_message = {
         "timestamp": datetime.timestamp(datetime.now()),
         "from_module": "POLICY_MANAGER",
         "from_component": "POLICY_INTERPRETER",
         "to_module": "POLICY_MANAGER",
         "to_component": "CONFLICT_DETECTOR",
         "incoming": True,
         "method": "REST",
         "data": data,
         "notes": "MCDT request"
        }
        remote_logger.info(logging_message)

        # We assume the input is an xml
        logger.debug("MCDTSERVICE receives: {}".format(data))
        m2l_translator = M2LTranslator()
        mspl_object =  m2l_translator.load(data)
        logger.debug("MSPL Loaded successfully")
        #mspl_object =  M2LTranslator.load(data)

        # Policy conclict detection
        detector = MSPLConflictDetector()
        conflicts_dependencies = detector.detect(mspl_object)
        resp.body = self.to_json(conflicts_dependencies)

        logging_message = {
         "timestamp": datetime.timestamp(datetime.now()),
         "from_module": "POLICY_MANAGER",
         "from_component": "CONFLICT_DETECTOR",
         "to_module": "POLICY_MANAGER",
         "to_component": "POLICY_INTERPRETER",
         "incoming": False,
         "method": "REST",
         "data": {"conflicts_dependencies":conflicts_dependencies},
         "notes": "MCDT result"
        }
        
        remote_logger.info(logging_message)
        

class HealthService(object):

    def on_get(self, req, resp):
        resp.body = json.dumps({"status":"OK"})


# falcon.API instances are callable WSGI apps
app = falcon.API()

# Resources are represented by long-lived class instances
mcdtservice = MCDTService()
health_service = HealthService()

# things will handle all requests to the '/things' URL path
app.add_route('/mcdtservice', mcdtservice)
app.add_route('/health', health_service)

