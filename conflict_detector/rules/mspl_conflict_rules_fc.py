# mspl_conflict_rules_fc.py

from pyke import contexts, pattern, fc_rule, knowledge_base

pyke_version = '1.1.1'
compiler_version = 1

def verify_id_conflicts(rule, context = None, index = None):
  engine = rule.rule_base.engine
  if context is None: context = contexts.simple_context()
  try:
    with knowledge_base.Gen_once if index == 0 \
             else engine.lookup('mspls', 'mspl', context,
                                rule.foreach_patterns(0)) \
      as gen_0:
      for dummy in gen_0:
        tot = 0
        with knowledge_base.Gen_once if index == 1 \
                 else engine.lookup('mspls', 'mspl', context,
                                    rule.foreach_patterns(1)) \
          as gen_1:
          for dummy in gen_1:
            if context.lookup_data('mspl1').id == context.lookup_data('mspl2').id:
              tot = tot + 1
              if tot > 1:
                print("ID Conflict!")
                engine.assert_('mspls', 'mspl_conflict',
                               (rule.pattern(0).as_data(context),
                                rule.pattern(1).as_data(context),
                                rule.pattern(2).as_data(context),)),
                rule.rule_base.num_fc_rules_triggered += 1
  finally:
    context.done()

def verify_same_filtering_l4_behaviour(rule, context = None, index = None):
  engine = rule.rule_base.engine
  if context is None: context = contexts.simple_context()
  try:
    with knowledge_base.Gen_once if index == 0 \
             else engine.lookup('mspls', 'mspl', context,
                                rule.foreach_patterns(0)) \
      as gen_0:
      for dummy in gen_0:
        with knowledge_base.Gen_once if index == 1 \
                 else engine.lookup('mspls', 'mspl', context,
                                    rule.foreach_patterns(1)) \
          as gen_1:
          for dummy in gen_1:
            if context.lookup_data('mspl1').id != context.lookup_data('mspl2').id:
              if context.lookup_data('mspl1').configuration.capability[0].Name == context.lookup_data('mspl2').configuration.capability[0].Name == "Filtering_L4":
                if context.lookup_data('mspl1').configuration.configurationRule[0].configurationRuleAction.FilteringActionType == context.lookup_data('mspl2').configuration.configurationRule[0].configurationRuleAction.FilteringActionType:
                  if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourceAddress == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourceAddress:
                    if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.DestinationAddress == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.DestinationAddress:
                      if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourcePort == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourcePort:
                        if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.DestinationPort == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.DestinationPort:
                          if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.Interface == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.Interface:
                            if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.ProtocolType == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.ProtocolType:
                              print(context.lookup_data('mspl1').id,"Same L4 behaviour Conflict!",context.lookup_data('mspl2').id)
                              engine.assert_('mspls', 'mspl_conflict',
                                             (rule.pattern(0).as_data(context),
                                              rule.pattern(1).as_data(context),
                                              rule.pattern(2).as_data(context),)),
                              rule.rule_base.num_fc_rules_triggered += 1
  finally:
    context.done()

def verify_managers_conflict_l4_behaviour(rule, context = None, index = None):
  engine = rule.rule_base.engine
  if context is None: context = contexts.simple_context()
  try:
    with knowledge_base.Gen_once if index == 0 \
             else engine.lookup('mspls', 'mspl', context,
                                rule.foreach_patterns(0)) \
      as gen_0:
      for dummy in gen_0:
        with knowledge_base.Gen_once if index == 1 \
                 else engine.lookup('mspls', 'mspl', context,
                                    rule.foreach_patterns(1)) \
          as gen_1:
          for dummy in gen_1:
            if context.lookup_data('mspl1').configuration.capability[0].Name == context.lookup_data('mspl2').configuration.capability[0].Name == "Filtering_L4":
              if context.lookup_data('mspl1').configuration.configurationRule[0].configurationRuleAction.FilteringActionType == "ALLOW":
                if context.lookup_data('mspl2').configuration.configurationRule[0].configurationRuleAction.FilteringActionType == "DENY":
                  if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourceAddress == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourceAddress:
                    if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.DestinationAddress == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.DestinationAddress:
                      if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourcePort == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourcePort:
                        if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.DestinationPort == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.DestinationPort:
                          if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.Interface == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.Interface:
                            if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.ProtocolType == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.ProtocolType:
                              print(context.lookup_data('mspl1').id,"Managers Conflict!",context.lookup_data('mspl2'))
                              engine.assert_('mspls', 'mspl_conflict',
                                             (rule.pattern(0).as_data(context),
                                              rule.pattern(1).as_data(context),
                                              rule.pattern(2).as_data(context),)),
                              rule.rule_base.num_fc_rules_triggered += 1
  finally:
    context.done()

def verify_managers_conflict_traffic_divert_behavior(rule, context = None, index = None):
  engine = rule.rule_base.engine
  if context is None: context = contexts.simple_context()
  try:
    with knowledge_base.Gen_once if index == 0 \
             else engine.lookup('mspls', 'mspl', context,
                                rule.foreach_patterns(0)) \
      as gen_0:
      for dummy in gen_0:
        with knowledge_base.Gen_once if index == 1 \
                 else engine.lookup('mspls', 'mspl', context,
                                    rule.foreach_patterns(1)) \
          as gen_1:
          for dummy in gen_1:
            if context.lookup_data('mspl1').configuration.capability[0].Name == "Traffic_Divert"		:
              if context.lookup_data('mspl2').configuration.capability[0].Name == "Filtering_L4":
                if context.lookup_data('mspl1').configuration.configurationRule[0].configurationRuleAction.TrafficDivertActionType == "FORWARD":
                  if context.lookup_data('mspl2').configuration.configurationRule[0].configurationRuleAction.FilteringActionType == "DENY":
                    if context.lookup_data('mspl2').priority == context.lookup_data('mspl1').priority:
                      if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourceAddress == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourceAddress:
                        if context.lookup_data('mspl1').configuration.configurationRule[0].configurationRuleAction.packetDivertAction.packetFilterCondition.DestinationAddress == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.DestinationAddress:
                          if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourcePort == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourcePort:
                            if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.DestinationPort == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.DestinationPort:
                              if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.Interface == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.Interface:
                                if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.ProtocolType == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.ProtocolType:
                                  print(context.lookup_data('mspl1').id,"Managers Conflict!",context.lookup_data('mspl2'))
                                  engine.assert_('mspls', 'mspl_conflict',
                                                 (rule.pattern(0).as_data(context),
                                                  rule.pattern(1).as_data(context),
                                                  rule.pattern(2).as_data(context),)),
                                  rule.rule_base.num_fc_rules_triggered += 1
  finally:
    context.done()

def verify_duties_conflict(rule, context = None, index = None):
  engine = rule.rule_base.engine
  if context is None: context = contexts.simple_context()
  try:
    with knowledge_base.Gen_once if index == 0 \
             else engine.lookup('mspls', 'mspl', context,
                                rule.foreach_patterns(0)) \
      as gen_0:
      for dummy in gen_0:
        with knowledge_base.Gen_once if index == 1 \
                 else engine.lookup('mspls', 'mspl', context,
                                    rule.foreach_patterns(1)) \
          as gen_1:
          for dummy in gen_1:
            if context.lookup_data('mspl1').configuration.capability[0].Name != context.lookup_data('mspl2').configuration.capability[0].Name:
              if context.lookup_data('mspl1').configuration.capability[0].Name == "DTLS_protocol":
                if context.lookup_data('mspl2').configuration.capability[0].Name == "Network_traffic_analysis":
                  if context.lookup_data('mspl1').configuration.configurationRule[0].configurationRuleAction.technologyActionParameters.technologyParameter[0].localEndpoint == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.monitoringConfigurationCondition[0].packetFilterCondition.SourceAddress:
                    if context.lookup_data('mspl1').configuration.configurationRule[0].configurationRuleAction.technologyActionParameters.technologyParameter[0].remoteEndpoint == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.monitoringConfigurationCondition[0].packetFilterCondition.DestinationAddress:
                      print(context.lookup_data('mspl1'), "Duties Conflict!", context.lookup_data('mspl2'))
                      engine.assert_('mspls', 'mspl_conflict',
                                     (rule.pattern(0).as_data(context),
                                      rule.pattern(1).as_data(context),
                                      rule.pattern(2).as_data(context),)),
                      rule.rule_base.num_fc_rules_triggered += 1
  finally:
    context.done()

def verify_priority_dependency(rule, context = None, index = None):
  engine = rule.rule_base.engine
  if context is None: context = contexts.simple_context()
  try:
    with knowledge_base.Gen_once if index == 0 \
             else engine.lookup('mspls', 'mspl', context,
                                rule.foreach_patterns(0)) \
      as gen_0:
      for dummy in gen_0:
        with knowledge_base.Gen_once if index == 1 \
                 else engine.lookup('mspls', 'mspl', context,
                                    rule.foreach_patterns(1)) \
          as gen_1:
          for dummy in gen_1:
            if context.lookup_data('mspl1').priority:
              if context.lookup_data('mspl2').priority:
                if context.lookup_data('mspl1').priority > context.lookup_data('mspl2').priority:
                  if context.lookup_data('mspl1').dependencies:
                    if hasattr(context.lookup_data('mspl1').dependencies.dependency[0].configurationCondition,"policyID"):
                      if context.lookup_data('mspl1').dependencies.dependency[0].configurationCondition.policyID:
                        if context.lookup_data('mspl1').dependencies.dependency[0].configurationCondition.policyID == context.lookup_data('mspl2').id:
                          print(context.lookup_data('mspl1'), "Priority dependency Conflict!", context.lookup_data('mspl2'))
                          engine.assert_('mspls', 'mspl_conflict',
                                         (rule.pattern(0).as_data(context),
                                          rule.pattern(1).as_data(context),
                                          rule.pattern(2).as_data(context),)),
                          rule.rule_base.num_fc_rules_triggered += 1
  finally:
    context.done()

def verify_dependency(rule, context = None, index = None):
  engine = rule.rule_base.engine
  if context is None: context = contexts.simple_context()
  try:
    with knowledge_base.Gen_once if index == 0 \
             else engine.lookup('mspls', 'mspl', context,
                                rule.foreach_patterns(0)) \
      as gen_0:
      for dummy in gen_0:
        with knowledge_base.Gen_once if index == 1 \
                 else engine.lookup('mspls', 'mspl', context,
                                    rule.foreach_patterns(1)) \
          as gen_1:
          for dummy in gen_1:
            if context.lookup_data('mspl1').dependencies:
              if hasattr(context.lookup_data('mspl1').dependencies.dependency[0].configurationCondition,"policyID"):
                if context.lookup_data('mspl1').dependencies.dependency[0].configurationCondition.policyID:
                  if context.lookup_data('mspl1').dependencies.dependency[0].configurationCondition.policyID == context.lookup_data('mspl2').id:
                    if context.lookup_data('mspl1').dependencies.dependency[0].configurationCondition.status != context.lookup_data('status2'):
                      print("Policy Dependency!")
                      engine.assert_('mspls', 'mspl_dependency',
                                     (rule.pattern(0).as_data(context),
                                      rule.pattern(1).as_data(context),
                                      rule.pattern(2).as_data(context),)),
                      rule.rule_base.num_fc_rules_triggered += 1
  finally:
    context.done()

def verify_event(rule, context = None, index = None):
  engine = rule.rule_base.engine
  if context is None: context = contexts.simple_context()
  try:
    with knowledge_base.Gen_once if index == 0 \
             else engine.lookup('mspls', 'mspl', context,
                                rule.foreach_patterns(0)) \
      as gen_0:
      for dummy in gen_0:
        if context.lookup_data('mspl1').dependencies:
          if hasattr(context.lookup_data('mspl1').dependencies.dependency[0],"eventID"):
            if context.lookup_data('mspl1').dependencies.dependency[0].eventID:
              mark4 = context.mark(True)
              if rule.pattern(0).match_data(context, context,
                      context.lookup_data('mspl1').dependencies.dependency[0].eventID):
                context.end_save_all_undo()
                print("Event Conflict!")
                engine.assert_('mspls', 'mspl_dependency',
                               (rule.pattern(1).as_data(context),
                                rule.pattern(2).as_data(context),
                                rule.pattern(0).as_data(context),)),
                rule.rule_base.num_fc_rules_triggered += 1
              else: context.end_save_all_undo()
              context.undo_to_mark(mark4)
  finally:
    context.done()

def verify_override_conflict_l4_behaviour(rule, context = None, index = None):
  engine = rule.rule_base.engine
  if context is None: context = contexts.simple_context()
  try:
    with knowledge_base.Gen_once if index == 0 \
             else engine.lookup('mspls', 'mspl', context,
                                rule.foreach_patterns(0)) \
      as gen_0:
      for dummy in gen_0:
        with knowledge_base.Gen_once if index == 1 \
                 else engine.lookup('mspls', 'mspl', context,
                                    rule.foreach_patterns(1)) \
          as gen_1:
          for dummy in gen_1:
            if context.lookup_data('mspl1').id != context.lookup_data('mspl2').id:
              if context.lookup_data('mspl1').configuration.capability[0].Name == context.lookup_data('mspl2').configuration.capability[0].Name == "Filtering_L4":
                if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourceAddress == context.lookup_data('mspl2').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourceAddress:
                  if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.DestinationAddress == None:
                    if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.SourcePort == None:
                      if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.DestinationPort == None:
                        if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.Interface == None:
                          if context.lookup_data('mspl1').configuration.configurationRule[0].configurationCondition.packetFilterCondition.ProtocolType == None:
                            print(context.lookup_data('mspl1').id,"Override Conflict!",context.lookup_data('mspl2'))
                            engine.assert_('mspls', 'mspl_conflict',
                                           (rule.pattern(0).as_data(context),
                                            rule.pattern(1).as_data(context),
                                            rule.pattern(2).as_data(context),)),
                            rule.rule_base.num_fc_rules_triggered += 1
  finally:
    context.done()

def populate(engine):
  This_rule_base = engine.get_create('mspl_conflict_rules')
  
  fc_rule.fc_rule('verify_id_conflicts', This_rule_base, verify_id_conflicts,
    (('mspls', 'mspl',
      (contexts.variable('mspl1'),
       contexts.variable('status1'),),
      False),
     ('mspls', 'mspl',
      (contexts.variable('mspl2'),
       contexts.variable('status2'),),
      False),),
    (contexts.variable('mspl1'),
     pattern.pattern_literal('id_conflict'),
     contexts.variable('mspl2'),))
  
  fc_rule.fc_rule('verify_same_filtering_l4_behaviour', This_rule_base, verify_same_filtering_l4_behaviour,
    (('mspls', 'mspl',
      (contexts.variable('mspl1'),
       contexts.variable('status1'),),
      False),
     ('mspls', 'mspl',
      (contexts.variable('mspl2'),
       contexts.variable('status2'),),
      False),),
    (contexts.variable('mspl1'),
     pattern.pattern_literal('same_behaviour_filtering_l4_conflict'),
     contexts.variable('mspl2'),))
  
  fc_rule.fc_rule('verify_managers_conflict_l4_behaviour', This_rule_base, verify_managers_conflict_l4_behaviour,
    (('mspls', 'mspl',
      (contexts.variable('mspl1'),
       contexts.variable('status1'),),
      False),
     ('mspls', 'mspl',
      (contexts.variable('mspl2'),
       contexts.variable('status2'),),
      False),),
    (contexts.variable('mspl1'),
     pattern.pattern_literal('managers_conflict_l4_behaviour'),
     contexts.variable('mspl2'),))
  
  fc_rule.fc_rule('verify_managers_conflict_traffic_divert_behavior', This_rule_base, verify_managers_conflict_traffic_divert_behavior,
    (('mspls', 'mspl',
      (contexts.variable('mspl1'),
       contexts.variable('status1'),),
      False),
     ('mspls', 'mspl',
      (contexts.variable('mspl2'),
       contexts.variable('status2'),),
      False),),
    (contexts.variable('mspl1'),
     pattern.pattern_literal('managers_conflict_l4_behaviour'),
     contexts.variable('mspl2'),))
  
  fc_rule.fc_rule('verify_duties_conflict', This_rule_base, verify_duties_conflict,
    (('mspls', 'mspl',
      (contexts.variable('mspl1'),
       contexts.variable('status1'),),
      False),
     ('mspls', 'mspl',
      (contexts.variable('mspl2'),
       contexts.variable('status2'),),
      False),),
    (contexts.variable('mspl1'),
     pattern.pattern_literal('duties_conflict'),
     contexts.variable('mspl2'),))
  
  fc_rule.fc_rule('verify_priority_dependency', This_rule_base, verify_priority_dependency,
    (('mspls', 'mspl',
      (contexts.variable('mspl1'),
       contexts.variable('status1'),),
      False),
     ('mspls', 'mspl',
      (contexts.variable('mspl2'),
       contexts.variable('status2'),),
      False),),
    (contexts.variable('mspl1'),
     pattern.pattern_literal('priority_dependency_conflict'),
     contexts.variable('mspl2'),))
  
  fc_rule.fc_rule('verify_dependency', This_rule_base, verify_dependency,
    (('mspls', 'mspl',
      (contexts.variable('mspl1'),
       contexts.variable('status1'),),
      False),
     ('mspls', 'mspl',
      (contexts.variable('mspl2'),
       contexts.variable('status2'),),
      False),),
    (contexts.variable('mspl1'),
     pattern.pattern_literal('policy_dependency'),
     contexts.variable('mspl2'),))
  
  fc_rule.fc_rule('verify_event', This_rule_base, verify_event,
    (('mspls', 'mspl',
      (contexts.variable('mspl1'),
       contexts.variable('status1'),),
      False),),
    (contexts.variable('event_id'),
     contexts.variable('mspl1'),
     pattern.pattern_literal('event_dependency'),))
  
  fc_rule.fc_rule('verify_override_conflict_l4_behaviour', This_rule_base, verify_override_conflict_l4_behaviour,
    (('mspls', 'mspl',
      (contexts.variable('mspl1'),
       contexts.variable('status1'),),
      False),
     ('mspls', 'mspl',
      (contexts.variable('mspl2'),
       contexts.variable('status2'),),
      False),),
    (contexts.variable('mspl1'),
     pattern.pattern_literal('override_conflict_l4_behaviour'),
     contexts.variable('mspl2'),))


Krb_filename = '../mspl_conflict_rules.krb'
Krb_lineno_map = (
    ((12, 16), (4, 4)),
    ((17, 17), (5, 5)),
    ((18, 22), (6, 6)),
    ((23, 23), (7, 7)),
    ((24, 24), (8, 8)),
    ((25, 25), (9, 9)),
    ((26, 26), (11, 11)),
    ((27, 30), (12, 12)),
    ((39, 43), (18, 18)),
    ((44, 48), (19, 19)),
    ((49, 49), (20, 20)),
    ((50, 50), (21, 21)),
    ((51, 51), (22, 22)),
    ((52, 52), (23, 23)),
    ((53, 53), (24, 24)),
    ((54, 54), (25, 25)),
    ((55, 55), (26, 26)),
    ((56, 56), (27, 27)),
    ((57, 57), (28, 28)),
    ((58, 58), (30, 30)),
    ((59, 62), (31, 31)),
    ((71, 75), (37, 37)),
    ((76, 80), (39, 39)),
    ((81, 81), (41, 41)),
    ((82, 82), (42, 42)),
    ((83, 83), (43, 43)),
    ((84, 84), (44, 44)),
    ((85, 85), (45, 45)),
    ((86, 86), (46, 46)),
    ((87, 87), (47, 47)),
    ((88, 88), (48, 48)),
    ((89, 89), (49, 49)),
    ((90, 90), (51, 51)),
    ((91, 94), (52, 52)),
    ((103, 107), (58, 58)),
    ((108, 112), (60, 60)),
    ((113, 113), (62, 62)),
    ((114, 114), (63, 63)),
    ((115, 115), (64, 64)),
    ((116, 116), (65, 65)),
    ((117, 117), (66, 66)),
    ((118, 118), (67, 67)),
    ((119, 119), (68, 68)),
    ((120, 120), (69, 69)),
    ((121, 121), (70, 70)),
    ((122, 122), (71, 71)),
    ((123, 123), (72, 72)),
    ((124, 124), (74, 74)),
    ((125, 128), (75, 75)),
    ((137, 141), (81, 81)),
    ((142, 146), (82, 82)),
    ((147, 147), (83, 83)),
    ((148, 148), (84, 84)),
    ((149, 149), (85, 85)),
    ((150, 150), (86, 86)),
    ((151, 151), (87, 87)),
    ((152, 152), (89, 89)),
    ((153, 156), (90, 90)),
    ((165, 169), (95, 95)),
    ((170, 174), (96, 96)),
    ((175, 175), (97, 97)),
    ((176, 176), (98, 98)),
    ((177, 177), (99, 99)),
    ((178, 178), (100, 100)),
    ((179, 179), (101, 101)),
    ((180, 180), (102, 102)),
    ((181, 181), (103, 103)),
    ((182, 182), (105, 105)),
    ((183, 186), (106, 106)),
    ((195, 199), (111, 111)),
    ((200, 204), (112, 112)),
    ((205, 205), (113, 113)),
    ((206, 206), (114, 114)),
    ((207, 207), (115, 115)),
    ((208, 208), (116, 116)),
    ((209, 209), (117, 117)),
    ((210, 210), (119, 119)),
    ((211, 214), (120, 120)),
    ((223, 227), (126, 126)),
    ((228, 228), (127, 127)),
    ((229, 229), (128, 128)),
    ((230, 230), (129, 129)),
    ((233, 233), (130, 130)),
    ((235, 235), (132, 132)),
    ((236, 239), (133, 133)),
    ((250, 254), (139, 139)),
    ((255, 259), (140, 140)),
    ((260, 260), (141, 141)),
    ((261, 261), (142, 142)),
    ((262, 262), (143, 143)),
    ((263, 263), (144, 144)),
    ((264, 264), (145, 145)),
    ((265, 265), (146, 146)),
    ((266, 266), (147, 147)),
    ((267, 267), (148, 148)),
    ((268, 268), (150, 150)),
    ((269, 272), (151, 151)),
)
