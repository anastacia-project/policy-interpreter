# # -*- coding: utf-8 -*-
# policy_interpreter.py
"""
This python module implements the MSPL conflict detector inside the ANASTACIA European Project.
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2019, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"


import contextlib
import sys
import time
from pyke import knowledge_engine, krb_traceback, goal
import datetime
import uuid
import requests
import json

# This allows execute the module directly for testing pruposes
if __name__ == "__main__":
	POLICY_REPOSITORY_SERVICE_URL = "http://localhost:8004/api/v1/"
else:
	from settings import POLICY_REPOSITORY_SERVICE_URL

import os
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
	sys.path.insert(0, parentPath)
from m2ltranslator import M2LTranslator
from settings import logging, POLICY_ENFORCEMENT
logger = logging.getLogger(__name__)



class MSPLConflictDetector():

	def detect(self, mspl_orchestration_object):
		# We are loading Pyke rules base we have defined in pricing.krb
		if not isinstance(mspl_orchestration_object, list):
			mspl_objects = mspl_orchestration_object.ITResource
		else:
			mspl_objects = mspl_orchestration_object

		engine = knowledge_engine.engine((__file__, '.rules'))
		engine.activate('mspl_conflict_rules')

		# TODO: Retrieve all mspls from policy repository and assert them
		# Including real status
		#logger.debug("Retrieving policies")
		result = requests.get("{}{}".format(POLICY_REPOSITORY_SERVICE_URL,POLICY_ENFORCEMENT))
		#logger.debug(result.text)
		# TODO: Parse the results to MSPL and include them in facts
		current_policies = json.loads(result.text)
		mspl_translator = M2LTranslator()
		for mspl_data in current_policies:
			logger.debug("TRIYING TO LOAD: {}".format(mspl_data["mspl"]["mspl"]))
			mspl_object = mspl_translator.load(mspl_data["mspl"]["mspl"])
			logger.debug("asserting {}".format(mspl_object.id))
			engine.assert_('mspls', 'mspl', (mspl_object,mspl_data["status"],))

		#mspl_orchestration_object = M2LTranslator.load(xml_input)

		# Add the new orchestration policy with status "pending"
		#logger.debug("Adding asserts")
		for mspl_object in mspl_objects:
			logger.debug("asserting {}".format(mspl_object.id))
			if mspl_object.dependencies:
				logger.debug(type(mspl_object.dependencies.dependency[0]))
			engine.assert_('mspls', 'mspl', (mspl_object,"P",))
		
		#logger.debug(engine.knowledge_bases['mspls'].get_entity_list('mspl').dump_specific_facts())
		mspl_facts = engine.knowledge_bases['mspls'].get_entity_list('mspl').case_specific_facts
		logger.debug("Current mspl facts:")
		for fact in mspl_facts:
			#logger.debug("{},{}".format(fact[0],str(fact[1]) if len(fact)>1 else "Non-conflict"))
			logger.debug("{},{}".format(fact[0].id, fact[1]))

		mspl_conflict_facts = engine.knowledge_bases['mspls'].get_entity_list('mspl_conflict').case_specific_facts
		mspl_dependency_facts = engine.knowledge_bases['mspls'].get_entity_list('mspl_dependency').case_specific_facts
		#mspl_conflicts = set()
		mspl_conflicts = []
		logger.debug("Current mspl conflicts:")
		for fact in mspl_conflict_facts:
			logger.debug("{},{},{}".format(fact[0].id,str(fact[1]),fact[2].id if hasattr(fact[2],"id") else fact[2]))
			#mspl_conflicts.add((fact[0].id,str(fact[1]),fact[2].id))
			mspl_conflicts.append((fact[0].id,str(fact[1]),fact[2].id if hasattr(fact[2],"id") else fact[2]))

		mspl_dependencies = []
		logger.debug("Current mspl dependencies:")
		for fact in mspl_dependency_facts:
			logger.debug("{},{},{}".format(fact[0].id,str(fact[1]),fact[2].id if hasattr(fact[2],"id") else fact[2]))
			#mspl_conflicts.add((fact[0].id,str(fact[1]),fact[2].id))
			mspl_dependencies.append((fact[0].id,str(fact[1]),fact[2].id if hasattr(fact[2],"id") else fact[2]))


		mspl_conflicts_dependencies = {"mspl_conflicts":mspl_conflicts,
										"mspl_dependencies":mspl_dependencies}
		# events conflicts

		
		#for mspl_conflict in mspl_conflicts:
		#	logger.debug(mspl_conflict)
		#Now, we have our rules and our facts, let's just prove that a package applies.
		
		#try:	
		#	# Test over pricing rules, using 'packages' as base knowledge and type and delta as vars
		#	vals, plans = engine.prove_1_goal('policy_conflict_rules.mspls($mspl,$conflict)')
		#	logger.debug("Conflicts detected")
		#	logger.debug(vals)

		#except knowledge_engine.CanNotProve:
		#	print("No conflict applies")
		
		# Maybe probe is not neccesary since we can retrieve the info from the facts
		#with engine.prove_goal('policy_conflict_rules.mspls($mspl1,$conflict,$mspl2)') as gen:
		#	for vals, plan in gen:
		#		logger.debug(vals)
		
	
		#Resetting the engine is needed to be able to apply a new fact base.
		engine.reset()

		return mspl_conflicts_dependencies

if __name__ == "__main__":
	# Comment these lines when integrate with the real service	
	print("Running...")
	#f=open("../test/m2lservice/orchestration/conflict_detection/conflicts.xml", "r")
	f=open(sys.argv[1], "r")
	xml_input = f.read()
	#logger.debug(xml_input)
	mspl_translator = M2LTranslator()
	mspl_orchestration_object = mspl_translator.load(xml_input)
	conflict_detector = MSPLConflictDetector()
	conflict_detector.detect(mspl_orchestration_object)


"""
class PricingExample():

	def which_package(self,packages, duration):
		# We are loading Pyke rules base we have defined in pricing.krb

		engine = knowledge_engine.engine((__file__, '.rules'))
		engine.activate('pricing')

		#Creating the facts base from the package list.
		# This assert specific facts as package('daily',)
		for package in packages:
			engine.assert_('packages', 'package', (package,))

		print(dir(engine.knowledge_bases['packages']))
		print(engine.knowledge_bases['packages'].get_entity_list('package').dump_specific_facts())
		
		
		#Now, we have our rules and our facts, let's just prove that a package applies.
		try:	
			# Test over pricing rules, using 'packages' as base knowledge and type and delta as vars
			vals, plans = engine.prove_1_goal('pricing.packages($type, $delta)', delta=duration)
			print(vals['type'])
		#We cannot prove that any package applies therefore no package applies.

		except knowledge_engine.CanNotProve:
			print("No package applies")
	
		#Resetting the engine is needed to be able to apply a new fact base.
		engine.reset()

if __name__ == "__main__":
	print("Running...")
	conflict_detector = PricingExample()
	a_day = datetime.timedelta(days=1)
	conflict_detector.which_package(['daily', 'weekly'], a_day)
"""
