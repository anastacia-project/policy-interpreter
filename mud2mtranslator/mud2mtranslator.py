# # -*- coding: utf-8 -*-
"""
This python module implements a MSPL->Low-level translator inside the ANASTACIA European Project,
using/extending the HSPL/MSPL languages defined in SECURED project.
How to use:
	python3 m2ltranslator.py [MSPL_FILE.xml]

"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"


import json
import requests
import uuid
from settings import logging, LOCAL_SECURITY_ENABLER_PROVIDER_URL, SEP_GET_PLUGIN, SEP_TIMEOUT, PLUGIN_FOLDER, PLUGIN_PREFIX
logger = logging.getLogger(__name__)
# mspl models
import mspl
#from utils import generate_device_ipv6, timing
import copy

"""
	Output example:
	{"omspl_translation": {"mspl_id":ID, "mspl": omspl_text,"mspl_translations":mspl_translations}}
"""

class MUD2MTranslator:
	'Class in charge to translate MUD to MSPL security policies'

	POLICY_TEMPLATES_URL = "http://policy-repository.es:8004/api/v1/mspl-templates/?capabilities="
	SECURITY_ORCHESTRATOR_API = "http://orchestrator:8002/meservice/"
	ORCHESTRATION = "Orchestration"
	TRAFFIC_DIVERT = "Traffic_Divert"
	FORWARD = "forwarding"
	SAME_MANUFACTURER = "same-manufacturer"
	CONTROLLER = "controller"
	ACTION_CAPABILITIES = {FORWARD:TRAFFIC_DIVERT}
	SYS_MODEL = {CONTROLLER:{"http://iot_controller.com": ["2001:720:1710:4:5054:ff:feec:e209/128"]},
				SAME_MANUFACTURER: ["aaaa::3/128","aaaa::4/128","aaaa::5/128","aaaa::6/128"]}

	def get_mspl_template(self, capability):
		try:
			response = requests.get("{}{}".format(self.POLICY_TEMPLATES_URL,capability))
			json_response = json.loads(response.text)
			#logger.info(json_response)
		except Exception as e:
			#logger.info(str(e))
			return None
		return json_response[0]["mspl"]


	def fill_mspl_traffic_divert(self, mspl_template, source, destination, port, protocol_type):

		mspl_template = mspl_template.replace("<SourceAddress>","<SourceAddress>{}".format(source))
		if port and protocol_type:
			mspl_template = mspl_template.replace("</SourceAddress>","</SourceAddress>\n<DestinationPort>{}</DestinationPort>\n<ProtocolType>{}</ProtocolType>\n".format(port,protocol_type))
		return mspl_template.replace("<DestinationAddress>","<DestinationAddress>{}".format(destination))
		"""
		return mspl_template.replace("<SourceAddress>","<SourceAddress>{}".format(source))\
			.replace("</SourceAddress>","</SourceAddress>\n<DestinationPort>{}</DestinationPort>\n<ProtocolType>{}</ProtocolType>\n".format(port,protocol_type))\
			.replace("<DestinationAddress>","<DestinationAddress>{}".format(destination))#\
			#.replace('id=""','id="{}"'.format("mspl_{}".format(uuid.uuid4().hex)))
		"""

	def fill_mspl_template(self,omspl_template, capability, source, destination, port, protocol_type):
		# TODO: decide the capability depending on the actions
		if not self.mspl_template.keys():
			self.mspl_template[capability] = self.get_mspl_template(capability)
		mspl_template = copy.copy(self.mspl_template[capability])
		#logger.info("mspl_template: {}".format(mspl_template))
		mspl = getattr(self,"fill_mspl_{}".format(capability.lower()))(mspl_template, source, destination, port, protocol_type)
		return omspl_template.replace("</ITResourceOrchestration>","{}</ITResourceOrchestration>".format(mspl))


	def generate_mspl_to(self,omspl_template, capability, source, destination, port, protocol_type):
		return self.fill_mspl_template(omspl_template, capability, source, destination, port, protocol_type)



	def generate_mspl_fr(self,omspl_template, capability, source, destination, port, protocol_type):
		return self.fill_mspl_template(omspl_template, capability, destination, source, port, protocol_type)


	#@timing("MUD2MService")
	def translate(self,device_mud):
		device_mud_obj = json.loads(device_mud)

		logger.debug(device_mud_obj)

		mud_obj = device_mud_obj[0]
		iot_addr = device_mud_obj[1]["device-ip"]

		# Get a policy for orchestration
		omspl_template = self.get_mspl_template(self.ORCHESTRATION)

		#from_device_policy = mud_obj["ietf-mud:mud"]["from-device-policy"]["access-lists"]["access-list"][0]["name"]
		#to_device_policy = mud_obj["ietf-mud:mud"]["to-device-policy"]["access-lists"]["access-list"][0]["name"]

		acls = mud_obj["ietf-access-control-list:acls"]["acl"]
		mspl_policies = []
		logger.debug("ACLS:{}".format(acls))
		self.mspl_template = {}
		for acl in acls:
			from_to = acl["name"][-2:]
			aces = acl["aces"]["ace"]
			for ace in aces:
				matches = ace["matches"]
				logger.debug("MATCHES: {}".format(matches))

				# parse ietf-mud:mud matches
				matches_ietf_mud_mud = matches["ietf-mud:mud"]
				sources = []

				for key,value in matches_ietf_mud_mud.items():
					logger.debug("KEY IS {} AND VALUE IS {}".format(key,value))
					sources+= self.SYS_MODEL[key][value] if not isinstance(value, list) else self.SYS_MODEL[key]
					#sources+= generate_device_ipv6(value)
					
				# parse protocol
				port = None
				protocol_type = None
				if "udp" in matches.keys():
					port = matches["udp"]["source-port"]["port"] if "source-port" in matches["udp"].keys() else matches["udp"]["destination-port"]["port"]  
					protocol_type = "UDP"

				destination = iot_addr
				actions = ace["actions"]
				capability = self.ACTION_CAPABILITIES[next(iter(actions))]
				logger.debug("Calling generate_mspl_{}".format(from_to))
				for source in sources:
					logger.debug("SOURCE: {}".format(source))
					kwargs = {"omspl_template":omspl_template,"capability":capability,"source":source,\
						"destination":destination,"port":port,"protocol_type":protocol_type}
					omspl_template = getattr(self,"generate_mspl_{}".format(from_to))(omspl_template, capability, source, destination,\
						port, protocol_type)
				#logger.info(omspl_template)


		logger.debug(omspl_template)
		return omspl_template


"""
if __name__ == "__main__":
	xml_file = sys.argv[1] 
	m2ltranslator = M2LTranslator()
	#logger.debug("Reading mspl file...")
	xml_source = open(xml_file).read()
	mspl_policy = m2ltranslator.load_mspl(xml_source)
	# Pretty print
	separator = "*"
	separator_group = separator*5
	title = "MSPL POLICY"
	print("\n{}{}{}".format(separator_group,title,separator_group))
	print(mspl_policy)
	print("{}{}{}\n".format(separator_group,separator*len(title),separator_group))
"""	