#!/bin/bash

 USAGE=" USAGE: sudo ./2-run_docker.sh policy_interpreter"

if [ $# -eq 0 ]
  then
    echo $USAGE
    exit 1
fi
sudo docker rm $1
sudo docker run --name $1 -v $(pwd):/app -p 8000:8000 -i -t $1

#sudo docker run --name $1 -p $2 -v /home/amzarca/anastacia-framework/iot_controller:/app -i -t $1
#sudo docker run --name $1 -d -p $2 $1
# sudo docker run --name iot_controller -p 8003:8003 -v /home/amzarca/anastacia-framework/iot_controller:/app -i -t iot_controller