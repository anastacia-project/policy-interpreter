# # -*- coding: utf-8 -*-
# policy_interpreter.py
"""
This python module implements the HSPL/MSPL policy interpreter inside the ANASTACIA European Project,
extending the HSPL/MSPL language defined in secured project.
How to use:
	Run the server: gunicorn policy_interpreter:app
	Request for HSPL/MSPL translation: curl localhost:8000/h2mservice -d @[hspl_policy.json]
	Request for /Lower translation: curl localhost:8000/m2lservice -d @[mspl_policy.json]
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

# Let's get this party started!
import falcon
import json
import requests
from datetime import datetime
from h2mrefiner import H2MRefiner
#from m2ltranslator import M2LTranslator
#from mud2mtranslator import MUD2MTranslator
#from utils import timing
from settings import logging, remote_logger, POLICY_REPOSITORY_SERVICE_URL, SET_TRANSLATION_URL, SET_REFINEMENT_URL
logger = logging.getLogger(__name__)

class H2EService(object):

    #SECURITY_ORCHESTRATOR_URL = "http://orchestrator:8002/meservice"
    
    """ High to enforcement service"""
    def on_get(self, req, resp):
        """ GET Method is not allowed """
        raise falcon.HTTPMethodNotAllowed('GET method not supported',
                                        'Please, uses POST in order to provide the HSPL.')

    def on_post(self, req, resp):
        """Handles POST requests"""
        # H2MRefinement
        h2mservice = H2MService()
        mspl_policies = h2mservice.on_post(req,resp)
        

        # Request the enforcement to orchestrator
        response = requests.post("{}".format(self.SECURITY_ORCHESTRATOR_URL),json=mspl_policies)
        #logger.debug("Security Orchestrator response {}".format(response.text))
        resp.status = falcon.HTTP_200 
        resp.body = response.text
        return resp.body
        # TODO: manage connection errors

    def on_delete(self, req, resp):
        """Handles POST requests"""
        # H2MRefinement
        h2mservice = H2MService()
        mspl_policies = h2mservice.on_post(req,resp)
        

        # Request the enforcement to orchestrator
        response = requests.delete("{}".format(self.SECURITY_ORCHESTRATOR_URL),json=mspl_policies)
        #logger.debug("Security Orchestrator response {}".format(response.text))
        resp.status = falcon.HTTP_200 
        resp.body = response.text
        return resp.body
        # TODO: manage connection errors
        

class H2MService(object):

    
    def on_get(self, req, resp):
        """ GET Method is not allowed """
        raise falcon.HTTPMethodNotAllowed('GET method not supported',
                                        'Please, uses POST in order to provide the HSPL.')
    #@timing("H2MService")
    def on_post(self, req, resp):
        logger.info("H2MService")
        """Handles GET requests"""
        
        body = req.stream.read().decode("utf-8")
        if not body:
            raise falcon.HTTPBadRequest('Empty request body',
                                        'A valid JSON document is required.')

        logging_message = {
         "timestamp": datetime.timestamp(datetime.now()),
         "from_module": "POLICY_EDITOR_TOOL",
         "from_component": "POLICY_EDITOR_TOOL",
         "to_module": "POLICY_MANAGER",
         "to_component": "POLICY_INTERPRETER",
         "incoming": True,
         "method": "REST",
         "data": body,
         "notes": "H2M request"
        }
        remote_logger.info(logging_message)


        logger.debug("Received {}".format(body))
        #json_input = json.loads(body)
        #hspl_list = json_input["hspl_list"]
        logger.debug("H2MService")
        
        # Parse the HSPL
        #logger.debug("Starting HSPL to MSPL Refinement...")
        h2mrefiner = H2MRefiner()
        mspl_policy = h2mrefiner.get_mspl(body)
        #mspl_policy = h2mrefiner.get_mspl(hspl_list)
        #logger.debug("H2M RETURNS: {}".format(mspl_policy))

        # Post the result in the policy repository
        try:
            requests.post("{}{}".format(POLICY_REPOSITORY_SERVICE_URL,SET_REFINEMENT_URL),json={"hspl":body,"mspl":mspl_policy})
        except Exception as e:
            logger.exception(e)
            raise falcon.HTTPInternalServerError("Error registering the refinement in Policy Repository", e)

        resp.status = falcon.HTTP_200  
        resp.body = mspl_policy

        logging_message = {
         "timestamp": datetime.timestamp(datetime.now()),
         "from_module": "POLICY_MANAGER",
         "from_component": "POLICY_INTERPRETER",
         "to_module": "POLICY_EDITOR_TOOL",
         "to_component": "POLICY_EDITOR_TOOL",
         "incoming": False,
         "method": "REST",
         "data": {"MSPL-OP":mspl_policy},
         "notes": "H2M result"
        }
        remote_logger.info(logging_message)


        return resp.body
        

class HealthService(object):

    def on_get(self, req, resp):
        resp.body = json.dumps({"status":"OK"})

# falcon.API instances are callable WSGI apps
app = falcon.API()

# Resources are represented by long-lived class instances
h2mservice = H2MService()
h2eservice = H2EService()
health_service = HealthService()

# things will handle all requests to the '/things' URL path
app.add_route('/h2mservice', h2mservice)
app.add_route('/h2eservice', h2eservice)
app.add_route('/health', health_service)

