# # -*- coding: utf-8 -*-
"""
This python module implements a HSPL->MSPL refiner inside the ANASTACIA European Project,
using/extending the HSPL/MSPL languages defined in SECURED project.
How to use:
	python3 h2mrefiner.py [HSPL_FILE.xml]

"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

import sys,os
import json
import requests
import sysmodel

# Insert the parent path into the sys.path in order to import the mspl
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
	sys.path.insert(0, parentPath)

# hspl and mspl models
import hspl    
import mspl
from m2ltranslator.m2ltranslator import PolicyIDGenerator, PolicyTextGenerator,\
 ITResourceOrchestrationTypeCustom, ITResourceTypeCustom
from settings import SECURITY_ENABLER_PROVIDER_URL, LOCAL_SECURITY_ENABLER_PROVIDER_URL,\
 SEP_TIMEOUT, SYS_MODEL, CAPABILITY_MATCHING, logging, SM_USER, SM_PASS
logger = logging.getLogger(__name__)


class MappingType(hspl.MappingType):

	def get_mspl(self):
		return self.hspl_list.get_mspl()


class HSPL_Orchestration(hspl.HSPL_Orchestration):

	HSPL_MSPL_ID = {}

	def build_mspl_base(self):
		# Generate the main MSPL skeleton (if one hspl generates multiple MSPL this should be done for each one)
		mspl_policy = ITResourceOrchestrationTypeCustom()
		mspl_policy.generate_ID()
		#mspl_policy.ID = "mspl_{}".format(uuid.uuid4().hex)
		return mspl_policy

	def get_mspl(self):
		# Get base mspl
		PolicyIDGenerator.generate_ID(self,"ohspl")
		orchestration_mspl = self.build_mspl_base()

		# For each hspl
		for hspl_object in self.hspl:
			# Regenerate HSPL id
			PolicyIDGenerator.generate_ID(hspl_object,"hspl")
			hspl_object.orchestrationID = self.id
			# Generate the mspls and assign orchestration ID
			mspls = hspl_object.to_mspl()
			for mspl_object in mspls:
				mspl_object.orchestrationID = orchestration_mspl.id
			# Add the mspl to the list
			orchestration_mspl.ITResource.extend(mspls)

		#logger.debug(orchestration_mspl.to_text())

		# This solves error ITResource not subclass ITResource
		#mspl.ITResourceOrchestrationType._SetSupersedingClass(ITResourceOrchestrationTypeCustom)
		#mspl.ITResourceType._SetSupersedingClass(ITResourceTypeCustom)

		return orchestration_mspl.to_text()


def basic_auth_api(api_instance):
	# Configure HTTP basic authorization: basicAuth
	api_instance.api_client.configuration.username = SM_USER
	api_instance.api_client.configuration.password = SM_PASS


def get_subject_from_system_model(name):
	subject = None
	name = name.split("#")[1] if "#" in name else name 
	subject = get_device_by_name(name)
	if not subject:
		subject = get_room_by_name(name)
	return subject


def get_device_by_name(name):
	api_instance = sysmodel.PNFApi()
	basic_auth_api(api_instance)
	try:
		devices = api_instance.p_nf_list(name=name)
		if devices.results:
			device_id = devices.results[0].id
			api_instance = sysmodel.DeviceinterfaceApi()
			basic_auth_api(api_instance)
			ifaces = api_instance.deviceinterface_list(device=device_id)
			if ifaces.results:
				ifaz = ifaces.results[0]
			return {"address":ifaz.interfaceinfos.global_ipv6,"prot_conf_type":"DTLS_protocol"}
	except Exception as e:
		logger.info(e)
	return None


def get_room_by_name(name):
	# Get room
	api_instance = sysmodel.RoomApi()
	basic_auth_api(api_instance)
	try:
		rooms = api_instance.room_list(name=name)
		if rooms.results:
			room = rooms.results[0]
			# Get room interface
			logger.debug("GET ROOM INTERFACES FOR ROOM {}".format(room.id))
			api_instance = sysmodel.RoominterfaceApi()
			basic_auth_api(api_instance)
			room_interfaces = api_instance.roominterface_list(room=room.id)
			logger.debug(room_interfaces)
			if room_interfaces.results:
				ifaz = room_interfaces.results[0]
			return {"interface":ifaz.name}
	except Exception as e:
		logger.info(e)
	return None


class HSPL(hspl.HSPL):

	SEP_GET_PLUGINS = "/get_plugins?capabilities="

	def build_mspl_base(self):
		logger.debug("CALLING BUILD_MSPL_BASE ON HSPL FOR HSPL: {}".format(self.id))
		# Generate the main MSPL skeleton (if one hspl generates multiple MSPL this should be done for each one)
		mspl_policy = ITResourceTypeCustom()
		#mspl_policy.ID = "mspl_{}".format(uuid.uuid4().hex)
		mspl_policy.generate_ID()
		HSPL_Orchestration.HSPL_MSPL_ID[self.id] = mspl_policy.id
		mspl_policy.configuration = mspl.RuleSetConfiguration()
		mspl_policy.configuration.resolutionStrategy = mspl.FMR()
		mspl_policy.configuration.Name = "Conf0"
		mspl_policy.enablerCandidates = mspl.EnablerCandidates()
		mspl_policy.enablerCandidates.extend(self.candidate_security_enablers)
		# Priority
		mspl_policy.priority = self.priority
		# Refine dependenceis if required
		if self.dependencies:
			mspl_dependencies = mspl.Dependencies()
			for dependency in self.dependencies.dependency:
				if "Policy" in type(dependency).__name__: 
					mspl_dependency = mspl.PolicyDependency()
					mspl_configuration_condition = mspl.PolicyDependencyCondition(isCNF=True)
					mspl_configuration_condition.policyID = HSPL_Orchestration.HSPL_MSPL_ID[dependency.dependencyCondition.policyID]
					mspl_configuration_condition.status = dependency.dependencyCondition.status
					mspl_dependency.configurationCondition = mspl_configuration_condition
				else:
					# TODO: Since HSPL is growing up, we should follow the same approach of
					# MSPL translation
					mspl_dependency = mspl.EventDependency()
					mspl_dependency.eventID = dependency.eventID
					mspl_configuration_condition = mspl.ConfigurationCondition(isCNF=True)
					mspl_dependency.configurationCondition = mspl_configuration_condition
				mspl_dependencies.append(mspl_dependency)
			logger.debug("DEPENDENCY TYPE: {}".format(type(dependency).__name__))
			mspl_policy.dependencies = mspl_dependencies

		# A second use of to_text generates error
		#logger.debug(mspl_policy.to_text())

		return mspl_policy


	def to_mspl(self):
		# Get the main HSPL fields common for all MSPLs
		if not self.action in self.action.itervalues():
			raise ValueError('The action {} does not exist, please try with the current supported actions {}'.format(self.action,self.action.itervalues()))
		if not self.objectH in self.objectH.itervalues():
			raise ValueError('The objectH {} does not exist, please try with the current supported objectH {}'.format(self.objectH,self.objectH.itervalues()))

		# Get the subject (It should be retrieved from the system model)

		logger.debug("Looking for {}".format(self.subject))
		sys_model_subject = get_subject_from_system_model(self.subject)
		self.src = sys_model_subject if sys_model_subject else SYS_MODEL[self.subject.lower()] if self.subject.lower() in SYS_MODEL.keys() else None
		#logger.debug("Looking for {}".format(self.subject.lower()))
		#if self.subject.lower() in SYS_MODEL.keys():
		#	self.src = SYS_MODEL[self.subject.lower()]

		##logger.debug("SUBJECT: {}".format(self.src))

		# Verify if policy is network related
		key_object = "traffic"
		if not key_object in self.objectH.lower():
			key_object = self.objectH

		# Sysmodel trnslation for object [TO REVIEW IF NECCESARY]
		logger.debug("Looking for {}".format(self.objectH.lower()))
		self.object = self.objectH
		if self.objectH.lower() in SYS_MODEL.keys():
			self.object = SYS_MODEL[self.objectH.lower()]

		# Capability matching (TODO: capability list)
		try:
			self.identified_capabilities = CAPABILITY_MATCHING[self.action.lower()][self.objectH.lower()]
		except KeyError as e:
			print("Not found capability matching for {} {}.".format(self.action,self.objectH))
			print(e)
			sys.exit(1)

		# Request to security enabler provider by candidate security enablers
		try:
			response = requests.get("{}{}{}".format(SECURITY_ENABLER_PROVIDER_URL,\
				self.SEP_GET_PLUGINS,','.join(self.identified_capabilities)),timeout=SEP_TIMEOUT)
			#logger.debug("Using REMOTE Security Enabler Provider")
		except:
			response = requests.get("{}{}{}".format(LOCAL_SECURITY_ENABLER_PROVIDER_URL,\
				self.SEP_GET_PLUGINS,','.join(self.identified_capabilities)),timeout=SEP_TIMEOUT)
			#logger.debug("Error conecting to REMOTE SEP, using LOCAL Security Enabler Provider")
			# Verify if it works or try to access to local
		#logger.debug("Security Enabler provider response {}".format(response))
		# Decode the JSON response
		try:
			self.candidate_security_enablers = json.loads(response.text)["candidate_security_enablers"]
			#logger.debug("Candidate security enablers {}".format(self.candidate_security_enablers))
		except JSONDecodeError as e:
			resp.status = falcon.HTTP_500
			resp.body = str(e)
			return
		# Verify if exists at least one enabler
		if not self.candidate_security_enablers:
			raise ValueError('There are not enablers able to enforce {}'.format(self.identified_capabilities))


		# Refinement process
		try:
			action_mspl_refinement_function = getattr(self, "get_{}_{}_mspl".format(self.action,key_object))
		except AttributeError as e:
			print("Currently, get_{}_mspl is not implemented.".format(self.action))
			print(e)
			sys.exit(1)

		# Generate MSPL for the capability ()
		return action_mspl_refinement_function()

	def get_resource_info(self):
		# TODO: Change this for a call to sysmodel
		resource = SYS_MODEL[self.fields.resource.resource_property[0].key.lower()] if self.fields and self.fields.resource else None
		resource_info = {}
		resource_info["address"] = self.src["address"]
		resource_info["port"] = resource["port"]
		resource_info["resource"] = resource["resource"]
		return resource_info

	def generate_packet_filtering_condition(self):
		protocol = self.filtering_condition["protocol"] if "protocol" in self.filtering_condition.keys() else None
		src_address = self.filtering_condition["src_addr"] if "src_addr" in self.filtering_condition.keys() else None
		dst_address = self.filtering_condition["dst_addr"] if "dst_addr" in self.filtering_condition.keys() else None
		src_port = self.filtering_condition["src_port"] if "src_port" in self.filtering_condition.keys() else None
		dst_port = self.filtering_condition["dst_port"] if "dst_port" in self.filtering_condition.keys() else None
		interface = self.filtering_condition["interface"] if "interface" in self.filtering_condition.keys() else None
		return mspl.PacketFilterCondition(SourceAddress=src_address, \
			DestinationAddress=dst_address,ProtocolType=protocol,SourcePort=src_port,DestinationPort=dst_port, \
			Interface=interface)


	def generate_Filtering_L4_mspl_configuration(self, mspl_policy_configuration):
		#logger.debug("Generating {} MSPL".format(mspl.CapabilityType.Filtering_L4))

		# Set the MSPL capability
		mspl_capability = mspl.Capability(Name=mspl.CapabilityType.Filtering_L4)
		mspl_policy_configuration.capability.append(mspl_capability)

		# To generate a configuration rule
		configuration_rule = mspl.ConfigurationRule(isCNF="false",Name="Rule0")

		# To generate configuration rule action
		configuration_rule_action = mspl.FilteringAction(FilteringActionType=self.filtering_action)

		# For filtering we choose filtering configuration condition as condition
		configuration_condition = mspl.FilteringConfigurationCondition(isCNF="false")

		# TODO: Recovery the priority from somewere
		external_data = mspl.Priority(value_=500)

		# Filter parameters are configured using subject and objectH
		print(self.subject,str(self.object))

		configuration_condition.packetFilterCondition = self.generate_packet_filtering_condition()
		#configuration_condition.packetFilterCondition = mspl.PacketFilterCondition(SourceAddress=src_address, \
		#	DestinationAddress=dst_address,ProtocolType=protocol,SourcePort=src_port,DestinationPort=dst_port, \
		#	Interface=interface)

		# Set the configuration parameters
		configuration_rule.configurationRuleAction = configuration_rule_action
		configuration_rule.configurationCondition = configuration_condition
		configuration_rule.externalData = external_data
		#configuration_rule.HSPL.append(mspl.HSPL(HSPL_id=self.id,HSPL_text=self.toDOM().toprettyxml(encoding="utf-8").decode("utf-8").replace("ns1","tns").replace(' xmlns:tns="http://www.example.org/Refinement_Schema"','')))

		# Generates the configurationRule
		mspl_policy_configuration.configurationRule.append(configuration_rule) 

	def generate_Traffic_Divert_mspl_configuration(self, mspl_policy_configuration):
		#logger.debug("Generating {} MSPL".format(mspl.CapabilityType.Traffic_Divert))
		# Set the MSPL capability
		mspl_capability = mspl.Capability(Name=mspl.CapabilityType.Traffic_Divert)
		mspl_policy_configuration.capability.append(mspl_capability)

		# To generate a configuration rule
		configuration_rule = mspl.ConfigurationRule(isCNF="false",Name="Rule0")

		# To generate configuration rule action
		configuration_rule_action = mspl.TrafficDivertAction(TrafficDivertActionType=self.traffic_divert_action)

		# To generate packet divert action
		packetDivertAction = mspl.TrafficDivertConfigurationCondition(isCNF="false")
		
		#packetFilterCondition = mspl.PacketFilterCondition(DestinationAddress=self.fwd_action["dst_addr"])

		packetFilterCondition = mspl.PacketFilterCondition()
		packetFilterCondition.DestinationAddress = self.fwd_action["dst_addr"] if "dst_addr" in self.fwd_action.keys() else None
		packetFilterCondition.Interface = self.fwd_action["interface"] if "interface" in self.fwd_action.keys() else None

		packetDivertAction.packetFilterCondition = packetFilterCondition

		#logger.debug(packetDivertAction)

		configuration_rule_action.packetDivertAction = packetDivertAction

		# For filtering we choose filtering configuration condition as condition
		configuration_condition = mspl.TrafficDivertConfigurationCondition(isCNF="false")

		# TODO: Recovery the priority from somewere
		external_data = mspl.Priority(value_=500)

		print(self.subject,str(self.object))

		configuration_condition.packetFilterCondition = self.generate_packet_filtering_condition()

		# Set the configuration parameters
		configuration_rule.configurationRuleAction = configuration_rule_action
		configuration_rule.configurationCondition = configuration_condition
		configuration_rule.externalData = external_data

		# Generates the configurationRule
		mspl_policy_configuration.configurationRule.append(configuration_rule) 

	def get_enable_resource_mspl(self):

		def get_EnableAction_configuration_rule_action():
			configuration_rule_action = mspl.EnableAction(EnableActionType=mspl.EnableActionType(enable="true"))
			return configuration_rule_action

		def get_PowerMgmtAction_configuration_rule_action():
			configuration_rule_action = mspl.PowerMgmtAction(PowerMgmtActionType="OFF")
			return configuration_rule_action

		# Build base
		mspl_policy = self.build_mspl_base()

		# Set the MSPL capability
		mspl_capability = mspl.Capability(Name=mspl.CapabilityType.IoT_control)
		mspl_policy.configuration.capability.append(mspl_capability)

		# To generate a configuration rule
		configuration_rule = mspl.ConfigurationRule(isCNF="false",Name="Rule0")

		resource = SYS_MODEL[self.fields.resource.resource_property[0].key.lower()] if self.fields and self.fields.resource else None
		if self.fields.purpose:
			method = self.fields.purpose.purpose_property[0].key
		if resource:
			# Verify if is implemented
			configuration_rule_action = locals()["get_{}_configuration_rule_action".format(resource["action"])]()

		# For filtering we choose filtering configuration condition as condition
		configuration_condition = mspl.FilteringConfigurationCondition(isCNF="false")

		# TODO: Recovery the priority from somewere
		external_data = mspl.Priority(value_=500)

		# Get specific sensor information
		resource_info = self.get_resource_info()
		self.filtering_condition = {}
		self.filtering_condition["dst_port"] = resource_info["port"] if "port" in resource_info.keys() else None
		self.filtering_condition["dst_addr"] = resource_info["address"] if "address" in resource_info.keys() else None
		configuration_condition.packetFilterCondition = self.generate_packet_filtering_condition()


		configuration_condition.applicationLayerCondition = mspl.IoTApplicationLayerCondition(URL=resource_info["resource"],\
			method=method)

		# Set the configuration parameters
		configuration_rule.configurationRuleAction = configuration_rule_action
		configuration_rule.configurationCondition = configuration_condition
		configuration_rule.externalData = external_data
		#configuration_rule.HSPL.append(mspl.HSPL(HSPL_id=self.id,HSPL_text=self.toDOM().toprettyxml(encoding="utf-8").decode("utf-8").replace("ns1","tns").replace(' xmlns:tns="http://www.example.org/Refinement_Schema"','')))

		# Generates the configurationRule
		mspl_policy.configuration.configurationRule.append(configuration_rule) 
		text_mspl_policy = mspl_policy.toDOM(element_name="ITResource").toprettyxml(encoding="utf-8").decode("utf-8").replace(":ns1","").replace("ns1:","").replace(":ns2","").replace("ns2:","")
		#return [{"mspl":text_mspl_policy,"candidate_security_enablers":self.candidate_security_enablers}]
		return [mspl_policy]


	def get_no_authorise_access_traffic_mspl(self):
		# Verify the object and discern traffic or resource
		# Build base
		mspl_policy = self.build_mspl_base()
		# Target
		target = {}
		target = SYS_MODEL[self.fields.traffic_target.target_name[0].lower()] if self.fields and self.fields.traffic_target else {}
		# Action
		self.filtering_action = "DENY"
		# Build the filtering condition using, subject, object and target
		self.filtering_condition = {}
		self.filtering_condition["src_addr"] = self.src["address"] if "address" in self.src.keys() else None
		self.filtering_condition["interface"] = self.src["interface"] if "interface" in self.src.keys() else None
		self.filtering_condition["dst_port"] = self.object["port"] if "port" in self.object.keys() else None
		self.filtering_condition["protocol"] = self.object["protocol"] if "protocol" in self.object.keys() else None
		self.filtering_condition["dst_addr"] = target["address"] if "address" in target.keys() else None
		self.generate_Filtering_L4_mspl_configuration(mspl_policy.configuration)
		
		text_mspl_policy = mspl_policy.toDOM(element_name="ITResource").toprettyxml(encoding="utf-8").decode("utf-8").replace(":ns1","").replace("ns1:","").replace(":ns2","").replace("ns2:","")
		logger.debug(text_mspl_policy)
		return [mspl_policy]
		#return [{"mspl":text_mspl_policy,"candidate_security_enablers":self.candidate_security_enablers}]

	def get_authorise_access_traffic_mspl(self):



		authorise_access_mspl_list = []

		# Allow traffic source
		mspl_policy = self.build_mspl_base()
		# Target
		target = {}
		target = SYS_MODEL[self.fields.traffic_target.target_name[0].lower()] if self.fields and self.fields.traffic_target else {}
		logger.info("TARGET: {}".format(target))

		# CAUTION!!! HOT FIX: Case where IoT device acts as client
		if target["address"] == "2001:720:1710:4:f816:3eff:fe16:27a3/128":
			target_temp = self.src
			self.src = target
			target = target_temp

		# Action
		self.traffic_divert_action = "FORWARD"
		self.fwd_action = {}
		self.fwd_action["dst_addr"] = target["address"] if "address" in target.keys() else None
		self.fwd_action["interface"] = target["interface"] if "interface" in target.keys() else None
		self.filtering_condition = {}
		self.filtering_condition["src_addr"] = self.src["address"] if "address" in self.src.keys() else None
		self.filtering_condition["dst_addr"] = target["address"] if "address" in target.keys() else None
		self.filtering_condition["interface"] = self.src["interface"] if "interface" in self.src.keys() else None
		self.filtering_condition["dst_port"] = self.object["port"] if "port" in self.object.keys() else None
		self.filtering_condition["protocol"] = self.object["protocol"] if "protocol" in self.object.keys() else None

		self.generate_Traffic_Divert_mspl_configuration(mspl_policy.configuration)

		mspl_policy1 = mspl_policy

		# Allow traffic destination
		mspl_policy = self.build_mspl_base()
		self.fwd_action = {}
		self.fwd_action["dst_addr"] = self.src["address"] if "address" in self.src.keys() else None
		self.fwd_action["interface"] = self.src["interface"] if "interface" in self.src.keys() else None
		# Filtering condition
		self.filtering_condition = {}
		self.filtering_condition["src_addr"] = target["address"] if "address" in target.keys() else None
		self.filtering_condition["dst_addr"] = self.src["address"] if "address" in self.src.keys() else None
		self.filtering_condition["interface"] = target["interface"] if "interface" in target.keys() else None
		self.filtering_condition["src_port"] = self.object["port"] if "port" in self.object.keys() else None
		self.filtering_condition["protocol"] = self.object["protocol"] if "protocol" in self.object.keys() else None

		self.generate_Traffic_Divert_mspl_configuration(mspl_policy.configuration)

		mspl_policy2 = mspl_policy

		#return authorise_access_mspl_list
		return [mspl_policy1,mspl_policy2]

	def generate_authorisation_resource_mspl(self,authorisation_action_type):
		# Build base
		mspl_policy = self.build_mspl_base()

		# Set the MSPL capability
		mspl_capability = mspl.Capability(Name=mspl.CapabilityType.AuthoriseAccess_resurce)
		mspl_policy.configuration.capability.append(mspl_capability)

		# To generate a configuration rule
		configuration_rule = mspl.ConfigurationRule(isCNF="false",Name="Rule0")
		# Get fields
		resource = SYS_MODEL[self.fields.resource.resource_property[0].key.lower()] if self.fields and self.fields.resource else None
		traffic_target = SYS_MODEL[self.fields.traffic_target.target_name[0].lower()] if self.fields and self.fields.traffic_target else None
		method = self.fields.purpose.purpose_property[0].value_

		resource_info = self.get_resource_info()

		configuration_rule_action = mspl.AuthorizationAction(AuthorizationActionType=authorisation_action_type,AuthorizationSubject=self.src["address"],\
			AuthorizationTarget=traffic_target["address"])
			
		# For filtering we choose filtering configuration condition as condition
		#configuration_condition = mspl.FilteringConfigurationCondition(isCNF="false")
		configuration_condition = mspl.AuthorizationCondition(isCNF="false")

		# TODO: Recovery the priority from somewere
		external_data = mspl.Priority(value_=500)

		# Get specific sensor information
		resource_info = self.get_resource_info()
		self.filtering_condition = {}
		self.filtering_condition["src_addr"] = self.src["address"] if "address" in self.src.keys() else None
		self.filtering_condition["dst_addr"] = traffic_target["address"] if "address" in traffic_target.keys() else None
		configuration_condition.packetFilterCondition = self.generate_packet_filtering_condition()

		configuration_condition.applicationLayerCondition = mspl.IoTApplicationLayerCondition(URL=resource_info["resource"],\
			method=method)

		# Set the configuration parameters
		configuration_rule.configurationRuleAction = configuration_rule_action
		configuration_rule.configurationCondition = configuration_condition
		configuration_rule.externalData = external_data
		#configuration_rule.HSPL.append(mspl.HSPL(HSPL_id=self.id,HSPL_text=self.toDOM().toprettyxml(encoding="utf-8").decode("utf-8").replace("ns1","tns").replace(' xmlns:tns="http://www.example.org/Refinement_Schema"','')))

		# Generates the configurationRule
		mspl_policy.configuration.configurationRule.append(configuration_rule) 
		text_mspl_policy = mspl_policy.toDOM(element_name="ITResource").toprettyxml(encoding="utf-8").decode("utf-8").replace(":ns1","").replace("ns1:","").replace(":ns2","").replace("ns2:","")
		#return [{"mspl":text_mspl_policy,"candidate_security_enablers":self.candidate_security_enablers}]
		return [mspl_policy]


	def get_no_authorise_access_resource_mspl(self):
		return self.generate_authorisation_resource_mspl("DENY")
		

	def get_authorise_access_resource_mspl(self):
		return self.generate_authorisation_resource_mspl("ALLOW")

	def get_require_authentication_traffic_mspl(self):
		require_authentication_mspl_list = []
		# Generate filter all 
		# Build base
		mspl_policy = self.build_mspl_base()
		# Generate fitlering configuration
		self.filtering_action = "DENY"
		self.object["src_addr"] = self.src["address"]
		self.generate_Filtering_L4_mspl_configuration(mspl_policy.configuration)
		text_mspl_policy = mspl_policy.toDOM(element_name="ITResource").toprettyxml(encoding="utf-8").decode("utf-8").replace(":ns1","").replace("ns1:","").replace(":ns2","").replace("ns2:","")
		require_authentication_mspl_list.append({"mspl":text_mspl_policy,"candidate_security_enablers":self.candidate_security_enablers})
		
		# Get the Authentication transport protocol
		authentication_transport_protocol = self.fields.type_content.content_name[0].lower()
		if authentication_transport_protocol in SYS_MODEL.keys():
			authentication_transport_protocol_values = SYS_MODEL[authentication_transport_protocol]
		else:
			raise ValueError('There are not information for the authentication protocol {}'.format(authentication_transport_protocol))

		authentication_entity = self.fields.resource.resource_property[0].key.lower()
		if authentication_entity in SYS_MODEL.keys():
			authentication_entity_values = SYS_MODEL[authentication_entity]
		else:
			raise ValueError('There are not information for the authentication entity {}'.format(authentication_entity))

		# Allow authentication traffic source
		mspl_policy = self.build_mspl_base()
		self.traffic_divert_action = "FORWARD"
		self.fwd_action = {}
		self.fwd_action["dst_addr"] = authentication_entity_values["address"]
		self.filtering_condition = {}
		self.filtering_condition["dst_port"] = authentication_transport_protocol_values["port"]
		self.filtering_condition["protocol"] = authentication_transport_protocol_values["protocol"]

		self.generate_Traffic_Divert_mspl_configuration(mspl_policy.configuration)
		text_mspl_policy = mspl_policy.toDOM(element_name="ITResource").toprettyxml(encoding="utf-8").decode("utf-8").replace(":ns1","").replace("ns1:","").replace(":ns2","").replace("ns2:","")
		require_authentication_mspl_list.append({"mspl":text_mspl_policy,"candidate_security_enablers":self.candidate_security_enablers})
		
		# Allow authentication traffic destination
		mspl_policy = self.build_mspl_base()
		self.fwd_action = {}
		self.fwd_action["dst_addr"] = self.src["address"]
		self.filtering_condition = {}
		self.filtering_condition["src_port"] = authentication_transport_protocol_values["port"]
		self.filtering_condition["protocol"] = authentication_transport_protocol_values["protocol"]

		self.generate_Traffic_Divert_mspl_configuration(mspl_policy.configuration)
		text_mspl_policy = mspl_policy.toDOM(element_name="ITResource").toprettyxml(encoding="utf-8").decode("utf-8").replace(":ns1","").replace("ns1:","").replace(":ns2","").replace("ns2:","")
		text_mspl_policy = mspl_policy.toxml(element_name="ITResource").replace(":ns1","").replace("ns1:","").replace(":ns2","").replace("ns2:","")
		require_authentication_mspl_list.append({"mspl":text_mspl_policy,"candidate_security_enablers":self.candidate_security_enablers})
		
		return require_authentication_mspl_list


	def get_prot_conf_integr_traffic_mspl(self):
		# At this point, the capability depends on the source
		#target = SYS_MODEL[self.fields.traffic_target.target_name[0].lower()] if self.fields and self.fields.traffic_target else None
		return getattr(self,"generate_{}_mspl_configuration".format(self.src["prot_conf_type"]))()


	def generate_DTLS_protocol_mspl(self):

		# Build base
		mspl_policy = self.build_mspl_base()

		# Set the MSPL capability
		mspl_capability = mspl.Capability(Name=mspl.CapabilityType.DTLS_protocol)
		mspl_policy.configuration.capability.append(mspl_capability)

		# To generate a configuration rule
		configuration_rule = mspl.ConfigurationRule(isCNF="false",Name="Rule0")

		# Get target
		traffic_target = SYS_MODEL[self.fields.traffic_target.target_name[0].lower()] if self.fields and self.fields.traffic_target else None

		technology_parameter = mspl.DTLSTechnologyParameter(localEndpoint=self.src["address"],remoteEndpoint=traffic_target["address"])

		#authentication_parameters = mspl.AuthenticationParameters(psKey_value="DTLS_KEY_EXAMPLE")

		technology_action_parameters = mspl.technologyActionParameters(technologyParameter=[technology_parameter])

		technology_action_security_property = [mspl.Confidentiality(encryptionAlgorithm="AES",keySize="128",mode="CCM"),\
		mspl.Integrity(integrityAlgorithm="sha1")]

		configuration_rule_action = mspl.DataProtectionAction(technology="DTLS",technologyActionParameters=technology_action_parameters,\
			technologyActionSecurityProperty=technology_action_security_property)

		#configuration_rule_action.technologyActionSecurityProperty.append(mspl.Confidentiality(encryptionAlgorithm="AES",keySize="128",mode="CCM"))\
		#.append(mspl.Integrity(integrityAlgorithm="sha1"))

		# For filtering we choose filtering configuration condition as condition
		configuration_condition = mspl.DataProtectionCondition(isCNF="false")

		# TODO: Recovery the priority from somewere
		external_data = mspl.Priority(value_=500)
		
		# Set the configuration parameters
		configuration_rule.configurationRuleAction = configuration_rule_action
		configuration_rule.configurationCondition = configuration_condition
		configuration_rule.externalData = external_data
		#configuration_rule.HSPL.append(mspl.HSPL(HSPL_id=self.id,HSPL_text=self.toDOM().toprettyxml(encoding="utf-8").decode("utf-8").replace("ns1","tns").replace(' xmlns:tns="http://www.example.org/Refinement_Schema"','')))

		# Generates the configurationRule
		mspl_policy.configuration.configurationRule.append(configuration_rule) 
		text_mspl_policy = mspl_policy.toDOM(element_name="ITResource").toprettyxml(encoding="utf-8").decode("utf-8").replace(":ns1","").replace("ns1:","").replace(":ns2","").replace("ns2:","")
		#return [{"mspl":text_mspl_policy,"candidate_security_enablers":self.candidate_security_enablers}]
		#return text_mspl_policy
		return mspl_policy


	def generate_DTLS_protocol_mspl_configuration(self):
		DEVICES_QTTY = 1
		mspl_list_enabler_candidates = []
		# If there is no address...
		if not "address" in self.src.keys():
			# Simulate getting all IoT devices in the specified interface
			##logger.debug("BULK OPERATION WITH {}".format(self.src))
			DEVICES_QTTY = self.src["nodes"]
			self.src = SYS_MODEL["sensor-1"]
			

		for i in range(DEVICES_QTTY):
			# Generate DTLS1
			mspl_dtls_1 = self.generate_DTLS_protocol_mspl()

			# Generate DTLS2
			mspl_dtls_2 = self.generate_DTLS_protocol_mspl()
			candidate_1 = mspl_dtls_2.enablerCandidates.enabler[1]
			mspl_dtls_2.enablerCandidates.enabler[1] = mspl_dtls_2.enablerCandidates.enabler[0]
			mspl_dtls_2.enablerCandidates.enabler[0] = candidate_1

		return [mspl_dtls_1,mspl_dtls_2]
		

	def get_config_authentication_PANA_mspl(self):
		# Build base
		mspl_policy = self.build_mspl_base()

		# Set the MSPL capability
		mspl_capability = mspl.Capability(Name=mspl.CapabilityType.Authentication)
		mspl_policy.configuration.capability.append(mspl_capability)

		# To generate a configuration rule
		configuration_rule = mspl.ConfigurationRule(isCNF="false",Name="Rule0")

		# Get target
		traffic_target = SYS_MODEL[self.fields.traffic_target.target_name[0].lower()] if self.fields and self.fields.traffic_target else None

		# Get Authentication method
		if self.fields.purpose:
			method = self.fields.purpose.purpose_property[0].value_

		logger.debug("METHOD: {}".format(method))

		# Get Authentication mechanism
		authentication_mechanism = str(self.object)

		# Authentication target
		self.filtering_condition = {}
		self.filtering_condition["protocol"] = traffic_target["protocol"] if "protocol" in traffic_target.keys() else None
		self.filtering_condition["dst_addr"] = traffic_target["address"] if "address" in traffic_target.keys() else None
		self.filtering_condition["dst_port"] = traffic_target["port"] if "port" in traffic_target.keys() else None
		self.filtering_condition["interface"] = traffic_target["interface"] if "interface" in traffic_target.keys() else None
		authentication_target = self.generate_packet_filtering_condition()

		# Authentication option
		authentication_option = mspl.AuthenticationOption()

		authentication_option.AuthenticationTarget = authentication_target
		authentication_option.AuthenticationMethod = method
		authentication_option.AuthenticationMechanism = authentication_mechanism
		authentication_option.AuthenticationParameters = mspl.AuthenticationParameters()

		# Confighure rule action
		configuration_rule_action = mspl.AuthenticationAction()
		configuration_rule_action.AuthenticationOption.append(authentication_option)
		
		# Configuration condition
		source = self.src["address"] if "address" in self.src.keys() else self.src["interface"]
		configuration_condition = mspl.AuthenticationCondition(isCNF="false", AuthenticationSubject=source)
		

		# TODO: Recovery the priority from somewere
		external_data = mspl.Priority(value_=500)
		
		# Set the configuration parameters
		configuration_rule.configurationRuleAction = configuration_rule_action
		configuration_rule.configurationCondition = configuration_condition
		configuration_rule.externalData = external_data
		#configuration_rule.HSPL.append(mspl.HSPL(HSPL_id=self.id,HSPL_text=self.toDOM().toprettyxml(encoding="utf-8").decode("utf-8").replace("ns1","tns").replace(' xmlns:tns="http://www.example.org/Refinement_Schema"','')))

		# Generates the configurationRule
		mspl_policy.configuration.configurationRule.append(configuration_rule) 

		return [mspl_policy]


class H2MRefiner:
	'Class in charge to refine HSPL to MSPL'

	def get_mspl(self,hspl_source):
		'Return the MSPL from the hspl_source'
		# Customize the pyxb generated classes with our own behavior
		global hspl
		hspl.MappingType._SetSupersedingClass(MappingType)
		hspl.HSPL_Orchestration._SetSupersedingClass(HSPL_Orchestration)
		hspl.HSPL._SetSupersedingClass(HSPL)
		#Load the xml
		hspl_policy = hspl.CreateFromDocument(hspl_source)
		# Start the parser process using the xml root element
		return hspl_policy.get_mspl()

if __name__ == "__main__":
	xml_file = sys.argv[1] 
	h2mrefiner = H2MRefiner()
	#logger.debug("Reading mspl file...")
	xml_source = open(xml_file).read()
	mspl_policy = h2mrefiner.get_mspl(xml_source)
	# Pretty print
	separator = "*"
	separator_group = separator*5
	title = "MSPL POLICY"
	print("\n{}{}{}".format(separator_group,title,separator_group))
	print(mspl_policy)
	print("{}{}{}\n".format(separator_group,separator*len(title),separator_group))