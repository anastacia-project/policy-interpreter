#!/bin/bash

#remove all files
rm -rf *.py __* raw
#generate the parser
pyxbgen -u mspl.xsd -m mspl --write-for-customization

rm -rf mspl.py _IoTHoneyNet.py
echo "# -*- coding: utf-8 -*-
import sys
from .raw.mspl import *
from .raw._IoTHoneyNet import *" > __init__.py
#from .mspl_custom import *

# Replace the import in order to find the module
#import _IoTHoneyNet as _ImportedBinding__IoTHoneyNet
sed -i -e 's/import _IoTHoneyNet/from . import _IoTHoneyNet/g' raw/mspl.py
