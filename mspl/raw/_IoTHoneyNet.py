# ./raw/_IoTHoneyNet.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:ccbc541b03496acb8b920dd57f9ecc1ed7c75542
# Generated 2019-12-03 19:54:46.242864 by PyXB version 1.2.6 using Python 3.7.1.final.0
# Namespace http://www.example.org/IoTHoneynetSchema [xmlns:IoTHoneyNet]

from __future__ import unicode_literals
import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys
import pyxb.utils.six as _six
# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:5f8304fa-15fe-11ea-a561-0242ac180008')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.6'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# A holder for module-level binding classes so we can access them from
# inside class definitions where property names may conflict.
_module_typeBindings = pyxb.utils.utility.Object()

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.NamespaceForURI('http://www.example.org/IoTHoneynetSchema', create_if_missing=True)
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement, default_namespace=default_namespace)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, _six.text_type):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Atomic simple type: {http://www.example.org/IoTHoneynetSchema}ioTResourceType
class ioTResourceType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ioTResourceType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 108, 2)
    _Documentation = None
ioTResourceType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=ioTResourceType, enum_prefix=None)
ioTResourceType.TEMPERATURE = ioTResourceType._CF_enumeration.addEnumeration(unicode_value='TEMPERATURE', tag='TEMPERATURE')
ioTResourceType.HUMIDITY = ioTResourceType._CF_enumeration.addEnumeration(unicode_value='HUMIDITY', tag='HUMIDITY')
ioTResourceType.PRESSURE = ioTResourceType._CF_enumeration.addEnumeration(unicode_value='PRESSURE', tag='PRESSURE')
ioTResourceType.LIGHT = ioTResourceType._CF_enumeration.addEnumeration(unicode_value='LIGHT', tag='LIGHT')
ioTResourceType._InitializeFacetMap(ioTResourceType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'ioTResourceType', ioTResourceType)
_module_typeBindings.ioTResourceType = ioTResourceType

# Atomic simple type: {http://www.example.org/IoTHoneynetSchema}interactionLevel
class interactionLevel (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'interactionLevel')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 117, 2)
    _Documentation = None
interactionLevel._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=interactionLevel, enum_prefix=None)
interactionLevel.LOW = interactionLevel._CF_enumeration.addEnumeration(unicode_value='LOW', tag='LOW')
interactionLevel.MEDIUM = interactionLevel._CF_enumeration.addEnumeration(unicode_value='MEDIUM', tag='MEDIUM')
interactionLevel.HIGH = interactionLevel._CF_enumeration.addEnumeration(unicode_value='HIGH', tag='HIGH')
interactionLevel._InitializeFacetMap(interactionLevel._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'interactionLevel', interactionLevel)
_module_typeBindings.interactionLevel = interactionLevel

# Complex type {http://www.example.org/IoTHoneynetSchema}ioTHoneyNetType with content type ELEMENT_ONLY
class ioTHoneyNetType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/IoTHoneynetSchema}ioTHoneyNetType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ioTHoneyNetType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 9, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/IoTHoneynetSchema}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__httpwww_example_orgIoTHoneynetSchema_ioTHoneyNetType_httpwww_example_orgIoTHoneynetSchemaname', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 11, 6), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}net uses Python identifier net
    __net = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'net'), 'net', '__httpwww_example_orgIoTHoneynetSchema_ioTHoneyNetType_httpwww_example_orgIoTHoneynetSchemanet', True, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 12, 6), )

    
    net = property(__net.value, __net.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}router uses Python identifier router
    __router = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'router'), 'router', '__httpwww_example_orgIoTHoneynetSchema_ioTHoneyNetType_httpwww_example_orgIoTHoneynetSchemarouter', True, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 13, 6), )

    
    router = property(__router.value, __router.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}containmentGateway uses Python identifier containmentGateway
    __containmentGateway = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'containmentGateway'), 'containmentGateway', '__httpwww_example_orgIoTHoneynetSchema_ioTHoneyNetType_httpwww_example_orgIoTHoneynetSchemacontainmentGateway', True, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 14, 6), )

    
    containmentGateway = property(__containmentGateway.value, __containmentGateway.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}ioTHoneyPot uses Python identifier ioTHoneyPot
    __ioTHoneyPot = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ioTHoneyPot'), 'ioTHoneyPot', '__httpwww_example_orgIoTHoneynetSchema_ioTHoneyNetType_httpwww_example_orgIoTHoneynetSchemaioTHoneyPot', True, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 15, 6), )

    
    ioTHoneyPot = property(__ioTHoneyPot.value, __ioTHoneyPot.set, None, None)

    _ElementMap.update({
        __name.name() : __name,
        __net.name() : __net,
        __router.name() : __router,
        __containmentGateway.name() : __containmentGateway,
        __ioTHoneyPot.name() : __ioTHoneyPot
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ioTHoneyNetType = ioTHoneyNetType
Namespace.addCategoryObject('typeBinding', 'ioTHoneyNetType', ioTHoneyNetType)


# Complex type {http://www.example.org/IoTHoneynetSchema}honeyNetType with content type ELEMENT_ONLY
class honeyNetType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/IoTHoneynetSchema}honeyNetType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'honeyNetType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 19, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/IoTHoneynetSchema}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__httpwww_example_orgIoTHoneynetSchema_honeyNetType_httpwww_example_orgIoTHoneynetSchemaname', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 21, 6), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}net uses Python identifier net
    __net = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'net'), 'net', '__httpwww_example_orgIoTHoneynetSchema_honeyNetType_httpwww_example_orgIoTHoneynetSchemanet', True, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 22, 6), )

    
    net = property(__net.value, __net.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}router uses Python identifier router
    __router = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'router'), 'router', '__httpwww_example_orgIoTHoneynetSchema_honeyNetType_httpwww_example_orgIoTHoneynetSchemarouter', True, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 23, 6), )

    
    router = property(__router.value, __router.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}containmentGateway uses Python identifier containmentGateway
    __containmentGateway = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'containmentGateway'), 'containmentGateway', '__httpwww_example_orgIoTHoneynetSchema_honeyNetType_httpwww_example_orgIoTHoneynetSchemacontainmentGateway', True, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 24, 6), )

    
    containmentGateway = property(__containmentGateway.value, __containmentGateway.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}honeyPot uses Python identifier honeyPot
    __honeyPot = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'honeyPot'), 'honeyPot', '__httpwww_example_orgIoTHoneynetSchema_honeyNetType_httpwww_example_orgIoTHoneynetSchemahoneyPot', True, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 25, 6), )

    
    honeyPot = property(__honeyPot.value, __honeyPot.set, None, None)

    _ElementMap.update({
        __name.name() : __name,
        __net.name() : __net,
        __router.name() : __router,
        __containmentGateway.name() : __containmentGateway,
        __honeyPot.name() : __honeyPot
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.honeyNetType = honeyNetType
Namespace.addCategoryObject('typeBinding', 'honeyNetType', honeyNetType)


# Complex type {http://www.example.org/IoTHoneynetSchema}netType with content type ELEMENT_ONLY
class netType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/IoTHoneynetSchema}netType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'netType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 30, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/IoTHoneynetSchema}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__httpwww_example_orgIoTHoneynetSchema_netType_httpwww_example_orgIoTHoneynetSchemaname', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 32, 12), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Attribute id uses Python identifier id
    __id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'id'), 'id', '__httpwww_example_orgIoTHoneynetSchema_netType_id', pyxb.binding.datatypes.byte)
    __id._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 34, 10)
    __id._UseLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 34, 10)
    
    id = property(__id.value, __id.set, None, None)

    _ElementMap.update({
        __name.name() : __name
    })
    _AttributeMap.update({
        __id.name() : __id
    })
_module_typeBindings.netType = netType
Namespace.addCategoryObject('typeBinding', 'netType', netType)


# Complex type {http://www.example.org/IoTHoneynetSchema}routerType with content type ELEMENT_ONLY
class routerType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/IoTHoneynetSchema}routerType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'routerType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 38, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/IoTHoneynetSchema}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__httpwww_example_orgIoTHoneynetSchema_routerType_httpwww_example_orgIoTHoneynetSchemaname', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 40, 6), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}if uses Python identifier if_
    __if = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'if'), 'if_', '__httpwww_example_orgIoTHoneynetSchema_routerType_httpwww_example_orgIoTHoneynetSchemaif', True, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 41, 6), )

    
    if_ = property(__if.value, __if.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}route uses Python identifier route
    __route = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'route'), 'route', '__httpwww_example_orgIoTHoneynetSchema_routerType_httpwww_example_orgIoTHoneynetSchemaroute', True, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 42, 6), )

    
    route = property(__route.value, __route.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}operatingSystem uses Python identifier operatingSystem
    __operatingSystem = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'operatingSystem'), 'operatingSystem', '__httpwww_example_orgIoTHoneynetSchema_routerType_httpwww_example_orgIoTHoneynetSchemaoperatingSystem', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 43, 6), )

    
    operatingSystem = property(__operatingSystem.value, __operatingSystem.set, None, None)

    
    # Attribute id uses Python identifier id
    __id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'id'), 'id', '__httpwww_example_orgIoTHoneynetSchema_routerType_id', pyxb.binding.datatypes.byte)
    __id._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 45, 4)
    __id._UseLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 45, 4)
    
    id = property(__id.value, __id.set, None, None)

    _ElementMap.update({
        __name.name() : __name,
        __if.name() : __if,
        __route.name() : __route,
        __operatingSystem.name() : __operatingSystem
    })
    _AttributeMap.update({
        __id.name() : __id
    })
_module_typeBindings.routerType = routerType
Namespace.addCategoryObject('typeBinding', 'routerType', routerType)


# Complex type {http://www.example.org/IoTHoneynetSchema}routeType with content type ELEMENT_ONLY
class routeType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/IoTHoneynetSchema}routeType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'routeType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 48, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/IoTHoneynetSchema}dst uses Python identifier dst
    __dst = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'dst'), 'dst', '__httpwww_example_orgIoTHoneynetSchema_routeType_httpwww_example_orgIoTHoneynetSchemadst', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 50, 6), )

    
    dst = property(__dst.value, __dst.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}gw uses Python identifier gw
    __gw = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'gw'), 'gw', '__httpwww_example_orgIoTHoneynetSchema_routeType_httpwww_example_orgIoTHoneynetSchemagw', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 51, 6), )

    
    gw = property(__gw.value, __gw.set, None, None)

    
    # Attribute id uses Python identifier id
    __id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'id'), 'id', '__httpwww_example_orgIoTHoneynetSchema_routeType_id', pyxb.binding.datatypes.byte)
    __id._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 53, 4)
    __id._UseLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 53, 4)
    
    id = property(__id.value, __id.set, None, None)

    _ElementMap.update({
        __dst.name() : __dst,
        __gw.name() : __gw
    })
    _AttributeMap.update({
        __id.name() : __id
    })
_module_typeBindings.routeType = routeType
Namespace.addCategoryObject('typeBinding', 'routeType', routeType)


# Complex type {http://www.example.org/IoTHoneynetSchema}containmentGatewayType with content type ELEMENT_ONLY
class containmentGatewayType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/IoTHoneynetSchema}containmentGatewayType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'containmentGatewayType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 57, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/IoTHoneynetSchema}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__httpwww_example_orgIoTHoneynetSchema_containmentGatewayType_httpwww_example_orgIoTHoneynetSchemaname', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 59, 6), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}if uses Python identifier if_
    __if = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'if'), 'if_', '__httpwww_example_orgIoTHoneynetSchema_containmentGatewayType_httpwww_example_orgIoTHoneynetSchemaif', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 60, 6), )

    
    if_ = property(__if.value, __if.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}operatingSystem uses Python identifier operatingSystem
    __operatingSystem = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'operatingSystem'), 'operatingSystem', '__httpwww_example_orgIoTHoneynetSchema_containmentGatewayType_httpwww_example_orgIoTHoneynetSchemaoperatingSystem', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 61, 6), )

    
    operatingSystem = property(__operatingSystem.value, __operatingSystem.set, None, None)

    _ElementMap.update({
        __name.name() : __name,
        __if.name() : __if,
        __operatingSystem.name() : __operatingSystem
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.containmentGatewayType = containmentGatewayType
Namespace.addCategoryObject('typeBinding', 'containmentGatewayType', containmentGatewayType)


# Complex type {http://www.example.org/IoTHoneynetSchema}honeyPotType with content type ELEMENT_ONLY
class honeyPotType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/IoTHoneynetSchema}honeyPotType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'honeyPotType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 66, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/IoTHoneynetSchema}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__httpwww_example_orgIoTHoneynetSchema_honeyPotType_httpwww_example_orgIoTHoneynetSchemaname', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 68, 6), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}interaction_level uses Python identifier interaction_level
    __interaction_level = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'interaction_level'), 'interaction_level', '__httpwww_example_orgIoTHoneynetSchema_honeyPotType_httpwww_example_orgIoTHoneynetSchemainteraction_level', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 69, 6), )

    
    interaction_level = property(__interaction_level.value, __interaction_level.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}if uses Python identifier if_
    __if = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'if'), 'if_', '__httpwww_example_orgIoTHoneynetSchema_honeyPotType_httpwww_example_orgIoTHoneynetSchemaif', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 70, 6), )

    
    if_ = property(__if.value, __if.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}operatingSystem uses Python identifier operatingSystem
    __operatingSystem = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'operatingSystem'), 'operatingSystem', '__httpwww_example_orgIoTHoneynetSchema_honeyPotType_httpwww_example_orgIoTHoneynetSchemaoperatingSystem', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 71, 6), )

    
    operatingSystem = property(__operatingSystem.value, __operatingSystem.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}software uses Python identifier software
    __software = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'software'), 'software', '__httpwww_example_orgIoTHoneynetSchema_honeyPotType_httpwww_example_orgIoTHoneynetSchemasoftware', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 72, 6), )

    
    software = property(__software.value, __software.set, None, None)

    
    # Attribute id uses Python identifier id
    __id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'id'), 'id', '__httpwww_example_orgIoTHoneynetSchema_honeyPotType_id', pyxb.binding.datatypes.string)
    __id._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 74, 4)
    __id._UseLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 74, 4)
    
    id = property(__id.value, __id.set, None, None)

    _ElementMap.update({
        __name.name() : __name,
        __interaction_level.name() : __interaction_level,
        __if.name() : __if,
        __operatingSystem.name() : __operatingSystem,
        __software.name() : __software
    })
    _AttributeMap.update({
        __id.name() : __id
    })
_module_typeBindings.honeyPotType = honeyPotType
Namespace.addCategoryObject('typeBinding', 'honeyPotType', honeyPotType)


# Complex type {http://www.example.org/IoTHoneynetSchema}physicalLocation with content type ELEMENT_ONLY
class physicalLocation (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/IoTHoneynetSchema}physicalLocation with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'physicalLocation')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 101, 0)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/IoTHoneynetSchema}x uses Python identifier x
    __x = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'x'), 'x', '__httpwww_example_orgIoTHoneynetSchema_physicalLocation_httpwww_example_orgIoTHoneynetSchemax', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 103, 6), )

    
    x = property(__x.value, __x.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}y uses Python identifier y
    __y = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'y'), 'y', '__httpwww_example_orgIoTHoneynetSchema_physicalLocation_httpwww_example_orgIoTHoneynetSchemay', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 104, 6), )

    
    y = property(__y.value, __y.set, None, None)

    _ElementMap.update({
        __x.name() : __x,
        __y.name() : __y
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.physicalLocation = physicalLocation
Namespace.addCategoryObject('typeBinding', 'physicalLocation', physicalLocation)


# Complex type {http://www.example.org/IoTHoneynetSchema}ifType with content type ELEMENT_ONLY
class ifType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/IoTHoneynetSchema}ifType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ifType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 126, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/IoTHoneynetSchema}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__httpwww_example_orgIoTHoneynetSchema_ifType_httpwww_example_orgIoTHoneynetSchemaname', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 128, 6), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}mac_addr uses Python identifier mac_addr
    __mac_addr = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'mac_addr'), 'mac_addr', '__httpwww_example_orgIoTHoneynetSchema_ifType_httpwww_example_orgIoTHoneynetSchemamac_addr', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 129, 6), )

    
    mac_addr = property(__mac_addr.value, __mac_addr.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}ip uses Python identifier ip
    __ip = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ip'), 'ip', '__httpwww_example_orgIoTHoneynetSchema_ifType_httpwww_example_orgIoTHoneynetSchemaip', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 130, 6), )

    
    ip = property(__ip.value, __ip.set, None, None)

    
    # Attribute id uses Python identifier id
    __id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'id'), 'id', '__httpwww_example_orgIoTHoneynetSchema_ifType_id', pyxb.binding.datatypes.byte)
    __id._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 132, 4)
    __id._UseLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 132, 4)
    
    id = property(__id.value, __id.set, None, None)

    
    # Attribute net uses Python identifier net
    __net = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'net'), 'net', '__httpwww_example_orgIoTHoneynetSchema_ifType_net', pyxb.binding.datatypes.string)
    __net._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 133, 4)
    __net._UseLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 133, 4)
    
    net = property(__net.value, __net.set, None, None)

    _ElementMap.update({
        __name.name() : __name,
        __mac_addr.name() : __mac_addr,
        __ip.name() : __ip
    })
    _AttributeMap.update({
        __id.name() : __id,
        __net.name() : __net
    })
_module_typeBindings.ifType = ifType
Namespace.addCategoryObject('typeBinding', 'ifType', ifType)


# Complex type {http://www.example.org/IoTHoneynetSchema}operatingSystemType with content type ELEMENT_ONLY
class operatingSystemType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/IoTHoneynetSchema}operatingSystemType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'operatingSystemType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 136, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/IoTHoneynetSchema}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__httpwww_example_orgIoTHoneynetSchema_operatingSystemType_httpwww_example_orgIoTHoneynetSchemaname', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 138, 6), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}version uses Python identifier version
    __version = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'version'), 'version', '__httpwww_example_orgIoTHoneynetSchema_operatingSystemType_httpwww_example_orgIoTHoneynetSchemaversion', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 139, 6), )

    
    version = property(__version.value, __version.set, None, None)

    _ElementMap.update({
        __name.name() : __name,
        __version.name() : __version
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.operatingSystemType = operatingSystemType
Namespace.addCategoryObject('typeBinding', 'operatingSystemType', operatingSystemType)


# Complex type {http://www.example.org/IoTHoneynetSchema}softwareType with content type ELEMENT_ONLY
class softwareType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/IoTHoneynetSchema}softwareType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'softwareType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 143, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/IoTHoneynetSchema}name uses Python identifier name
    __name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'name'), 'name', '__httpwww_example_orgIoTHoneynetSchema_softwareType_httpwww_example_orgIoTHoneynetSchemaname', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 145, 6), )

    
    name = property(__name.value, __name.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}version uses Python identifier version
    __version = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'version'), 'version', '__httpwww_example_orgIoTHoneynetSchema_softwareType_httpwww_example_orgIoTHoneynetSchemaversion', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 146, 6), )

    
    version = property(__version.value, __version.set, None, None)

    
    # Attribute id uses Python identifier id
    __id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'id'), 'id', '__httpwww_example_orgIoTHoneynetSchema_softwareType_id', pyxb.binding.datatypes.byte)
    __id._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 148, 4)
    __id._UseLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 148, 4)
    
    id = property(__id.value, __id.set, None, None)

    _ElementMap.update({
        __name.name() : __name,
        __version.name() : __version
    })
    _AttributeMap.update({
        __id.name() : __id
    })
_module_typeBindings.softwareType = softwareType
Namespace.addCategoryObject('typeBinding', 'softwareType', softwareType)


# Complex type {http://www.example.org/IoTHoneynetSchema}ioTHoneyPotType with content type ELEMENT_ONLY
class ioTHoneyPotType (honeyPotType):
    """Complex type {http://www.example.org/IoTHoneynetSchema}ioTHoneyPotType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ioTHoneyPotType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 77, 2)
    _ElementMap = honeyPotType._ElementMap.copy()
    _AttributeMap = honeyPotType._AttributeMap.copy()
    # Base type is honeyPotType
    
    # Element name ({http://www.example.org/IoTHoneynetSchema}name) inherited from {http://www.example.org/IoTHoneynetSchema}honeyPotType
    
    # Element interaction_level ({http://www.example.org/IoTHoneynetSchema}interaction_level) inherited from {http://www.example.org/IoTHoneynetSchema}honeyPotType
    
    # Element if_ ({http://www.example.org/IoTHoneynetSchema}if) inherited from {http://www.example.org/IoTHoneynetSchema}honeyPotType
    
    # Element operatingSystem ({http://www.example.org/IoTHoneynetSchema}operatingSystem) inherited from {http://www.example.org/IoTHoneynetSchema}honeyPotType
    
    # Element software ({http://www.example.org/IoTHoneynetSchema}software) inherited from {http://www.example.org/IoTHoneynetSchema}honeyPotType
    
    # Element {http://www.example.org/IoTHoneynetSchema}model uses Python identifier model
    __model = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'model'), 'model', '__httpwww_example_orgIoTHoneynetSchema_ioTHoneyPotType_httpwww_example_orgIoTHoneynetSchemamodel', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 81, 8), )

    
    model = property(__model.value, __model.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}location uses Python identifier location
    __location = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'location'), 'location', '__httpwww_example_orgIoTHoneynetSchema_ioTHoneyPotType_httpwww_example_orgIoTHoneynetSchemalocation', False, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 82, 16), )

    
    location = property(__location.value, __location.set, None, None)

    
    # Element {http://www.example.org/IoTHoneynetSchema}resource uses Python identifier resource
    __resource = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'resource'), 'resource', '__httpwww_example_orgIoTHoneynetSchema_ioTHoneyPotType_httpwww_example_orgIoTHoneynetSchemaresource', True, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 83, 16), )

    
    resource = property(__resource.value, __resource.set, None, None)

    
    # Attribute id inherited from {http://www.example.org/IoTHoneynetSchema}honeyPotType
    _ElementMap.update({
        __model.name() : __model,
        __location.name() : __location,
        __resource.name() : __resource
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ioTHoneyPotType = ioTHoneyPotType
Namespace.addCategoryObject('typeBinding', 'ioTHoneyPotType', ioTHoneyPotType)


# Complex type {http://www.example.org/IoTHoneynetSchema}ioTRouterType with content type ELEMENT_ONLY
class ioTRouterType (ioTHoneyPotType):
    """Complex type {http://www.example.org/IoTHoneynetSchema}ioTRouterType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ioTRouterType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 90, 2)
    _ElementMap = ioTHoneyPotType._ElementMap.copy()
    _AttributeMap = ioTHoneyPotType._AttributeMap.copy()
    # Base type is ioTHoneyPotType
    
    # Element name ({http://www.example.org/IoTHoneynetSchema}name) inherited from {http://www.example.org/IoTHoneynetSchema}honeyPotType
    
    # Element interaction_level ({http://www.example.org/IoTHoneynetSchema}interaction_level) inherited from {http://www.example.org/IoTHoneynetSchema}honeyPotType
    
    # Element if_ ({http://www.example.org/IoTHoneynetSchema}if) inherited from {http://www.example.org/IoTHoneynetSchema}honeyPotType
    
    # Element operatingSystem ({http://www.example.org/IoTHoneynetSchema}operatingSystem) inherited from {http://www.example.org/IoTHoneynetSchema}honeyPotType
    
    # Element software ({http://www.example.org/IoTHoneynetSchema}software) inherited from {http://www.example.org/IoTHoneynetSchema}honeyPotType
    
    # Element model ({http://www.example.org/IoTHoneynetSchema}model) inherited from {http://www.example.org/IoTHoneynetSchema}ioTHoneyPotType
    
    # Element location ({http://www.example.org/IoTHoneynetSchema}location) inherited from {http://www.example.org/IoTHoneynetSchema}ioTHoneyPotType
    
    # Element resource ({http://www.example.org/IoTHoneynetSchema}resource) inherited from {http://www.example.org/IoTHoneynetSchema}ioTHoneyPotType
    
    # Element {http://www.example.org/IoTHoneynetSchema}route uses Python identifier route
    __route = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'route'), 'route', '__httpwww_example_orgIoTHoneynetSchema_ioTRouterType_httpwww_example_orgIoTHoneynetSchemaroute', True, pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 94, 16), )

    
    route = property(__route.value, __route.set, None, None)

    
    # Attribute id inherited from {http://www.example.org/IoTHoneynetSchema}honeyPotType
    _ElementMap.update({
        __route.name() : __route
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ioTRouterType = ioTRouterType
Namespace.addCategoryObject('typeBinding', 'ioTRouterType', ioTRouterType)


ioTHoneyNet = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ioTHoneyNet'), ioTHoneyNetType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 7, 2))
Namespace.addCategoryObject('elementBinding', ioTHoneyNet.name().localName(), ioTHoneyNet)



ioTHoneyNetType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), pyxb.binding.datatypes.string, scope=ioTHoneyNetType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 11, 6)))

ioTHoneyNetType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'net'), netType, scope=ioTHoneyNetType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 12, 6)))

ioTHoneyNetType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'router'), ioTRouterType, scope=ioTHoneyNetType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 13, 6)))

ioTHoneyNetType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'containmentGateway'), containmentGatewayType, scope=ioTHoneyNetType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 14, 6)))

ioTHoneyNetType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ioTHoneyPot'), ioTHoneyPotType, scope=ioTHoneyNetType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 15, 6)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 12, 6))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 13, 6))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 14, 6))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 15, 6))
    counters.add(cc_3)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ioTHoneyNetType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 11, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(ioTHoneyNetType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'net')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 12, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(ioTHoneyNetType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'router')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 13, 6))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(ioTHoneyNetType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'containmentGateway')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 14, 6))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(ioTHoneyNetType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ioTHoneyPot')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 15, 6))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ioTHoneyNetType._Automaton = _BuildAutomaton()




honeyNetType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), pyxb.binding.datatypes.string, scope=honeyNetType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 21, 6)))

honeyNetType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'net'), netType, scope=honeyNetType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 22, 6)))

honeyNetType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'router'), routerType, scope=honeyNetType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 23, 6)))

honeyNetType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'containmentGateway'), containmentGatewayType, scope=honeyNetType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 24, 6)))

honeyNetType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'honeyPot'), honeyPotType, scope=honeyNetType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 25, 6)))

def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 22, 6))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 23, 6))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 24, 6))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 25, 6))
    counters.add(cc_3)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(honeyNetType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 21, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(honeyNetType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'net')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 22, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(honeyNetType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'router')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 23, 6))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(honeyNetType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'containmentGateway')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 24, 6))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(honeyNetType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'honeyPot')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 25, 6))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
honeyNetType._Automaton = _BuildAutomaton_()




netType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), pyxb.binding.datatypes.string, scope=netType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 32, 12)))

def _BuildAutomaton_2 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_2
    del _BuildAutomaton_2
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(netType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 32, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
netType._Automaton = _BuildAutomaton_2()




routerType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), pyxb.binding.datatypes.string, scope=routerType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 40, 6)))

routerType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'if'), ifType, scope=routerType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 41, 6)))

routerType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'route'), routeType, scope=routerType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 42, 6)))

routerType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'operatingSystem'), operatingSystemType, scope=routerType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 43, 6)))

def _BuildAutomaton_3 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_3
    del _BuildAutomaton_3
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 42, 6))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(routerType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 40, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(routerType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'if')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 41, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(routerType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'route')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 42, 6))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(routerType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'operatingSystem')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 43, 6))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
routerType._Automaton = _BuildAutomaton_3()




routeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'dst'), pyxb.binding.datatypes.string, scope=routeType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 50, 6)))

routeType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'gw'), pyxb.binding.datatypes.string, scope=routeType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 51, 6)))

def _BuildAutomaton_4 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_4
    del _BuildAutomaton_4
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(routeType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dst')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 50, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(routeType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'gw')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 51, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
routeType._Automaton = _BuildAutomaton_4()




containmentGatewayType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), pyxb.binding.datatypes.string, scope=containmentGatewayType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 59, 6)))

containmentGatewayType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'if'), ifType, scope=containmentGatewayType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 60, 6)))

containmentGatewayType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'operatingSystem'), operatingSystemType, scope=containmentGatewayType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 61, 6)))

def _BuildAutomaton_5 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_5
    del _BuildAutomaton_5
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(containmentGatewayType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 59, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(containmentGatewayType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'if')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 60, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(containmentGatewayType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'operatingSystem')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 61, 6))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
containmentGatewayType._Automaton = _BuildAutomaton_5()




honeyPotType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), pyxb.binding.datatypes.string, scope=honeyPotType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 68, 6)))

honeyPotType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'interaction_level'), interactionLevel, scope=honeyPotType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 69, 6)))

honeyPotType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'if'), ifType, scope=honeyPotType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 70, 6)))

honeyPotType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'operatingSystem'), operatingSystemType, scope=honeyPotType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 71, 6)))

honeyPotType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'software'), softwareType, scope=honeyPotType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 72, 6)))

def _BuildAutomaton_6 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_6
    del _BuildAutomaton_6
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(honeyPotType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 68, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(honeyPotType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'interaction_level')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 69, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(honeyPotType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'if')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 70, 6))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(honeyPotType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'operatingSystem')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 71, 6))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(honeyPotType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'software')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 72, 6))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
honeyPotType._Automaton = _BuildAutomaton_6()




physicalLocation._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'x'), pyxb.binding.datatypes.decimal, scope=physicalLocation, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 103, 6)))

physicalLocation._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'y'), pyxb.binding.datatypes.decimal, scope=physicalLocation, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 104, 6)))

def _BuildAutomaton_7 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_7
    del _BuildAutomaton_7
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 103, 6))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 104, 6))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(physicalLocation._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'x')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 103, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(physicalLocation._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'y')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 104, 6))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
physicalLocation._Automaton = _BuildAutomaton_7()




ifType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), pyxb.binding.datatypes.string, scope=ifType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 128, 6)))

ifType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'mac_addr'), pyxb.binding.datatypes.string, scope=ifType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 129, 6)))

ifType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ip'), pyxb.binding.datatypes.string, scope=ifType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 130, 6)))

def _BuildAutomaton_8 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_8
    del _BuildAutomaton_8
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ifType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 128, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ifType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'mac_addr')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 129, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ifType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ip')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 130, 6))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ifType._Automaton = _BuildAutomaton_8()




operatingSystemType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), pyxb.binding.datatypes.string, scope=operatingSystemType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 138, 6)))

operatingSystemType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'version'), pyxb.binding.datatypes.string, scope=operatingSystemType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 139, 6)))

def _BuildAutomaton_9 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_9
    del _BuildAutomaton_9
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(operatingSystemType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 138, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(operatingSystemType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'version')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 139, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
operatingSystemType._Automaton = _BuildAutomaton_9()




softwareType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'name'), pyxb.binding.datatypes.string, scope=softwareType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 145, 6)))

softwareType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'version'), pyxb.binding.datatypes.float, scope=softwareType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 146, 6)))

def _BuildAutomaton_10 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_10
    del _BuildAutomaton_10
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(softwareType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 145, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(softwareType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'version')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 146, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
softwareType._Automaton = _BuildAutomaton_10()




ioTHoneyPotType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'model'), pyxb.binding.datatypes.string, scope=ioTHoneyPotType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 81, 8)))

ioTHoneyPotType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'location'), physicalLocation, scope=ioTHoneyPotType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 82, 16)))

ioTHoneyPotType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'resource'), ioTResourceType, scope=ioTHoneyPotType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 83, 16)))

def _BuildAutomaton_11 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_11
    del _BuildAutomaton_11
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 81, 8))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 82, 16))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 83, 16))
    counters.add(cc_2)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ioTHoneyPotType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 68, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ioTHoneyPotType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'interaction_level')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 69, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ioTHoneyPotType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'if')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 70, 6))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ioTHoneyPotType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'operatingSystem')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 71, 6))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ioTHoneyPotType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'software')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 72, 6))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(ioTHoneyPotType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'model')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 81, 8))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(ioTHoneyPotType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'location')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 82, 16))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(ioTHoneyPotType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'resource')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 83, 16))
    st_7 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    transitions.append(fac.Transition(st_7, [
         ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_7._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ioTHoneyPotType._Automaton = _BuildAutomaton_11()




ioTRouterType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'route'), routeType, scope=ioTRouterType, location=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 94, 16)))

def _BuildAutomaton_12 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_12
    del _BuildAutomaton_12
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 81, 8))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 82, 16))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 83, 16))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 94, 16))
    counters.add(cc_3)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ioTRouterType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'name')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 68, 6))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ioTRouterType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'interaction_level')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 69, 6))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ioTRouterType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'if')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 70, 6))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ioTRouterType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'operatingSystem')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 71, 6))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ioTRouterType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'software')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 72, 6))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(ioTRouterType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'model')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 81, 8))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(ioTRouterType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'location')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 82, 16))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(ioTRouterType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'resource')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 83, 16))
    st_7 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(ioTRouterType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'route')), pyxb.utils.utility.Location('/app/mspl/iot-honeynet.xsd', 94, 16))
    st_8 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    transitions.append(fac.Transition(st_7, [
         ]))
    transitions.append(fac.Transition(st_8, [
         ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_8._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ioTRouterType._Automaton = _BuildAutomaton_12()

