# ./raw/mspl.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:accd3881a9e703fc92bc24ccfc1da02a0e6756aa
# Generated 2019-12-03 19:54:46.243286 by PyXB version 1.2.6 using Python 3.7.1.final.0
# Namespace http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd

from __future__ import unicode_literals
import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys
import pyxb.utils.six as _six
# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:5f8304fa-15fe-11ea-a561-0242ac180008')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.6'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# A holder for module-level binding classes so we can access them from
# inside class definitions where property names may conflict.
_module_typeBindings = pyxb.utils.utility.Object()

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes
from . import _IoTHoneyNet as _ImportedBinding__IoTHoneyNet

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.NamespaceForURI('http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd', create_if_missing=True)
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement, default_namespace=default_namespace)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, _six.text_type):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Atomic simple type: {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyActionType
class PrivacyActionType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PrivacyActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 516, 1)
    _Documentation = None
PrivacyActionType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=PrivacyActionType, enum_prefix=None)
PrivacyActionType.DATA = PrivacyActionType._CF_enumeration.addEnumeration(unicode_value='DATA', tag='DATA')
PrivacyActionType._InitializeFacetMap(PrivacyActionType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'PrivacyActionType', PrivacyActionType)
_module_typeBindings.PrivacyActionType = PrivacyActionType

# Atomic simple type: {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataAggregationActionType
class DataAggregationActionType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DataAggregationActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 554, 1)
    _Documentation = None
DataAggregationActionType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=DataAggregationActionType, enum_prefix=None)
DataAggregationActionType.SIMPLE_AGGREGATION = DataAggregationActionType._CF_enumeration.addEnumeration(unicode_value='SIMPLE_AGGREGATION', tag='SIMPLE_AGGREGATION')
DataAggregationActionType._InitializeFacetMap(DataAggregationActionType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'DataAggregationActionType', DataAggregationActionType)
_module_typeBindings.DataAggregationActionType = DataAggregationActionType

# Atomic simple type: {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AnonymityActionType
class AnonymityActionType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AnonymityActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 586, 1)
    _Documentation = None
AnonymityActionType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=AnonymityActionType, enum_prefix=None)
AnonymityActionType.SENDER = AnonymityActionType._CF_enumeration.addEnumeration(unicode_value='SENDER', tag='SENDER')
AnonymityActionType.RECEIVER = AnonymityActionType._CF_enumeration.addEnumeration(unicode_value='RECEIVER', tag='RECEIVER')
AnonymityActionType.COMMUNICATION = AnonymityActionType._CF_enumeration.addEnumeration(unicode_value='COMMUNICATION', tag='COMMUNICATION')
AnonymityActionType._InitializeFacetMap(AnonymityActionType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'AnonymityActionType', AnonymityActionType)
_module_typeBindings.AnonymityActionType = AnonymityActionType

# Atomic simple type: {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}MonitoringActionType
class MonitoringActionType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MonitoringActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 683, 1)
    _Documentation = None
MonitoringActionType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=MonitoringActionType, enum_prefix=None)
MonitoringActionType.ALERT = MonitoringActionType._CF_enumeration.addEnumeration(unicode_value='ALERT', tag='ALERT')
MonitoringActionType.SECURITY_ANALYSIS = MonitoringActionType._CF_enumeration.addEnumeration(unicode_value='SECURITY_ANALYSIS', tag='SECURITY_ANALYSIS')
MonitoringActionType.ENABLE_SESS_STATS = MonitoringActionType._CF_enumeration.addEnumeration(unicode_value='ENABLE_SESS_STATS', tag='ENABLE_SESS_STATS')
MonitoringActionType.ENABLE_NO_SESS_STATS = MonitoringActionType._CF_enumeration.addEnumeration(unicode_value='ENABLE_NO_SESS_STATS', tag='ENABLE_NO_SESS_STATS')
MonitoringActionType.BEHAVIORAL = MonitoringActionType._CF_enumeration.addEnumeration(unicode_value='BEHAVIORAL', tag='BEHAVIORAL')
MonitoringActionType._InitializeFacetMap(MonitoringActionType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'MonitoringActionType', MonitoringActionType)
_module_typeBindings.MonitoringActionType = MonitoringActionType

# Atomic simple type: {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}NetworkSlicingActionType
class NetworkSlicingActionType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'NetworkSlicingActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 728, 1)
    _Documentation = None
NetworkSlicingActionType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=NetworkSlicingActionType, enum_prefix=None)
NetworkSlicingActionType.QUARANTINE = NetworkSlicingActionType._CF_enumeration.addEnumeration(unicode_value='QUARANTINE', tag='QUARANTINE')
NetworkSlicingActionType.DISCONNECT = NetworkSlicingActionType._CF_enumeration.addEnumeration(unicode_value='DISCONNECT', tag='DISCONNECT')
NetworkSlicingActionType._InitializeFacetMap(NetworkSlicingActionType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'NetworkSlicingActionType', NetworkSlicingActionType)
_module_typeBindings.NetworkSlicingActionType = NetworkSlicingActionType

# Atomic simple type: {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficDivertActionType
class TrafficDivertActionType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TrafficDivertActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 976, 1)
    _Documentation = None
TrafficDivertActionType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=TrafficDivertActionType, enum_prefix=None)
TrafficDivertActionType.FORWARD = TrafficDivertActionType._CF_enumeration.addEnumeration(unicode_value='FORWARD', tag='FORWARD')
TrafficDivertActionType.MIRRORING = TrafficDivertActionType._CF_enumeration.addEnumeration(unicode_value='MIRRORING', tag='MIRRORING')
TrafficDivertActionType.NAT = TrafficDivertActionType._CF_enumeration.addEnumeration(unicode_value='NAT', tag='NAT')
TrafficDivertActionType.ENCAPSULATION = TrafficDivertActionType._CF_enumeration.addEnumeration(unicode_value='ENCAPSULATION', tag='ENCAPSULATION')
TrafficDivertActionType._InitializeFacetMap(TrafficDivertActionType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'TrafficDivertActionType', TrafficDivertActionType)
_module_typeBindings.TrafficDivertActionType = TrafficDivertActionType

# Atomic simple type: {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PowerMgmtActionType
class PowerMgmtActionType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PowerMgmtActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1158, 1)
    _Documentation = None
PowerMgmtActionType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=PowerMgmtActionType, enum_prefix=None)
PowerMgmtActionType.OFF = PowerMgmtActionType._CF_enumeration.addEnumeration(unicode_value='OFF', tag='OFF')
PowerMgmtActionType.RESET = PowerMgmtActionType._CF_enumeration.addEnumeration(unicode_value='RESET', tag='RESET')
PowerMgmtActionType._InitializeFacetMap(PowerMgmtActionType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'PowerMgmtActionType', PowerMgmtActionType)
_module_typeBindings.PowerMgmtActionType = PowerMgmtActionType

# Atomic simple type: {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}VIoTHoneyNetActionType
class VIoTHoneyNetActionType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VIoTHoneyNetActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1176, 1)
    _Documentation = None
VIoTHoneyNetActionType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=VIoTHoneyNetActionType, enum_prefix=None)
VIoTHoneyNetActionType.DEPLOY = VIoTHoneyNetActionType._CF_enumeration.addEnumeration(unicode_value='DEPLOY', tag='DEPLOY')
VIoTHoneyNetActionType.RESET = VIoTHoneyNetActionType._CF_enumeration.addEnumeration(unicode_value='RESET', tag='RESET')
VIoTHoneyNetActionType.REMOVE = VIoTHoneyNetActionType._CF_enumeration.addEnumeration(unicode_value='REMOVE', tag='REMOVE')
VIoTHoneyNetActionType._InitializeFacetMap(VIoTHoneyNetActionType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'VIoTHoneyNetActionType', VIoTHoneyNetActionType)
_module_typeBindings.VIoTHoneyNetActionType = VIoTHoneyNetActionType

# Atomic simple type: {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthorizationActionType
class AuthorizationActionType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AuthorizationActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1209, 1)
    _Documentation = None
AuthorizationActionType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=AuthorizationActionType, enum_prefix=None)
AuthorizationActionType.ALLOW = AuthorizationActionType._CF_enumeration.addEnumeration(unicode_value='ALLOW', tag='ALLOW')
AuthorizationActionType.DENY = AuthorizationActionType._CF_enumeration.addEnumeration(unicode_value='DENY', tag='DENY')
AuthorizationActionType.PUSH = AuthorizationActionType._CF_enumeration.addEnumeration(unicode_value='PUSH', tag='PUSH')
AuthorizationActionType._InitializeFacetMap(AuthorizationActionType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'AuthorizationActionType', AuthorizationActionType)
_module_typeBindings.AuthorizationActionType = AuthorizationActionType

# Atomic simple type: [anonymous]
class STD_ANON (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1265, 4)
    _Documentation = None
STD_ANON._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON, value=pyxb.binding.datatypes.int(0))
STD_ANON._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON, value=pyxb.binding.datatypes.int(2))
STD_ANON._InitializeFacetMap(STD_ANON._CF_minInclusive,
   STD_ANON._CF_maxInclusive)
_module_typeBindings.STD_ANON = STD_ANON

# Atomic simple type: [anonymous]
class STD_ANON_ (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1274, 4)
    _Documentation = None
STD_ANON_._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_, value=pyxb.binding.datatypes.int(0))
STD_ANON_._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_, value=pyxb.binding.datatypes.int(8))
STD_ANON_._InitializeFacetMap(STD_ANON_._CF_minInclusive,
   STD_ANON_._CF_maxInclusive)
_module_typeBindings.STD_ANON_ = STD_ANON_

# Atomic simple type: [anonymous]
class STD_ANON_2 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1282, 4)
    _Documentation = None
STD_ANON_2._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_2, value=pyxb.binding.datatypes.int(0))
STD_ANON_2._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_2, value=pyxb.binding.datatypes.int(2))
STD_ANON_2._InitializeFacetMap(STD_ANON_2._CF_minInclusive,
   STD_ANON_2._CF_maxInclusive)
_module_typeBindings.STD_ANON_2 = STD_ANON_2

# Atomic simple type: [anonymous]
class STD_ANON_3 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1308, 3)
    _Documentation = None
STD_ANON_3._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_3, value=pyxb.binding.datatypes.int(0))
STD_ANON_3._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_3, value=pyxb.binding.datatypes.int(4))
STD_ANON_3._InitializeFacetMap(STD_ANON_3._CF_minInclusive,
   STD_ANON_3._CF_maxInclusive)
_module_typeBindings.STD_ANON_3 = STD_ANON_3

# Atomic simple type: [anonymous]
class STD_ANON_4 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1317, 3)
    _Documentation = None
STD_ANON_4._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_4, value=pyxb.binding.datatypes.int(0))
STD_ANON_4._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_4, value=pyxb.binding.datatypes.int(4))
STD_ANON_4._InitializeFacetMap(STD_ANON_4._CF_minInclusive,
   STD_ANON_4._CF_maxInclusive)
_module_typeBindings.STD_ANON_4 = STD_ANON_4

# Atomic simple type: [anonymous]
class STD_ANON_5 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1325, 3)
    _Documentation = None
STD_ANON_5._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_5, value=pyxb.binding.datatypes.int(0))
STD_ANON_5._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_5, value=pyxb.binding.datatypes.int(4))
STD_ANON_5._InitializeFacetMap(STD_ANON_5._CF_minInclusive,
   STD_ANON_5._CF_maxInclusive)
_module_typeBindings.STD_ANON_5 = STD_ANON_5

# Atomic simple type: [anonymous]
class STD_ANON_6 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1333, 3)
    _Documentation = None
STD_ANON_6._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_6, value=pyxb.binding.datatypes.int(0))
STD_ANON_6._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_6, value=pyxb.binding.datatypes.int(4))
STD_ANON_6._InitializeFacetMap(STD_ANON_6._CF_minInclusive,
   STD_ANON_6._CF_maxInclusive)
_module_typeBindings.STD_ANON_6 = STD_ANON_6

# Atomic simple type: [anonymous]
class STD_ANON_7 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1344, 3)
    _Documentation = None
STD_ANON_7._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_7, value=pyxb.binding.datatypes.int(0))
STD_ANON_7._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_7, value=pyxb.binding.datatypes.int(4))
STD_ANON_7._InitializeFacetMap(STD_ANON_7._CF_minInclusive,
   STD_ANON_7._CF_maxInclusive)
_module_typeBindings.STD_ANON_7 = STD_ANON_7

# Atomic simple type: [anonymous]
class STD_ANON_8 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1352, 3)
    _Documentation = None
STD_ANON_8._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_8, value=pyxb.binding.datatypes.int(0))
STD_ANON_8._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_8, value=pyxb.binding.datatypes.int(4))
STD_ANON_8._InitializeFacetMap(STD_ANON_8._CF_minInclusive,
   STD_ANON_8._CF_maxInclusive)
_module_typeBindings.STD_ANON_8 = STD_ANON_8

# Atomic simple type: [anonymous]
class STD_ANON_9 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1360, 3)
    _Documentation = None
STD_ANON_9._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_9, value=pyxb.binding.datatypes.int(0))
STD_ANON_9._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_9, value=pyxb.binding.datatypes.int(4))
STD_ANON_9._InitializeFacetMap(STD_ANON_9._CF_minInclusive,
   STD_ANON_9._CF_maxInclusive)
_module_typeBindings.STD_ANON_9 = STD_ANON_9

# Atomic simple type: [anonymous]
class STD_ANON_10 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1369, 3)
    _Documentation = None
STD_ANON_10._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_10, value=pyxb.binding.datatypes.int(0))
STD_ANON_10._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_10, value=pyxb.binding.datatypes.int(4))
STD_ANON_10._InitializeFacetMap(STD_ANON_10._CF_minInclusive,
   STD_ANON_10._CF_maxInclusive)
_module_typeBindings.STD_ANON_10 = STD_ANON_10

# Atomic simple type: [anonymous]
class STD_ANON_11 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1378, 3)
    _Documentation = None
STD_ANON_11._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_11, value=pyxb.binding.datatypes.int(0))
STD_ANON_11._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_11, value=pyxb.binding.datatypes.int(4))
STD_ANON_11._InitializeFacetMap(STD_ANON_11._CF_minInclusive,
   STD_ANON_11._CF_maxInclusive)
_module_typeBindings.STD_ANON_11 = STD_ANON_11

# Atomic simple type: [anonymous]
class STD_ANON_12 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1387, 3)
    _Documentation = None
STD_ANON_12._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_12, value=pyxb.binding.datatypes.int(0))
STD_ANON_12._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_12, value=pyxb.binding.datatypes.int(4))
STD_ANON_12._InitializeFacetMap(STD_ANON_12._CF_minInclusive,
   STD_ANON_12._CF_maxInclusive)
_module_typeBindings.STD_ANON_12 = STD_ANON_12

# Atomic simple type: [anonymous]
class STD_ANON_13 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1395, 3)
    _Documentation = None
STD_ANON_13._CF_minExclusive = pyxb.binding.facets.CF_minExclusive(value_datatype=pyxb.binding.datatypes.int, value=pyxb.binding.datatypes.long(0))
STD_ANON_13._CF_maxExclusive = pyxb.binding.facets.CF_maxExclusive(value_datatype=pyxb.binding.datatypes.int, value=pyxb.binding.datatypes.long(4))
STD_ANON_13._InitializeFacetMap(STD_ANON_13._CF_minExclusive,
   STD_ANON_13._CF_maxExclusive)
_module_typeBindings.STD_ANON_13 = STD_ANON_13

# Atomic simple type: [anonymous]
class STD_ANON_14 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1406, 3)
    _Documentation = None
STD_ANON_14._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_14, value=pyxb.binding.datatypes.int(-3))
STD_ANON_14._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_14, value=pyxb.binding.datatypes.int(2))
STD_ANON_14._InitializeFacetMap(STD_ANON_14._CF_minInclusive,
   STD_ANON_14._CF_maxInclusive)
_module_typeBindings.STD_ANON_14 = STD_ANON_14

# Atomic simple type: [anonymous]
class STD_ANON_15 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1414, 3)
    _Documentation = None
STD_ANON_15._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_15, value=pyxb.binding.datatypes.int(-3))
STD_ANON_15._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_15, value=pyxb.binding.datatypes.int(2))
STD_ANON_15._InitializeFacetMap(STD_ANON_15._CF_minInclusive,
   STD_ANON_15._CF_maxInclusive)
_module_typeBindings.STD_ANON_15 = STD_ANON_15

# Atomic simple type: [anonymous]
class STD_ANON_16 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1422, 3)
    _Documentation = None
STD_ANON_16._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_16, value=pyxb.binding.datatypes.int(-3))
STD_ANON_16._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_16, value=pyxb.binding.datatypes.int(2))
STD_ANON_16._InitializeFacetMap(STD_ANON_16._CF_minInclusive,
   STD_ANON_16._CF_maxInclusive)
_module_typeBindings.STD_ANON_16 = STD_ANON_16

# Atomic simple type: [anonymous]
class STD_ANON_17 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1430, 3)
    _Documentation = None
STD_ANON_17._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_17, value=pyxb.binding.datatypes.int(-3))
STD_ANON_17._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_17, value=pyxb.binding.datatypes.int(2))
STD_ANON_17._InitializeFacetMap(STD_ANON_17._CF_minInclusive,
   STD_ANON_17._CF_maxInclusive)
_module_typeBindings.STD_ANON_17 = STD_ANON_17

# Atomic simple type: [anonymous]
class STD_ANON_18 (pyxb.binding.datatypes.int):

    """An atomic simple type."""

    _ExpandedName = None
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1438, 3)
    _Documentation = None
STD_ANON_18._CF_minInclusive = pyxb.binding.facets.CF_minInclusive(value_datatype=STD_ANON_18, value=pyxb.binding.datatypes.int(-3))
STD_ANON_18._CF_maxInclusive = pyxb.binding.facets.CF_maxInclusive(value_datatype=STD_ANON_18, value=pyxb.binding.datatypes.int(2))
STD_ANON_18._InitializeFacetMap(STD_ANON_18._CF_minInclusive,
   STD_ANON_18._CF_maxInclusive)
_module_typeBindings.STD_ANON_18 = STD_ANON_18

# Atomic simple type: {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}CapabilityType
class CapabilityType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CapabilityType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1680, 1)
    _Documentation = None
CapabilityType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=CapabilityType, enum_prefix=None)
CapabilityType.Filtering_L4 = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Filtering_L4', tag='Filtering_L4')
CapabilityType.Filtering_L3 = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Filtering_L3', tag='Filtering_L3')
CapabilityType.Filtering_L7 = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Filtering_L7', tag='Filtering_L7')
CapabilityType.Timing = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Timing', tag='Timing')
CapabilityType.TrafficInspection_L7 = CapabilityType._CF_enumeration.addEnumeration(unicode_value='TrafficInspection_L7', tag='TrafficInspection_L7')
CapabilityType.Filtering_3G4G = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Filtering_3G4G', tag='Filtering_3G4G')
CapabilityType.Filtering_DNS = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Filtering_DNS', tag='Filtering_DNS')
CapabilityType.Offline_malware_analysis = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Offline_malware_analysis', tag='Offline_malware_analysis')
CapabilityType.Online_SPAM_analysis = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Online_SPAM_analysis', tag='Online_SPAM_analysis')
CapabilityType.Online_antivirus_analysis = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Online_antivirus_analysis', tag='Online_antivirus_analysis')
CapabilityType.Network_traffic_analysis = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Network_traffic_analysis', tag='Network_traffic_analysis')
CapabilityType.DDos_attack_protection = CapabilityType._CF_enumeration.addEnumeration(unicode_value='DDos_attack_protection', tag='DDos_attack_protection')
CapabilityType.lawful_interception = CapabilityType._CF_enumeration.addEnumeration(unicode_value='lawful_interception', tag='lawful_interception')
CapabilityType.Count_L4Connection = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Count_L4Connection', tag='Count_L4Connection')
CapabilityType.Count_DNS = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Count_DNS', tag='Count_DNS')
CapabilityType.Protection_confidentiality = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Protection_confidentiality', tag='Protection_confidentiality')
CapabilityType.Protection_integrity = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Protection_integrity', tag='Protection_integrity')
CapabilityType.Compress = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Compress', tag='Compress')
CapabilityType.Logging = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Logging', tag='Logging')
CapabilityType.AuthoriseAccess_resurce = CapabilityType._CF_enumeration.addEnumeration(unicode_value='AuthoriseAccess_resurce', tag='AuthoriseAccess_resurce')
CapabilityType.Reduce_bandwidth = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Reduce_bandwidth', tag='Reduce_bandwidth')
CapabilityType.Online_security_analyzer = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Online_security_analyzer', tag='Online_security_analyzer')
CapabilityType.Basic_parental_control = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Basic_parental_control', tag='Basic_parental_control')
CapabilityType.Advanced_parental_control = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Advanced_parental_control', tag='Advanced_parental_control')
CapabilityType.IPSec_protocol = CapabilityType._CF_enumeration.addEnumeration(unicode_value='IPSec_protocol', tag='IPSec_protocol')
CapabilityType.TLS_protocol = CapabilityType._CF_enumeration.addEnumeration(unicode_value='TLS_protocol', tag='TLS_protocol')
CapabilityType.reencrypt = CapabilityType._CF_enumeration.addEnumeration(unicode_value='reencrypt', tag='reencrypt')
CapabilityType.antiPhishing = CapabilityType._CF_enumeration.addEnumeration(unicode_value='antiPhishing', tag='antiPhishing')
CapabilityType.Network_Anonymity = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Network_Anonymity', tag='Network_Anonymity')
CapabilityType.IoT_control = CapabilityType._CF_enumeration.addEnumeration(unicode_value='IoT_control', tag='IoT_control')
CapabilityType.DTLS_protocol = CapabilityType._CF_enumeration.addEnumeration(unicode_value='DTLS_protocol', tag='DTLS_protocol')
CapabilityType.Authentication = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Authentication', tag='Authentication')
CapabilityType.Traffic_Divert = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Traffic_Divert', tag='Traffic_Divert')
CapabilityType.IoT_honeynet = CapabilityType._CF_enumeration.addEnumeration(unicode_value='IoT_honeynet', tag='IoT_honeynet')
CapabilityType.Privacy = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Privacy', tag='Privacy')
CapabilityType.QoS = CapabilityType._CF_enumeration.addEnumeration(unicode_value='QoS', tag='QoS')
CapabilityType.Data_aggregation = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Data_aggregation', tag='Data_aggregation')
CapabilityType.Traffic_MIX = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Traffic_MIX', tag='Traffic_MIX')
CapabilityType.Onion_Routing = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Onion_Routing', tag='Onion_Routing')
CapabilityType.Data_analysis = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Data_analysis', tag='Data_analysis')
CapabilityType.Network_slicing = CapabilityType._CF_enumeration.addEnumeration(unicode_value='Network_slicing', tag='Network_slicing')
CapabilityType._InitializeFacetMap(CapabilityType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'CapabilityType', CapabilityType)
_module_typeBindings.CapabilityType = CapabilityType

# Atomic simple type: {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}LevelType
class LevelType (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LevelType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1746, 1)
    _Documentation = None
LevelType._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=LevelType, enum_prefix=None)
LevelType.child = LevelType._CF_enumeration.addEnumeration(unicode_value='child', tag='child')
LevelType.adolescent = LevelType._CF_enumeration.addEnumeration(unicode_value='adolescent', tag='adolescent')
LevelType.pgr = LevelType._CF_enumeration.addEnumeration(unicode_value='pgr', tag='pgr')
LevelType.universal = LevelType._CF_enumeration.addEnumeration(unicode_value='universal', tag='universal')
LevelType._InitializeFacetMap(LevelType._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'LevelType', LevelType)
_module_typeBindings.LevelType = LevelType

# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ITResourceOrchestrationType with content type ELEMENT_ONLY
class ITResourceOrchestrationType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ITResourceOrchestrationType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ITResourceOrchestrationType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 13, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ITResource uses Python identifier ITResource
    __ITResource = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ITResource'), 'ITResource', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ITResourceOrchestrationType_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdITResource', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 15, 3), )

    
    ITResource = property(__ITResource.value, __ITResource.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}priority uses Python identifier priority
    __priority = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'priority'), 'priority', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ITResourceOrchestrationType_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpriority', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 16, 3), )

    
    priority = property(__priority.value, __priority.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}dependencies uses Python identifier dependencies
    __dependencies = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'dependencies'), 'dependencies', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ITResourceOrchestrationType_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsddependencies', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 17, 3), )

    
    dependencies = property(__dependencies.value, __dependencies.set, None, None)

    
    # Attribute id uses Python identifier id
    __id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'id'), 'id', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ITResourceOrchestrationType_id', pyxb.binding.datatypes.string)
    __id._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 20, 2)
    __id._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 20, 2)
    
    id = property(__id.value, __id.set, None, None)

    _ElementMap.update({
        __ITResource.name() : __ITResource,
        __priority.name() : __priority,
        __dependencies.name() : __dependencies
    })
    _AttributeMap.update({
        __id.name() : __id
    })
_module_typeBindings.ITResourceOrchestrationType = ITResourceOrchestrationType
Namespace.addCategoryObject('typeBinding', 'ITResourceOrchestrationType', ITResourceOrchestrationType)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ITResourceType with content type ELEMENT_ONLY
class ITResourceType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ITResourceType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ITResourceType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 23, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}configuration uses Python identifier configuration
    __configuration = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'configuration'), 'configuration', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ITResourceType_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdconfiguration', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 25, 3), )

    
    configuration = property(__configuration.value, __configuration.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}priority uses Python identifier priority
    __priority = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'priority'), 'priority', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ITResourceType_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpriority', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 27, 3), )

    
    priority = property(__priority.value, __priority.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}dependencies uses Python identifier dependencies
    __dependencies = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'dependencies'), 'dependencies', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ITResourceType_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsddependencies', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 28, 3), )

    
    dependencies = property(__dependencies.value, __dependencies.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}enablerCandidates uses Python identifier enablerCandidates
    __enablerCandidates = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'enablerCandidates'), 'enablerCandidates', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ITResourceType_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdenablerCandidates', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 29, 3), )

    
    enablerCandidates = property(__enablerCandidates.value, __enablerCandidates.set, None, None)

    
    # Attribute id uses Python identifier id
    __id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'id'), 'id', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ITResourceType_id', pyxb.binding.datatypes.string)
    __id._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 32, 2)
    __id._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 32, 2)
    
    id = property(__id.value, __id.set, None, None)

    
    # Attribute orchestrationID uses Python identifier orchestrationID
    __orchestrationID = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'orchestrationID'), 'orchestrationID', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ITResourceType_orchestrationID', pyxb.binding.datatypes.string)
    __orchestrationID._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 33, 2)
    __orchestrationID._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 33, 2)
    
    orchestrationID = property(__orchestrationID.value, __orchestrationID.set, None, None)

    _ElementMap.update({
        __configuration.name() : __configuration,
        __priority.name() : __priority,
        __dependencies.name() : __dependencies,
        __enablerCandidates.name() : __enablerCandidates
    })
    _AttributeMap.update({
        __id.name() : __id,
        __orchestrationID.name() : __orchestrationID
    })
_module_typeBindings.ITResourceType = ITResourceType
Namespace.addCategoryObject('typeBinding', 'ITResourceType', ITResourceType)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Dependencies with content type ELEMENT_ONLY
class Dependencies (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Dependencies with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Dependencies')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 48, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}dependency uses Python identifier dependency
    __dependency = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'dependency'), 'dependency', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Dependencies_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsddependency', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 50, 3), )

    
    dependency = property(__dependency.value, __dependency.set, None, None)

    _ElementMap.update({
        __dependency.name() : __dependency
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Dependencies = Dependencies
Namespace.addCategoryObject('typeBinding', 'Dependencies', Dependencies)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Dependency with content type EMPTY
class Dependency (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Dependency with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Dependency')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 55, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Dependency = Dependency
Namespace.addCategoryObject('typeBinding', 'Dependency', Dependency)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EnablerCandidates with content type ELEMENT_ONLY
class EnablerCandidates (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EnablerCandidates with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EnablerCandidates')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 92, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}enabler uses Python identifier enabler
    __enabler = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'enabler'), 'enabler', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_EnablerCandidates_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdenabler', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 94, 3), )

    
    enabler = property(__enabler.value, __enabler.set, None, None)

    _ElementMap.update({
        __enabler.name() : __enabler
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.EnablerCandidates = EnablerCandidates
Namespace.addCategoryObject('typeBinding', 'EnablerCandidates', EnablerCandidates)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Configuration with content type ELEMENT_ONLY
class Configuration (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Configuration with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Configuration')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 102, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}capability uses Python identifier capability
    __capability = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'capability'), 'capability', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Configuration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdcapability', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 104, 3), )

    
    capability = property(__capability.value, __capability.set, None, None)

    _ElementMap.update({
        __capability.name() : __capability
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Configuration = Configuration
Namespace.addCategoryObject('typeBinding', 'Configuration', Configuration)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability with content type ELEMENT_ONLY
class Capability (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Capability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 108, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name uses Python identifier Name
    __Name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Name'), 'Name', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Capability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdName', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3), )

    
    Name = property(__Name.value, __Name.set, None, None)

    _ElementMap.update({
        __Name.name() : __Name
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Capability = Capability
Namespace.addCategoryObject('typeBinding', 'Capability', Capability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationAction with content type EMPTY
class ConfigurationAction (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationAction with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ConfigurationAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 351, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ConfigurationAction = ConfigurationAction
Namespace.addCategoryObject('typeBinding', 'ConfigurationAction', ConfigurationAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationRule with content type ELEMENT_ONLY
class ConfigurationRule (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationRule with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ConfigurationRule')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 353, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}configurationRuleAction uses Python identifier configurationRuleAction
    __configurationRuleAction = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'configurationRuleAction'), 'configurationRuleAction', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ConfigurationRule_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdconfigurationRuleAction', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 355, 3), )

    
    configurationRuleAction = property(__configurationRuleAction.value, __configurationRuleAction.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}configurationCondition uses Python identifier configurationCondition
    __configurationCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'configurationCondition'), 'configurationCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ConfigurationRule_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdconfigurationCondition', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 357, 3), )

    
    configurationCondition = property(__configurationCondition.value, __configurationCondition.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}externalData uses Python identifier externalData
    __externalData = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'externalData'), 'externalData', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ConfigurationRule_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdexternalData', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 359, 3), )

    
    externalData = property(__externalData.value, __externalData.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name uses Python identifier Name
    __Name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Name'), 'Name', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ConfigurationRule_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdName', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 360, 3), )

    
    Name = property(__Name.value, __Name.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF uses Python identifier isCNF
    __isCNF = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'isCNF'), 'isCNF', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ConfigurationRule_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdisCNF', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 361, 3), )

    
    isCNF = property(__isCNF.value, __isCNF.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}HSPL uses Python identifier HSPL
    __HSPL = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'HSPL'), 'HSPL', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ConfigurationRule_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdHSPL', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 362, 3), )

    
    HSPL = property(__HSPL.value, __HSPL.set, None, None)

    _ElementMap.update({
        __configurationRuleAction.name() : __configurationRuleAction,
        __configurationCondition.name() : __configurationCondition,
        __externalData.name() : __externalData,
        __Name.name() : __Name,
        __isCNF.name() : __isCNF,
        __HSPL.name() : __HSPL
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ConfigurationRule = ConfigurationRule
Namespace.addCategoryObject('typeBinding', 'ConfigurationRule', ConfigurationRule)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition with content type ELEMENT_ONLY
class ConfigurationCondition (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ConfigurationCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 367, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF uses Python identifier isCNF
    __isCNF = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'isCNF'), 'isCNF', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ConfigurationCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdisCNF', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3), )

    
    isCNF = property(__isCNF.value, __isCNF.set, None, None)

    _ElementMap.update({
        __isCNF.name() : __isCNF
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ConfigurationCondition = ConfigurationCondition
Namespace.addCategoryObject('typeBinding', 'ConfigurationCondition', ConfigurationCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ExternalData with content type EMPTY
class ExternalData (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ExternalData with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ExternalData')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 374, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ExternalData = ExternalData
Namespace.addCategoryObject('typeBinding', 'ExternalData', ExternalData)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ResolutionStrategy with content type ELEMENT_ONLY
class ResolutionStrategy (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ResolutionStrategy with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ResolutionStrategy')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 386, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}externalData uses Python identifier externalData
    __externalData = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'externalData'), 'externalData', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ResolutionStrategy_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdexternalData', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3), )

    
    externalData = property(__externalData.value, __externalData.set, None, None)

    _ElementMap.update({
        __externalData.name() : __externalData
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ResolutionStrategy = ResolutionStrategy
Namespace.addCategoryObject('typeBinding', 'ResolutionStrategy', ResolutionStrategy)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyMethod with content type EMPTY
class PrivacyMethod (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyMethod with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PrivacyMethod')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 475, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PrivacyMethod = PrivacyMethod
Namespace.addCategoryObject('typeBinding', 'PrivacyMethod', PrivacyMethod)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}KeyValue with content type ELEMENT_ONLY
class KeyValue (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}KeyValue with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'KeyValue')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 508, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}key uses Python identifier key
    __key = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'key'), 'key', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_KeyValue_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdkey', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 510, 3), )

    
    key = property(__key.value, __key.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value'), 'value_', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_KeyValue_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdvalue', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 511, 3), )

    
    value_ = property(__value.value, __value.set, None, None)

    _ElementMap.update({
        __key.name() : __key,
        __value.name() : __value
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.KeyValue = KeyValue
Namespace.addCategoryObject('typeBinding', 'KeyValue', KeyValue)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}SignatureList with content type ELEMENT_ONLY
class SignatureList (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}SignatureList with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SignatureList')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 659, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}signature uses Python identifier signature
    __signature = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'signature'), 'signature', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_SignatureList_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdsignature', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 661, 3), )

    
    signature = property(__signature.value, __signature.set, None, None)

    _ElementMap.update({
        __signature.name() : __signature
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.SignatureList = SignatureList
Namespace.addCategoryObject('typeBinding', 'SignatureList', SignatureList)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}NetworkSlicingConditionParameters with content type ELEMENT_ONLY
class NetworkSlicingConditionParameters (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}NetworkSlicingConditionParameters with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'NetworkSlicingConditionParameters')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 719, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}SourceMAC uses Python identifier SourceMAC
    __SourceMAC = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'SourceMAC'), 'SourceMAC', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_NetworkSlicingConditionParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdSourceMAC', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 721, 3), )

    
    SourceMAC = property(__SourceMAC.value, __SourceMAC.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DestinationMAC uses Python identifier DestinationMAC
    __DestinationMAC = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'DestinationMAC'), 'DestinationMAC', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_NetworkSlicingConditionParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdDestinationMAC', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 722, 3), )

    
    DestinationMAC = property(__DestinationMAC.value, __DestinationMAC.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}SliceID uses Python identifier SliceID
    __SliceID = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'SliceID'), 'SliceID', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_NetworkSlicingConditionParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdSliceID', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 723, 3), )

    
    SliceID = property(__SliceID.value, __SliceID.set, None, None)

    _ElementMap.update({
        __SourceMAC.name() : __SourceMAC,
        __DestinationMAC.name() : __DestinationMAC,
        __SliceID.name() : __SliceID
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.NetworkSlicingConditionParameters = NetworkSlicingConditionParameters
Namespace.addCategoryObject('typeBinding', 'NetworkSlicingConditionParameters', NetworkSlicingConditionParameters)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PacketFilterCondition with content type ELEMENT_ONLY
class PacketFilterCondition (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PacketFilterCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PacketFilterCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 779, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}SourceMAC uses Python identifier SourceMAC
    __SourceMAC = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'SourceMAC'), 'SourceMAC', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PacketFilterCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdSourceMAC', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 781, 3), )

    
    SourceMAC = property(__SourceMAC.value, __SourceMAC.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DestinationMAC uses Python identifier DestinationMAC
    __DestinationMAC = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'DestinationMAC'), 'DestinationMAC', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PacketFilterCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdDestinationMAC', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 782, 6), )

    
    DestinationMAC = property(__DestinationMAC.value, __DestinationMAC.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}SourceAddress uses Python identifier SourceAddress
    __SourceAddress = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'SourceAddress'), 'SourceAddress', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PacketFilterCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdSourceAddress', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 783, 3), )

    
    SourceAddress = property(__SourceAddress.value, __SourceAddress.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DestinationAddress uses Python identifier DestinationAddress
    __DestinationAddress = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'DestinationAddress'), 'DestinationAddress', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PacketFilterCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdDestinationAddress', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 784, 3), )

    
    DestinationAddress = property(__DestinationAddress.value, __DestinationAddress.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}SourcePort uses Python identifier SourcePort
    __SourcePort = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'SourcePort'), 'SourcePort', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PacketFilterCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdSourcePort', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 785, 3), )

    
    SourcePort = property(__SourcePort.value, __SourcePort.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DestinationPort uses Python identifier DestinationPort
    __DestinationPort = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'DestinationPort'), 'DestinationPort', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PacketFilterCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdDestinationPort', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 786, 3), )

    
    DestinationPort = property(__DestinationPort.value, __DestinationPort.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}direction uses Python identifier direction
    __direction = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'direction'), 'direction', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PacketFilterCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsddirection', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 787, 3), )

    
    direction = property(__direction.value, __direction.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Interface uses Python identifier Interface
    __Interface = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Interface'), 'Interface', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PacketFilterCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdInterface', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 788, 3), )

    
    Interface = property(__Interface.value, __Interface.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ProtocolType uses Python identifier ProtocolType
    __ProtocolType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ProtocolType'), 'ProtocolType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PacketFilterCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdProtocolType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 789, 3), )

    
    ProtocolType = property(__ProtocolType.value, __ProtocolType.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}State uses Python identifier State
    __State = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'State'), 'State', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PacketFilterCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdState', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 790, 3), )

    
    State = property(__State.value, __State.set, None, None)

    _ElementMap.update({
        __SourceMAC.name() : __SourceMAC,
        __DestinationMAC.name() : __DestinationMAC,
        __SourceAddress.name() : __SourceAddress,
        __DestinationAddress.name() : __DestinationAddress,
        __SourcePort.name() : __SourcePort,
        __DestinationPort.name() : __DestinationPort,
        __direction.name() : __direction,
        __Interface.name() : __Interface,
        __ProtocolType.name() : __ProtocolType,
        __State.name() : __State
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PacketFilterCondition = PacketFilterCondition
Namespace.addCategoryObject('typeBinding', 'PacketFilterCondition', PacketFilterCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}QoSCondition with content type ELEMENT_ONLY
class QoSCondition (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}QoSCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'QoSCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 796, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}profile uses Python identifier profile
    __profile = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'profile'), 'profile', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_QoSCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdprofile', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 798, 3), )

    
    profile = property(__profile.value, __profile.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}throughput uses Python identifier throughput
    __throughput = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'throughput'), 'throughput', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_QoSCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdthroughput', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 799, 3), )

    
    throughput = property(__throughput.value, __throughput.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}transitDelay uses Python identifier transitDelay
    __transitDelay = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'transitDelay'), 'transitDelay', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_QoSCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdtransitDelay', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 800, 3), )

    
    transitDelay = property(__transitDelay.value, __transitDelay.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}priority uses Python identifier priority
    __priority = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'priority'), 'priority', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_QoSCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpriority', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 801, 3), )

    
    priority = property(__priority.value, __priority.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}errorRate uses Python identifier errorRate
    __errorRate = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'errorRate'), 'errorRate', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_QoSCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsderrorRate', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 802, 3), )

    
    errorRate = property(__errorRate.value, __errorRate.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}resilence uses Python identifier resilence
    __resilence = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'resilence'), 'resilence', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_QoSCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdresilence', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 803, 3), )

    
    resilence = property(__resilence.value, __resilence.set, None, None)

    _ElementMap.update({
        __profile.name() : __profile,
        __throughput.name() : __throughput,
        __transitDelay.name() : __transitDelay,
        __priority.name() : __priority,
        __errorRate.name() : __errorRate,
        __resilence.name() : __resilence
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.QoSCondition = QoSCondition
Namespace.addCategoryObject('typeBinding', 'QoSCondition', QoSCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ApplicationLayerCondition with content type ELEMENT_ONLY
class ApplicationLayerCondition (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ApplicationLayerCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ApplicationLayerCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 810, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}applicationProtocol uses Python identifier applicationProtocol
    __applicationProtocol = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'applicationProtocol'), 'applicationProtocol', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ApplicationLayerCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdapplicationProtocol', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 812, 3), )

    
    applicationProtocol = property(__applicationProtocol.value, __applicationProtocol.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}URL uses Python identifier URL
    __URL = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'URL'), 'URL', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ApplicationLayerCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdURL', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 813, 3), )

    
    URL = property(__URL.value, __URL.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}httpCondition uses Python identifier httpCondition
    __httpCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'httpCondition'), 'httpCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ApplicationLayerCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdhttpCondition', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 814, 3), )

    
    httpCondition = property(__httpCondition.value, __httpCondition.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}fileExtension uses Python identifier fileExtension
    __fileExtension = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'fileExtension'), 'fileExtension', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ApplicationLayerCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdfileExtension', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 816, 3), )

    
    fileExtension = property(__fileExtension.value, __fileExtension.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}mimeType uses Python identifier mimeType
    __mimeType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'mimeType'), 'mimeType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ApplicationLayerCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdmimeType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 817, 3), )

    
    mimeType = property(__mimeType.value, __mimeType.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}maxconn uses Python identifier maxconn
    __maxconn = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'maxconn'), 'maxconn', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ApplicationLayerCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdmaxconn', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 819, 3), )

    
    maxconn = property(__maxconn.value, __maxconn.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}dst_domain uses Python identifier dst_domain
    __dst_domain = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'dst_domain'), 'dst_domain', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ApplicationLayerCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsddst_domain', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 821, 3), )

    
    dst_domain = property(__dst_domain.value, __dst_domain.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}src_domain uses Python identifier src_domain
    __src_domain = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'src_domain'), 'src_domain', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ApplicationLayerCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdsrc_domain', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 822, 3), )

    
    src_domain = property(__src_domain.value, __src_domain.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}URL_regex uses Python identifier URL_regex
    __URL_regex = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'URL_regex'), 'URL_regex', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ApplicationLayerCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdURL_regex', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 824, 3), )

    
    URL_regex = property(__URL_regex.value, __URL_regex.set, None, None)

    _ElementMap.update({
        __applicationProtocol.name() : __applicationProtocol,
        __URL.name() : __URL,
        __httpCondition.name() : __httpCondition,
        __fileExtension.name() : __fileExtension,
        __mimeType.name() : __mimeType,
        __maxconn.name() : __maxconn,
        __dst_domain.name() : __dst_domain,
        __src_domain.name() : __src_domain,
        __URL_regex.name() : __URL_regex
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ApplicationLayerCondition = ApplicationLayerCondition
Namespace.addCategoryObject('typeBinding', 'ApplicationLayerCondition', ApplicationLayerCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}StatefulCondition with content type ELEMENT_ONLY
class StatefulCondition (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}StatefulCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'StatefulCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 842, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}State uses Python identifier State
    __State = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'State'), 'State', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_StatefulCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdState', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 844, 3), )

    
    State = property(__State.value, __State.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}limitRuleHits uses Python identifier limitRuleHits
    __limitRuleHits = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'limitRuleHits'), 'limitRuleHits', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_StatefulCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdlimitRuleHits', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 845, 3), )

    
    limitRuleHits = property(__limitRuleHits.value, __limitRuleHits.set, None, None)

    _ElementMap.update({
        __State.name() : __State,
        __limitRuleHits.name() : __limitRuleHits
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.StatefulCondition = StatefulCondition
Namespace.addCategoryObject('typeBinding', 'StatefulCondition', StatefulCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TimeCondition with content type ELEMENT_ONLY
class TimeCondition (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TimeCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TimeCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 849, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TimeZone uses Python identifier TimeZone
    __TimeZone = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'TimeZone'), 'TimeZone', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TimeCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdTimeZone', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 851, 3), )

    
    TimeZone = property(__TimeZone.value, __TimeZone.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Weekday uses Python identifier Weekday
    __Weekday = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Weekday'), 'Weekday', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TimeCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdWeekday', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 852, 3), )

    
    Weekday = property(__Weekday.value, __Weekday.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Time uses Python identifier Time
    __Time = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Time'), 'Time', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TimeCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdTime', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 853, 3), )

    
    Time = property(__Time.value, __Time.set, None, None)

    _ElementMap.update({
        __TimeZone.name() : __TimeZone,
        __Weekday.name() : __Weekday,
        __Time.name() : __Time
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.TimeCondition = TimeCondition
Namespace.addCategoryObject('typeBinding', 'TimeCondition', TimeCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EventCondition with content type ELEMENT_ONLY
class EventCondition (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EventCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EventCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 878, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}events uses Python identifier events
    __events = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'events'), 'events', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_EventCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdevents', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 880, 3), )

    
    events = property(__events.value, __events.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}interval uses Python identifier interval
    __interval = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'interval'), 'interval', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_EventCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdinterval', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 881, 3), )

    
    interval = property(__interval.value, __interval.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}threshold uses Python identifier threshold
    __threshold = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'threshold'), 'threshold', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_EventCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdthreshold', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 882, 3), )

    
    threshold = property(__threshold.value, __threshold.set, None, None)

    _ElementMap.update({
        __events.name() : __events,
        __interval.name() : __interval,
        __threshold.name() : __threshold
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.EventCondition = EventCondition
Namespace.addCategoryObject('typeBinding', 'EventCondition', EventCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationOption with content type ELEMENT_ONLY
class AuthenticationOption (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationOption with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AuthenticationOption')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1229, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationTarget uses Python identifier AuthenticationTarget
    __AuthenticationTarget = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationTarget'), 'AuthenticationTarget', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationOption_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdAuthenticationTarget', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1231, 3), )

    
    AuthenticationTarget = property(__AuthenticationTarget.value, __AuthenticationTarget.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationMethod uses Python identifier AuthenticationMethod
    __AuthenticationMethod = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationMethod'), 'AuthenticationMethod', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationOption_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdAuthenticationMethod', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1232, 3), )

    
    AuthenticationMethod = property(__AuthenticationMethod.value, __AuthenticationMethod.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationMechanism uses Python identifier AuthenticationMechanism
    __AuthenticationMechanism = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationMechanism'), 'AuthenticationMechanism', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationOption_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdAuthenticationMechanism', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1233, 3), )

    
    AuthenticationMechanism = property(__AuthenticationMechanism.value, __AuthenticationMechanism.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationParameters uses Python identifier AuthenticationParameters
    __AuthenticationParameters = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationParameters'), 'AuthenticationParameters', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationOption_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdAuthenticationParameters', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1234, 3), )

    
    AuthenticationParameters = property(__AuthenticationParameters.value, __AuthenticationParameters.set, None, None)

    _ElementMap.update({
        __AuthenticationTarget.name() : __AuthenticationTarget,
        __AuthenticationMethod.name() : __AuthenticationMethod,
        __AuthenticationMechanism.name() : __AuthenticationMechanism,
        __AuthenticationParameters.name() : __AuthenticationParameters
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AuthenticationOption = AuthenticationOption
Namespace.addCategoryObject('typeBinding', 'AuthenticationOption', AuthenticationOption)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Pics with content type ELEMENT_ONLY
class Pics (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Pics with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Pics')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1260, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ICRA uses Python identifier ICRA
    __ICRA = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ICRA'), 'ICRA', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Pics_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdICRA', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1262, 3), )

    
    ICRA = property(__ICRA.value, __ICRA.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RSAC uses Python identifier RSAC
    __RSAC = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'RSAC'), 'RSAC', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Pics_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdRSAC', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1263, 3), )

    
    RSAC = property(__RSAC.value, __RSAC.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}evaluWEB uses Python identifier evaluWEB
    __evaluWEB = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'evaluWEB'), 'evaluWEB', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Pics_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdevaluWEB', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1264, 3), )

    
    evaluWEB = property(__evaluWEB.value, __evaluWEB.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}CyberNOTsex uses Python identifier CyberNOTsex
    __CyberNOTsex = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'CyberNOTsex'), 'CyberNOTsex', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Pics_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdCyberNOTsex', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1273, 3), )

    
    CyberNOTsex = property(__CyberNOTsex.value, __CyberNOTsex.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Weburbia uses Python identifier Weburbia
    __Weburbia = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Weburbia'), 'Weburbia', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Pics_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdWeburbia', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1281, 3), )

    
    Weburbia = property(__Weburbia.value, __Weburbia.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Vancouver uses Python identifier Vancouver
    __Vancouver = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Vancouver'), 'Vancouver', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Pics_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdVancouver', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1289, 3), )

    
    Vancouver = property(__Vancouver.value, __Vancouver.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}SafeNet uses Python identifier SafeNet
    __SafeNet = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'SafeNet'), 'SafeNet', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Pics_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdSafeNet', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1290, 3), )

    
    SafeNet = property(__SafeNet.value, __SafeNet.set, None, None)

    _ElementMap.update({
        __ICRA.name() : __ICRA,
        __RSAC.name() : __RSAC,
        __evaluWEB.name() : __evaluWEB,
        __CyberNOTsex.name() : __CyberNOTsex,
        __Weburbia.name() : __Weburbia,
        __Vancouver.name() : __Vancouver,
        __SafeNet.name() : __SafeNet
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Pics = Pics
Namespace.addCategoryObject('typeBinding', 'Pics', Pics)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ICRA with content type EMPTY
class ICRA (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ICRA with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ICRA')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1295, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute ICRAchat uses Python identifier ICRAchat
    __ICRAchat = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'ICRAchat'), 'ICRAchat', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ICRA_ICRAchat', pyxb.binding.datatypes.boolean)
    __ICRAchat._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1296, 2)
    __ICRAchat._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1296, 2)
    
    ICRAchat = property(__ICRAchat.value, __ICRAchat.set, None, None)

    
    # Attribute ICRAlanguagesexual uses Python identifier ICRAlanguagesexual
    __ICRAlanguagesexual = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'ICRAlanguagesexual'), 'ICRAlanguagesexual', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ICRA_ICRAlanguagesexual', pyxb.binding.datatypes.boolean)
    __ICRAlanguagesexual._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1297, 2)
    __ICRAlanguagesexual._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1297, 2)
    
    ICRAlanguagesexual = property(__ICRAlanguagesexual.value, __ICRAlanguagesexual.set, None, None)

    
    # Attribute ICRAnuditygraphic uses Python identifier ICRAnuditygraphic
    __ICRAnuditygraphic = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'ICRAnuditygraphic'), 'ICRAnuditygraphic', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ICRA_ICRAnuditygraphic', pyxb.binding.datatypes.boolean)
    __ICRAnuditygraphic._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1298, 2)
    __ICRAnuditygraphic._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1298, 2)
    
    ICRAnuditygraphic = property(__ICRAnuditygraphic.value, __ICRAnuditygraphic.set, None, None)

    
    # Attribute ICRAnuditytopless uses Python identifier ICRAnuditytopless
    __ICRAnuditytopless = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'ICRAnuditytopless'), 'ICRAnuditytopless', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ICRA_ICRAnuditytopless', pyxb.binding.datatypes.boolean)
    __ICRAnuditytopless._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1299, 2)
    __ICRAnuditytopless._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1299, 2)
    
    ICRAnuditytopless = property(__ICRAnuditytopless.value, __ICRAnuditytopless.set, None, None)

    
    # Attribute ICRAnuditybottoms uses Python identifier ICRAnuditybottoms
    __ICRAnuditybottoms = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'ICRAnuditybottoms'), 'ICRAnuditybottoms', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ICRA_ICRAnuditybottoms', pyxb.binding.datatypes.boolean)
    __ICRAnuditybottoms._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1300, 2)
    __ICRAnuditybottoms._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1300, 2)
    
    ICRAnuditybottoms = property(__ICRAnuditybottoms.value, __ICRAnuditybottoms.set, None, None)

    
    # Attribute ICRAdrugsuse uses Python identifier ICRAdrugsuse
    __ICRAdrugsuse = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'ICRAdrugsuse'), 'ICRAdrugsuse', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ICRA_ICRAdrugsuse', pyxb.binding.datatypes.boolean)
    __ICRAdrugsuse._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1301, 2)
    __ICRAdrugsuse._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1301, 2)
    
    ICRAdrugsuse = property(__ICRAdrugsuse.value, __ICRAdrugsuse.set, None, None)

    
    # Attribute ICRAdrugsalcohol uses Python identifier ICRAdrugsalcohol
    __ICRAdrugsalcohol = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'ICRAdrugsalcohol'), 'ICRAdrugsalcohol', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ICRA_ICRAdrugsalcohol', pyxb.binding.datatypes.boolean)
    __ICRAdrugsalcohol._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1302, 2)
    __ICRAdrugsalcohol._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1302, 2)
    
    ICRAdrugsalcohol = property(__ICRAdrugsalcohol.value, __ICRAdrugsalcohol.set, None, None)

    
    # Attribute ICRAviolencetofantasy uses Python identifier ICRAviolencetofantasy
    __ICRAviolencetofantasy = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'ICRAviolencetofantasy'), 'ICRAviolencetofantasy', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ICRA_ICRAviolencetofantasy', pyxb.binding.datatypes.boolean)
    __ICRAviolencetofantasy._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1303, 2)
    __ICRAviolencetofantasy._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1303, 2)
    
    ICRAviolencetofantasy = property(__ICRAviolencetofantasy.value, __ICRAviolencetofantasy.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __ICRAchat.name() : __ICRAchat,
        __ICRAlanguagesexual.name() : __ICRAlanguagesexual,
        __ICRAnuditygraphic.name() : __ICRAnuditygraphic,
        __ICRAnuditytopless.name() : __ICRAnuditytopless,
        __ICRAnuditybottoms.name() : __ICRAnuditybottoms,
        __ICRAdrugsuse.name() : __ICRAdrugsuse,
        __ICRAdrugsalcohol.name() : __ICRAdrugsalcohol,
        __ICRAviolencetofantasy.name() : __ICRAviolencetofantasy
    })
_module_typeBindings.ICRA = ICRA
Namespace.addCategoryObject('typeBinding', 'ICRA', ICRA)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TechnologyActionSecurityProperty with content type EMPTY
class TechnologyActionSecurityProperty (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TechnologyActionSecurityProperty with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TechnologyActionSecurityProperty')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1461, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.TechnologyActionSecurityProperty = TechnologyActionSecurityProperty
Namespace.addCategoryObject('typeBinding', 'TechnologyActionSecurityProperty', TechnologyActionSecurityProperty)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ActionParameters with content type ELEMENT_ONLY
class ActionParameters (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ActionParameters with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ActionParameters')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1499, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}keyExchange uses Python identifier keyExchange
    __keyExchange = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'keyExchange'), 'keyExchange', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ActionParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdkeyExchange', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1501, 3), )

    
    keyExchange = property(__keyExchange.value, __keyExchange.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}technologyParameter uses Python identifier technologyParameter
    __technologyParameter = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'technologyParameter'), 'technologyParameter', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ActionParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdtechnologyParameter', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1503, 3), )

    
    technologyParameter = property(__technologyParameter.value, __technologyParameter.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}additionalNetworkConfigurationParameters uses Python identifier additionalNetworkConfigurationParameters
    __additionalNetworkConfigurationParameters = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'additionalNetworkConfigurationParameters'), 'additionalNetworkConfigurationParameters', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ActionParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdadditionalNetworkConfigurationParameters', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1505, 3), )

    
    additionalNetworkConfigurationParameters = property(__additionalNetworkConfigurationParameters.value, __additionalNetworkConfigurationParameters.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}authenticationParameters uses Python identifier authenticationParameters
    __authenticationParameters = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'authenticationParameters'), 'authenticationParameters', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ActionParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdauthenticationParameters', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1507, 3), )

    
    authenticationParameters = property(__authenticationParameters.value, __authenticationParameters.set, None, None)

    _ElementMap.update({
        __keyExchange.name() : __keyExchange,
        __technologyParameter.name() : __technologyParameter,
        __additionalNetworkConfigurationParameters.name() : __additionalNetworkConfigurationParameters,
        __authenticationParameters.name() : __authenticationParameters
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ActionParameters = ActionParameters
Namespace.addCategoryObject('typeBinding', 'ActionParameters', ActionParameters)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}KeyExchangeParameter with content type ELEMENT_ONLY
class KeyExchangeParameter (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}KeyExchangeParameter with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'KeyExchangeParameter')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1511, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}keyExchangeAction uses Python identifier keyExchangeAction
    __keyExchangeAction = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'keyExchangeAction'), 'keyExchangeAction', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_KeyExchangeParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdkeyExchangeAction', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1513, 3), )

    
    keyExchangeAction = property(__keyExchangeAction.value, __keyExchangeAction.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}hashAlgorithm uses Python identifier hashAlgorithm
    __hashAlgorithm = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'hashAlgorithm'), 'hashAlgorithm', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_KeyExchangeParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdhashAlgorithm', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1514, 3), )

    
    hashAlgorithm = property(__hashAlgorithm.value, __hashAlgorithm.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}symmetricEncryptionAlgorithm uses Python identifier symmetricEncryptionAlgorithm
    __symmetricEncryptionAlgorithm = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'symmetricEncryptionAlgorithm'), 'symmetricEncryptionAlgorithm', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_KeyExchangeParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdsymmetricEncryptionAlgorithm', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1515, 3), )

    
    symmetricEncryptionAlgorithm = property(__symmetricEncryptionAlgorithm.value, __symmetricEncryptionAlgorithm.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}authenticationType uses Python identifier authenticationType
    __authenticationType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'authenticationType'), 'authenticationType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_KeyExchangeParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdauthenticationType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1516, 3), )

    
    authenticationType = property(__authenticationType.value, __authenticationType.set, None, None)

    _ElementMap.update({
        __keyExchangeAction.name() : __keyExchangeAction,
        __hashAlgorithm.name() : __hashAlgorithm,
        __symmetricEncryptionAlgorithm.name() : __symmetricEncryptionAlgorithm,
        __authenticationType.name() : __authenticationType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.KeyExchangeParameter = KeyExchangeParameter
Namespace.addCategoryObject('typeBinding', 'KeyExchangeParameter', KeyExchangeParameter)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TechnologySpecificParameters with content type EMPTY
class TechnologySpecificParameters (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TechnologySpecificParameters with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TechnologySpecificParameters')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1519, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.TechnologySpecificParameters = TechnologySpecificParameters
Namespace.addCategoryObject('typeBinding', 'TechnologySpecificParameters', TechnologySpecificParameters)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AdditionalNetworkConfigurationParameters with content type EMPTY
class AdditionalNetworkConfigurationParameters (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AdditionalNetworkConfigurationParameters with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AdditionalNetworkConfigurationParameters')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1591, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AdditionalNetworkConfigurationParameters = AdditionalNetworkConfigurationParameters
Namespace.addCategoryObject('typeBinding', 'AdditionalNetworkConfigurationParameters', AdditionalNetworkConfigurationParameters)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationParameters with content type ELEMENT_ONLY
class AuthenticationParameters (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationParameters with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AuthenticationParameters')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1634, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}psKey_value uses Python identifier psKey_value
    __psKey_value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'psKey_value'), 'psKey_value', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpsKey_value', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1636, 3), )

    
    psKey_value = property(__psKey_value.value, __psKey_value.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}psKey_path uses Python identifier psKey_path
    __psKey_path = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'psKey_path'), 'psKey_path', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpsKey_path', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1637, 3), )

    
    psKey_path = property(__psKey_path.value, __psKey_path.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ca_path uses Python identifier ca_path
    __ca_path = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ca_path'), 'ca_path', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdca_path', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1638, 3), )

    
    ca_path = property(__ca_path.value, __ca_path.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}cert_path uses Python identifier cert_path
    __cert_path = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'cert_path'), 'cert_path', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdcert_path', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1639, 3), )

    
    cert_path = property(__cert_path.value, __cert_path.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}publicKey_path uses Python identifier publicKey_path
    __publicKey_path = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'publicKey_path'), 'publicKey_path', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpublicKey_path', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1640, 3), )

    
    publicKey_path = property(__publicKey_path.value, __publicKey_path.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}publicKey_filename uses Python identifier publicKey_filename
    __publicKey_filename = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'publicKey_filename'), 'publicKey_filename', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpublicKey_filename', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1641, 3), )

    
    publicKey_filename = property(__publicKey_filename.value, __publicKey_filename.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}publicKey_passphrase uses Python identifier publicKey_passphrase
    __publicKey_passphrase = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'publicKey_passphrase'), 'publicKey_passphrase', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpublicKey_passphrase', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1642, 3), )

    
    publicKey_passphrase = property(__publicKey_passphrase.value, __publicKey_passphrase.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ca_id uses Python identifier ca_id
    __ca_id = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ca_id'), 'ca_id', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdca_id', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1643, 3), )

    
    ca_id = property(__ca_id.value, __ca_id.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ca_filename uses Python identifier ca_filename
    __ca_filename = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ca_filename'), 'ca_filename', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdca_filename', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1644, 3), )

    
    ca_filename = property(__ca_filename.value, __ca_filename.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}cert_id uses Python identifier cert_id
    __cert_id = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'cert_id'), 'cert_id', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdcert_id', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1645, 3), )

    
    cert_id = property(__cert_id.value, __cert_id.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}cert_filename uses Python identifier cert_filename
    __cert_filename = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'cert_filename'), 'cert_filename', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdcert_filename', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1646, 3), )

    
    cert_filename = property(__cert_filename.value, __cert_filename.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}remote_id uses Python identifier remote_id
    __remote_id = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'remote_id'), 'remote_id', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationParameters_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdremote_id', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1647, 3), )

    
    remote_id = property(__remote_id.value, __remote_id.set, None, None)

    _ElementMap.update({
        __psKey_value.name() : __psKey_value,
        __psKey_path.name() : __psKey_path,
        __ca_path.name() : __ca_path,
        __cert_path.name() : __cert_path,
        __publicKey_path.name() : __publicKey_path,
        __publicKey_filename.name() : __publicKey_filename,
        __publicKey_passphrase.name() : __publicKey_passphrase,
        __ca_id.name() : __ca_id,
        __ca_filename.name() : __ca_filename,
        __cert_id.name() : __cert_id,
        __cert_filename.name() : __cert_filename,
        __remote_id.name() : __remote_id
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AuthenticationParameters = AuthenticationParameters
Namespace.addCategoryObject('typeBinding', 'AuthenticationParameters', AuthenticationParameters)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EnableActionType with content type EMPTY
class EnableActionType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EnableActionType with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EnableActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1656, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute enable uses Python identifier enable
    __enable = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'enable'), 'enable', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_EnableActionType_enable', pyxb.binding.datatypes.boolean)
    __enable._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1657, 2)
    __enable._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1657, 2)
    
    enable = property(__enable.value, __enable.set, None, None)

    
    # Attribute objectToEnable uses Python identifier objectToEnable
    __objectToEnable = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'objectToEnable'), 'objectToEnable', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_EnableActionType_objectToEnable', pyxb.binding.datatypes.string)
    __objectToEnable._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1658, 2)
    __objectToEnable._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1658, 2)
    
    objectToEnable = property(__objectToEnable.value, __objectToEnable.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __enable.name() : __enable,
        __objectToEnable.name() : __objectToEnable
    })
_module_typeBindings.EnableActionType = EnableActionType
Namespace.addCategoryObject('typeBinding', 'EnableActionType', EnableActionType)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ReduceBandwidthActionType with content type EMPTY
class ReduceBandwidthActionType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ReduceBandwidthActionType with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ReduceBandwidthActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1661, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute downlink_bandwidth_value uses Python identifier downlink_bandwidth_value
    __downlink_bandwidth_value = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'downlink_bandwidth_value'), 'downlink_bandwidth_value', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ReduceBandwidthActionType_downlink_bandwidth_value', pyxb.binding.datatypes.double)
    __downlink_bandwidth_value._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1662, 2)
    __downlink_bandwidth_value._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1662, 2)
    
    downlink_bandwidth_value = property(__downlink_bandwidth_value.value, __downlink_bandwidth_value.set, None, None)

    
    # Attribute uplink_bandwidth_value uses Python identifier uplink_bandwidth_value
    __uplink_bandwidth_value = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'uplink_bandwidth_value'), 'uplink_bandwidth_value', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ReduceBandwidthActionType_uplink_bandwidth_value', pyxb.binding.datatypes.double)
    __uplink_bandwidth_value._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1663, 2)
    __uplink_bandwidth_value._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1663, 2)
    
    uplink_bandwidth_value = property(__uplink_bandwidth_value.value, __uplink_bandwidth_value.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __downlink_bandwidth_value.name() : __downlink_bandwidth_value,
        __uplink_bandwidth_value.name() : __uplink_bandwidth_value
    })
_module_typeBindings.ReduceBandwidthActionType = ReduceBandwidthActionType
Namespace.addCategoryObject('typeBinding', 'ReduceBandwidthActionType', ReduceBandwidthActionType)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}CountActionType with content type EMPTY
class CountActionType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}CountActionType with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CountActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1666, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CountActionType = CountActionType
Namespace.addCategoryObject('typeBinding', 'CountActionType', CountActionType)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveActionType with content type EMPTY
class RemoveActionType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveActionType with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RemoveActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1668, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.RemoveActionType = RemoveActionType
Namespace.addCategoryObject('typeBinding', 'RemoveActionType', RemoveActionType)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}CheckActionType with content type EMPTY
class CheckActionType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}CheckActionType with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CheckActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1670, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CheckActionType = CheckActionType
Namespace.addCategoryObject('typeBinding', 'CheckActionType', CheckActionType)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveAdvertisementActionType with content type EMPTY
class RemoveAdvertisementActionType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveAdvertisementActionType with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RemoveAdvertisementActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1672, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.RemoveAdvertisementActionType = RemoveAdvertisementActionType
Namespace.addCategoryObject('typeBinding', 'RemoveAdvertisementActionType', RemoveAdvertisementActionType)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveTrackingTechniquesActionType with content type EMPTY
class RemoveTrackingTechniquesActionType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveTrackingTechniquesActionType with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RemoveTrackingTechniquesActionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1674, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.RemoveTrackingTechniquesActionType = RemoveTrackingTechniquesActionType
Namespace.addCategoryObject('typeBinding', 'RemoveTrackingTechniquesActionType', RemoveTrackingTechniquesActionType)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PurposeConditionType with content type EMPTY
class PurposeConditionType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PurposeConditionType with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PurposeConditionType')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1676, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute propose uses Python identifier propose
    __propose = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'propose'), 'propose', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PurposeConditionType_propose', pyxb.binding.datatypes.string)
    __propose._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1677, 2)
    __propose._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1677, 2)
    
    propose = property(__propose.value, __propose.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __propose.name() : __propose
    })
_module_typeBindings.PurposeConditionType = PurposeConditionType
Namespace.addCategoryObject('typeBinding', 'PurposeConditionType', PurposeConditionType)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}HSPL with content type EMPTY
class HSPL (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}HSPL with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'HSPL')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1741, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute HSPL_id uses Python identifier HSPL_id
    __HSPL_id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'HSPL_id'), 'HSPL_id', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_HSPL_HSPL_id', pyxb.binding.datatypes.string)
    __HSPL_id._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1742, 2)
    __HSPL_id._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1742, 2)
    
    HSPL_id = property(__HSPL_id.value, __HSPL_id.set, None, None)

    
    # Attribute HSPL_text uses Python identifier HSPL_text
    __HSPL_text = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'HSPL_text'), 'HSPL_text', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_HSPL_HSPL_text', pyxb.binding.datatypes.string)
    __HSPL_text._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1743, 2)
    __HSPL_text._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1743, 2)
    
    HSPL_text = property(__HSPL_text.value, __HSPL_text.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __HSPL_id.name() : __HSPL_id,
        __HSPL_text.name() : __HSPL_text
    })
_module_typeBindings.HSPL = HSPL
Namespace.addCategoryObject('typeBinding', 'HSPL', HSPL)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}HTTPCondition with content type ELEMENT_ONLY
class HTTPCondition (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}HTTPCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'HTTPCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1755, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}httpMetod uses Python identifier httpMetod
    __httpMetod = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'httpMetod'), 'httpMetod', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_HTTPCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdhttpMetod', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1757, 3), )

    
    httpMetod = property(__httpMetod.value, __httpMetod.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}browser uses Python identifier browser
    __browser = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'browser'), 'browser', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_HTTPCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdbrowser', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1758, 3), )

    
    browser = property(__browser.value, __browser.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}user_cert uses Python identifier user_cert
    __user_cert = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'user_cert'), 'user_cert', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_HTTPCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsduser_cert', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1759, 3), )

    
    user_cert = property(__user_cert.value, __user_cert.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ca_cert uses Python identifier ca_cert
    __ca_cert = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ca_cert'), 'ca_cert', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_HTTPCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdca_cert', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1760, 3), )

    
    ca_cert = property(__ca_cert.value, __ca_cert.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}request_mime_type uses Python identifier request_mime_type
    __request_mime_type = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'request_mime_type'), 'request_mime_type', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_HTTPCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdrequest_mime_type', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1761, 3), )

    
    request_mime_type = property(__request_mime_type.value, __request_mime_type.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}response_mime_type uses Python identifier response_mime_type
    __response_mime_type = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'response_mime_type'), 'response_mime_type', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_HTTPCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdresponse_mime_type', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1762, 3), )

    
    response_mime_type = property(__response_mime_type.value, __response_mime_type.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}http_regex_header uses Python identifier http_regex_header
    __http_regex_header = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'http_regex_header'), 'http_regex_header', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_HTTPCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdhttp_regex_header', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1763, 3), )

    
    http_regex_header = property(__http_regex_header.value, __http_regex_header.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}http_status uses Python identifier http_status
    __http_status = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'http_status'), 'http_status', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_HTTPCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdhttp_status', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1764, 3), )

    
    http_status = property(__http_status.value, __http_status.set, None, None)

    _ElementMap.update({
        __httpMetod.name() : __httpMetod,
        __browser.name() : __browser,
        __user_cert.name() : __user_cert,
        __ca_cert.name() : __ca_cert,
        __request_mime_type.name() : __request_mime_type,
        __response_mime_type.name() : __response_mime_type,
        __http_regex_header.name() : __http_regex_header,
        __http_status.name() : __http_status
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.HTTPCondition = HTTPCondition
Namespace.addCategoryObject('typeBinding', 'HTTPCondition', HTTPCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PolicyDependency with content type ELEMENT_ONLY
class PolicyDependency (Dependency):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PolicyDependency with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PolicyDependency')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 58, 1)
    _ElementMap = Dependency._ElementMap.copy()
    _AttributeMap = Dependency._AttributeMap.copy()
    # Base type is Dependency
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}configurationCondition uses Python identifier configurationCondition
    __configurationCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'configurationCondition'), 'configurationCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PolicyDependency_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdconfigurationCondition', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 62, 5), )

    
    configurationCondition = property(__configurationCondition.value, __configurationCondition.set, None, None)

    _ElementMap.update({
        __configurationCondition.name() : __configurationCondition
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PolicyDependency = PolicyDependency
Namespace.addCategoryObject('typeBinding', 'PolicyDependency', PolicyDependency)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PolicyDependencyCondition with content type ELEMENT_ONLY
class PolicyDependencyCondition (ConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PolicyDependencyCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PolicyDependencyCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 69, 1)
    _ElementMap = ConfigurationCondition._ElementMap.copy()
    _AttributeMap = ConfigurationCondition._AttributeMap.copy()
    # Base type is ConfigurationCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}policyID uses Python identifier policyID
    __policyID = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'policyID'), 'policyID', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PolicyDependencyCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpolicyID', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 73, 5), )

    
    policyID = property(__policyID.value, __policyID.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}status uses Python identifier status
    __status = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'status'), 'status', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PolicyDependencyCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdstatus', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 74, 5), )

    
    status = property(__status.value, __status.set, None, None)

    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    _ElementMap.update({
        __policyID.name() : __policyID,
        __status.name() : __status
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PolicyDependencyCondition = PolicyDependencyCondition
Namespace.addCategoryObject('typeBinding', 'PolicyDependencyCondition', PolicyDependencyCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EventDependency with content type ELEMENT_ONLY
class EventDependency (Dependency):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EventDependency with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EventDependency')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 80, 1)
    _ElementMap = Dependency._ElementMap.copy()
    _AttributeMap = Dependency._AttributeMap.copy()
    # Base type is Dependency
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}eventID uses Python identifier eventID
    __eventID = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'eventID'), 'eventID', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_EventDependency_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdeventID', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 84, 5), )

    
    eventID = property(__eventID.value, __eventID.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}configurationCondition uses Python identifier configurationCondition
    __configurationCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'configurationCondition'), 'configurationCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_EventDependency_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdconfigurationCondition', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 85, 5), )

    
    configurationCondition = property(__configurationCondition.value, __configurationCondition.set, None, None)

    _ElementMap.update({
        __eventID.name() : __eventID,
        __configurationCondition.name() : __configurationCondition
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.EventDependency = EventDependency
Namespace.addCategoryObject('typeBinding', 'EventDependency', EventDependency)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthorizationCapability with content type ELEMENT_ONLY
class AuthorizationCapability (Capability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthorizationCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AuthorizationCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 114, 1)
    _ElementMap = Capability._ElementMap.copy()
    _AttributeMap = Capability._AttributeMap.copy()
    # Base type is Capability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AuthorizationCapability = AuthorizationCapability
Namespace.addCategoryObject('typeBinding', 'AuthorizationCapability', AuthorizationCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationCapability with content type ELEMENT_ONLY
class AuthenticationCapability (Capability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AuthenticationCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 158, 1)
    _ElementMap = Capability._ElementMap.copy()
    _AttributeMap = Capability._AttributeMap.copy()
    # Base type is Capability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AuthenticationCapability = AuthenticationCapability
Namespace.addCategoryObject('typeBinding', 'AuthenticationCapability', AuthenticationCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficAnalysisCapability with content type ELEMENT_ONLY
class TrafficAnalysisCapability (Capability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficAnalysisCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TrafficAnalysisCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 181, 1)
    _ElementMap = Capability._ElementMap.copy()
    _AttributeMap = Capability._AttributeMap.copy()
    # Base type is Capability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportOnlineTraficAnalysis uses Python identifier supportOnlineTraficAnalysis
    __supportOnlineTraficAnalysis = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'supportOnlineTraficAnalysis'), 'supportOnlineTraficAnalysis', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TrafficAnalysisCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdsupportOnlineTraficAnalysis', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 185, 5), )

    
    supportOnlineTraficAnalysis = property(__supportOnlineTraficAnalysis.value, __supportOnlineTraficAnalysis.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportOfflineTraficAnalysis uses Python identifier supportOfflineTraficAnalysis
    __supportOfflineTraficAnalysis = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'supportOfflineTraficAnalysis'), 'supportOfflineTraficAnalysis', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TrafficAnalysisCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdsupportOfflineTraficAnalysis', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 186, 5), )

    
    supportOfflineTraficAnalysis = property(__supportOfflineTraficAnalysis.value, __supportOfflineTraficAnalysis.set, None, None)

    _ElementMap.update({
        __supportOnlineTraficAnalysis.name() : __supportOnlineTraficAnalysis,
        __supportOfflineTraficAnalysis.name() : __supportOfflineTraficAnalysis
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.TrafficAnalysisCapability = TrafficAnalysisCapability
Namespace.addCategoryObject('typeBinding', 'TrafficAnalysisCapability', TrafficAnalysisCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ResourceScannerCapability with content type ELEMENT_ONLY
class ResourceScannerCapability (Capability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ResourceScannerCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ResourceScannerCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 232, 1)
    _ElementMap = Capability._ElementMap.copy()
    _AttributeMap = Capability._AttributeMap.copy()
    # Base type is Capability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}resourceType uses Python identifier resourceType
    __resourceType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'resourceType'), 'resourceType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ResourceScannerCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdresourceType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 236, 5), )

    
    resourceType = property(__resourceType.value, __resourceType.set, None, None)

    _ElementMap.update({
        __resourceType.name() : __resourceType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ResourceScannerCapability = ResourceScannerCapability
Namespace.addCategoryObject('typeBinding', 'ResourceScannerCapability', ResourceScannerCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionCapability with content type ELEMENT_ONLY
class DataProtectionCapability (Capability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DataProtectionCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 266, 1)
    _ElementMap = Capability._ElementMap.copy()
    _AttributeMap = Capability._AttributeMap.copy()
    # Base type is Capability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportsDataAuthenticationAndIntegrity uses Python identifier supportsDataAuthenticationAndIntegrity
    __supportsDataAuthenticationAndIntegrity = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'supportsDataAuthenticationAndIntegrity'), 'supportsDataAuthenticationAndIntegrity', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_DataProtectionCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdsupportsDataAuthenticationAndIntegrity', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 270, 5), )

    
    supportsDataAuthenticationAndIntegrity = property(__supportsDataAuthenticationAndIntegrity.value, __supportsDataAuthenticationAndIntegrity.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportsDigitalSignature uses Python identifier supportsDigitalSignature
    __supportsDigitalSignature = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'supportsDigitalSignature'), 'supportsDigitalSignature', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_DataProtectionCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdsupportsDigitalSignature', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 272, 5), )

    
    supportsDigitalSignature = property(__supportsDigitalSignature.value, __supportsDigitalSignature.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportsEncryption uses Python identifier supportsEncryption
    __supportsEncryption = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'supportsEncryption'), 'supportsEncryption', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_DataProtectionCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdsupportsEncryption', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 274, 5), )

    
    supportsEncryption = property(__supportsEncryption.value, __supportsEncryption.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportsKeyExchange uses Python identifier supportsKeyExchange
    __supportsKeyExchange = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'supportsKeyExchange'), 'supportsKeyExchange', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_DataProtectionCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdsupportsKeyExchange', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 275, 5), )

    
    supportsKeyExchange = property(__supportsKeyExchange.value, __supportsKeyExchange.set, None, None)

    _ElementMap.update({
        __supportsDataAuthenticationAndIntegrity.name() : __supportsDataAuthenticationAndIntegrity,
        __supportsDigitalSignature.name() : __supportsDigitalSignature,
        __supportsEncryption.name() : __supportsEncryption,
        __supportsKeyExchange.name() : __supportsKeyExchange
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.DataProtectionCapability = DataProtectionCapability
Namespace.addCategoryObject('typeBinding', 'DataProtectionCapability', DataProtectionCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IdentityProtectionCapability with content type ELEMENT_ONLY
class IdentityProtectionCapability (Capability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IdentityProtectionCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'IdentityProtectionCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 287, 1)
    _ElementMap = Capability._ElementMap.copy()
    _AttributeMap = Capability._AttributeMap.copy()
    # Base type is Capability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.IdentityProtectionCapability = IdentityProtectionCapability
Namespace.addCategoryObject('typeBinding', 'IdentityProtectionCapability', IdentityProtectionCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AddressTranslationCapability with content type ELEMENT_ONLY
class AddressTranslationCapability (Capability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AddressTranslationCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AddressTranslationCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 299, 1)
    _ElementMap = Capability._ElementMap.copy()
    _AttributeMap = Capability._AttributeMap.copy()
    # Base type is Capability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AddressTranslationCapability = AddressTranslationCapability
Namespace.addCategoryObject('typeBinding', 'AddressTranslationCapability', AddressTranslationCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RoutingCapability with content type ELEMENT_ONLY
class RoutingCapability (Capability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RoutingCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RoutingCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 330, 1)
    _ElementMap = Capability._ElementMap.copy()
    _AttributeMap = Capability._AttributeMap.copy()
    # Base type is Capability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.RoutingCapability = RoutingCapability
Namespace.addCategoryObject('typeBinding', 'RoutingCapability', RoutingCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RuleSetConfiguration with content type ELEMENT_ONLY
class RuleSetConfiguration (Configuration):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RuleSetConfiguration with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RuleSetConfiguration')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 336, 1)
    _ElementMap = Configuration._ElementMap.copy()
    _AttributeMap = Configuration._AttributeMap.copy()
    # Base type is Configuration
    
    # Element capability ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}capability) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Configuration
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}defaultAction uses Python identifier defaultAction
    __defaultAction = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'defaultAction'), 'defaultAction', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RuleSetConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsddefaultAction', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 340, 5), )

    
    defaultAction = property(__defaultAction.value, __defaultAction.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}configurationRule uses Python identifier configurationRule
    __configurationRule = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'configurationRule'), 'configurationRule', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RuleSetConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdconfigurationRule', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 342, 5), )

    
    configurationRule = property(__configurationRule.value, __configurationRule.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}resolutionStrategy uses Python identifier resolutionStrategy
    __resolutionStrategy = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'resolutionStrategy'), 'resolutionStrategy', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RuleSetConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdresolutionStrategy', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 344, 5), )

    
    resolutionStrategy = property(__resolutionStrategy.value, __resolutionStrategy.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name uses Python identifier Name
    __Name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Name'), 'Name', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RuleSetConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdName', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 346, 5), )

    
    Name = property(__Name.value, __Name.set, None, None)

    _ElementMap.update({
        __defaultAction.name() : __defaultAction,
        __configurationRule.name() : __configurationRule,
        __resolutionStrategy.name() : __resolutionStrategy,
        __Name.name() : __Name
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.RuleSetConfiguration = RuleSetConfiguration
Namespace.addCategoryObject('typeBinding', 'RuleSetConfiguration', RuleSetConfiguration)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Priority with content type ELEMENT_ONLY
class Priority (ExternalData):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Priority with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Priority')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 376, 1)
    _ElementMap = ExternalData._ElementMap.copy()
    _AttributeMap = ExternalData._AttributeMap.copy()
    # Base type is ExternalData
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value'), 'value_', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Priority_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdvalue', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 380, 5), )

    
    value_ = property(__value.value, __value.set, None, None)

    _ElementMap.update({
        __value.name() : __value
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Priority = Priority
Namespace.addCategoryObject('typeBinding', 'Priority', Priority)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FMR with content type ELEMENT_ONLY
class FMR (ResolutionStrategy):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FMR with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FMR')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 393, 1)
    _ElementMap = ResolutionStrategy._ElementMap.copy()
    _AttributeMap = ResolutionStrategy._AttributeMap.copy()
    # Base type is ResolutionStrategy
    
    # Element externalData ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}externalData) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ResolutionStrategy
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.FMR = FMR
Namespace.addCategoryObject('typeBinding', 'FMR', FMR)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DTP with content type ELEMENT_ONLY
class DTP (ResolutionStrategy):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DTP with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DTP')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 399, 1)
    _ElementMap = ResolutionStrategy._ElementMap.copy()
    _AttributeMap = ResolutionStrategy._AttributeMap.copy()
    # Base type is ResolutionStrategy
    
    # Element externalData ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}externalData) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ResolutionStrategy
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.DTP = DTP
Namespace.addCategoryObject('typeBinding', 'DTP', DTP)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ATP with content type ELEMENT_ONLY
class ATP (ResolutionStrategy):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ATP with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ATP')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 405, 1)
    _ElementMap = ResolutionStrategy._ElementMap.copy()
    _AttributeMap = ResolutionStrategy._AttributeMap.copy()
    # Base type is ResolutionStrategy
    
    # Element externalData ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}externalData) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ResolutionStrategy
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ATP = ATP
Namespace.addCategoryObject('typeBinding', 'ATP', ATP)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}MSTP with content type ELEMENT_ONLY
class MSTP (ResolutionStrategy):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}MSTP with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MSTP')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 411, 1)
    _ElementMap = ResolutionStrategy._ElementMap.copy()
    _AttributeMap = ResolutionStrategy._AttributeMap.copy()
    # Base type is ResolutionStrategy
    
    # Element externalData ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}externalData) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ResolutionStrategy
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.MSTP = MSTP
Namespace.addCategoryObject('typeBinding', 'MSTP', MSTP)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}LSTP with content type ELEMENT_ONLY
class LSTP (ResolutionStrategy):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}LSTP with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LSTP')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 417, 1)
    _ElementMap = ResolutionStrategy._ElementMap.copy()
    _AttributeMap = ResolutionStrategy._AttributeMap.copy()
    # Base type is ResolutionStrategy
    
    # Element externalData ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}externalData) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ResolutionStrategy
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.LSTP = LSTP
Namespace.addCategoryObject('typeBinding', 'LSTP', LSTP)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ALL with content type ELEMENT_ONLY
class ALL (ResolutionStrategy):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ALL with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ALL')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 423, 1)
    _ElementMap = ResolutionStrategy._ElementMap.copy()
    _AttributeMap = ResolutionStrategy._AttributeMap.copy()
    # Base type is ResolutionStrategy
    
    # Element externalData ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}externalData) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ResolutionStrategy
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ALL = ALL
Namespace.addCategoryObject('typeBinding', 'ALL', ALL)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionCondition with content type ELEMENT_ONLY
class DataProtectionCondition (ConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DataProtectionCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 429, 1)
    _ElementMap = ConfigurationCondition._ElementMap.copy()
    _AttributeMap = ConfigurationCondition._AttributeMap.copy()
    # Base type is ConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition uses Python identifier packetFilterCondition
    __packetFilterCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition'), 'packetFilterCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_DataProtectionCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpacketFilterCondition', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5), )

    
    packetFilterCondition = property(__packetFilterCondition.value, __packetFilterCondition.set, None, None)

    _ElementMap.update({
        __packetFilterCondition.name() : __packetFilterCondition
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.DataProtectionCondition = DataProtectionCondition
Namespace.addCategoryObject('typeBinding', 'DataProtectionCondition', DataProtectionCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PurposeCondition with content type ELEMENT_ONLY
class PurposeCondition (ConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PurposeCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PurposeCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 440, 1)
    _ElementMap = ConfigurationCondition._ElementMap.copy()
    _AttributeMap = ConfigurationCondition._AttributeMap.copy()
    # Base type is ConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PurposeCondition uses Python identifier PurposeCondition
    __PurposeCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'PurposeCondition'), 'PurposeCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PurposeCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdPurposeCondition', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 444, 5), )

    
    PurposeCondition = property(__PurposeCondition.value, __PurposeCondition.set, None, None)

    _ElementMap.update({
        __PurposeCondition.name() : __PurposeCondition
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PurposeCondition = PurposeCondition
Namespace.addCategoryObject('typeBinding', 'PurposeCondition', PurposeCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyAction with content type ELEMENT_ONLY
class PrivacyAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PrivacyAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 463, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyActionType uses Python identifier PrivacyActionType
    __PrivacyActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'PrivacyActionType'), 'PrivacyActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PrivacyAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdPrivacyActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 467, 5), )

    
    PrivacyActionType = property(__PrivacyActionType.value, __PrivacyActionType.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyMethod uses Python identifier PrivacyMethod
    __PrivacyMethod = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'PrivacyMethod'), 'PrivacyMethod', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PrivacyAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdPrivacyMethod', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 469, 5), )

    
    PrivacyMethod = property(__PrivacyMethod.value, __PrivacyMethod.set, None, None)

    _ElementMap.update({
        __PrivacyActionType.name() : __PrivacyActionType,
        __PrivacyMethod.name() : __PrivacyMethod
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PrivacyAction = PrivacyAction
Namespace.addCategoryObject('typeBinding', 'PrivacyAction', PrivacyAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyIBMethod with content type ELEMENT_ONLY
class PrivacyIBMethod (PrivacyMethod):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyIBMethod with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PrivacyIBMethod')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 477, 1)
    _ElementMap = PrivacyMethod._ElementMap.copy()
    _AttributeMap = PrivacyMethod._AttributeMap.copy()
    # Base type is PrivacyMethod
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IB uses Python identifier IB
    __IB = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'IB'), 'IB', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PrivacyIBMethod_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdIB', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 481, 5), )

    
    IB = property(__IB.value, __IB.set, None, None)

    _ElementMap.update({
        __IB.name() : __IB
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PrivacyIBMethod = PrivacyIBMethod
Namespace.addCategoryObject('typeBinding', 'PrivacyIBMethod', PrivacyIBMethod)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyABMethod with content type ELEMENT_ONLY
class PrivacyABMethod (PrivacyMethod):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyABMethod with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PrivacyABMethod')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 487, 1)
    _ElementMap = PrivacyMethod._ElementMap.copy()
    _AttributeMap = PrivacyMethod._AttributeMap.copy()
    # Base type is PrivacyMethod
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}attribute uses Python identifier attribute
    __attribute = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'attribute'), 'attribute', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PrivacyABMethod_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdattribute', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 491, 5), )

    
    attribute = property(__attribute.value, __attribute.set, None, None)

    _ElementMap.update({
        __attribute.name() : __attribute
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PrivacyABMethod = PrivacyABMethod
Namespace.addCategoryObject('typeBinding', 'PrivacyABMethod', PrivacyABMethod)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyPKIMethod with content type ELEMENT_ONLY
class PrivacyPKIMethod (PrivacyMethod):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyPKIMethod with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PrivacyPKIMethod')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 497, 1)
    _ElementMap = PrivacyMethod._ElementMap.copy()
    _AttributeMap = PrivacyMethod._AttributeMap.copy()
    # Base type is PrivacyMethod
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}pkiParameters uses Python identifier pkiParameters
    __pkiParameters = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'pkiParameters'), 'pkiParameters', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PrivacyPKIMethod_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpkiParameters', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 501, 5), )

    
    pkiParameters = property(__pkiParameters.value, __pkiParameters.set, None, None)

    _ElementMap.update({
        __pkiParameters.name() : __pkiParameters
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PrivacyPKIMethod = PrivacyPKIMethod
Namespace.addCategoryObject('typeBinding', 'PrivacyPKIMethod', PrivacyPKIMethod)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataAgreggationConfigurationConditions with content type ELEMENT_ONLY
class DataAgreggationConfigurationConditions (ConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataAgreggationConfigurationConditions with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DataAgreggationConfigurationConditions')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 523, 1)
    _ElementMap = ConfigurationCondition._ElementMap.copy()
    _AttributeMap = ConfigurationCondition._AttributeMap.copy()
    # Base type is ConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}dataAggregationConfigurationCondition uses Python identifier dataAggregationConfigurationCondition
    __dataAggregationConfigurationCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'dataAggregationConfigurationCondition'), 'dataAggregationConfigurationCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_DataAgreggationConfigurationConditions_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsddataAggregationConfigurationCondition', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 527, 5), )

    
    dataAggregationConfigurationCondition = property(__dataAggregationConfigurationCondition.value, __dataAggregationConfigurationCondition.set, None, None)

    _ElementMap.update({
        __dataAggregationConfigurationCondition.name() : __dataAggregationConfigurationCondition
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.DataAgreggationConfigurationConditions = DataAgreggationConfigurationConditions
Namespace.addCategoryObject('typeBinding', 'DataAgreggationConfigurationConditions', DataAgreggationConfigurationConditions)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataAggregationAction with content type ELEMENT_ONLY
class DataAggregationAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataAggregationAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DataAggregationAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 541, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}dataAggregationActionType uses Python identifier dataAggregationActionType
    __dataAggregationActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'dataAggregationActionType'), 'dataAggregationActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_DataAggregationAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsddataAggregationActionType', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 545, 5), )

    
    dataAggregationActionType = property(__dataAggregationActionType.value, __dataAggregationActionType.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}aditionalRuleParameters uses Python identifier aditionalRuleParameters
    __aditionalRuleParameters = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'aditionalRuleParameters'), 'aditionalRuleParameters', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_DataAggregationAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdaditionalRuleParameters', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 547, 5), )

    
    aditionalRuleParameters = property(__aditionalRuleParameters.value, __aditionalRuleParameters.set, None, None)

    _ElementMap.update({
        __dataAggregationActionType.name() : __dataAggregationActionType,
        __aditionalRuleParameters.name() : __aditionalRuleParameters
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.DataAggregationAction = DataAggregationAction
Namespace.addCategoryObject('typeBinding', 'DataAggregationAction', DataAggregationAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AnonymityAction with content type ELEMENT_ONLY
class AnonymityAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AnonymityAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AnonymityAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 569, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}anonymityActionType uses Python identifier anonymityActionType
    __anonymityActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'anonymityActionType'), 'anonymityActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AnonymityAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdanonymityActionType', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 573, 5), )

    
    anonymityActionType = property(__anonymityActionType.value, __anonymityActionType.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}anonymityTarget uses Python identifier anonymityTarget
    __anonymityTarget = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'anonymityTarget'), 'anonymityTarget', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AnonymityAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdanonymityTarget', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 575, 5), )

    
    anonymityTarget = property(__anonymityTarget.value, __anonymityTarget.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}anonymityTechnologyParameters uses Python identifier anonymityTechnologyParameters
    __anonymityTechnologyParameters = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'anonymityTechnologyParameters'), 'anonymityTechnologyParameters', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AnonymityAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdanonymityTechnologyParameters', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 577, 5), )

    
    anonymityTechnologyParameters = property(__anonymityTechnologyParameters.value, __anonymityTechnologyParameters.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}aditionalAnonymityParameters uses Python identifier aditionalAnonymityParameters
    __aditionalAnonymityParameters = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'aditionalAnonymityParameters'), 'aditionalAnonymityParameters', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AnonymityAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdaditionalAnonymityParameters', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 579, 5), )

    
    aditionalAnonymityParameters = property(__aditionalAnonymityParameters.value, __aditionalAnonymityParameters.set, None, None)

    _ElementMap.update({
        __anonymityActionType.name() : __anonymityActionType,
        __anonymityTarget.name() : __anonymityTarget,
        __anonymityTechnologyParameters.name() : __anonymityTechnologyParameters,
        __aditionalAnonymityParameters.name() : __aditionalAnonymityParameters
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AnonymityAction = AnonymityAction
Namespace.addCategoryObject('typeBinding', 'AnonymityAction', AnonymityAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AnonymityTechnologyParameter with content type EMPTY
class AnonymityTechnologyParameter (TechnologySpecificParameters):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AnonymityTechnologyParameter with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AnonymityTechnologyParameter')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 594, 1)
    _ElementMap = TechnologySpecificParameters._ElementMap.copy()
    _AttributeMap = TechnologySpecificParameters._AttributeMap.copy()
    # Base type is TechnologySpecificParameters
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AnonymityTechnologyParameter = AnonymityTechnologyParameter
Namespace.addCategoryObject('typeBinding', 'AnonymityTechnologyParameter', AnonymityTechnologyParameter)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficMIXERTechnologyParameter with content type ELEMENT_ONLY
class TrafficMIXERTechnologyParameter (TechnologySpecificParameters):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficMIXERTechnologyParameter with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TrafficMIXERTechnologyParameter')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 618, 1)
    _ElementMap = TechnologySpecificParameters._ElementMap.copy()
    _AttributeMap = TechnologySpecificParameters._AttributeMap.copy()
    # Base type is TechnologySpecificParameters
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}GW uses Python identifier GW
    __GW = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'GW'), 'GW', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TrafficMIXERTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdGW', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 622, 5), )

    
    GW = property(__GW.value, __GW.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}peerID uses Python identifier peerID
    __peerID = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'peerID'), 'peerID', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TrafficMIXERTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpeerID', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 623, 5), )

    
    peerID = property(__peerID.value, __peerID.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}key uses Python identifier key
    __key = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'key'), 'key', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TrafficMIXERTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdkey', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 624, 5), )

    
    key = property(__key.value, __key.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}cryptoBackend uses Python identifier cryptoBackend
    __cryptoBackend = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'cryptoBackend'), 'cryptoBackend', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TrafficMIXERTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdcryptoBackend', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 625, 5), )

    
    cryptoBackend = property(__cryptoBackend.value, __cryptoBackend.set, None, None)

    _ElementMap.update({
        __GW.name() : __GW,
        __peerID.name() : __peerID,
        __key.name() : __key,
        __cryptoBackend.name() : __cryptoBackend
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.TrafficMIXERTechnologyParameter = TrafficMIXERTechnologyParameter
Namespace.addCategoryObject('typeBinding', 'TrafficMIXERTechnologyParameter', TrafficMIXERTechnologyParameter)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}MonitoringConfigurationConditions with content type ELEMENT_ONLY
class MonitoringConfigurationConditions (ConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}MonitoringConfigurationConditions with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MonitoringConfigurationConditions')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 633, 1)
    _ElementMap = ConfigurationCondition._ElementMap.copy()
    _AttributeMap = ConfigurationCondition._AttributeMap.copy()
    # Base type is ConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}monitoringConfigurationCondition uses Python identifier monitoringConfigurationCondition
    __monitoringConfigurationCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'monitoringConfigurationCondition'), 'monitoringConfigurationCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_MonitoringConfigurationConditions_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdmonitoringConfigurationCondition', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 637, 5), )

    
    monitoringConfigurationCondition = property(__monitoringConfigurationCondition.value, __monitoringConfigurationCondition.set, None, None)

    _ElementMap.update({
        __monitoringConfigurationCondition.name() : __monitoringConfigurationCondition
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.MonitoringConfigurationConditions = MonitoringConfigurationConditions
Namespace.addCategoryObject('typeBinding', 'MonitoringConfigurationConditions', MonitoringConfigurationConditions)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}MonitoringAction with content type ELEMENT_ONLY
class MonitoringAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}MonitoringAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MonitoringAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 665, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}monitoringActionType uses Python identifier monitoringActionType
    __monitoringActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'monitoringActionType'), 'monitoringActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_MonitoringAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdmonitoringActionType', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 669, 5), )

    
    monitoringActionType = property(__monitoringActionType.value, __monitoringActionType.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ProbeID uses Python identifier ProbeID
    __ProbeID = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ProbeID'), 'ProbeID', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_MonitoringAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdProbeID', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 671, 5), )

    
    ProbeID = property(__ProbeID.value, __ProbeID.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}reportPeriodicity uses Python identifier reportPeriodicity
    __reportPeriodicity = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'reportPeriodicity'), 'reportPeriodicity', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_MonitoringAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdreportPeriodicity', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 672, 5), )

    
    reportPeriodicity = property(__reportPeriodicity.value, __reportPeriodicity.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}reportPerFlow uses Python identifier reportPerFlow
    __reportPerFlow = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'reportPerFlow'), 'reportPerFlow', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_MonitoringAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdreportPerFlow', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 673, 5), )

    
    reportPerFlow = property(__reportPerFlow.value, __reportPerFlow.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ruleID uses Python identifier ruleID
    __ruleID = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ruleID'), 'ruleID', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_MonitoringAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdruleID', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 674, 5), )

    
    ruleID = property(__ruleID.value, __ruleID.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}count uses Python identifier count
    __count = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'count'), 'count', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_MonitoringAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdcount', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 675, 5), )

    
    count = property(__count.value, __count.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}aditionalMonitoringParameters uses Python identifier aditionalMonitoringParameters
    __aditionalMonitoringParameters = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'aditionalMonitoringParameters'), 'aditionalMonitoringParameters', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_MonitoringAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdaditionalMonitoringParameters', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 676, 5), )

    
    aditionalMonitoringParameters = property(__aditionalMonitoringParameters.value, __aditionalMonitoringParameters.set, None, None)

    _ElementMap.update({
        __monitoringActionType.name() : __monitoringActionType,
        __ProbeID.name() : __ProbeID,
        __reportPeriodicity.name() : __reportPeriodicity,
        __reportPerFlow.name() : __reportPerFlow,
        __ruleID.name() : __ruleID,
        __count.name() : __count,
        __aditionalMonitoringParameters.name() : __aditionalMonitoringParameters
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.MonitoringAction = MonitoringAction
Namespace.addCategoryObject('typeBinding', 'MonitoringAction', MonitoringAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}NetworkSlicingConfigurationCondition with content type ELEMENT_ONLY
class NetworkSlicingConfigurationCondition (ConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}NetworkSlicingConfigurationCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'NetworkSlicingConfigurationCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 696, 1)
    _ElementMap = ConfigurationCondition._ElementMap.copy()
    _AttributeMap = ConfigurationCondition._AttributeMap.copy()
    # Base type is ConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}networkSlicingConditionParameters uses Python identifier networkSlicingConditionParameters
    __networkSlicingConditionParameters = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'networkSlicingConditionParameters'), 'networkSlicingConditionParameters', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_NetworkSlicingConfigurationCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdnetworkSlicingConditionParameters', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 700, 5), )

    
    networkSlicingConditionParameters = property(__networkSlicingConditionParameters.value, __networkSlicingConditionParameters.set, None, None)

    _ElementMap.update({
        __networkSlicingConditionParameters.name() : __networkSlicingConditionParameters
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.NetworkSlicingConfigurationCondition = NetworkSlicingConfigurationCondition
Namespace.addCategoryObject('typeBinding', 'NetworkSlicingConfigurationCondition', NetworkSlicingConfigurationCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}NetworkSlicingAction with content type ELEMENT_ONLY
class NetworkSlicingAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}NetworkSlicingAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'NetworkSlicingAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 707, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}NetworkSlicingActionType uses Python identifier NetworkSlicingActionType
    __NetworkSlicingActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'NetworkSlicingActionType'), 'NetworkSlicingActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_NetworkSlicingAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdNetworkSlicingActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 711, 5), )

    
    NetworkSlicingActionType = property(__NetworkSlicingActionType.value, __NetworkSlicingActionType.set, None, None)

    _ElementMap.update({
        __NetworkSlicingActionType.name() : __NetworkSlicingActionType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.NetworkSlicingAction = NetworkSlicingAction
Namespace.addCategoryObject('typeBinding', 'NetworkSlicingAction', NetworkSlicingAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}QoSAction with content type ELEMENT_ONLY
class QoSAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}QoSAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'QoSAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 746, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}qosAction uses Python identifier qosAction
    __qosAction = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'qosAction'), 'qosAction', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_QoSAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdqosAction', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 750, 5), )

    
    qosAction = property(__qosAction.value, __qosAction.set, None, None)

    _ElementMap.update({
        __qosAction.name() : __qosAction
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.QoSAction = QoSAction
Namespace.addCategoryObject('typeBinding', 'QoSAction', QoSAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition with content type ELEMENT_ONLY
class FilteringConfigurationCondition (ConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FilteringConfigurationCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 759, 1)
    _ElementMap = ConfigurationCondition._ElementMap.copy()
    _AttributeMap = ConfigurationCondition._AttributeMap.copy()
    # Base type is ConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition uses Python identifier packetFilterCondition
    __packetFilterCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition'), 'packetFilterCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_FilteringConfigurationCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpacketFilterCondition', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5), )

    
    packetFilterCondition = property(__packetFilterCondition.value, __packetFilterCondition.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}statefulCondition uses Python identifier statefulCondition
    __statefulCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'statefulCondition'), 'statefulCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_FilteringConfigurationCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdstatefulCondition', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5), )

    
    statefulCondition = property(__statefulCondition.value, __statefulCondition.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}timeCondition uses Python identifier timeCondition
    __timeCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'timeCondition'), 'timeCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_FilteringConfigurationCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdtimeCondition', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5), )

    
    timeCondition = property(__timeCondition.value, __timeCondition.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}applicationLayerCondition uses Python identifier applicationLayerCondition
    __applicationLayerCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerCondition'), 'applicationLayerCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_FilteringConfigurationCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdapplicationLayerCondition', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5), )

    
    applicationLayerCondition = property(__applicationLayerCondition.value, __applicationLayerCondition.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}qosCondition uses Python identifier qosCondition
    __qosCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'qosCondition'), 'qosCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_FilteringConfigurationCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdqosCondition', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5), )

    
    qosCondition = property(__qosCondition.value, __qosCondition.set, None, None)

    _ElementMap.update({
        __packetFilterCondition.name() : __packetFilterCondition,
        __statefulCondition.name() : __statefulCondition,
        __timeCondition.name() : __timeCondition,
        __applicationLayerCondition.name() : __applicationLayerCondition,
        __qosCondition.name() : __qosCondition
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.FilteringConfigurationCondition = FilteringConfigurationCondition
Namespace.addCategoryObject('typeBinding', 'FilteringConfigurationCondition', FilteringConfigurationCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IoTApplicationLayerCondition with content type ELEMENT_ONLY
class IoTApplicationLayerCondition (ApplicationLayerCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IoTApplicationLayerCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'IoTApplicationLayerCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 830, 1)
    _ElementMap = ApplicationLayerCondition._ElementMap.copy()
    _AttributeMap = ApplicationLayerCondition._AttributeMap.copy()
    # Base type is ApplicationLayerCondition
    
    # Element applicationProtocol ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}applicationProtocol) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ApplicationLayerCondition
    
    # Element URL ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}URL) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ApplicationLayerCondition
    
    # Element httpCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}httpCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ApplicationLayerCondition
    
    # Element fileExtension ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}fileExtension) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ApplicationLayerCondition
    
    # Element mimeType ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}mimeType) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ApplicationLayerCondition
    
    # Element maxconn ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}maxconn) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ApplicationLayerCondition
    
    # Element dst_domain ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}dst_domain) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ApplicationLayerCondition
    
    # Element src_domain ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}src_domain) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ApplicationLayerCondition
    
    # Element URL_regex ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}URL_regex) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ApplicationLayerCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}method uses Python identifier method
    __method = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'method'), 'method', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IoTApplicationLayerCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdmethod', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 834, 5), )

    
    method = property(__method.value, __method.set, None, None)

    _ElementMap.update({
        __method.name() : __method
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.IoTApplicationLayerCondition = IoTApplicationLayerCondition
Namespace.addCategoryObject('typeBinding', 'IoTApplicationLayerCondition', IoTApplicationLayerCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Anti-malwareCondition with content type ELEMENT_ONLY
class Anti_malwareCondition (ConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Anti-malwareCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Anti-malwareCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 887, 1)
    _ElementMap = ConfigurationCondition._ElementMap.copy()
    _AttributeMap = ConfigurationCondition._AttributeMap.copy()
    # Base type is ConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}fileSystemCondition uses Python identifier fileSystemCondition
    __fileSystemCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'fileSystemCondition'), 'fileSystemCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Anti_malwareCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdfileSystemCondition', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 891, 5), )

    
    fileSystemCondition = property(__fileSystemCondition.value, __fileSystemCondition.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}applicationLayerCondition uses Python identifier applicationLayerCondition
    __applicationLayerCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerCondition'), 'applicationLayerCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Anti_malwareCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdapplicationLayerCondition', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 893, 5), )

    
    applicationLayerCondition = property(__applicationLayerCondition.value, __applicationLayerCondition.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}eventCondition uses Python identifier eventCondition
    __eventCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'eventCondition'), 'eventCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Anti_malwareCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdeventCondition', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 895, 5), )

    
    eventCondition = property(__eventCondition.value, __eventCondition.set, None, None)

    _ElementMap.update({
        __fileSystemCondition.name() : __fileSystemCondition,
        __applicationLayerCondition.name() : __applicationLayerCondition,
        __eventCondition.name() : __eventCondition
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Anti_malwareCondition = Anti_malwareCondition
Namespace.addCategoryObject('typeBinding', 'Anti-malwareCondition', Anti_malwareCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}LoggingCondition with content type ELEMENT_ONLY
class LoggingCondition (ConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}LoggingCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LoggingCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 902, 1)
    _ElementMap = ConfigurationCondition._ElementMap.copy()
    _AttributeMap = ConfigurationCondition._AttributeMap.copy()
    # Base type is ConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}eventCondition uses Python identifier eventCondition
    __eventCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'eventCondition'), 'eventCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_LoggingCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdeventCondition', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 906, 5), )

    
    eventCondition = property(__eventCondition.value, __eventCondition.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}object uses Python identifier object
    __object = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'object'), 'object', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_LoggingCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdobject', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 909, 5), )

    
    object = property(__object.value, __object.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetCondition uses Python identifier packetCondition
    __packetCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'packetCondition'), 'packetCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_LoggingCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpacketCondition', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 912, 5), )

    
    packetCondition = property(__packetCondition.value, __packetCondition.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}applicationCondition uses Python identifier applicationCondition
    __applicationCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'applicationCondition'), 'applicationCondition', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_LoggingCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdapplicationCondition', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 914, 5), )

    
    applicationCondition = property(__applicationCondition.value, __applicationCondition.set, None, None)

    _ElementMap.update({
        __eventCondition.name() : __eventCondition,
        __object.name() : __object,
        __packetCondition.name() : __packetCondition,
        __applicationCondition.name() : __applicationCondition
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.LoggingCondition = LoggingCondition
Namespace.addCategoryObject('typeBinding', 'LoggingCondition', LoggingCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficDivertAction with content type ELEMENT_ONLY
class TrafficDivertAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficDivertAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TrafficDivertAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 963, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficDivertActionType uses Python identifier TrafficDivertActionType
    __TrafficDivertActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'TrafficDivertActionType'), 'TrafficDivertActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TrafficDivertAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdTrafficDivertActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 967, 5), )

    
    TrafficDivertActionType = property(__TrafficDivertActionType.value, __TrafficDivertActionType.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetDivertAction uses Python identifier packetDivertAction
    __packetDivertAction = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'packetDivertAction'), 'packetDivertAction', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TrafficDivertAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpacketDivertAction', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 969, 5), )

    
    packetDivertAction = property(__packetDivertAction.value, __packetDivertAction.set, None, None)

    _ElementMap.update({
        __TrafficDivertActionType.name() : __TrafficDivertActionType,
        __packetDivertAction.name() : __packetDivertAction
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.TrafficDivertAction = TrafficDivertAction
Namespace.addCategoryObject('typeBinding', 'TrafficDivertAction', TrafficDivertAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringAction with content type ELEMENT_ONLY
class FilteringAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FilteringAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 995, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringActionType uses Python identifier FilteringActionType
    __FilteringActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'FilteringActionType'), 'FilteringActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_FilteringAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdFilteringActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 999, 5), )

    
    FilteringActionType = property(__FilteringActionType.value, __FilteringActionType.set, None, None)

    _ElementMap.update({
        __FilteringActionType.name() : __FilteringActionType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.FilteringAction = FilteringAction
Namespace.addCategoryObject('typeBinding', 'FilteringAction', FilteringAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EnableAction with content type ELEMENT_ONLY
class EnableAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EnableAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EnableAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1006, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EnableActionType uses Python identifier EnableActionType
    __EnableActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'EnableActionType'), 'EnableActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_EnableAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdEnableActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1010, 5), )

    
    EnableActionType = property(__EnableActionType.value, __EnableActionType.set, None, None)

    _ElementMap.update({
        __EnableActionType.name() : __EnableActionType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.EnableAction = EnableAction
Namespace.addCategoryObject('typeBinding', 'EnableAction', EnableAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ReduceBandwidthAction with content type ELEMENT_ONLY
class ReduceBandwidthAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ReduceBandwidthAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ReduceBandwidthAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1017, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ReduceBandwidthActionType uses Python identifier ReduceBandwidthActionType
    __ReduceBandwidthActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ReduceBandwidthActionType'), 'ReduceBandwidthActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ReduceBandwidthAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdReduceBandwidthActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1021, 5), )

    
    ReduceBandwidthActionType = property(__ReduceBandwidthActionType.value, __ReduceBandwidthActionType.set, None, None)

    _ElementMap.update({
        __ReduceBandwidthActionType.name() : __ReduceBandwidthActionType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ReduceBandwidthAction = ReduceBandwidthAction
Namespace.addCategoryObject('typeBinding', 'ReduceBandwidthAction', ReduceBandwidthAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}CountAction with content type ELEMENT_ONLY
class CountAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}CountAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CountAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1030, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}CountActionType uses Python identifier CountActionType
    __CountActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'CountActionType'), 'CountActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_CountAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdCountActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1034, 5), )

    
    CountActionType = property(__CountActionType.value, __CountActionType.set, None, None)

    _ElementMap.update({
        __CountActionType.name() : __CountActionType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CountAction = CountAction
Namespace.addCategoryObject('typeBinding', 'CountAction', CountAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}CheckAction with content type ELEMENT_ONLY
class CheckAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}CheckAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'CheckAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1041, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}CheckActionType uses Python identifier CheckActionType
    __CheckActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'CheckActionType'), 'CheckActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_CheckAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdCheckActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1045, 5), )

    
    CheckActionType = property(__CheckActionType.value, __CheckActionType.set, None, None)

    _ElementMap.update({
        __CheckActionType.name() : __CheckActionType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.CheckAction = CheckAction
Namespace.addCategoryObject('typeBinding', 'CheckAction', CheckAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveAction with content type ELEMENT_ONLY
class RemoveAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RemoveAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1051, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveActionType uses Python identifier RemoveActionType
    __RemoveActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'RemoveActionType'), 'RemoveActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RemoveAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdRemoveActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1055, 5), )

    
    RemoveActionType = property(__RemoveActionType.value, __RemoveActionType.set, None, None)

    _ElementMap.update({
        __RemoveActionType.name() : __RemoveActionType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.RemoveAction = RemoveAction
Namespace.addCategoryObject('typeBinding', 'RemoveAction', RemoveAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Anti-malwareAction with content type ELEMENT_ONLY
class Anti_malwareAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Anti-malwareAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Anti-malwareAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1061, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}anti-malwareActionType uses Python identifier anti_malwareActionType
    __anti_malwareActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'anti-malwareActionType'), 'anti_malwareActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Anti_malwareAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdanti_malwareActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1065, 5), )

    
    anti_malwareActionType = property(__anti_malwareActionType.value, __anti_malwareActionType.set, None, None)

    _ElementMap.update({
        __anti_malwareActionType.name() : __anti_malwareActionType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Anti_malwareAction = Anti_malwareAction
Namespace.addCategoryObject('typeBinding', 'Anti-malwareAction', Anti_malwareAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}LoggingAction with content type ELEMENT_ONLY
class LoggingAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}LoggingAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LoggingAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1072, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}loggingActionType uses Python identifier loggingActionType
    __loggingActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'loggingActionType'), 'loggingActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_LoggingAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdloggingActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1076, 5), )

    
    loggingActionType = property(__loggingActionType.value, __loggingActionType.set, None, None)

    _ElementMap.update({
        __loggingActionType.name() : __loggingActionType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.LoggingAction = LoggingAction
Namespace.addCategoryObject('typeBinding', 'LoggingAction', LoggingAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}BootstrappingAction with content type EMPTY
class BootstrappingAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}BootstrappingAction with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BootstrappingAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1142, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.BootstrappingAction = BootstrappingAction
Namespace.addCategoryObject('typeBinding', 'BootstrappingAction', BootstrappingAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PowerMgmtAction with content type ELEMENT_ONLY
class PowerMgmtAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PowerMgmtAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PowerMgmtAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1148, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PowerMgmtActionType uses Python identifier PowerMgmtActionType
    __PowerMgmtActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'PowerMgmtActionType'), 'PowerMgmtActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PowerMgmtAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdPowerMgmtActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1152, 5), )

    
    PowerMgmtActionType = property(__PowerMgmtActionType.value, __PowerMgmtActionType.set, None, None)

    _ElementMap.update({
        __PowerMgmtActionType.name() : __PowerMgmtActionType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PowerMgmtAction = PowerMgmtAction
Namespace.addCategoryObject('typeBinding', 'PowerMgmtAction', PowerMgmtAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}VIoTHoneyNetAction with content type ELEMENT_ONLY
class VIoTHoneyNetAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}VIoTHoneyNetAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VIoTHoneyNetAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1165, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}VIoTHoneyNetActionType uses Python identifier VIoTHoneyNetActionType
    __VIoTHoneyNetActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'VIoTHoneyNetActionType'), 'VIoTHoneyNetActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_VIoTHoneyNetAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdVIoTHoneyNetActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1169, 5), )

    
    VIoTHoneyNetActionType = property(__VIoTHoneyNetActionType.value, __VIoTHoneyNetActionType.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ioTHoneyNet uses Python identifier ioTHoneyNet
    __ioTHoneyNet = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ioTHoneyNet'), 'ioTHoneyNet', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_VIoTHoneyNetAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdioTHoneyNet', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1170, 5), )

    
    ioTHoneyNet = property(__ioTHoneyNet.value, __ioTHoneyNet.set, None, None)

    _ElementMap.update({
        __VIoTHoneyNetActionType.name() : __VIoTHoneyNetActionType,
        __ioTHoneyNet.name() : __ioTHoneyNet
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.VIoTHoneyNetAction = VIoTHoneyNetAction
Namespace.addCategoryObject('typeBinding', 'VIoTHoneyNetAction', VIoTHoneyNetAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthorizationAction with content type ELEMENT_ONLY
class AuthorizationAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthorizationAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AuthorizationAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1184, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthorizationActionType uses Python identifier AuthorizationActionType
    __AuthorizationActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationActionType'), 'AuthorizationActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthorizationAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdAuthorizationActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1188, 5), )

    
    AuthorizationActionType = property(__AuthorizationActionType.value, __AuthorizationActionType.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthorizationSubject uses Python identifier AuthorizationSubject
    __AuthorizationSubject = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationSubject'), 'AuthorizationSubject', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthorizationAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdAuthorizationSubject', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1190, 5), )

    
    AuthorizationSubject = property(__AuthorizationSubject.value, __AuthorizationSubject.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthorizationTarget uses Python identifier AuthorizationTarget
    __AuthorizationTarget = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationTarget'), 'AuthorizationTarget', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthorizationAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdAuthorizationTarget', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1191, 5), )

    
    AuthorizationTarget = property(__AuthorizationTarget.value, __AuthorizationTarget.set, None, None)

    _ElementMap.update({
        __AuthorizationActionType.name() : __AuthorizationActionType,
        __AuthorizationSubject.name() : __AuthorizationSubject,
        __AuthorizationTarget.name() : __AuthorizationTarget
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AuthorizationAction = AuthorizationAction
Namespace.addCategoryObject('typeBinding', 'AuthorizationAction', AuthorizationAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationAction with content type ELEMENT_ONLY
class AuthenticationAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AuthenticationAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1217, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationOption uses Python identifier AuthenticationOption
    __AuthenticationOption = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationOption'), 'AuthenticationOption', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdAuthenticationOption', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1222, 5), )

    
    AuthenticationOption = property(__AuthenticationOption.value, __AuthenticationOption.set, None, None)

    _ElementMap.update({
        __AuthenticationOption.name() : __AuthenticationOption
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AuthenticationAction = AuthenticationAction
Namespace.addCategoryObject('typeBinding', 'AuthenticationAction', AuthenticationAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RSAC with content type EMPTY
class RSAC (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RSAC with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RSAC')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1306, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute RSACviolence uses Python identifier RSACviolence
    __RSACviolence = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'RSACviolence'), 'RSACviolence', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RSAC_RSACviolence', _module_typeBindings.STD_ANON_3)
    __RSACviolence._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1307, 2)
    __RSACviolence._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1307, 2)
    
    RSACviolence = property(__RSACviolence.value, __RSACviolence.set, None, None)

    
    # Attribute RSACsex uses Python identifier RSACsex
    __RSACsex = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'RSACsex'), 'RSACsex', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RSAC_RSACsex', _module_typeBindings.STD_ANON_4)
    __RSACsex._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1316, 2)
    __RSACsex._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1316, 2)
    
    RSACsex = property(__RSACsex.value, __RSACsex.set, None, None)

    
    # Attribute RSACnudity uses Python identifier RSACnudity
    __RSACnudity = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'RSACnudity'), 'RSACnudity', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RSAC_RSACnudity', _module_typeBindings.STD_ANON_5)
    __RSACnudity._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1324, 2)
    __RSACnudity._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1324, 2)
    
    RSACnudity = property(__RSACnudity.value, __RSACnudity.set, None, None)

    
    # Attribute RSAClanguage uses Python identifier RSAClanguage
    __RSAClanguage = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'RSAClanguage'), 'RSAClanguage', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RSAC_RSAClanguage', _module_typeBindings.STD_ANON_6)
    __RSAClanguage._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1332, 2)
    __RSAClanguage._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1332, 2)
    
    RSAClanguage = property(__RSAClanguage.value, __RSAClanguage.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __RSACviolence.name() : __RSACviolence,
        __RSACsex.name() : __RSACsex,
        __RSACnudity.name() : __RSACnudity,
        __RSAClanguage.name() : __RSAClanguage
    })
_module_typeBindings.RSAC = RSAC
Namespace.addCategoryObject('typeBinding', 'RSAC', RSAC)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}SafeNet with content type EMPTY
class SafeNet (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}SafeNet with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'SafeNet')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1342, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute SafeSurfprofanity uses Python identifier SafeSurfprofanity
    __SafeSurfprofanity = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'SafeSurfprofanity'), 'SafeSurfprofanity', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_SafeNet_SafeSurfprofanity', _module_typeBindings.STD_ANON_7)
    __SafeSurfprofanity._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1343, 2)
    __SafeSurfprofanity._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1343, 2)
    
    SafeSurfprofanity = property(__SafeSurfprofanity.value, __SafeSurfprofanity.set, None, None)

    
    # Attribute SafeSurfheterosexualthemes uses Python identifier SafeSurfheterosexualthemes
    __SafeSurfheterosexualthemes = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'SafeSurfheterosexualthemes'), 'SafeSurfheterosexualthemes', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_SafeNet_SafeSurfheterosexualthemes', _module_typeBindings.STD_ANON_8)
    __SafeSurfheterosexualthemes._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1351, 2)
    __SafeSurfheterosexualthemes._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1351, 2)
    
    SafeSurfheterosexualthemes = property(__SafeSurfheterosexualthemes.value, __SafeSurfheterosexualthemes.set, None, None)

    
    # Attribute SafeSurfhomosexualthemes uses Python identifier SafeSurfhomosexualthemes
    __SafeSurfhomosexualthemes = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'SafeSurfhomosexualthemes'), 'SafeSurfhomosexualthemes', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_SafeNet_SafeSurfhomosexualthemes', _module_typeBindings.STD_ANON_9)
    __SafeSurfhomosexualthemes._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1359, 2)
    __SafeSurfhomosexualthemes._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1359, 2)
    
    SafeSurfhomosexualthemes = property(__SafeSurfhomosexualthemes.value, __SafeSurfhomosexualthemes.set, None, None)

    
    # Attribute SafeSurfviolence uses Python identifier SafeSurfviolence
    __SafeSurfviolence = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'SafeSurfviolence'), 'SafeSurfviolence', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_SafeNet_SafeSurfviolence', _module_typeBindings.STD_ANON_10)
    __SafeSurfviolence._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1368, 2)
    __SafeSurfviolence._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1368, 2)
    
    SafeSurfviolence = property(__SafeSurfviolence.value, __SafeSurfviolence.set, None, None)

    
    # Attribute SafeSurfdruguse uses Python identifier SafeSurfdruguse
    __SafeSurfdruguse = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'SafeSurfdruguse'), 'SafeSurfdruguse', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_SafeNet_SafeSurfdruguse', _module_typeBindings.STD_ANON_11)
    __SafeSurfdruguse._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1377, 2)
    __SafeSurfdruguse._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1377, 2)
    
    SafeSurfdruguse = property(__SafeSurfdruguse.value, __SafeSurfdruguse.set, None, None)

    
    # Attribute SafeSurfotheradultthemes uses Python identifier SafeSurfotheradultthemes
    __SafeSurfotheradultthemes = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'SafeSurfotheradultthemes'), 'SafeSurfotheradultthemes', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_SafeNet_SafeSurfotheradultthemes', _module_typeBindings.STD_ANON_12)
    __SafeSurfotheradultthemes._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1386, 2)
    __SafeSurfotheradultthemes._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1386, 2)
    
    SafeSurfotheradultthemes = property(__SafeSurfotheradultthemes.value, __SafeSurfotheradultthemes.set, None, None)

    
    # Attribute SafeSurfgambling uses Python identifier SafeSurfgambling
    __SafeSurfgambling = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'SafeSurfgambling'), 'SafeSurfgambling', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_SafeNet_SafeSurfgambling', _module_typeBindings.STD_ANON_13)
    __SafeSurfgambling._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1394, 2)
    __SafeSurfgambling._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1394, 2)
    
    SafeSurfgambling = property(__SafeSurfgambling.value, __SafeSurfgambling.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __SafeSurfprofanity.name() : __SafeSurfprofanity,
        __SafeSurfheterosexualthemes.name() : __SafeSurfheterosexualthemes,
        __SafeSurfhomosexualthemes.name() : __SafeSurfhomosexualthemes,
        __SafeSurfviolence.name() : __SafeSurfviolence,
        __SafeSurfdruguse.name() : __SafeSurfdruguse,
        __SafeSurfotheradultthemes.name() : __SafeSurfotheradultthemes,
        __SafeSurfgambling.name() : __SafeSurfgambling
    })
_module_typeBindings.SafeNet = SafeNet
Namespace.addCategoryObject('typeBinding', 'SafeNet', SafeNet)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Vancouver with content type EMPTY
class Vancouver (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Vancouver with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Vancouver')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1404, 1)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute Vancouvereducationalcontent uses Python identifier Vancouvereducationalcontent
    __Vancouvereducationalcontent = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'Vancouvereducationalcontent'), 'Vancouvereducationalcontent', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Vancouver_Vancouvereducationalcontent', _module_typeBindings.STD_ANON_14)
    __Vancouvereducationalcontent._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1405, 2)
    __Vancouvereducationalcontent._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1405, 2)
    
    Vancouvereducationalcontent = property(__Vancouvereducationalcontent.value, __Vancouvereducationalcontent.set, None, None)

    
    # Attribute Vancouverviolence uses Python identifier Vancouverviolence
    __Vancouverviolence = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'Vancouverviolence'), 'Vancouverviolence', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Vancouver_Vancouverviolence', _module_typeBindings.STD_ANON_15)
    __Vancouverviolence._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1413, 2)
    __Vancouverviolence._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1413, 2)
    
    Vancouverviolence = property(__Vancouverviolence.value, __Vancouverviolence.set, None, None)

    
    # Attribute Vancouversex uses Python identifier Vancouversex
    __Vancouversex = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'Vancouversex'), 'Vancouversex', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Vancouver_Vancouversex', _module_typeBindings.STD_ANON_16)
    __Vancouversex._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1421, 2)
    __Vancouversex._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1421, 2)
    
    Vancouversex = property(__Vancouversex.value, __Vancouversex.set, None, None)

    
    # Attribute Vancouverprofanity uses Python identifier Vancouverprofanity
    __Vancouverprofanity = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'Vancouverprofanity'), 'Vancouverprofanity', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Vancouver_Vancouverprofanity', _module_typeBindings.STD_ANON_17)
    __Vancouverprofanity._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1429, 2)
    __Vancouverprofanity._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1429, 2)
    
    Vancouverprofanity = property(__Vancouverprofanity.value, __Vancouverprofanity.set, None, None)

    
    # Attribute Vancouvergambling uses Python identifier Vancouvergambling
    __Vancouvergambling = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'Vancouvergambling'), 'Vancouvergambling', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Vancouver_Vancouvergambling', _module_typeBindings.STD_ANON_18)
    __Vancouvergambling._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1437, 2)
    __Vancouvergambling._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1437, 2)
    
    Vancouvergambling = property(__Vancouvergambling.value, __Vancouvergambling.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __Vancouvereducationalcontent.name() : __Vancouvereducationalcontent,
        __Vancouverviolence.name() : __Vancouverviolence,
        __Vancouversex.name() : __Vancouversex,
        __Vancouverprofanity.name() : __Vancouverprofanity,
        __Vancouvergambling.name() : __Vancouvergambling
    })
_module_typeBindings.Vancouver = Vancouver
Namespace.addCategoryObject('typeBinding', 'Vancouver', Vancouver)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionAction with content type ELEMENT_ONLY
class DataProtectionAction (ConfigurationAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DataProtectionAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1447, 1)
    _ElementMap = ConfigurationAction._ElementMap.copy()
    _AttributeMap = ConfigurationAction._AttributeMap.copy()
    # Base type is ConfigurationAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}technology uses Python identifier technology
    __technology = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'technology'), 'technology', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_DataProtectionAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdtechnology', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1451, 5), )

    
    technology = property(__technology.value, __technology.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}technologyActionParameters uses Python identifier technologyActionParameters
    __technologyActionParameters = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'technologyActionParameters'), 'technologyActionParameters', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_DataProtectionAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdtechnologyActionParameters', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1452, 5), )

    
    technologyActionParameters = property(__technologyActionParameters.value, __technologyActionParameters.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}technologyActionSecurityProperty uses Python identifier technologyActionSecurityProperty
    __technologyActionSecurityProperty = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'technologyActionSecurityProperty'), 'technologyActionSecurityProperty', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_DataProtectionAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdtechnologyActionSecurityProperty', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1454, 5), )

    
    technologyActionSecurityProperty = property(__technologyActionSecurityProperty.value, __technologyActionSecurityProperty.set, None, None)

    _ElementMap.update({
        __technology.name() : __technology,
        __technologyActionParameters.name() : __technologyActionParameters,
        __technologyActionSecurityProperty.name() : __technologyActionSecurityProperty
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.DataProtectionAction = DataProtectionAction
Namespace.addCategoryObject('typeBinding', 'DataProtectionAction', DataProtectionAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Integrity with content type ELEMENT_ONLY
class Integrity (TechnologyActionSecurityProperty):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Integrity with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Integrity')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1462, 1)
    _ElementMap = TechnologyActionSecurityProperty._ElementMap.copy()
    _AttributeMap = TechnologyActionSecurityProperty._AttributeMap.copy()
    # Base type is TechnologyActionSecurityProperty
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}integrityAlgorithm uses Python identifier integrityAlgorithm
    __integrityAlgorithm = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'integrityAlgorithm'), 'integrityAlgorithm', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Integrity_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdintegrityAlgorithm', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1466, 5), )

    
    integrityAlgorithm = property(__integrityAlgorithm.value, __integrityAlgorithm.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}integrityHeader uses Python identifier integrityHeader
    __integrityHeader = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'integrityHeader'), 'integrityHeader', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Integrity_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdintegrityHeader', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1467, 5), )

    
    integrityHeader = property(__integrityHeader.value, __integrityHeader.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}integrityPayload uses Python identifier integrityPayload
    __integrityPayload = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'integrityPayload'), 'integrityPayload', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Integrity_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdintegrityPayload', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1468, 5), )

    
    integrityPayload = property(__integrityPayload.value, __integrityPayload.set, None, None)

    _ElementMap.update({
        __integrityAlgorithm.name() : __integrityAlgorithm,
        __integrityHeader.name() : __integrityHeader,
        __integrityPayload.name() : __integrityPayload
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Integrity = Integrity
Namespace.addCategoryObject('typeBinding', 'Integrity', Integrity)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Authentication with content type ELEMENT_ONLY
class Authentication (TechnologyActionSecurityProperty):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Authentication with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Authentication')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1473, 1)
    _ElementMap = TechnologyActionSecurityProperty._ElementMap.copy()
    _AttributeMap = TechnologyActionSecurityProperty._AttributeMap.copy()
    # Base type is TechnologyActionSecurityProperty
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}serverAuthenticationMechanism uses Python identifier serverAuthenticationMechanism
    __serverAuthenticationMechanism = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'serverAuthenticationMechanism'), 'serverAuthenticationMechanism', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Authentication_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdserverAuthenticationMechanism', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1477, 5), )

    
    serverAuthenticationMechanism = property(__serverAuthenticationMechanism.value, __serverAuthenticationMechanism.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}clientAuthenticationMechanism uses Python identifier clientAuthenticationMechanism
    __clientAuthenticationMechanism = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'clientAuthenticationMechanism'), 'clientAuthenticationMechanism', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Authentication_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdclientAuthenticationMechanism', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1479, 5), )

    
    clientAuthenticationMechanism = property(__clientAuthenticationMechanism.value, __clientAuthenticationMechanism.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}peerAuthenticationMechanism uses Python identifier peerAuthenticationMechanism
    __peerAuthenticationMechanism = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'peerAuthenticationMechanism'), 'peerAuthenticationMechanism', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Authentication_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpeerAuthenticationMechanism', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1481, 5), )

    
    peerAuthenticationMechanism = property(__peerAuthenticationMechanism.value, __peerAuthenticationMechanism.set, None, None)

    _ElementMap.update({
        __serverAuthenticationMechanism.name() : __serverAuthenticationMechanism,
        __clientAuthenticationMechanism.name() : __clientAuthenticationMechanism,
        __peerAuthenticationMechanism.name() : __peerAuthenticationMechanism
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Authentication = Authentication
Namespace.addCategoryObject('typeBinding', 'Authentication', Authentication)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Confidentiality with content type ELEMENT_ONLY
class Confidentiality (TechnologyActionSecurityProperty):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Confidentiality with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Confidentiality')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1487, 1)
    _ElementMap = TechnologyActionSecurityProperty._ElementMap.copy()
    _AttributeMap = TechnologyActionSecurityProperty._AttributeMap.copy()
    # Base type is TechnologyActionSecurityProperty
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}encryptionAlgorithm uses Python identifier encryptionAlgorithm
    __encryptionAlgorithm = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'encryptionAlgorithm'), 'encryptionAlgorithm', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Confidentiality_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdencryptionAlgorithm', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1491, 5), )

    
    encryptionAlgorithm = property(__encryptionAlgorithm.value, __encryptionAlgorithm.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}keySize uses Python identifier keySize
    __keySize = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'keySize'), 'keySize', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Confidentiality_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdkeySize', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1492, 5), )

    
    keySize = property(__keySize.value, __keySize.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}mode uses Python identifier mode
    __mode = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'mode'), 'mode', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Confidentiality_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdmode', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1493, 5), )

    
    mode = property(__mode.value, __mode.set, None, None)

    _ElementMap.update({
        __encryptionAlgorithm.name() : __encryptionAlgorithm,
        __keySize.name() : __keySize,
        __mode.name() : __mode
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Confidentiality = Confidentiality
Namespace.addCategoryObject('typeBinding', 'Confidentiality', Confidentiality)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IPsecTechnologyParameter with content type ELEMENT_ONLY
class IPsecTechnologyParameter (TechnologySpecificParameters):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IPsecTechnologyParameter with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'IPsecTechnologyParameter')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1520, 1)
    _ElementMap = TechnologySpecificParameters._ElementMap.copy()
    _AttributeMap = TechnologySpecificParameters._AttributeMap.copy()
    # Base type is TechnologySpecificParameters
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IPsecProtocol uses Python identifier IPsecProtocol
    __IPsecProtocol = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'IPsecProtocol'), 'IPsecProtocol', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IPsecTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdIPsecProtocol', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1524, 5), )

    
    IPsecProtocol = property(__IPsecProtocol.value, __IPsecProtocol.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isTunnel uses Python identifier isTunnel
    __isTunnel = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'isTunnel'), 'isTunnel', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IPsecTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdisTunnel', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1525, 5), )

    
    isTunnel = property(__isTunnel.value, __isTunnel.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}localEndpoint uses Python identifier localEndpoint
    __localEndpoint = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'localEndpoint'), 'localEndpoint', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IPsecTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdlocalEndpoint', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1526, 5), )

    
    localEndpoint = property(__localEndpoint.value, __localEndpoint.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}remoteEndpoint uses Python identifier remoteEndpoint
    __remoteEndpoint = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'remoteEndpoint'), 'remoteEndpoint', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IPsecTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdremoteEndpoint', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1527, 5), )

    
    remoteEndpoint = property(__remoteEndpoint.value, __remoteEndpoint.set, None, None)

    _ElementMap.update({
        __IPsecProtocol.name() : __IPsecProtocol,
        __isTunnel.name() : __isTunnel,
        __localEndpoint.name() : __localEndpoint,
        __remoteEndpoint.name() : __remoteEndpoint
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.IPsecTechnologyParameter = IPsecTechnologyParameter
Namespace.addCategoryObject('typeBinding', 'IPsecTechnologyParameter', IPsecTechnologyParameter)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IKETechnologyParameter with content type ELEMENT_ONLY
class IKETechnologyParameter (TechnologySpecificParameters):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IKETechnologyParameter with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'IKETechnologyParameter')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1543, 1)
    _ElementMap = TechnologySpecificParameters._ElementMap.copy()
    _AttributeMap = TechnologySpecificParameters._AttributeMap.copy()
    # Base type is TechnologySpecificParameters
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}phase2_pfs_group uses Python identifier phase2_pfs_group
    __phase2_pfs_group = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'phase2_pfs_group'), 'phase2_pfs_group', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IKETechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdphase2_pfs_group', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1547, 5), )

    
    phase2_pfs_group = property(__phase2_pfs_group.value, __phase2_pfs_group.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}exchangeMode uses Python identifier exchangeMode
    __exchangeMode = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'exchangeMode'), 'exchangeMode', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IKETechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdexchangeMode', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1548, 5), )

    
    exchangeMode = property(__exchangeMode.value, __exchangeMode.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}phase1_dh_group uses Python identifier phase1_dh_group
    __phase1_dh_group = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'phase1_dh_group'), 'phase1_dh_group', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IKETechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdphase1_dh_group', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1549, 5), )

    
    phase1_dh_group = property(__phase1_dh_group.value, __phase1_dh_group.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}phase2_compression_algorithm uses Python identifier phase2_compression_algorithm
    __phase2_compression_algorithm = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'phase2_compression_algorithm'), 'phase2_compression_algorithm', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IKETechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdphase2_compression_algorithm', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1550, 5), )

    
    phase2_compression_algorithm = property(__phase2_compression_algorithm.value, __phase2_compression_algorithm.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}hash_algorithm uses Python identifier hash_algorithm
    __hash_algorithm = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'hash_algorithm'), 'hash_algorithm', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IKETechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdhash_algorithm', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1552, 5), )

    
    hash_algorithm = property(__hash_algorithm.value, __hash_algorithm.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ESN uses Python identifier ESN
    __ESN = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ESN'), 'ESN', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IKETechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdESN', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1553, 5), )

    
    ESN = property(__ESN.value, __ESN.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}encryptionAlgorithm uses Python identifier encryptionAlgorithm
    __encryptionAlgorithm = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'encryptionAlgorithm'), 'encryptionAlgorithm', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IKETechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdencryptionAlgorithm', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1554, 5), )

    
    encryptionAlgorithm = property(__encryptionAlgorithm.value, __encryptionAlgorithm.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}lifetime uses Python identifier lifetime
    __lifetime = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'lifetime'), 'lifetime', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IKETechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdlifetime', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1555, 5), )

    
    lifetime = property(__lifetime.value, __lifetime.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}rekey_margin uses Python identifier rekey_margin
    __rekey_margin = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'rekey_margin'), 'rekey_margin', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IKETechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdrekey_margin', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1556, 5), )

    
    rekey_margin = property(__rekey_margin.value, __rekey_margin.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}keyring_tries uses Python identifier keyring_tries
    __keyring_tries = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'keyring_tries'), 'keyring_tries', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IKETechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdkeyring_tries', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1557, 5), )

    
    keyring_tries = property(__keyring_tries.value, __keyring_tries.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}MOBIKE uses Python identifier MOBIKE
    __MOBIKE = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'MOBIKE'), 'MOBIKE', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_IKETechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdMOBIKE', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1558, 5), )

    
    MOBIKE = property(__MOBIKE.value, __MOBIKE.set, None, None)

    _ElementMap.update({
        __phase2_pfs_group.name() : __phase2_pfs_group,
        __exchangeMode.name() : __exchangeMode,
        __phase1_dh_group.name() : __phase1_dh_group,
        __phase2_compression_algorithm.name() : __phase2_compression_algorithm,
        __hash_algorithm.name() : __hash_algorithm,
        __ESN.name() : __ESN,
        __encryptionAlgorithm.name() : __encryptionAlgorithm,
        __lifetime.name() : __lifetime,
        __rekey_margin.name() : __rekey_margin,
        __keyring_tries.name() : __keyring_tries,
        __MOBIKE.name() : __MOBIKE
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.IKETechnologyParameter = IKETechnologyParameter
Namespace.addCategoryObject('typeBinding', 'IKETechnologyParameter', IKETechnologyParameter)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TLS_VPN_TechnologyParameter with content type ELEMENT_ONLY
class TLS_VPN_TechnologyParameter (TechnologySpecificParameters):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TLS_VPN_TechnologyParameter with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TLS_VPN_TechnologyParameter')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1563, 1)
    _ElementMap = TechnologySpecificParameters._ElementMap.copy()
    _AttributeMap = TechnologySpecificParameters._AttributeMap.copy()
    # Base type is TechnologySpecificParameters
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}peerPort uses Python identifier peerPort
    __peerPort = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'peerPort'), 'peerPort', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TLS_VPN_TechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpeerPort', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1567, 5), )

    
    peerPort = property(__peerPort.value, __peerPort.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}L4Protocol uses Python identifier L4Protocol
    __L4Protocol = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'L4Protocol'), 'L4Protocol', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TLS_VPN_TechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdL4Protocol', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1568, 5), )

    
    L4Protocol = property(__L4Protocol.value, __L4Protocol.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}localEndpoint uses Python identifier localEndpoint
    __localEndpoint = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'localEndpoint'), 'localEndpoint', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TLS_VPN_TechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdlocalEndpoint', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1569, 5), )

    
    localEndpoint = property(__localEndpoint.value, __localEndpoint.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}remoteEndpoint uses Python identifier remoteEndpoint
    __remoteEndpoint = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'remoteEndpoint'), 'remoteEndpoint', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TLS_VPN_TechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdremoteEndpoint', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1570, 5), )

    
    remoteEndpoint = property(__remoteEndpoint.value, __remoteEndpoint.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}virtualIPSource uses Python identifier virtualIPSource
    __virtualIPSource = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'virtualIPSource'), 'virtualIPSource', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TLS_VPN_TechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdvirtualIPSource', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1571, 5), )

    
    virtualIPSource = property(__virtualIPSource.value, __virtualIPSource.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}virtualIPDestination uses Python identifier virtualIPDestination
    __virtualIPDestination = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'virtualIPDestination'), 'virtualIPDestination', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TLS_VPN_TechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdvirtualIPDestination', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1572, 5), )

    
    virtualIPDestination = property(__virtualIPDestination.value, __virtualIPDestination.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}device uses Python identifier device
    __device = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'device'), 'device', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TLS_VPN_TechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsddevice', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1573, 5), )

    
    device = property(__device.value, __device.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}tlsMode uses Python identifier tlsMode
    __tlsMode = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'tlsMode'), 'tlsMode', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TLS_VPN_TechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdtlsMode', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1574, 5), )

    
    tlsMode = property(__tlsMode.value, __tlsMode.set, None, None)

    _ElementMap.update({
        __peerPort.name() : __peerPort,
        __L4Protocol.name() : __L4Protocol,
        __localEndpoint.name() : __localEndpoint,
        __remoteEndpoint.name() : __remoteEndpoint,
        __virtualIPSource.name() : __virtualIPSource,
        __virtualIPDestination.name() : __virtualIPDestination,
        __device.name() : __device,
        __tlsMode.name() : __tlsMode
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.TLS_VPN_TechnologyParameter = TLS_VPN_TechnologyParameter
Namespace.addCategoryObject('typeBinding', 'TLS_VPN_TechnologyParameter', TLS_VPN_TechnologyParameter)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TLS_SSL_TechnologyParameter with content type ELEMENT_ONLY
class TLS_SSL_TechnologyParameter (TechnologySpecificParameters):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TLS_SSL_TechnologyParameter with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TLS_SSL_TechnologyParameter')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1579, 1)
    _ElementMap = TechnologySpecificParameters._ElementMap.copy()
    _AttributeMap = TechnologySpecificParameters._AttributeMap.copy()
    # Base type is TechnologySpecificParameters
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ciphers-client uses Python identifier ciphers_client
    __ciphers_client = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ciphers-client'), 'ciphers_client', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TLS_SSL_TechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdciphers_client', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1583, 5), )

    
    ciphers_client = property(__ciphers_client.value, __ciphers_client.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ssl-version-client uses Python identifier ssl_version_client
    __ssl_version_client = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ssl-version-client'), 'ssl_version_client', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TLS_SSL_TechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdssl_version_client', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1584, 5), )

    
    ssl_version_client = property(__ssl_version_client.value, __ssl_version_client.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ciphers-server uses Python identifier ciphers_server
    __ciphers_server = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ciphers-server'), 'ciphers_server', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TLS_SSL_TechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdciphers_server', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1585, 5), )

    
    ciphers_server = property(__ciphers_server.value, __ciphers_server.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ssl-version-server uses Python identifier ssl_version_server
    __ssl_version_server = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'ssl-version-server'), 'ssl_version_server', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TLS_SSL_TechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdssl_version_server', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1586, 5), )

    
    ssl_version_server = property(__ssl_version_server.value, __ssl_version_server.set, None, None)

    _ElementMap.update({
        __ciphers_client.name() : __ciphers_client,
        __ssl_version_client.name() : __ssl_version_client,
        __ciphers_server.name() : __ciphers_server,
        __ssl_version_server.name() : __ssl_version_server
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.TLS_SSL_TechnologyParameter = TLS_SSL_TechnologyParameter
Namespace.addCategoryObject('typeBinding', 'TLS_SSL_TechnologyParameter', TLS_SSL_TechnologyParameter)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}reencryptNetworkConfiguration with content type EMPTY
class reencryptNetworkConfiguration (AdditionalNetworkConfigurationParameters):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}reencryptNetworkConfiguration with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'reencryptNetworkConfiguration')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1593, 1)
    _ElementMap = AdditionalNetworkConfigurationParameters._ElementMap.copy()
    _AttributeMap = AdditionalNetworkConfigurationParameters._AttributeMap.copy()
    # Base type is AdditionalNetworkConfigurationParameters
    
    # Attribute reencryption_strategy uses Python identifier reencryption_strategy
    __reencryption_strategy = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'reencryption_strategy'), 'reencryption_strategy', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_reencryptNetworkConfiguration_reencryption_strategy', pyxb.binding.datatypes.string)
    __reencryption_strategy._DeclarationLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1596, 4)
    __reencryption_strategy._UseLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1596, 4)
    
    reencryption_strategy = property(__reencryption_strategy.value, __reencryption_strategy.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __reencryption_strategy.name() : __reencryption_strategy
    })
_module_typeBindings.reencryptNetworkConfiguration = reencryptNetworkConfiguration
Namespace.addCategoryObject('typeBinding', 'reencryptNetworkConfiguration', reencryptNetworkConfiguration)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Site2SiteNetworkConfiguration with content type ELEMENT_ONLY
class Site2SiteNetworkConfiguration (AdditionalNetworkConfigurationParameters):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Site2SiteNetworkConfiguration with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Site2SiteNetworkConfiguration')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1603, 1)
    _ElementMap = AdditionalNetworkConfigurationParameters._ElementMap.copy()
    _AttributeMap = AdditionalNetworkConfigurationParameters._AttributeMap.copy()
    # Base type is AdditionalNetworkConfigurationParameters
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}localNetwork uses Python identifier localNetwork
    __localNetwork = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'localNetwork'), 'localNetwork', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Site2SiteNetworkConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdlocalNetwork', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1607, 5), )

    
    localNetwork = property(__localNetwork.value, __localNetwork.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}remoteNetwork uses Python identifier remoteNetwork
    __remoteNetwork = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'remoteNetwork'), 'remoteNetwork', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Site2SiteNetworkConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdremoteNetwork', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1608, 5), )

    
    remoteNetwork = property(__remoteNetwork.value, __remoteNetwork.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}localNetworkNetmask uses Python identifier localNetworkNetmask
    __localNetworkNetmask = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'localNetworkNetmask'), 'localNetworkNetmask', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Site2SiteNetworkConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdlocalNetworkNetmask', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1609, 5), )

    
    localNetworkNetmask = property(__localNetworkNetmask.value, __localNetworkNetmask.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}remoteNetworkNetmask uses Python identifier remoteNetworkNetmask
    __remoteNetworkNetmask = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'remoteNetworkNetmask'), 'remoteNetworkNetmask', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_Site2SiteNetworkConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdremoteNetworkNetmask', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1610, 5), )

    
    remoteNetworkNetmask = property(__remoteNetworkNetmask.value, __remoteNetworkNetmask.set, None, None)

    _ElementMap.update({
        __localNetwork.name() : __localNetwork,
        __remoteNetwork.name() : __remoteNetwork,
        __localNetworkNetmask.name() : __localNetworkNetmask,
        __remoteNetworkNetmask.name() : __remoteNetworkNetmask
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Site2SiteNetworkConfiguration = Site2SiteNetworkConfiguration
Namespace.addCategoryObject('typeBinding', 'Site2SiteNetworkConfiguration', Site2SiteNetworkConfiguration)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoteAccessNetworkConfiguration with content type ELEMENT_ONLY
class RemoteAccessNetworkConfiguration (AdditionalNetworkConfigurationParameters):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoteAccessNetworkConfiguration with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RemoteAccessNetworkConfiguration')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1615, 1)
    _ElementMap = AdditionalNetworkConfigurationParameters._ElementMap.copy()
    _AttributeMap = AdditionalNetworkConfigurationParameters._AttributeMap.copy()
    # Base type is AdditionalNetworkConfigurationParameters
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}startIPAddress uses Python identifier startIPAddress
    __startIPAddress = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'startIPAddress'), 'startIPAddress', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RemoteAccessNetworkConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdstartIPAddress', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1619, 5), )

    
    startIPAddress = property(__startIPAddress.value, __startIPAddress.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}maxClients uses Python identifier maxClients
    __maxClients = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'maxClients'), 'maxClients', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RemoteAccessNetworkConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdmaxClients', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1620, 5), )

    
    maxClients = property(__maxClients.value, __maxClients.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}netmask uses Python identifier netmask
    __netmask = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'netmask'), 'netmask', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RemoteAccessNetworkConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdnetmask', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1621, 5), )

    
    netmask = property(__netmask.value, __netmask.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}routedSubnets uses Python identifier routedSubnets
    __routedSubnets = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'routedSubnets'), 'routedSubnets', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RemoteAccessNetworkConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdroutedSubnets', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1622, 5), )

    
    routedSubnets = property(__routedSubnets.value, __routedSubnets.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}dnsServer uses Python identifier dnsServer
    __dnsServer = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'dnsServer'), 'dnsServer', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RemoteAccessNetworkConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsddnsServer', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1623, 5), )

    
    dnsServer = property(__dnsServer.value, __dnsServer.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}domainSuffix uses Python identifier domainSuffix
    __domainSuffix = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'domainSuffix'), 'domainSuffix', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RemoteAccessNetworkConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsddomainSuffix', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1624, 5), )

    
    domainSuffix = property(__domainSuffix.value, __domainSuffix.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}wins uses Python identifier wins
    __wins = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'wins'), 'wins', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RemoteAccessNetworkConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdwins', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1625, 5), )

    
    wins = property(__wins.value, __wins.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}localSubnet uses Python identifier localSubnet
    __localSubnet = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'localSubnet'), 'localSubnet', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RemoteAccessNetworkConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdlocalSubnet', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1627, 5), )

    
    localSubnet = property(__localSubnet.value, __localSubnet.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}remoteSubnet uses Python identifier remoteSubnet
    __remoteSubnet = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'remoteSubnet'), 'remoteSubnet', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RemoteAccessNetworkConfiguration_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdremoteSubnet', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1628, 5), )

    
    remoteSubnet = property(__remoteSubnet.value, __remoteSubnet.set, None, None)

    _ElementMap.update({
        __startIPAddress.name() : __startIPAddress,
        __maxClients.name() : __maxClients,
        __netmask.name() : __netmask,
        __routedSubnets.name() : __routedSubnets,
        __dnsServer.name() : __dnsServer,
        __domainSuffix.name() : __domainSuffix,
        __wins.name() : __wins,
        __localSubnet.name() : __localSubnet,
        __remoteSubnet.name() : __remoteSubnet
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.RemoteAccessNetworkConfiguration = RemoteAccessNetworkConfiguration
Namespace.addCategoryObject('typeBinding', 'RemoteAccessNetworkConfiguration', RemoteAccessNetworkConfiguration)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ChannelAuthorizationCapability with content type ELEMENT_ONLY
class ChannelAuthorizationCapability (AuthorizationCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ChannelAuthorizationCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ChannelAuthorizationCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 120, 1)
    _ElementMap = AuthorizationCapability._ElementMap.copy()
    _AttributeMap = AuthorizationCapability._AttributeMap.copy()
    # Base type is AuthorizationCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ChannelAuthorizationCapability = ChannelAuthorizationCapability
Namespace.addCategoryObject('typeBinding', 'ChannelAuthorizationCapability', ChannelAuthorizationCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TargetAuthzCapability with content type ELEMENT_ONLY
class TargetAuthzCapability (AuthorizationCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TargetAuthzCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TargetAuthzCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 140, 1)
    _ElementMap = AuthorizationCapability._ElementMap.copy()
    _AttributeMap = AuthorizationCapability._AttributeMap.copy()
    # Base type is AuthorizationCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.TargetAuthzCapability = TargetAuthzCapability
Namespace.addCategoryObject('typeBinding', 'TargetAuthzCapability', TargetAuthzCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthcEnforcementCapability with content type ELEMENT_ONLY
class AuthcEnforcementCapability (AuthenticationCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthcEnforcementCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AuthcEnforcementCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 164, 1)
    _ElementMap = AuthenticationCapability._ElementMap.copy()
    _AttributeMap = AuthenticationCapability._AttributeMap.copy()
    # Base type is AuthenticationCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AuthcEnforcementCapability = AuthcEnforcementCapability
Namespace.addCategoryObject('typeBinding', 'AuthcEnforcementCapability', AuthcEnforcementCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthcDecisionCapability with content type ELEMENT_ONLY
class AuthcDecisionCapability (AuthenticationCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthcDecisionCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AuthcDecisionCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 170, 1)
    _ElementMap = AuthenticationCapability._ElementMap.copy()
    _AttributeMap = AuthenticationCapability._AttributeMap.copy()
    # Base type is AuthenticationCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}authenticationMethods uses Python identifier authenticationMethods
    __authenticationMethods = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'authenticationMethods'), 'authenticationMethods', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthcDecisionCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdauthenticationMethods', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 174, 5), )

    
    authenticationMethods = property(__authenticationMethods.value, __authenticationMethods.set, None, None)

    _ElementMap.update({
        __authenticationMethods.name() : __authenticationMethods
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AuthcDecisionCapability = AuthcDecisionCapability
Namespace.addCategoryObject('typeBinding', 'AuthcDecisionCapability', AuthcDecisionCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IDSCapability with content type ELEMENT_ONLY
class IDSCapability (TrafficAnalysisCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IDSCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'IDSCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 192, 1)
    _ElementMap = TrafficAnalysisCapability._ElementMap.copy()
    _AttributeMap = TrafficAnalysisCapability._AttributeMap.copy()
    # Base type is TrafficAnalysisCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element supportOnlineTraficAnalysis ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportOnlineTraficAnalysis) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficAnalysisCapability
    
    # Element supportOfflineTraficAnalysis ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportOfflineTraficAnalysis) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficAnalysisCapability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.IDSCapability = IDSCapability
Namespace.addCategoryObject('typeBinding', 'IDSCapability', IDSCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IPSCapability with content type ELEMENT_ONLY
class IPSCapability (TrafficAnalysisCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IPSCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'IPSCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 198, 1)
    _ElementMap = TrafficAnalysisCapability._ElementMap.copy()
    _AttributeMap = TrafficAnalysisCapability._AttributeMap.copy()
    # Base type is TrafficAnalysisCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element supportOnlineTraficAnalysis ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportOnlineTraficAnalysis) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficAnalysisCapability
    
    # Element supportOfflineTraficAnalysis ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportOfflineTraficAnalysis) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficAnalysisCapability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.IPSCapability = IPSCapability
Namespace.addCategoryObject('typeBinding', 'IPSCapability', IPSCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}MalwaresAnalysisCapability with content type ELEMENT_ONLY
class MalwaresAnalysisCapability (TrafficAnalysisCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}MalwaresAnalysisCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MalwaresAnalysisCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 204, 1)
    _ElementMap = TrafficAnalysisCapability._ElementMap.copy()
    _AttributeMap = TrafficAnalysisCapability._AttributeMap.copy()
    # Base type is TrafficAnalysisCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element supportOnlineTraficAnalysis ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportOnlineTraficAnalysis) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficAnalysisCapability
    
    # Element supportOfflineTraficAnalysis ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportOfflineTraficAnalysis) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficAnalysisCapability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.MalwaresAnalysisCapability = MalwaresAnalysisCapability
Namespace.addCategoryObject('typeBinding', 'MalwaresAnalysisCapability', MalwaresAnalysisCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}LawfulInterceptionCapability with content type ELEMENT_ONLY
class LawfulInterceptionCapability (TrafficAnalysisCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}LawfulInterceptionCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LawfulInterceptionCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 221, 1)
    _ElementMap = TrafficAnalysisCapability._ElementMap.copy()
    _AttributeMap = TrafficAnalysisCapability._AttributeMap.copy()
    # Base type is TrafficAnalysisCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element supportOnlineTraficAnalysis ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportOnlineTraficAnalysis) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficAnalysisCapability
    
    # Element supportOfflineTraficAnalysis ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportOfflineTraficAnalysis) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficAnalysisCapability
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}trafficType uses Python identifier trafficType
    __trafficType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'trafficType'), 'trafficType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_LawfulInterceptionCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdtrafficType', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 225, 5), )

    
    trafficType = property(__trafficType.value, __trafficType.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Country uses Python identifier Country
    __Country = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Country'), 'Country', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_LawfulInterceptionCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdCountry', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 226, 5), )

    
    Country = property(__Country.value, __Country.set, None, None)

    _ElementMap.update({
        __trafficType.name() : __trafficType,
        __Country.name() : __Country
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.LawfulInterceptionCapability = LawfulInterceptionCapability
Namespace.addCategoryObject('typeBinding', 'LawfulInterceptionCapability', LawfulInterceptionCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}VulnerabilitiesScannerCapability with content type ELEMENT_ONLY
class VulnerabilitiesScannerCapability (ResourceScannerCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}VulnerabilitiesScannerCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'VulnerabilitiesScannerCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 242, 1)
    _ElementMap = ResourceScannerCapability._ElementMap.copy()
    _AttributeMap = ResourceScannerCapability._AttributeMap.copy()
    # Base type is ResourceScannerCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element resourceType ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}resourceType) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ResourceScannerCapability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.VulnerabilitiesScannerCapability = VulnerabilitiesScannerCapability
Namespace.addCategoryObject('typeBinding', 'VulnerabilitiesScannerCapability', VulnerabilitiesScannerCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}LoggingCapability with content type ELEMENT_ONLY
class LoggingCapability (ResourceScannerCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}LoggingCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'LoggingCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 254, 1)
    _ElementMap = ResourceScannerCapability._ElementMap.copy()
    _AttributeMap = ResourceScannerCapability._AttributeMap.copy()
    # Base type is ResourceScannerCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element resourceType ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}resourceType) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ResourceScannerCapability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.LoggingCapability = LoggingCapability
Namespace.addCategoryObject('typeBinding', 'LoggingCapability', LoggingCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EncryptionCapability with content type ELEMENT_ONLY
class EncryptionCapability (DataProtectionCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EncryptionCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EncryptionCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 281, 1)
    _ElementMap = DataProtectionCapability._ElementMap.copy()
    _AttributeMap = DataProtectionCapability._AttributeMap.copy()
    # Base type is DataProtectionCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element supportsDataAuthenticationAndIntegrity ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportsDataAuthenticationAndIntegrity) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionCapability
    
    # Element supportsDigitalSignature ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportsDigitalSignature) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionCapability
    
    # Element supportsEncryption ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportsEncryption) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionCapability
    
    # Element supportsKeyExchange ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportsKeyExchange) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionCapability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.EncryptionCapability = EncryptionCapability
Namespace.addCategoryObject('typeBinding', 'EncryptionCapability', EncryptionCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AnonimizerCapability with content type ELEMENT_ONLY
class AnonimizerCapability (IdentityProtectionCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AnonimizerCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AnonimizerCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 293, 1)
    _ElementMap = IdentityProtectionCapability._ElementMap.copy()
    _AttributeMap = IdentityProtectionCapability._AttributeMap.copy()
    # Base type is IdentityProtectionCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AnonimizerCapability = AnonimizerCapability
Namespace.addCategoryObject('typeBinding', 'AnonimizerCapability', AnonimizerCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ForwardProxyCapabiity with content type ELEMENT_ONLY
class ForwardProxyCapabiity (AddressTranslationCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ForwardProxyCapabiity with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ForwardProxyCapabiity')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 305, 1)
    _ElementMap = AddressTranslationCapability._ElementMap.copy()
    _AttributeMap = AddressTranslationCapability._AttributeMap.copy()
    # Base type is AddressTranslationCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ForwardProxyCapabiity = ForwardProxyCapabiity
Namespace.addCategoryObject('typeBinding', 'ForwardProxyCapabiity', ForwardProxyCapabiity)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ReverseProxyCapabiity with content type ELEMENT_ONLY
class ReverseProxyCapabiity (AddressTranslationCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ReverseProxyCapabiity with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ReverseProxyCapabiity')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 318, 1)
    _ElementMap = AddressTranslationCapability._ElementMap.copy()
    _AttributeMap = AddressTranslationCapability._AttributeMap.copy()
    # Base type is AddressTranslationCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ReverseProxyCapabiity = ReverseProxyCapabiity
Namespace.addCategoryObject('typeBinding', 'ReverseProxyCapabiity', ReverseProxyCapabiity)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyConfigurationCondition with content type ELEMENT_ONLY
class PrivacyConfigurationCondition (FilteringConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}PrivacyConfigurationCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PrivacyConfigurationCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 452, 1)
    _ElementMap = FilteringConfigurationCondition._ElementMap.copy()
    _AttributeMap = FilteringConfigurationCondition._AttributeMap.copy()
    # Base type is FilteringConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Subject uses Python identifier Subject
    __Subject = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Subject'), 'Subject', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PrivacyConfigurationCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdSubject', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 456, 5), )

    
    Subject = property(__Subject.value, __Subject.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Target uses Python identifier Target
    __Target = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'Target'), 'Target', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_PrivacyConfigurationCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdTarget', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 457, 5), )

    
    Target = property(__Target.value, __Target.set, None, None)

    
    # Element packetFilterCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element statefulCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}statefulCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element timeCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}timeCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element applicationLayerCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}applicationLayerCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element qosCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}qosCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    _ElementMap.update({
        __Subject.name() : __Subject,
        __Target.name() : __Target
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PrivacyConfigurationCondition = PrivacyConfigurationCondition
Namespace.addCategoryObject('typeBinding', 'PrivacyConfigurationCondition', PrivacyConfigurationCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataAggregationConfigurationCondition with content type ELEMENT_ONLY
class DataAggregationConfigurationCondition (FilteringConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataAggregationConfigurationCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DataAggregationConfigurationCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 535, 1)
    _ElementMap = FilteringConfigurationCondition._ElementMap.copy()
    _AttributeMap = FilteringConfigurationCondition._AttributeMap.copy()
    # Base type is FilteringConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element packetFilterCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element statefulCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}statefulCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element timeCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}timeCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element applicationLayerCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}applicationLayerCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element qosCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}qosCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.DataAggregationConfigurationCondition = DataAggregationConfigurationCondition
Namespace.addCategoryObject('typeBinding', 'DataAggregationConfigurationCondition', DataAggregationConfigurationCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AnonymityConfigurationCondition with content type ELEMENT_ONLY
class AnonymityConfigurationCondition (FilteringConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AnonymityConfigurationCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AnonymityConfigurationCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 562, 1)
    _ElementMap = FilteringConfigurationCondition._ElementMap.copy()
    _AttributeMap = FilteringConfigurationCondition._AttributeMap.copy()
    # Base type is FilteringConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element packetFilterCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element statefulCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}statefulCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element timeCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}timeCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element applicationLayerCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}applicationLayerCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element qosCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}qosCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AnonymityConfigurationCondition = AnonymityConfigurationCondition
Namespace.addCategoryObject('typeBinding', 'AnonymityConfigurationCondition', AnonymityConfigurationCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}OnionRoutingTechnologyParameter with content type ELEMENT_ONLY
class OnionRoutingTechnologyParameter (AnonymityTechnologyParameter):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}OnionRoutingTechnologyParameter with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'OnionRoutingTechnologyParameter')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 602, 1)
    _ElementMap = AnonymityTechnologyParameter._ElementMap.copy()
    _AttributeMap = AnonymityTechnologyParameter._AttributeMap.copy()
    # Base type is AnonymityTechnologyParameter
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}exitRelay uses Python identifier exitRelay
    __exitRelay = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'exitRelay'), 'exitRelay', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_OnionRoutingTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdexitRelay', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 606, 5), )

    
    exitRelay = property(__exitRelay.value, __exitRelay.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IPv6Exit uses Python identifier IPv6Exit
    __IPv6Exit = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'IPv6Exit'), 'IPv6Exit', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_OnionRoutingTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdIPv6Exit', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 607, 5), )

    
    IPv6Exit = property(__IPv6Exit.value, __IPv6Exit.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}publicRelay uses Python identifier publicRelay
    __publicRelay = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'publicRelay'), 'publicRelay', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_OnionRoutingTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpublicRelay', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 608, 5), )

    
    publicRelay = property(__publicRelay.value, __publicRelay.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}hiddenServiceDir uses Python identifier hiddenServiceDir
    __hiddenServiceDir = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'hiddenServiceDir'), 'hiddenServiceDir', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_OnionRoutingTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdhiddenServiceDir', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 609, 5), )

    
    hiddenServiceDir = property(__hiddenServiceDir.value, __hiddenServiceDir.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}hiddenServicePort uses Python identifier hiddenServicePort
    __hiddenServicePort = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'hiddenServicePort'), 'hiddenServicePort', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_OnionRoutingTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdhiddenServicePort', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 610, 5), )

    
    hiddenServicePort = property(__hiddenServicePort.value, __hiddenServicePort.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}relayBandwidthRate uses Python identifier relayBandwidthRate
    __relayBandwidthRate = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'relayBandwidthRate'), 'relayBandwidthRate', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_OnionRoutingTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdrelayBandwidthRate', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 611, 5), )

    
    relayBandwidthRate = property(__relayBandwidthRate.value, __relayBandwidthRate.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}relayBandwidthBurst uses Python identifier relayBandwidthBurst
    __relayBandwidthBurst = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'relayBandwidthBurst'), 'relayBandwidthBurst', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_OnionRoutingTechnologyParameter_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdrelayBandwidthBurst', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 612, 5), )

    
    relayBandwidthBurst = property(__relayBandwidthBurst.value, __relayBandwidthBurst.set, None, None)

    _ElementMap.update({
        __exitRelay.name() : __exitRelay,
        __IPv6Exit.name() : __IPv6Exit,
        __publicRelay.name() : __publicRelay,
        __hiddenServiceDir.name() : __hiddenServiceDir,
        __hiddenServicePort.name() : __hiddenServicePort,
        __relayBandwidthRate.name() : __relayBandwidthRate,
        __relayBandwidthBurst.name() : __relayBandwidthBurst
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.OnionRoutingTechnologyParameter = OnionRoutingTechnologyParameter
Namespace.addCategoryObject('typeBinding', 'OnionRoutingTechnologyParameter', OnionRoutingTechnologyParameter)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}MonitoringConfigurationCondition with content type ELEMENT_ONLY
class MonitoringConfigurationCondition (FilteringConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}MonitoringConfigurationCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MonitoringConfigurationCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 645, 1)
    _ElementMap = FilteringConfigurationCondition._ElementMap.copy()
    _AttributeMap = FilteringConfigurationCondition._AttributeMap.copy()
    # Base type is FilteringConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}detectionFilter uses Python identifier detectionFilter
    __detectionFilter = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'detectionFilter'), 'detectionFilter', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_MonitoringConfigurationCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsddetectionFilter', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 650, 5), )

    
    detectionFilter = property(__detectionFilter.value, __detectionFilter.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}signatureList uses Python identifier signatureList
    __signatureList = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'signatureList'), 'signatureList', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_MonitoringConfigurationCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdsignatureList', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 651, 5), )

    
    signatureList = property(__signatureList.value, __signatureList.set, None, None)

    
    # Element packetFilterCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element statefulCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}statefulCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element timeCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}timeCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element applicationLayerCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}applicationLayerCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element qosCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}qosCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    _ElementMap.update({
        __detectionFilter.name() : __detectionFilter,
        __signatureList.name() : __signatureList
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.MonitoringConfigurationCondition = MonitoringConfigurationCondition
Namespace.addCategoryObject('typeBinding', 'MonitoringConfigurationCondition', MonitoringConfigurationCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficDivertConfigurationCondition with content type ELEMENT_ONLY
class TrafficDivertConfigurationCondition (FilteringConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficDivertConfigurationCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TrafficDivertConfigurationCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 738, 1)
    _ElementMap = FilteringConfigurationCondition._ElementMap.copy()
    _AttributeMap = FilteringConfigurationCondition._AttributeMap.copy()
    # Base type is FilteringConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element packetFilterCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element statefulCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}statefulCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element timeCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}timeCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element applicationLayerCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}applicationLayerCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element qosCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}qosCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.TrafficDivertConfigurationCondition = TrafficDivertConfigurationCondition
Namespace.addCategoryObject('typeBinding', 'TrafficDivertConfigurationCondition', TrafficDivertConfigurationCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}InterfaceSelectionCondition with content type ELEMENT_ONLY
class InterfaceSelectionCondition (DataProtectionCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}InterfaceSelectionCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'InterfaceSelectionCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 857, 1)
    _ElementMap = DataProtectionCondition._ElementMap.copy()
    _AttributeMap = DataProtectionCondition._AttributeMap.copy()
    # Base type is DataProtectionCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element packetFilterCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionCondition
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.InterfaceSelectionCondition = InterfaceSelectionCondition
Namespace.addCategoryObject('typeBinding', 'InterfaceSelectionCondition', InterfaceSelectionCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataSelectionCondition with content type ELEMENT_ONLY
class DataSelectionCondition (DataProtectionCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataSelectionCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DataSelectionCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 862, 1)
    _ElementMap = DataProtectionCondition._ElementMap.copy()
    _AttributeMap = DataProtectionCondition._AttributeMap.copy()
    # Base type is DataProtectionCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element packetFilterCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionCondition
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.DataSelectionCondition = DataSelectionCondition
Namespace.addCategoryObject('typeBinding', 'DataSelectionCondition', DataSelectionCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficDivertEncapsulationAction with content type ELEMENT_ONLY
class TrafficDivertEncapsulationAction (TrafficDivertAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficDivertEncapsulationAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TrafficDivertEncapsulationAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 985, 1)
    _ElementMap = TrafficDivertAction._ElementMap.copy()
    _AttributeMap = TrafficDivertAction._AttributeMap.copy()
    # Base type is TrafficDivertAction
    
    # Element TrafficDivertActionType ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficDivertActionType) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficDivertAction
    
    # Element packetDivertAction ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetDivertAction) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficDivertAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EncapsulationParameters uses Python identifier EncapsulationParameters
    __EncapsulationParameters = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'EncapsulationParameters'), 'EncapsulationParameters', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_TrafficDivertEncapsulationAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdEncapsulationParameters', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 989, 5), )

    
    EncapsulationParameters = property(__EncapsulationParameters.value, __EncapsulationParameters.set, None, None)

    _ElementMap.update({
        __EncapsulationParameters.name() : __EncapsulationParameters
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.TrafficDivertEncapsulationAction = TrafficDivertEncapsulationAction
Namespace.addCategoryObject('typeBinding', 'TrafficDivertEncapsulationAction', TrafficDivertEncapsulationAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveAdvertisementAction with content type ELEMENT_ONLY
class RemoveAdvertisementAction (RemoveAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveAdvertisementAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RemoveAdvertisementAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1082, 1)
    _ElementMap = RemoveAction._ElementMap.copy()
    _AttributeMap = RemoveAction._AttributeMap.copy()
    # Base type is RemoveAction
    
    # Element RemoveActionType ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveActionType) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveAdvertisementActionType uses Python identifier RemoveAdvertisementActionType
    __RemoveAdvertisementActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'RemoveAdvertisementActionType'), 'RemoveAdvertisementActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RemoveAdvertisementAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdRemoveAdvertisementActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1086, 5), )

    
    RemoveAdvertisementActionType = property(__RemoveAdvertisementActionType.value, __RemoveAdvertisementActionType.set, None, None)

    _ElementMap.update({
        __RemoveAdvertisementActionType.name() : __RemoveAdvertisementActionType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.RemoveAdvertisementAction = RemoveAdvertisementAction
Namespace.addCategoryObject('typeBinding', 'RemoveAdvertisementAction', RemoveAdvertisementAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveTrackingTechniquesAction with content type ELEMENT_ONLY
class RemoveTrackingTechniquesAction (RemoveAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveTrackingTechniquesAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RemoveTrackingTechniquesAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1094, 1)
    _ElementMap = RemoveAction._ElementMap.copy()
    _AttributeMap = RemoveAction._AttributeMap.copy()
    # Base type is RemoveAction
    
    # Element RemoveActionType ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveActionType) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}RemoveTrackingTechniquesActionType uses Python identifier RemoveTrackingTechniquesActionType
    __RemoveTrackingTechniquesActionType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'RemoveTrackingTechniquesActionType'), 'RemoveTrackingTechniquesActionType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_RemoveTrackingTechniquesAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdRemoveTrackingTechniquesActionType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1098, 5), )

    
    RemoveTrackingTechniquesActionType = property(__RemoveTrackingTechniquesActionType.value, __RemoveTrackingTechniquesActionType.set, None, None)

    _ElementMap.update({
        __RemoveTrackingTechniquesActionType.name() : __RemoveTrackingTechniquesActionType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.RemoveTrackingTechniquesAction = RemoveTrackingTechniquesAction
Namespace.addCategoryObject('typeBinding', 'RemoveTrackingTechniquesAction', RemoveTrackingTechniquesAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ParentalControlAction with content type ELEMENT_ONLY
class ParentalControlAction (EnableAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ParentalControlAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ParentalControlAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1106, 1)
    _ElementMap = EnableAction._ElementMap.copy()
    _AttributeMap = EnableAction._AttributeMap.copy()
    # Base type is EnableAction
    
    # Element EnableActionType ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EnableActionType) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EnableAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}pics uses Python identifier pics
    __pics = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'pics'), 'pics', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_ParentalControlAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpics', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1111, 5), )

    
    pics = property(__pics.value, __pics.set, None, None)

    _ElementMap.update({
        __pics.name() : __pics
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ParentalControlAction = ParentalControlAction
Namespace.addCategoryObject('typeBinding', 'ParentalControlAction', ParentalControlAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AnonimityAction with content type ELEMENT_ONLY
class AnonimityAction (EnableAction):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AnonimityAction with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AnonimityAction')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1118, 1)
    _ElementMap = EnableAction._ElementMap.copy()
    _AttributeMap = EnableAction._AttributeMap.copy()
    # Base type is EnableAction
    
    # Element EnableActionType ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EnableActionType) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}EnableAction
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}country uses Python identifier country
    __country = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'country'), 'country', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AnonimityAction_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdcountry', True, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1122, 5), )

    
    country = property(__country.value, __country.set, None, None)

    _ElementMap.update({
        __country.name() : __country
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AnonimityAction = AnonimityAction
Namespace.addCategoryObject('typeBinding', 'AnonimityAction', AnonimityAction)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthorizationCondition with content type ELEMENT_ONLY
class AuthorizationCondition (FilteringConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthorizationCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AuthorizationCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1197, 1)
    _ElementMap = FilteringConfigurationCondition._ElementMap.copy()
    _AttributeMap = FilteringConfigurationCondition._AttributeMap.copy()
    # Base type is FilteringConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element packetFilterCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element statefulCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}statefulCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element timeCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}timeCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element applicationLayerCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}applicationLayerCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element qosCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}qosCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthorizationSubject uses Python identifier AuthorizationSubject
    __AuthorizationSubject = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationSubject'), 'AuthorizationSubject', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthorizationCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdAuthorizationSubject', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1201, 5), )

    
    AuthorizationSubject = property(__AuthorizationSubject.value, __AuthorizationSubject.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthorizationTarget uses Python identifier AuthorizationTarget
    __AuthorizationTarget = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationTarget'), 'AuthorizationTarget', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthorizationCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdAuthorizationTarget', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1202, 5), )

    
    AuthorizationTarget = property(__AuthorizationTarget.value, __AuthorizationTarget.set, None, None)

    _ElementMap.update({
        __AuthorizationSubject.name() : __AuthorizationSubject,
        __AuthorizationTarget.name() : __AuthorizationTarget
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AuthorizationCondition = AuthorizationCondition
Namespace.addCategoryObject('typeBinding', 'AuthorizationCondition', AuthorizationCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationCondition with content type ELEMENT_ONLY
class AuthenticationCondition (FilteringConfigurationCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AuthenticationCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1238, 1)
    _ElementMap = FilteringConfigurationCondition._ElementMap.copy()
    _AttributeMap = FilteringConfigurationCondition._AttributeMap.copy()
    # Base type is FilteringConfigurationCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element packetFilterCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element statefulCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}statefulCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element timeCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}timeCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element applicationLayerCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}applicationLayerCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element qosCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}qosCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringConfigurationCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthenticationSubject uses Python identifier AuthenticationSubject
    __AuthenticationSubject = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationSubject'), 'AuthenticationSubject', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_AuthenticationCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdAuthenticationSubject', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1242, 5), )

    
    AuthenticationSubject = property(__AuthenticationSubject.value, __AuthenticationSubject.set, None, None)

    _ElementMap.update({
        __AuthenticationSubject.name() : __AuthenticationSubject
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AuthenticationCondition = AuthenticationCondition
Namespace.addCategoryObject('typeBinding', 'AuthenticationCondition', AuthenticationCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DTLSTechnologyParameter with content type ELEMENT_ONLY
class DTLSTechnologyParameter (IPsecTechnologyParameter):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DTLSTechnologyParameter with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DTLSTechnologyParameter')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1535, 1)
    _ElementMap = IPsecTechnologyParameter._ElementMap.copy()
    _AttributeMap = IPsecTechnologyParameter._AttributeMap.copy()
    # Base type is IPsecTechnologyParameter
    
    # Element IPsecProtocol ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IPsecProtocol) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IPsecTechnologyParameter
    
    # Element isTunnel ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isTunnel) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IPsecTechnologyParameter
    
    # Element localEndpoint ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}localEndpoint) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IPsecTechnologyParameter
    
    # Element remoteEndpoint ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}remoteEndpoint) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}IPsecTechnologyParameter
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.DTLSTechnologyParameter = DTLSTechnologyParameter
Namespace.addCategoryObject('typeBinding', 'DTLSTechnologyParameter', DTLSTechnologyParameter)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringCapability with content type ELEMENT_ONLY
class FilteringCapability (ChannelAuthorizationCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FilteringCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FilteringCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 126, 1)
    _ElementMap = ChannelAuthorizationCapability._ElementMap.copy()
    _AttributeMap = ChannelAuthorizationCapability._AttributeMap.copy()
    # Base type is ChannelAuthorizationCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}stateful uses Python identifier stateful
    __stateful = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'stateful'), 'stateful', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_FilteringCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdstateful', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 130, 5), )

    
    stateful = property(__stateful.value, __stateful.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}applicationLayerFiltering uses Python identifier applicationLayerFiltering
    __applicationLayerFiltering = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerFiltering'), 'applicationLayerFiltering', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_FilteringCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdapplicationLayerFiltering', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 131, 5), )

    
    applicationLayerFiltering = property(__applicationLayerFiltering.value, __applicationLayerFiltering.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}httpFiltering uses Python identifier httpFiltering
    __httpFiltering = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'httpFiltering'), 'httpFiltering', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_FilteringCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdhttpFiltering', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 132, 5), )

    
    httpFiltering = property(__httpFiltering.value, __httpFiltering.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}contentInspection uses Python identifier contentInspection
    __contentInspection = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'contentInspection'), 'contentInspection', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_FilteringCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdcontentInspection', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 133, 5), )

    
    contentInspection = property(__contentInspection.value, __contentInspection.set, None, None)

    _ElementMap.update({
        __stateful.name() : __stateful,
        __applicationLayerFiltering.name() : __applicationLayerFiltering,
        __httpFiltering.name() : __httpFiltering,
        __contentInspection.name() : __contentInspection
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.FilteringCapability = FilteringCapability
Namespace.addCategoryObject('typeBinding', 'FilteringCapability', FilteringCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthzEnforcementCapability with content type ELEMENT_ONLY
class AuthzEnforcementCapability (TargetAuthzCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthzEnforcementCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AuthzEnforcementCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 146, 1)
    _ElementMap = TargetAuthzCapability._ElementMap.copy()
    _AttributeMap = TargetAuthzCapability._AttributeMap.copy()
    # Base type is TargetAuthzCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AuthzEnforcementCapability = AuthzEnforcementCapability
Namespace.addCategoryObject('typeBinding', 'AuthzEnforcementCapability', AuthzEnforcementCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthzDecisionCapabiliy with content type ELEMENT_ONLY
class AuthzDecisionCapabiliy (TargetAuthzCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}AuthzDecisionCapabiliy with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'AuthzDecisionCapabiliy')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 152, 1)
    _ElementMap = TargetAuthzCapability._ElementMap.copy()
    _AttributeMap = TargetAuthzCapability._AttributeMap.copy()
    # Base type is TargetAuthzCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.AuthzDecisionCapabiliy = AuthzDecisionCapabiliy
Namespace.addCategoryObject('typeBinding', 'AuthzDecisionCapabiliy', AuthzDecisionCapabiliy)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}MaliciousFileAnalysisCapability with content type ELEMENT_ONLY
class MaliciousFileAnalysisCapability (MalwaresAnalysisCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}MaliciousFileAnalysisCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MaliciousFileAnalysisCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 210, 1)
    _ElementMap = MalwaresAnalysisCapability._ElementMap.copy()
    _AttributeMap = MalwaresAnalysisCapability._AttributeMap.copy()
    # Base type is MalwaresAnalysisCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element supportOnlineTraficAnalysis ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportOnlineTraficAnalysis) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficAnalysisCapability
    
    # Element supportOfflineTraficAnalysis ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}supportOfflineTraficAnalysis) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficAnalysisCapability
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}fileType uses Python identifier fileType
    __fileType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'fileType'), 'fileType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_MaliciousFileAnalysisCapability_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdfileType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 214, 5), )

    
    fileType = property(__fileType.value, __fileType.set, None, None)

    _ElementMap.update({
        __fileType.name() : __fileType
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.MaliciousFileAnalysisCapability = MaliciousFileAnalysisCapability
Namespace.addCategoryObject('typeBinding', 'MaliciousFileAnalysisCapability', MaliciousFileAnalysisCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}BotnetDetectorCapability with content type ELEMENT_ONLY
class BotnetDetectorCapability (VulnerabilitiesScannerCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}BotnetDetectorCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'BotnetDetectorCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 248, 1)
    _ElementMap = VulnerabilitiesScannerCapability._ElementMap.copy()
    _AttributeMap = VulnerabilitiesScannerCapability._AttributeMap.copy()
    # Base type is VulnerabilitiesScannerCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element resourceType ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}resourceType) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ResourceScannerCapability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.BotnetDetectorCapability = BotnetDetectorCapability
Namespace.addCategoryObject('typeBinding', 'BotnetDetectorCapability', BotnetDetectorCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficRecordCapability with content type ELEMENT_ONLY
class TrafficRecordCapability (LoggingCapability):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}TrafficRecordCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'TrafficRecordCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 260, 1)
    _ElementMap = LoggingCapability._ElementMap.copy()
    _AttributeMap = LoggingCapability._AttributeMap.copy()
    # Base type is LoggingCapability
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    
    # Element resourceType ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}resourceType) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ResourceScannerCapability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.TrafficRecordCapability = TrafficRecordCapability
Namespace.addCategoryObject('typeBinding', 'TrafficRecordCapability', TrafficRecordCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}NattingCapability with content type ELEMENT_ONLY
class NattingCapability (ForwardProxyCapabiity):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}NattingCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'NattingCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 312, 1)
    _ElementMap = ForwardProxyCapabiity._ElementMap.copy()
    _AttributeMap = ForwardProxyCapabiity._AttributeMap.copy()
    # Base type is ForwardProxyCapabiity
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.NattingCapability = NattingCapability
Namespace.addCategoryObject('typeBinding', 'NattingCapability', NattingCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}URLRewritingCapability with content type ELEMENT_ONLY
class URLRewritingCapability (ReverseProxyCapabiity):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}URLRewritingCapability with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'URLRewritingCapability')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 324, 1)
    _ElementMap = ReverseProxyCapabiity._ElementMap.copy()
    _AttributeMap = ReverseProxyCapabiity._AttributeMap.copy()
    # Base type is ReverseProxyCapabiity
    
    # Element Name ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Name) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}Capability
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.URLRewritingCapability = URLRewritingCapability
Namespace.addCategoryObject('typeBinding', 'URLRewritingCapability', URLRewritingCapability)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FileSystemCondition with content type ELEMENT_ONLY
class FileSystemCondition (DataSelectionCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}FileSystemCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'FileSystemCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 867, 1)
    _ElementMap = DataSelectionCondition._ElementMap.copy()
    _AttributeMap = DataSelectionCondition._AttributeMap.copy()
    # Base type is DataSelectionCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element packetFilterCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}filename uses Python identifier filename
    __filename = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'filename'), 'filename', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_FileSystemCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdfilename', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 871, 5), )

    
    filename = property(__filename.value, __filename.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}path uses Python identifier path
    __path = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'path'), 'path', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_FileSystemCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdpath', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 872, 5), )

    
    path = property(__path.value, __path.set, None, None)

    _ElementMap.update({
        __filename.name() : __filename,
        __path.name() : __path
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.FileSystemCondition = FileSystemCondition
Namespace.addCategoryObject('typeBinding', 'FileSystemCondition', FileSystemCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DBDataSelectionCondition with content type ELEMENT_ONLY
class DBDataSelectionCondition (DataSelectionCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DBDataSelectionCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DBDataSelectionCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 923, 1)
    _ElementMap = DataSelectionCondition._ElementMap.copy()
    _AttributeMap = DataSelectionCondition._AttributeMap.copy()
    # Base type is DataSelectionCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element packetFilterCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}tableName uses Python identifier tableName
    __tableName = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'tableName'), 'tableName', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_DBDataSelectionCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdtableName', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 927, 5), )

    
    tableName = property(__tableName.value, __tableName.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}sqlQuery uses Python identifier sqlQuery
    __sqlQuery = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'sqlQuery'), 'sqlQuery', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_DBDataSelectionCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdsqlQuery', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 928, 5), )

    
    sqlQuery = property(__sqlQuery.value, __sqlQuery.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}viewName uses Python identifier viewName
    __viewName = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'viewName'), 'viewName', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_DBDataSelectionCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdviewName', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 929, 5), )

    
    viewName = property(__viewName.value, __viewName.set, None, None)

    _ElementMap.update({
        __tableName.name() : __tableName,
        __sqlQuery.name() : __sqlQuery,
        __viewName.name() : __viewName
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.DBDataSelectionCondition = DBDataSelectionCondition
Namespace.addCategoryObject('typeBinding', 'DBDataSelectionCondition', DBDataSelectionCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}XMLDataSelectionCondition with content type ELEMENT_ONLY
class XMLDataSelectionCondition (DataSelectionCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}XMLDataSelectionCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'XMLDataSelectionCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 934, 1)
    _ElementMap = DataSelectionCondition._ElementMap.copy()
    _AttributeMap = DataSelectionCondition._AttributeMap.copy()
    # Base type is DataSelectionCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element packetFilterCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}xmlDataType uses Python identifier xmlDataType
    __xmlDataType = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'xmlDataType'), 'xmlDataType', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_XMLDataSelectionCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdxmlDataType', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 938, 5), )

    
    xmlDataType = property(__xmlDataType.value, __xmlDataType.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}xmlNameSpace uses Python identifier xmlNameSpace
    __xmlNameSpace = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'xmlNameSpace'), 'xmlNameSpace', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_XMLDataSelectionCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdxmlNameSpace', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 939, 5), )

    
    xmlNameSpace = property(__xmlNameSpace.value, __xmlNameSpace.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}xmlQueryLanguage uses Python identifier xmlQueryLanguage
    __xmlQueryLanguage = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'xmlQueryLanguage'), 'xmlQueryLanguage', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_XMLDataSelectionCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdxmlQueryLanguage', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 940, 5), )

    
    xmlQueryLanguage = property(__xmlQueryLanguage.value, __xmlQueryLanguage.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}xmlQueryLanguageVersion uses Python identifier xmlQueryLanguageVersion
    __xmlQueryLanguageVersion = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'xmlQueryLanguageVersion'), 'xmlQueryLanguageVersion', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_XMLDataSelectionCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdxmlQueryLanguageVersion', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 941, 5), )

    
    xmlQueryLanguageVersion = property(__xmlQueryLanguageVersion.value, __xmlQueryLanguageVersion.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}xmlQueryExpression uses Python identifier xmlQueryExpression
    __xmlQueryExpression = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'xmlQueryExpression'), 'xmlQueryExpression', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_XMLDataSelectionCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdxmlQueryExpression', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 943, 5), )

    
    xmlQueryExpression = property(__xmlQueryExpression.value, __xmlQueryExpression.set, None, None)

    _ElementMap.update({
        __xmlDataType.name() : __xmlDataType,
        __xmlNameSpace.name() : __xmlNameSpace,
        __xmlQueryLanguage.name() : __xmlQueryLanguage,
        __xmlQueryLanguageVersion.name() : __xmlQueryLanguageVersion,
        __xmlQueryExpression.name() : __xmlQueryExpression
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.XMLDataSelectionCondition = XMLDataSelectionCondition
Namespace.addCategoryObject('typeBinding', 'XMLDataSelectionCondition', XMLDataSelectionCondition)


# Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}WSSecurityCondition with content type ELEMENT_ONLY
class WSSecurityCondition (XMLDataSelectionCondition):
    """Complex type {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}WSSecurityCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'WSSecurityCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 948, 1)
    _ElementMap = XMLDataSelectionCondition._ElementMap.copy()
    _AttributeMap = XMLDataSelectionCondition._AttributeMap.copy()
    # Base type is XMLDataSelectionCondition
    
    # Element isCNF ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}isCNF) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}ConfigurationCondition
    
    # Element packetFilterCondition ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}packetFilterCondition) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}DataProtectionCondition
    
    # Element xmlDataType ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}xmlDataType) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}XMLDataSelectionCondition
    
    # Element xmlNameSpace ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}xmlNameSpace) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}XMLDataSelectionCondition
    
    # Element xmlQueryLanguage ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}xmlQueryLanguage) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}XMLDataSelectionCondition
    
    # Element xmlQueryLanguageVersion ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}xmlQueryLanguageVersion) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}XMLDataSelectionCondition
    
    # Element xmlQueryExpression ({http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}xmlQueryExpression) inherited from {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}XMLDataSelectionCondition
    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}content_vs_element uses Python identifier content_vs_element
    __content_vs_element = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'content_vs_element'), 'content_vs_element', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_WSSecurityCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdcontent_vs_element', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 952, 5), )

    
    content_vs_element = property(__content_vs_element.value, __content_vs_element.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}body uses Python identifier body
    __body = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'body'), 'body', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_WSSecurityCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdbody', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 953, 5), )

    
    body = property(__body.value, __body.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}attachment uses Python identifier attachment
    __attachment = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'attachment'), 'attachment', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_WSSecurityCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdattachment', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 954, 5), )

    
    attachment = property(__attachment.value, __attachment.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}headerLocalName uses Python identifier headerLocalName
    __headerLocalName = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'headerLocalName'), 'headerLocalName', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_WSSecurityCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdheaderLocalName', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 955, 5), )

    
    headerLocalName = property(__headerLocalName.value, __headerLocalName.set, None, None)

    
    # Element {http://modeliosoft/xsddesigner/a22bd60b-ee3d-425c-8618-beb6a854051a/ITResource.xsd}headerNameSpace uses Python identifier headerNameSpace
    __headerNameSpace = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'headerNameSpace'), 'headerNameSpace', '__httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsd_WSSecurityCondition_httpmodeliosoftxsddesignera22bd60b_ee3d_425c_8618_beb6a854051aITResource_xsdheaderNameSpace', False, pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 956, 5), )

    
    headerNameSpace = property(__headerNameSpace.value, __headerNameSpace.set, None, None)

    _ElementMap.update({
        __content_vs_element.name() : __content_vs_element,
        __body.name() : __body,
        __attachment.name() : __attachment,
        __headerLocalName.name() : __headerLocalName,
        __headerNameSpace.name() : __headerNameSpace
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.WSSecurityCondition = WSSecurityCondition
Namespace.addCategoryObject('typeBinding', 'WSSecurityCondition', WSSecurityCondition)


technology = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'technology'), pyxb.binding.datatypes.string, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1651, 1))
Namespace.addCategoryObject('elementBinding', technology.name().localName(), technology)

ITResource = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ITResource'), ITResourceType, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 10, 1))
Namespace.addCategoryObject('elementBinding', ITResource.name().localName(), ITResource)

ITResourceOrchestration = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ITResourceOrchestration'), ITResourceOrchestrationType, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 12, 1))
Namespace.addCategoryObject('elementBinding', ITResourceOrchestration.name().localName(), ITResourceOrchestration)

technologyActionParameters = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'technologyActionParameters'), ActionParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1498, 1))
Namespace.addCategoryObject('elementBinding', technologyActionParameters.name().localName(), technologyActionParameters)



ITResourceOrchestrationType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ITResource'), ITResourceType, scope=ITResourceOrchestrationType, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 15, 3)))

ITResourceOrchestrationType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'priority'), pyxb.binding.datatypes.integer, scope=ITResourceOrchestrationType, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 16, 3)))

ITResourceOrchestrationType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'dependencies'), Dependencies, scope=ITResourceOrchestrationType, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 17, 3)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 15, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 16, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 17, 3))
    counters.add(cc_2)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(ITResourceOrchestrationType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ITResource')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 15, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(ITResourceOrchestrationType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'priority')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 16, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(ITResourceOrchestrationType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dependencies')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 17, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
ITResourceOrchestrationType._Automaton = _BuildAutomaton()




ITResourceType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'configuration'), Configuration, scope=ITResourceType, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 25, 3)))

ITResourceType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'priority'), pyxb.binding.datatypes.integer, scope=ITResourceType, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 27, 3)))

ITResourceType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'dependencies'), Dependencies, scope=ITResourceType, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 28, 3)))

ITResourceType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'enablerCandidates'), EnablerCandidates, scope=ITResourceType, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 29, 3)))

def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 27, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 28, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 29, 3))
    counters.add(cc_2)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ITResourceType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'configuration')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 25, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(ITResourceType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'priority')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 27, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(ITResourceType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dependencies')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 28, 3))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(ITResourceType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'enablerCandidates')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 29, 3))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ITResourceType._Automaton = _BuildAutomaton_()




Dependencies._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'dependency'), Dependency, scope=Dependencies, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 50, 3)))

def _BuildAutomaton_2 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_2
    del _BuildAutomaton_2
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(Dependencies._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dependency')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 50, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
Dependencies._Automaton = _BuildAutomaton_2()




EnablerCandidates._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'enabler'), pyxb.binding.datatypes.string, scope=EnablerCandidates, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 94, 3)))

def _BuildAutomaton_3 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_3
    del _BuildAutomaton_3
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(EnablerCandidates._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'enabler')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 94, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
EnablerCandidates._Automaton = _BuildAutomaton_3()




Configuration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'capability'), Capability, scope=Configuration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 104, 3)))

def _BuildAutomaton_4 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_4
    del _BuildAutomaton_4
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(Configuration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'capability')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 104, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
Configuration._Automaton = _BuildAutomaton_4()




Capability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Name'), CapabilityType, scope=Capability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3)))

def _BuildAutomaton_5 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_5
    del _BuildAutomaton_5
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(Capability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
Capability._Automaton = _BuildAutomaton_5()




ConfigurationRule._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'configurationRuleAction'), ConfigurationAction, scope=ConfigurationRule, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 355, 3)))

ConfigurationRule._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'configurationCondition'), ConfigurationCondition, scope=ConfigurationRule, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 357, 3)))

ConfigurationRule._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'externalData'), ExternalData, scope=ConfigurationRule, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 359, 3)))

ConfigurationRule._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Name'), pyxb.binding.datatypes.string, scope=ConfigurationRule, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 360, 3)))

ConfigurationRule._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'isCNF'), pyxb.binding.datatypes.boolean, scope=ConfigurationRule, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 361, 3)))

ConfigurationRule._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'HSPL'), HSPL, scope=ConfigurationRule, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 362, 3)))

def _BuildAutomaton_6 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_6
    del _BuildAutomaton_6
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 359, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 362, 3))
    counters.add(cc_1)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ConfigurationRule._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'configurationRuleAction')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 355, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ConfigurationRule._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'configurationCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 357, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ConfigurationRule._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'externalData')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 359, 3))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ConfigurationRule._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 360, 3))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ConfigurationRule._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 361, 3))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(ConfigurationRule._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'HSPL')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 362, 3))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
         ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ConfigurationRule._Automaton = _BuildAutomaton_6()




ConfigurationCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'isCNF'), pyxb.binding.datatypes.boolean, scope=ConfigurationCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3)))

def _BuildAutomaton_7 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_7
    del _BuildAutomaton_7
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ConfigurationCondition._Automaton = _BuildAutomaton_7()




ResolutionStrategy._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'externalData'), ExternalData, scope=ResolutionStrategy, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3)))

def _BuildAutomaton_8 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_8
    del _BuildAutomaton_8
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(ResolutionStrategy._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'externalData')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
ResolutionStrategy._Automaton = _BuildAutomaton_8()




KeyValue._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'key'), pyxb.binding.datatypes.string, scope=KeyValue, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 510, 3)))

KeyValue._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value'), pyxb.binding.datatypes.string, scope=KeyValue, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 511, 3)))

def _BuildAutomaton_9 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_9
    del _BuildAutomaton_9
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(KeyValue._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'key')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 510, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(KeyValue._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 511, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
KeyValue._Automaton = _BuildAutomaton_9()




SignatureList._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'signature'), pyxb.binding.datatypes.string, scope=SignatureList, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 661, 3)))

def _BuildAutomaton_10 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_10
    del _BuildAutomaton_10
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 661, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(SignatureList._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'signature')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 661, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
SignatureList._Automaton = _BuildAutomaton_10()




NetworkSlicingConditionParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'SourceMAC'), pyxb.binding.datatypes.string, scope=NetworkSlicingConditionParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 721, 3)))

NetworkSlicingConditionParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'DestinationMAC'), pyxb.binding.datatypes.string, scope=NetworkSlicingConditionParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 722, 3)))

NetworkSlicingConditionParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'SliceID'), pyxb.binding.datatypes.string, scope=NetworkSlicingConditionParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 723, 3)))

def _BuildAutomaton_11 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_11
    del _BuildAutomaton_11
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(NetworkSlicingConditionParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'SourceMAC')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 721, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(NetworkSlicingConditionParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'DestinationMAC')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 722, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(NetworkSlicingConditionParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'SliceID')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 723, 3))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
NetworkSlicingConditionParameters._Automaton = _BuildAutomaton_11()




PacketFilterCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'SourceMAC'), pyxb.binding.datatypes.string, scope=PacketFilterCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 781, 3)))

PacketFilterCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'DestinationMAC'), pyxb.binding.datatypes.string, scope=PacketFilterCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 782, 6)))

PacketFilterCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'SourceAddress'), pyxb.binding.datatypes.string, scope=PacketFilterCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 783, 3)))

PacketFilterCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'DestinationAddress'), pyxb.binding.datatypes.string, scope=PacketFilterCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 784, 3)))

PacketFilterCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'SourcePort'), pyxb.binding.datatypes.string, scope=PacketFilterCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 785, 3)))

PacketFilterCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'DestinationPort'), pyxb.binding.datatypes.string, scope=PacketFilterCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 786, 3)))

PacketFilterCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'direction'), pyxb.binding.datatypes.string, scope=PacketFilterCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 787, 3)))

PacketFilterCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Interface'), pyxb.binding.datatypes.string, scope=PacketFilterCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 788, 3)))

PacketFilterCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ProtocolType'), pyxb.binding.datatypes.string, scope=PacketFilterCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 789, 3)))

PacketFilterCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'State'), pyxb.binding.datatypes.string, scope=PacketFilterCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 790, 3)))

def _BuildAutomaton_12 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_12
    del _BuildAutomaton_12
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 781, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 782, 6))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 783, 3))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 784, 3))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 785, 3))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 786, 3))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 787, 3))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 788, 3))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 789, 3))
    counters.add(cc_8)
    cc_9 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 790, 3))
    counters.add(cc_9)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(PacketFilterCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'SourceMAC')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 781, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(PacketFilterCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'DestinationMAC')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 782, 6))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(PacketFilterCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'SourceAddress')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 783, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(PacketFilterCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'DestinationAddress')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 784, 3))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(PacketFilterCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'SourcePort')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 785, 3))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(PacketFilterCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'DestinationPort')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 786, 3))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(PacketFilterCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'direction')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 787, 3))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(PacketFilterCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Interface')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 788, 3))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(PacketFilterCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ProtocolType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 789, 3))
    st_8 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_9, False))
    symbol = pyxb.binding.content.ElementUse(PacketFilterCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'State')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 790, 3))
    st_9 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_8, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_8, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_9, True) ]))
    st_9._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
PacketFilterCondition._Automaton = _BuildAutomaton_12()




QoSCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'profile'), pyxb.binding.datatypes.string, scope=QoSCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 798, 3)))

QoSCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'throughput'), pyxb.binding.datatypes.integer, scope=QoSCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 799, 3)))

QoSCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'transitDelay'), pyxb.binding.datatypes.integer, scope=QoSCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 800, 3)))

QoSCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'priority'), pyxb.binding.datatypes.integer, scope=QoSCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 801, 3)))

QoSCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'errorRate'), pyxb.binding.datatypes.integer, scope=QoSCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 802, 3)))

QoSCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'resilence'), pyxb.binding.datatypes.integer, scope=QoSCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 803, 3)))

def _BuildAutomaton_13 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_13
    del _BuildAutomaton_13
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 798, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 799, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 800, 3))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 801, 3))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 802, 3))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 803, 3))
    counters.add(cc_5)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(QoSCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'profile')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 798, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(QoSCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'throughput')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 799, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(QoSCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'transitDelay')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 800, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(QoSCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'priority')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 801, 3))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(QoSCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'errorRate')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 802, 3))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(QoSCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'resilence')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 803, 3))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
QoSCondition._Automaton = _BuildAutomaton_13()




ApplicationLayerCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'applicationProtocol'), pyxb.binding.datatypes.string, scope=ApplicationLayerCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 812, 3)))

ApplicationLayerCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'URL'), pyxb.binding.datatypes.string, scope=ApplicationLayerCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 813, 3)))

ApplicationLayerCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'httpCondition'), HTTPCondition, scope=ApplicationLayerCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 814, 3)))

ApplicationLayerCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'fileExtension'), pyxb.binding.datatypes.string, scope=ApplicationLayerCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 816, 3)))

ApplicationLayerCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'mimeType'), pyxb.binding.datatypes.string, scope=ApplicationLayerCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 817, 3)))

ApplicationLayerCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'maxconn'), pyxb.binding.datatypes.int, scope=ApplicationLayerCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 819, 3)))

ApplicationLayerCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'dst_domain'), pyxb.binding.datatypes.string, scope=ApplicationLayerCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 821, 3)))

ApplicationLayerCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'src_domain'), pyxb.binding.datatypes.string, scope=ApplicationLayerCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 822, 3)))

ApplicationLayerCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'URL_regex'), pyxb.binding.datatypes.string, scope=ApplicationLayerCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 824, 3)))

def _BuildAutomaton_14 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_14
    del _BuildAutomaton_14
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 812, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 813, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 814, 3))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 816, 3))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 817, 3))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 819, 3))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 821, 3))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 822, 3))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 824, 3))
    counters.add(cc_8)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(ApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'applicationProtocol')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 812, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(ApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'URL')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 813, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(ApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'httpCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 814, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(ApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'fileExtension')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 816, 3))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(ApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'mimeType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 817, 3))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(ApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'maxconn')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 819, 3))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(ApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dst_domain')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 821, 3))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(ApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'src_domain')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 822, 3))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(ApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'URL_regex')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 824, 3))
    st_8 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_8, True) ]))
    st_8._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
ApplicationLayerCondition._Automaton = _BuildAutomaton_14()




StatefulCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'State'), pyxb.binding.datatypes.string, scope=StatefulCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 844, 3)))

StatefulCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'limitRuleHits'), pyxb.binding.datatypes.string, scope=StatefulCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 845, 3)))

def _BuildAutomaton_15 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_15
    del _BuildAutomaton_15
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 844, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 845, 3))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(StatefulCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'State')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 844, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(StatefulCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'limitRuleHits')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 845, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
StatefulCondition._Automaton = _BuildAutomaton_15()




TimeCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'TimeZone'), pyxb.binding.datatypes.string, scope=TimeCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 851, 3)))

TimeCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Weekday'), pyxb.binding.datatypes.string, scope=TimeCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 852, 3)))

TimeCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Time'), pyxb.binding.datatypes.string, scope=TimeCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 853, 3)))

def _BuildAutomaton_16 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_16
    del _BuildAutomaton_16
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 851, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 852, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 853, 3))
    counters.add(cc_2)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(TimeCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'TimeZone')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 851, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(TimeCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Weekday')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 852, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(TimeCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Time')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 853, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
TimeCondition._Automaton = _BuildAutomaton_16()




EventCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'events'), pyxb.binding.datatypes.string, scope=EventCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 880, 3)))

EventCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'interval'), pyxb.binding.datatypes.integer, scope=EventCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 881, 3)))

EventCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'threshold'), pyxb.binding.datatypes.integer, scope=EventCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 882, 3)))

def _BuildAutomaton_17 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_17
    del _BuildAutomaton_17
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 880, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 881, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 882, 3))
    counters.add(cc_2)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(EventCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'events')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 880, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(EventCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'interval')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 881, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(EventCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'threshold')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 882, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
EventCondition._Automaton = _BuildAutomaton_17()




AuthenticationOption._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationTarget'), PacketFilterCondition, scope=AuthenticationOption, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1231, 3)))

AuthenticationOption._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationMethod'), pyxb.binding.datatypes.string, scope=AuthenticationOption, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1232, 3)))

AuthenticationOption._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationMechanism'), pyxb.binding.datatypes.string, scope=AuthenticationOption, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1233, 3)))

AuthenticationOption._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationParameters'), AuthenticationParameters, scope=AuthenticationOption, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1234, 3)))

def _BuildAutomaton_18 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_18
    del _BuildAutomaton_18
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AuthenticationOption._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationTarget')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1231, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AuthenticationOption._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationMethod')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1232, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AuthenticationOption._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationMechanism')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1233, 3))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AuthenticationOption._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationParameters')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1234, 3))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AuthenticationOption._Automaton = _BuildAutomaton_18()




Pics._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ICRA'), ICRA, scope=Pics, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1262, 3)))

Pics._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'RSAC'), RSAC, scope=Pics, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1263, 3)))

Pics._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'evaluWEB'), STD_ANON, scope=Pics, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1264, 3)))

Pics._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'CyberNOTsex'), STD_ANON_, scope=Pics, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1273, 3)))

Pics._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Weburbia'), STD_ANON_2, scope=Pics, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1281, 3)))

Pics._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Vancouver'), Vancouver, scope=Pics, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1289, 3)))

Pics._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'SafeNet'), SafeNet, scope=Pics, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1290, 3)))

def _BuildAutomaton_19 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_19
    del _BuildAutomaton_19
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(Pics._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ICRA')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1262, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(Pics._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'RSAC')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1263, 3))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(Pics._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'evaluWEB')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1264, 3))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(Pics._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'CyberNOTsex')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1273, 3))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(Pics._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Weburbia')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1281, 3))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(Pics._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Vancouver')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1289, 3))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(Pics._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'SafeNet')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1290, 3))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
         ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
         ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    st_6._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
Pics._Automaton = _BuildAutomaton_19()




ActionParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'keyExchange'), KeyExchangeParameter, scope=ActionParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1501, 3)))

ActionParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'technologyParameter'), TechnologySpecificParameters, scope=ActionParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1503, 3)))

ActionParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'additionalNetworkConfigurationParameters'), AdditionalNetworkConfigurationParameters, scope=ActionParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1505, 3)))

ActionParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'authenticationParameters'), AuthenticationParameters, scope=ActionParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1507, 3)))

def _BuildAutomaton_20 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_20
    del _BuildAutomaton_20
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1501, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1505, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1507, 3))
    counters.add(cc_2)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ActionParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'keyExchange')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1501, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ActionParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'technologyParameter')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1503, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(ActionParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'additionalNetworkConfigurationParameters')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1505, 3))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(ActionParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'authenticationParameters')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1507, 3))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ActionParameters._Automaton = _BuildAutomaton_20()




KeyExchangeParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'keyExchangeAction'), pyxb.binding.datatypes.string, scope=KeyExchangeParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1513, 3)))

KeyExchangeParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'hashAlgorithm'), pyxb.binding.datatypes.string, scope=KeyExchangeParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1514, 3)))

KeyExchangeParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'symmetricEncryptionAlgorithm'), pyxb.binding.datatypes.string, scope=KeyExchangeParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1515, 3)))

KeyExchangeParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'authenticationType'), pyxb.binding.datatypes.string, scope=KeyExchangeParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1516, 3)))

def _BuildAutomaton_21 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_21
    del _BuildAutomaton_21
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1513, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1514, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1515, 3))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1516, 3))
    counters.add(cc_3)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(KeyExchangeParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'keyExchangeAction')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1513, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(KeyExchangeParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'hashAlgorithm')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1514, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(KeyExchangeParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'symmetricEncryptionAlgorithm')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1515, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(KeyExchangeParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'authenticationType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1516, 3))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
KeyExchangeParameter._Automaton = _BuildAutomaton_21()




AuthenticationParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'psKey_value'), pyxb.binding.datatypes.string, scope=AuthenticationParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1636, 3)))

AuthenticationParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'psKey_path'), pyxb.binding.datatypes.string, scope=AuthenticationParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1637, 3)))

AuthenticationParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ca_path'), pyxb.binding.datatypes.string, scope=AuthenticationParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1638, 3)))

AuthenticationParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'cert_path'), pyxb.binding.datatypes.string, scope=AuthenticationParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1639, 3)))

AuthenticationParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'publicKey_path'), pyxb.binding.datatypes.string, scope=AuthenticationParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1640, 3)))

AuthenticationParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'publicKey_filename'), pyxb.binding.datatypes.string, scope=AuthenticationParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1641, 3)))

AuthenticationParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'publicKey_passphrase'), pyxb.binding.datatypes.string, scope=AuthenticationParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1642, 3)))

AuthenticationParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ca_id'), pyxb.binding.datatypes.string, scope=AuthenticationParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1643, 3)))

AuthenticationParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ca_filename'), pyxb.binding.datatypes.string, scope=AuthenticationParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1644, 3)))

AuthenticationParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'cert_id'), pyxb.binding.datatypes.string, scope=AuthenticationParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1645, 3)))

AuthenticationParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'cert_filename'), pyxb.binding.datatypes.string, scope=AuthenticationParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1646, 3)))

AuthenticationParameters._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'remote_id'), pyxb.binding.datatypes.string, scope=AuthenticationParameters, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1647, 3)))

def _BuildAutomaton_22 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_22
    del _BuildAutomaton_22
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1636, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1637, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1638, 3))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1639, 3))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1640, 3))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1641, 3))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1642, 3))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1643, 3))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1644, 3))
    counters.add(cc_8)
    cc_9 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1645, 3))
    counters.add(cc_9)
    cc_10 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1646, 3))
    counters.add(cc_10)
    cc_11 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1647, 3))
    counters.add(cc_11)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(AuthenticationParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'psKey_value')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1636, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(AuthenticationParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'psKey_path')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1637, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(AuthenticationParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ca_path')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1638, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(AuthenticationParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'cert_path')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1639, 3))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(AuthenticationParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'publicKey_path')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1640, 3))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(AuthenticationParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'publicKey_filename')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1641, 3))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(AuthenticationParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'publicKey_passphrase')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1642, 3))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(AuthenticationParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ca_id')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1643, 3))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(AuthenticationParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ca_filename')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1644, 3))
    st_8 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_9, False))
    symbol = pyxb.binding.content.ElementUse(AuthenticationParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'cert_id')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1645, 3))
    st_9 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_10, False))
    symbol = pyxb.binding.content.ElementUse(AuthenticationParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'cert_filename')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1646, 3))
    st_10 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_10)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_11, False))
    symbol = pyxb.binding.content.ElementUse(AuthenticationParameters._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'remote_id')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1647, 3))
    st_11 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_11)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_8, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_8, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_8, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_9, True) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_9, False) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_9, False) ]))
    st_9._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_10, True) ]))
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_10, False) ]))
    st_10._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_11, [
        fac.UpdateInstruction(cc_11, True) ]))
    st_11._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
AuthenticationParameters._Automaton = _BuildAutomaton_22()




HTTPCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'httpMetod'), pyxb.binding.datatypes.string, scope=HTTPCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1757, 3)))

HTTPCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'browser'), pyxb.binding.datatypes.string, scope=HTTPCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1758, 3)))

HTTPCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'user_cert'), pyxb.binding.datatypes.string, scope=HTTPCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1759, 3)))

HTTPCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ca_cert'), pyxb.binding.datatypes.string, scope=HTTPCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1760, 3)))

HTTPCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'request_mime_type'), pyxb.binding.datatypes.string, scope=HTTPCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1761, 3)))

HTTPCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'response_mime_type'), pyxb.binding.datatypes.string, scope=HTTPCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1762, 3)))

HTTPCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'http_regex_header'), pyxb.binding.datatypes.string, scope=HTTPCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1763, 3)))

HTTPCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'http_status'), pyxb.binding.datatypes.string, scope=HTTPCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1764, 3)))

def _BuildAutomaton_23 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_23
    del _BuildAutomaton_23
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1757, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1758, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1759, 3))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1760, 3))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1761, 3))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1762, 3))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1763, 3))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1764, 3))
    counters.add(cc_7)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(HTTPCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'httpMetod')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1757, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(HTTPCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'browser')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1758, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(HTTPCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'user_cert')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1759, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(HTTPCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ca_cert')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1760, 3))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(HTTPCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'request_mime_type')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1761, 3))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(HTTPCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'response_mime_type')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1762, 3))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(HTTPCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'http_regex_header')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1763, 3))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(HTTPCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'http_status')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1764, 3))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    st_7._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
HTTPCondition._Automaton = _BuildAutomaton_23()




PolicyDependency._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'configurationCondition'), PolicyDependencyCondition, scope=PolicyDependency, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 62, 5)))

def _BuildAutomaton_24 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_24
    del _BuildAutomaton_24
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(PolicyDependency._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'configurationCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 62, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
PolicyDependency._Automaton = _BuildAutomaton_24()




PolicyDependencyCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'policyID'), pyxb.binding.datatypes.string, scope=PolicyDependencyCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 73, 5)))

PolicyDependencyCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'status'), pyxb.binding.datatypes.string, scope=PolicyDependencyCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 74, 5)))

def _BuildAutomaton_25 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_25
    del _BuildAutomaton_25
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(PolicyDependencyCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(PolicyDependencyCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'policyID')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 73, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(PolicyDependencyCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'status')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 74, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
PolicyDependencyCondition._Automaton = _BuildAutomaton_25()




EventDependency._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'eventID'), pyxb.binding.datatypes.string, scope=EventDependency, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 84, 5)))

EventDependency._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'configurationCondition'), ConfigurationCondition, scope=EventDependency, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 85, 5)))

def _BuildAutomaton_26 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_26
    del _BuildAutomaton_26
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(EventDependency._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'eventID')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 84, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(EventDependency._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'configurationCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 85, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
EventDependency._Automaton = _BuildAutomaton_26()




def _BuildAutomaton_27 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_27
    del _BuildAutomaton_27
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AuthorizationCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AuthorizationCapability._Automaton = _BuildAutomaton_27()




def _BuildAutomaton_28 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_28
    del _BuildAutomaton_28
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AuthenticationCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AuthenticationCapability._Automaton = _BuildAutomaton_28()




TrafficAnalysisCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'supportOnlineTraficAnalysis'), pyxb.binding.datatypes.boolean, scope=TrafficAnalysisCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 185, 5)))

TrafficAnalysisCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'supportOfflineTraficAnalysis'), pyxb.binding.datatypes.boolean, scope=TrafficAnalysisCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 186, 5)))

def _BuildAutomaton_29 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_29
    del _BuildAutomaton_29
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TrafficAnalysisCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TrafficAnalysisCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportOnlineTraficAnalysis')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 185, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(TrafficAnalysisCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportOfflineTraficAnalysis')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 186, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
TrafficAnalysisCapability._Automaton = _BuildAutomaton_29()




ResourceScannerCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'resourceType'), pyxb.binding.datatypes.string, scope=ResourceScannerCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 236, 5)))

def _BuildAutomaton_30 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_30
    del _BuildAutomaton_30
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ResourceScannerCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ResourceScannerCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'resourceType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 236, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ResourceScannerCapability._Automaton = _BuildAutomaton_30()




DataProtectionCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'supportsDataAuthenticationAndIntegrity'), pyxb.binding.datatypes.boolean, scope=DataProtectionCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 270, 5)))

DataProtectionCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'supportsDigitalSignature'), pyxb.binding.datatypes.boolean, scope=DataProtectionCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 272, 5)))

DataProtectionCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'supportsEncryption'), pyxb.binding.datatypes.boolean, scope=DataProtectionCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 274, 5)))

DataProtectionCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'supportsKeyExchange'), pyxb.binding.datatypes.boolean, scope=DataProtectionCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 275, 5)))

def _BuildAutomaton_31 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_31
    del _BuildAutomaton_31
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DataProtectionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DataProtectionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportsDataAuthenticationAndIntegrity')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 270, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DataProtectionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportsDigitalSignature')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 272, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DataProtectionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportsEncryption')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 274, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(DataProtectionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportsKeyExchange')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 275, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
DataProtectionCapability._Automaton = _BuildAutomaton_31()




def _BuildAutomaton_32 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_32
    del _BuildAutomaton_32
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(IdentityProtectionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
IdentityProtectionCapability._Automaton = _BuildAutomaton_32()




def _BuildAutomaton_33 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_33
    del _BuildAutomaton_33
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AddressTranslationCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AddressTranslationCapability._Automaton = _BuildAutomaton_33()




def _BuildAutomaton_34 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_34
    del _BuildAutomaton_34
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(RoutingCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
RoutingCapability._Automaton = _BuildAutomaton_34()




RuleSetConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'defaultAction'), ConfigurationAction, scope=RuleSetConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 340, 5)))

RuleSetConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'configurationRule'), ConfigurationRule, scope=RuleSetConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 342, 5)))

RuleSetConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'resolutionStrategy'), ResolutionStrategy, scope=RuleSetConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 344, 5)))

RuleSetConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Name'), pyxb.binding.datatypes.string, scope=RuleSetConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 346, 5)))

def _BuildAutomaton_35 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_35
    del _BuildAutomaton_35
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 340, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 342, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 344, 5))
    counters.add(cc_2)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(RuleSetConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'capability')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 104, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(RuleSetConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'defaultAction')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 340, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(RuleSetConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'configurationRule')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 342, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(RuleSetConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'resolutionStrategy')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 344, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(RuleSetConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 346, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
RuleSetConfiguration._Automaton = _BuildAutomaton_35()




Priority._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value'), pyxb.binding.datatypes.integer, scope=Priority, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 380, 5)))

def _BuildAutomaton_36 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_36
    del _BuildAutomaton_36
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(Priority._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 380, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
Priority._Automaton = _BuildAutomaton_36()




def _BuildAutomaton_37 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_37
    del _BuildAutomaton_37
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(FMR._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'externalData')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
FMR._Automaton = _BuildAutomaton_37()




def _BuildAutomaton_38 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_38
    del _BuildAutomaton_38
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(DTP._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'externalData')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
DTP._Automaton = _BuildAutomaton_38()




def _BuildAutomaton_39 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_39
    del _BuildAutomaton_39
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(ATP._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'externalData')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
ATP._Automaton = _BuildAutomaton_39()




def _BuildAutomaton_40 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_40
    del _BuildAutomaton_40
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(MSTP._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'externalData')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
MSTP._Automaton = _BuildAutomaton_40()




def _BuildAutomaton_41 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_41
    del _BuildAutomaton_41
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(LSTP._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'externalData')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
LSTP._Automaton = _BuildAutomaton_41()




def _BuildAutomaton_42 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_42
    del _BuildAutomaton_42
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(ALL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'externalData')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 388, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
ALL._Automaton = _BuildAutomaton_42()




DataProtectionCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition'), PacketFilterCondition, scope=DataProtectionCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5)))

def _BuildAutomaton_43 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_43
    del _BuildAutomaton_43
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(DataProtectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(DataProtectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
DataProtectionCondition._Automaton = _BuildAutomaton_43()




PurposeCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'PurposeCondition'), PurposeConditionType, scope=PurposeCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 444, 5)))

def _BuildAutomaton_44 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_44
    del _BuildAutomaton_44
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 444, 5))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(PurposeCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(PurposeCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'PurposeCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 444, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
PurposeCondition._Automaton = _BuildAutomaton_44()




PrivacyAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'PrivacyActionType'), PrivacyActionType, scope=PrivacyAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 467, 5)))

PrivacyAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'PrivacyMethod'), PrivacyMethod, scope=PrivacyAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 469, 5)))

def _BuildAutomaton_45 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_45
    del _BuildAutomaton_45
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(PrivacyAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'PrivacyActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 467, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(PrivacyAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'PrivacyMethod')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 469, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
PrivacyAction._Automaton = _BuildAutomaton_45()




PrivacyIBMethod._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'IB'), pyxb.binding.datatypes.string, scope=PrivacyIBMethod, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 481, 5)))

def _BuildAutomaton_46 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_46
    del _BuildAutomaton_46
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(PrivacyIBMethod._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'IB')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 481, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
PrivacyIBMethod._Automaton = _BuildAutomaton_46()




PrivacyABMethod._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'attribute'), KeyValue, scope=PrivacyABMethod, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 491, 5)))

def _BuildAutomaton_47 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_47
    del _BuildAutomaton_47
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(PrivacyABMethod._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'attribute')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 491, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
PrivacyABMethod._Automaton = _BuildAutomaton_47()




PrivacyPKIMethod._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'pkiParameters'), AuthenticationParameters, scope=PrivacyPKIMethod, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 501, 5)))

def _BuildAutomaton_48 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_48
    del _BuildAutomaton_48
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(PrivacyPKIMethod._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'pkiParameters')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 501, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
PrivacyPKIMethod._Automaton = _BuildAutomaton_48()




DataAgreggationConfigurationConditions._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'dataAggregationConfigurationCondition'), DataAggregationConfigurationCondition, scope=DataAgreggationConfigurationConditions, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 527, 5)))

def _BuildAutomaton_49 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_49
    del _BuildAutomaton_49
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DataAgreggationConfigurationConditions._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(DataAgreggationConfigurationConditions._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dataAggregationConfigurationCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 527, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
DataAgreggationConfigurationConditions._Automaton = _BuildAutomaton_49()




DataAggregationAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'dataAggregationActionType'), DataAggregationActionType, scope=DataAggregationAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 545, 5)))

DataAggregationAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'aditionalRuleParameters'), KeyValue, scope=DataAggregationAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 547, 5)))

def _BuildAutomaton_50 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_50
    del _BuildAutomaton_50
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 547, 5))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(DataAggregationAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dataAggregationActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 545, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(DataAggregationAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'aditionalRuleParameters')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 547, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
DataAggregationAction._Automaton = _BuildAutomaton_50()




AnonymityAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'anonymityActionType'), AnonymityActionType, scope=AnonymityAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 573, 5)))

AnonymityAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'anonymityTarget'), AnonymityConfigurationCondition, scope=AnonymityAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 575, 5)))

AnonymityAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'anonymityTechnologyParameters'), AnonymityTechnologyParameter, scope=AnonymityAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 577, 5)))

AnonymityAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'aditionalAnonymityParameters'), KeyValue, scope=AnonymityAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 579, 5)))

def _BuildAutomaton_51 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_51
    del _BuildAutomaton_51
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 575, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 577, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 579, 5))
    counters.add(cc_2)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AnonymityAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'anonymityActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 573, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(AnonymityAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'anonymityTarget')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 575, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(AnonymityAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'anonymityTechnologyParameters')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 577, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(AnonymityAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'aditionalAnonymityParameters')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 579, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AnonymityAction._Automaton = _BuildAutomaton_51()




TrafficMIXERTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'GW'), pyxb.binding.datatypes.string, scope=TrafficMIXERTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 622, 5)))

TrafficMIXERTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'peerID'), pyxb.binding.datatypes.string, scope=TrafficMIXERTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 623, 5)))

TrafficMIXERTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'key'), pyxb.binding.datatypes.string, scope=TrafficMIXERTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 624, 5)))

TrafficMIXERTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'cryptoBackend'), pyxb.binding.datatypes.string, scope=TrafficMIXERTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 625, 5)))

def _BuildAutomaton_52 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_52
    del _BuildAutomaton_52
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TrafficMIXERTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'GW')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 622, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TrafficMIXERTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'peerID')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 623, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TrafficMIXERTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'key')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 624, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(TrafficMIXERTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'cryptoBackend')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 625, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
TrafficMIXERTechnologyParameter._Automaton = _BuildAutomaton_52()




MonitoringConfigurationConditions._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'monitoringConfigurationCondition'), MonitoringConfigurationCondition, scope=MonitoringConfigurationConditions, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 637, 5)))

def _BuildAutomaton_53 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_53
    del _BuildAutomaton_53
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(MonitoringConfigurationConditions._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(MonitoringConfigurationConditions._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'monitoringConfigurationCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 637, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
MonitoringConfigurationConditions._Automaton = _BuildAutomaton_53()




MonitoringAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'monitoringActionType'), MonitoringActionType, scope=MonitoringAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 669, 5)))

MonitoringAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ProbeID'), pyxb.binding.datatypes.string, scope=MonitoringAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 671, 5)))

MonitoringAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'reportPeriodicity'), pyxb.binding.datatypes.integer, scope=MonitoringAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 672, 5)))

MonitoringAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'reportPerFlow'), pyxb.binding.datatypes.boolean, scope=MonitoringAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 673, 5)))

MonitoringAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ruleID'), pyxb.binding.datatypes.string, scope=MonitoringAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 674, 5)))

MonitoringAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'count'), pyxb.binding.datatypes.string, scope=MonitoringAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 675, 5)))

MonitoringAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'aditionalMonitoringParameters'), KeyValue, scope=MonitoringAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 676, 5)))

def _BuildAutomaton_54 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_54
    del _BuildAutomaton_54
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 671, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 672, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 673, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 674, 5))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 675, 5))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 676, 5))
    counters.add(cc_5)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(MonitoringAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'monitoringActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 669, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(MonitoringAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ProbeID')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 671, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(MonitoringAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'reportPeriodicity')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 672, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(MonitoringAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'reportPerFlow')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 673, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(MonitoringAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ruleID')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 674, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(MonitoringAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'count')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 675, 5))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(MonitoringAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'aditionalMonitoringParameters')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 676, 5))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, True) ]))
    st_6._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
MonitoringAction._Automaton = _BuildAutomaton_54()




NetworkSlicingConfigurationCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'networkSlicingConditionParameters'), NetworkSlicingConditionParameters, scope=NetworkSlicingConfigurationCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 700, 5)))

def _BuildAutomaton_55 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_55
    del _BuildAutomaton_55
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 700, 5))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(NetworkSlicingConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(NetworkSlicingConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'networkSlicingConditionParameters')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 700, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
NetworkSlicingConfigurationCondition._Automaton = _BuildAutomaton_55()




NetworkSlicingAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'NetworkSlicingActionType'), NetworkSlicingActionType, scope=NetworkSlicingAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 711, 5)))

def _BuildAutomaton_56 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_56
    del _BuildAutomaton_56
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(NetworkSlicingAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'NetworkSlicingActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 711, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
NetworkSlicingAction._Automaton = _BuildAutomaton_56()




QoSAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'qosAction'), QoSCondition, scope=QoSAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 750, 5)))

def _BuildAutomaton_57 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_57
    del _BuildAutomaton_57
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 750, 5))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(QoSAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'qosAction')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 750, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
QoSAction._Automaton = _BuildAutomaton_57()




FilteringConfigurationCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition'), PacketFilterCondition, scope=FilteringConfigurationCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5)))

FilteringConfigurationCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'statefulCondition'), StatefulCondition, scope=FilteringConfigurationCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5)))

FilteringConfigurationCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'timeCondition'), TimeCondition, scope=FilteringConfigurationCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5)))

FilteringConfigurationCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerCondition'), ApplicationLayerCondition, scope=FilteringConfigurationCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5)))

FilteringConfigurationCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'qosCondition'), QoSCondition, scope=FilteringConfigurationCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5)))

def _BuildAutomaton_58 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_58
    del _BuildAutomaton_58
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    counters.add(cc_4)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(FilteringConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(FilteringConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(FilteringConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'statefulCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(FilteringConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'timeCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(FilteringConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(FilteringConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'qosCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, True) ]))
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
FilteringConfigurationCondition._Automaton = _BuildAutomaton_58()




IoTApplicationLayerCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'method'), pyxb.binding.datatypes.string, scope=IoTApplicationLayerCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 834, 5)))

def _BuildAutomaton_59 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_59
    del _BuildAutomaton_59
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 812, 3))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 813, 3))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 814, 3))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 816, 3))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 817, 3))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 819, 3))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 821, 3))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 822, 3))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 824, 3))
    counters.add(cc_8)
    cc_9 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 834, 5))
    counters.add(cc_9)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(IoTApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'applicationProtocol')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 812, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(IoTApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'URL')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 813, 3))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(IoTApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'httpCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 814, 3))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(IoTApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'fileExtension')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 816, 3))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(IoTApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'mimeType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 817, 3))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(IoTApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'maxconn')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 819, 3))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(IoTApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dst_domain')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 821, 3))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(IoTApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'src_domain')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 822, 3))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(IoTApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'URL_regex')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 824, 3))
    st_8 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_9, False))
    symbol = pyxb.binding.content.ElementUse(IoTApplicationLayerCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'method')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 834, 5))
    st_9 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_8, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_8, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_9, True) ]))
    st_9._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
IoTApplicationLayerCondition._Automaton = _BuildAutomaton_59()




Anti_malwareCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'fileSystemCondition'), FileSystemCondition, scope=Anti_malwareCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 891, 5)))

Anti_malwareCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerCondition'), ApplicationLayerCondition, scope=Anti_malwareCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 893, 5)))

Anti_malwareCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'eventCondition'), EventCondition, scope=Anti_malwareCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 895, 5)))

def _BuildAutomaton_60 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_60
    del _BuildAutomaton_60
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 891, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 893, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 895, 5))
    counters.add(cc_2)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(Anti_malwareCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(Anti_malwareCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'fileSystemCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 891, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(Anti_malwareCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 893, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(Anti_malwareCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'eventCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 895, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
Anti_malwareCondition._Automaton = _BuildAutomaton_60()




LoggingCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'eventCondition'), EventCondition, scope=LoggingCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 906, 5)))

LoggingCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'object'), pyxb.binding.datatypes.string, scope=LoggingCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 909, 5)))

LoggingCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'packetCondition'), PacketFilterCondition, scope=LoggingCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 912, 5)))

LoggingCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'applicationCondition'), ApplicationLayerCondition, scope=LoggingCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 914, 5)))

def _BuildAutomaton_61 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_61
    del _BuildAutomaton_61
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 906, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 909, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 912, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 914, 5))
    counters.add(cc_3)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(LoggingCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(LoggingCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'eventCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 906, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(LoggingCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'object')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 909, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(LoggingCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 912, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(LoggingCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'applicationCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 914, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
LoggingCondition._Automaton = _BuildAutomaton_61()




TrafficDivertAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'TrafficDivertActionType'), TrafficDivertActionType, scope=TrafficDivertAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 967, 5)))

TrafficDivertAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'packetDivertAction'), TrafficDivertConfigurationCondition, scope=TrafficDivertAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 969, 5)))

def _BuildAutomaton_62 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_62
    del _BuildAutomaton_62
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TrafficDivertAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'TrafficDivertActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 967, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(TrafficDivertAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetDivertAction')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 969, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
TrafficDivertAction._Automaton = _BuildAutomaton_62()




FilteringAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'FilteringActionType'), pyxb.binding.datatypes.string, scope=FilteringAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 999, 5)))

def _BuildAutomaton_63 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_63
    del _BuildAutomaton_63
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(FilteringAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'FilteringActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 999, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
FilteringAction._Automaton = _BuildAutomaton_63()




EnableAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'EnableActionType'), EnableActionType, scope=EnableAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1010, 5)))

def _BuildAutomaton_64 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_64
    del _BuildAutomaton_64
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(EnableAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'EnableActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1010, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
EnableAction._Automaton = _BuildAutomaton_64()




ReduceBandwidthAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ReduceBandwidthActionType'), ReduceBandwidthActionType, scope=ReduceBandwidthAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1021, 5)))

def _BuildAutomaton_65 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_65
    del _BuildAutomaton_65
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ReduceBandwidthAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ReduceBandwidthActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1021, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ReduceBandwidthAction._Automaton = _BuildAutomaton_65()




CountAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'CountActionType'), CountActionType, scope=CountAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1034, 5)))

def _BuildAutomaton_66 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_66
    del _BuildAutomaton_66
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CountAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'CountActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1034, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CountAction._Automaton = _BuildAutomaton_66()




CheckAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'CheckActionType'), CheckActionType, scope=CheckAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1045, 5)))

def _BuildAutomaton_67 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_67
    del _BuildAutomaton_67
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(CheckAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'CheckActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1045, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
CheckAction._Automaton = _BuildAutomaton_67()




RemoveAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'RemoveActionType'), RemoveActionType, scope=RemoveAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1055, 5)))

def _BuildAutomaton_68 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_68
    del _BuildAutomaton_68
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(RemoveAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'RemoveActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1055, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
RemoveAction._Automaton = _BuildAutomaton_68()




Anti_malwareAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'anti-malwareActionType'), pyxb.binding.datatypes.string, scope=Anti_malwareAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1065, 5)))

def _BuildAutomaton_69 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_69
    del _BuildAutomaton_69
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(Anti_malwareAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'anti-malwareActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1065, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
Anti_malwareAction._Automaton = _BuildAutomaton_69()




LoggingAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'loggingActionType'), pyxb.binding.datatypes.string, scope=LoggingAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1076, 5)))

def _BuildAutomaton_70 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_70
    del _BuildAutomaton_70
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(LoggingAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'loggingActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1076, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
LoggingAction._Automaton = _BuildAutomaton_70()




PowerMgmtAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'PowerMgmtActionType'), PowerMgmtActionType, scope=PowerMgmtAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1152, 5)))

def _BuildAutomaton_71 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_71
    del _BuildAutomaton_71
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(PowerMgmtAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'PowerMgmtActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1152, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
PowerMgmtAction._Automaton = _BuildAutomaton_71()




VIoTHoneyNetAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'VIoTHoneyNetActionType'), VIoTHoneyNetActionType, scope=VIoTHoneyNetAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1169, 5)))

VIoTHoneyNetAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ioTHoneyNet'), _ImportedBinding__IoTHoneyNet.ioTHoneyNetType, scope=VIoTHoneyNetAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1170, 5)))

def _BuildAutomaton_72 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_72
    del _BuildAutomaton_72
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(VIoTHoneyNetAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'VIoTHoneyNetActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1169, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(VIoTHoneyNetAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ioTHoneyNet')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1170, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
VIoTHoneyNetAction._Automaton = _BuildAutomaton_72()




AuthorizationAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationActionType'), AuthorizationActionType, scope=AuthorizationAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1188, 5)))

AuthorizationAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationSubject'), pyxb.binding.datatypes.string, scope=AuthorizationAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1190, 5)))

AuthorizationAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationTarget'), pyxb.binding.datatypes.string, scope=AuthorizationAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1191, 5)))

def _BuildAutomaton_73 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_73
    del _BuildAutomaton_73
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1190, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1191, 5))
    counters.add(cc_1)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AuthorizationAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1188, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(AuthorizationAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationSubject')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1190, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(AuthorizationAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationTarget')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1191, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AuthorizationAction._Automaton = _BuildAutomaton_73()




AuthenticationAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationOption'), AuthenticationOption, scope=AuthenticationAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1222, 5)))

def _BuildAutomaton_74 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_74
    del _BuildAutomaton_74
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AuthenticationAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationOption')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1222, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AuthenticationAction._Automaton = _BuildAutomaton_74()




DataProtectionAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'technology'), pyxb.binding.datatypes.string, scope=DataProtectionAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1451, 5)))

DataProtectionAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'technologyActionParameters'), ActionParameters, scope=DataProtectionAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1452, 5)))

DataProtectionAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'technologyActionSecurityProperty'), TechnologyActionSecurityProperty, scope=DataProtectionAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1454, 5)))

def _BuildAutomaton_75 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_75
    del _BuildAutomaton_75
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1454, 5))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DataProtectionAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'technology')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1451, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(DataProtectionAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'technologyActionParameters')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1452, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(DataProtectionAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'technologyActionSecurityProperty')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1454, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
DataProtectionAction._Automaton = _BuildAutomaton_75()




Integrity._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'integrityAlgorithm'), pyxb.binding.datatypes.string, scope=Integrity, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1466, 5)))

Integrity._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'integrityHeader'), pyxb.binding.datatypes.boolean, scope=Integrity, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1467, 5)))

Integrity._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'integrityPayload'), pyxb.binding.datatypes.boolean, scope=Integrity, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1468, 5)))

def _BuildAutomaton_76 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_76
    del _BuildAutomaton_76
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1466, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1467, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1468, 5))
    counters.add(cc_2)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(Integrity._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'integrityAlgorithm')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1466, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(Integrity._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'integrityHeader')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1467, 5))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(Integrity._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'integrityPayload')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1468, 5))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
Integrity._Automaton = _BuildAutomaton_76()




Authentication._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'serverAuthenticationMechanism'), pyxb.binding.datatypes.string, scope=Authentication, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1477, 5)))

Authentication._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'clientAuthenticationMechanism'), pyxb.binding.datatypes.string, scope=Authentication, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1479, 5)))

Authentication._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'peerAuthenticationMechanism'), pyxb.binding.datatypes.string, scope=Authentication, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1481, 5)))

def _BuildAutomaton_77 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_77
    del _BuildAutomaton_77
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1477, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1479, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1481, 5))
    counters.add(cc_2)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(Authentication._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'serverAuthenticationMechanism')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1477, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(Authentication._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'clientAuthenticationMechanism')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1479, 5))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(Authentication._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'peerAuthenticationMechanism')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1481, 5))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
Authentication._Automaton = _BuildAutomaton_77()




Confidentiality._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'encryptionAlgorithm'), pyxb.binding.datatypes.string, scope=Confidentiality, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1491, 5)))

Confidentiality._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'keySize'), pyxb.binding.datatypes.string, scope=Confidentiality, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1492, 5)))

Confidentiality._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'mode'), pyxb.binding.datatypes.string, scope=Confidentiality, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1493, 5)))

def _BuildAutomaton_78 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_78
    del _BuildAutomaton_78
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1491, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1492, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1493, 5))
    counters.add(cc_2)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(Confidentiality._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'encryptionAlgorithm')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1491, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(Confidentiality._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'keySize')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1492, 5))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(Confidentiality._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'mode')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1493, 5))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
Confidentiality._Automaton = _BuildAutomaton_78()




IPsecTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'IPsecProtocol'), pyxb.binding.datatypes.string, scope=IPsecTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1524, 5)))

IPsecTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'isTunnel'), pyxb.binding.datatypes.boolean, scope=IPsecTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1525, 5)))

IPsecTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'localEndpoint'), pyxb.binding.datatypes.string, scope=IPsecTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1526, 5)))

IPsecTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'remoteEndpoint'), pyxb.binding.datatypes.string, scope=IPsecTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1527, 5)))

def _BuildAutomaton_79 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_79
    del _BuildAutomaton_79
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1524, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1525, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1526, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1527, 5))
    counters.add(cc_3)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(IPsecTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'IPsecProtocol')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1524, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(IPsecTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isTunnel')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1525, 5))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(IPsecTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'localEndpoint')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1526, 5))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(IPsecTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'remoteEndpoint')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1527, 5))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
IPsecTechnologyParameter._Automaton = _BuildAutomaton_79()




IKETechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'phase2_pfs_group'), pyxb.binding.datatypes.string, scope=IKETechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1547, 5)))

IKETechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'exchangeMode'), pyxb.binding.datatypes.string, scope=IKETechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1548, 5)))

IKETechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'phase1_dh_group'), pyxb.binding.datatypes.string, scope=IKETechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1549, 5)))

IKETechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'phase2_compression_algorithm'), pyxb.binding.datatypes.string, scope=IKETechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1550, 5)))

IKETechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'hash_algorithm'), pyxb.binding.datatypes.string, scope=IKETechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1552, 5)))

IKETechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ESN'), pyxb.binding.datatypes.boolean, scope=IKETechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1553, 5)))

IKETechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'encryptionAlgorithm'), pyxb.binding.datatypes.string, scope=IKETechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1554, 5)))

IKETechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'lifetime'), pyxb.binding.datatypes.string, scope=IKETechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1555, 5)))

IKETechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'rekey_margin'), pyxb.binding.datatypes.string, scope=IKETechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1556, 5)))

IKETechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'keyring_tries'), pyxb.binding.datatypes.string, scope=IKETechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1557, 5)))

IKETechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'MOBIKE'), pyxb.binding.datatypes.boolean, scope=IKETechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1558, 5)))

def _BuildAutomaton_80 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_80
    del _BuildAutomaton_80
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1547, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1548, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1549, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1550, 5))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1552, 5))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1553, 5))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1555, 5))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1556, 5))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1557, 5))
    counters.add(cc_8)
    cc_9 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1558, 5))
    counters.add(cc_9)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(IKETechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'phase2_pfs_group')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1547, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(IKETechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'exchangeMode')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1548, 5))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(IKETechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'phase1_dh_group')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1549, 5))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(IKETechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'phase2_compression_algorithm')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1550, 5))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(IKETechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'hash_algorithm')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1552, 5))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(IKETechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ESN')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1553, 5))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(IKETechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'encryptionAlgorithm')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1554, 5))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(IKETechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'lifetime')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1555, 5))
    st_7 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(IKETechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'rekey_margin')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1556, 5))
    st_8 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(IKETechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'keyring_tries')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1557, 5))
    st_9 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_9, False))
    symbol = pyxb.binding.content.ElementUse(IKETechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'MOBIKE')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1558, 5))
    st_10 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_10)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
         ]))
    transitions.append(fac.Transition(st_8, [
         ]))
    transitions.append(fac.Transition(st_9, [
         ]))
    transitions.append(fac.Transition(st_10, [
         ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_7, False) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_8, True) ]))
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_8, False) ]))
    st_9._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_10, [
        fac.UpdateInstruction(cc_9, True) ]))
    st_10._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
IKETechnologyParameter._Automaton = _BuildAutomaton_80()




TLS_VPN_TechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'peerPort'), pyxb.binding.datatypes.string, scope=TLS_VPN_TechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1567, 5)))

TLS_VPN_TechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'L4Protocol'), pyxb.binding.datatypes.string, scope=TLS_VPN_TechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1568, 5)))

TLS_VPN_TechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'localEndpoint'), pyxb.binding.datatypes.string, scope=TLS_VPN_TechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1569, 5)))

TLS_VPN_TechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'remoteEndpoint'), pyxb.binding.datatypes.string, scope=TLS_VPN_TechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1570, 5)))

TLS_VPN_TechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'virtualIPSource'), pyxb.binding.datatypes.string, scope=TLS_VPN_TechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1571, 5)))

TLS_VPN_TechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'virtualIPDestination'), pyxb.binding.datatypes.string, scope=TLS_VPN_TechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1572, 5)))

TLS_VPN_TechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'device'), pyxb.binding.datatypes.string, scope=TLS_VPN_TechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1573, 5)))

TLS_VPN_TechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'tlsMode'), pyxb.binding.datatypes.string, scope=TLS_VPN_TechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1574, 5)))

def _BuildAutomaton_81 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_81
    del _BuildAutomaton_81
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1567, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1568, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1569, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1570, 5))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1571, 5))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1572, 5))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1573, 5))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1574, 5))
    counters.add(cc_7)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(TLS_VPN_TechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'peerPort')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1567, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(TLS_VPN_TechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'L4Protocol')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1568, 5))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(TLS_VPN_TechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'localEndpoint')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1569, 5))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(TLS_VPN_TechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'remoteEndpoint')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1570, 5))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(TLS_VPN_TechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'virtualIPSource')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1571, 5))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(TLS_VPN_TechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'virtualIPDestination')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1572, 5))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(TLS_VPN_TechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'device')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1573, 5))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(TLS_VPN_TechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'tlsMode')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1574, 5))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    st_7._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
TLS_VPN_TechnologyParameter._Automaton = _BuildAutomaton_81()




TLS_SSL_TechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ciphers-client'), pyxb.binding.datatypes.string, scope=TLS_SSL_TechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1583, 5)))

TLS_SSL_TechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ssl-version-client'), pyxb.binding.datatypes.string, scope=TLS_SSL_TechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1584, 5)))

TLS_SSL_TechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ciphers-server'), pyxb.binding.datatypes.string, scope=TLS_SSL_TechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1585, 5)))

TLS_SSL_TechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'ssl-version-server'), pyxb.binding.datatypes.string, scope=TLS_SSL_TechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1586, 5)))

def _BuildAutomaton_82 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_82
    del _BuildAutomaton_82
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TLS_SSL_TechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ciphers-client')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1583, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TLS_SSL_TechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ssl-version-client')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1584, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TLS_SSL_TechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ciphers-server')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1585, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(TLS_SSL_TechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'ssl-version-server')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1586, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
TLS_SSL_TechnologyParameter._Automaton = _BuildAutomaton_82()




Site2SiteNetworkConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'localNetwork'), pyxb.binding.datatypes.string, scope=Site2SiteNetworkConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1607, 5)))

Site2SiteNetworkConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'remoteNetwork'), pyxb.binding.datatypes.string, scope=Site2SiteNetworkConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1608, 5)))

Site2SiteNetworkConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'localNetworkNetmask'), pyxb.binding.datatypes.string, scope=Site2SiteNetworkConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1609, 5)))

Site2SiteNetworkConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'remoteNetworkNetmask'), pyxb.binding.datatypes.string, scope=Site2SiteNetworkConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1610, 5)))

def _BuildAutomaton_83 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_83
    del _BuildAutomaton_83
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1607, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1608, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1609, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1610, 5))
    counters.add(cc_3)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(Site2SiteNetworkConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'localNetwork')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1607, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(Site2SiteNetworkConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'remoteNetwork')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1608, 5))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(Site2SiteNetworkConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'localNetworkNetmask')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1609, 5))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(Site2SiteNetworkConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'remoteNetworkNetmask')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1610, 5))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
Site2SiteNetworkConfiguration._Automaton = _BuildAutomaton_83()




RemoteAccessNetworkConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'startIPAddress'), pyxb.binding.datatypes.string, scope=RemoteAccessNetworkConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1619, 5)))

RemoteAccessNetworkConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'maxClients'), pyxb.binding.datatypes.integer, scope=RemoteAccessNetworkConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1620, 5)))

RemoteAccessNetworkConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'netmask'), pyxb.binding.datatypes.string, scope=RemoteAccessNetworkConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1621, 5)))

RemoteAccessNetworkConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'routedSubnets'), pyxb.binding.datatypes.string, scope=RemoteAccessNetworkConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1622, 5)))

RemoteAccessNetworkConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'dnsServer'), pyxb.binding.datatypes.string, scope=RemoteAccessNetworkConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1623, 5)))

RemoteAccessNetworkConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'domainSuffix'), pyxb.binding.datatypes.string, scope=RemoteAccessNetworkConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1624, 5)))

RemoteAccessNetworkConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'wins'), pyxb.binding.datatypes.string, scope=RemoteAccessNetworkConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1625, 5)))

RemoteAccessNetworkConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'localSubnet'), pyxb.binding.datatypes.string, scope=RemoteAccessNetworkConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1627, 5)))

RemoteAccessNetworkConfiguration._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'remoteSubnet'), pyxb.binding.datatypes.string, scope=RemoteAccessNetworkConfiguration, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1628, 5)))

def _BuildAutomaton_84 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_84
    del _BuildAutomaton_84
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1619, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1620, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1621, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1622, 5))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1623, 5))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1624, 5))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1625, 5))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1627, 5))
    counters.add(cc_7)
    cc_8 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1628, 5))
    counters.add(cc_8)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(RemoteAccessNetworkConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'startIPAddress')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1619, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(RemoteAccessNetworkConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'maxClients')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1620, 5))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(RemoteAccessNetworkConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'netmask')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1621, 5))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(RemoteAccessNetworkConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'routedSubnets')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1622, 5))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(RemoteAccessNetworkConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dnsServer')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1623, 5))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(RemoteAccessNetworkConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'domainSuffix')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1624, 5))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(RemoteAccessNetworkConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'wins')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1625, 5))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(RemoteAccessNetworkConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'localSubnet')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1627, 5))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_8, False))
    symbol = pyxb.binding.content.ElementUse(RemoteAccessNetworkConfiguration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'remoteSubnet')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1628, 5))
    st_8 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_7, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_8, True) ]))
    st_8._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
RemoteAccessNetworkConfiguration._Automaton = _BuildAutomaton_84()




def _BuildAutomaton_85 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_85
    del _BuildAutomaton_85
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ChannelAuthorizationCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ChannelAuthorizationCapability._Automaton = _BuildAutomaton_85()




def _BuildAutomaton_86 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_86
    del _BuildAutomaton_86
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(TargetAuthzCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
TargetAuthzCapability._Automaton = _BuildAutomaton_86()




def _BuildAutomaton_87 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_87
    del _BuildAutomaton_87
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AuthcEnforcementCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AuthcEnforcementCapability._Automaton = _BuildAutomaton_87()




AuthcDecisionCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'authenticationMethods'), pyxb.binding.datatypes.string, scope=AuthcDecisionCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 174, 5)))

def _BuildAutomaton_88 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_88
    del _BuildAutomaton_88
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AuthcDecisionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AuthcDecisionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'authenticationMethods')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 174, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AuthcDecisionCapability._Automaton = _BuildAutomaton_88()




def _BuildAutomaton_89 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_89
    del _BuildAutomaton_89
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(IDSCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(IDSCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportOnlineTraficAnalysis')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 185, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(IDSCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportOfflineTraficAnalysis')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 186, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
IDSCapability._Automaton = _BuildAutomaton_89()




def _BuildAutomaton_90 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_90
    del _BuildAutomaton_90
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(IPSCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(IPSCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportOnlineTraficAnalysis')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 185, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(IPSCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportOfflineTraficAnalysis')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 186, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
IPSCapability._Automaton = _BuildAutomaton_90()




def _BuildAutomaton_91 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_91
    del _BuildAutomaton_91
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(MalwaresAnalysisCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(MalwaresAnalysisCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportOnlineTraficAnalysis')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 185, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(MalwaresAnalysisCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportOfflineTraficAnalysis')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 186, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
MalwaresAnalysisCapability._Automaton = _BuildAutomaton_91()




LawfulInterceptionCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'trafficType'), pyxb.binding.datatypes.string, scope=LawfulInterceptionCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 225, 5)))

LawfulInterceptionCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Country'), pyxb.binding.datatypes.string, scope=LawfulInterceptionCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 226, 5)))

def _BuildAutomaton_92 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_92
    del _BuildAutomaton_92
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(LawfulInterceptionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(LawfulInterceptionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportOnlineTraficAnalysis')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 185, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(LawfulInterceptionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportOfflineTraficAnalysis')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 186, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(LawfulInterceptionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'trafficType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 225, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(LawfulInterceptionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Country')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 226, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
LawfulInterceptionCapability._Automaton = _BuildAutomaton_92()




def _BuildAutomaton_93 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_93
    del _BuildAutomaton_93
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(VulnerabilitiesScannerCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(VulnerabilitiesScannerCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'resourceType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 236, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
VulnerabilitiesScannerCapability._Automaton = _BuildAutomaton_93()




def _BuildAutomaton_94 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_94
    del _BuildAutomaton_94
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(LoggingCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(LoggingCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'resourceType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 236, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
LoggingCapability._Automaton = _BuildAutomaton_94()




def _BuildAutomaton_95 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_95
    del _BuildAutomaton_95
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(EncryptionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(EncryptionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportsDataAuthenticationAndIntegrity')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 270, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(EncryptionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportsDigitalSignature')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 272, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(EncryptionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportsEncryption')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 274, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(EncryptionCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportsKeyExchange')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 275, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
EncryptionCapability._Automaton = _BuildAutomaton_95()




def _BuildAutomaton_96 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_96
    del _BuildAutomaton_96
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AnonimizerCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AnonimizerCapability._Automaton = _BuildAutomaton_96()




def _BuildAutomaton_97 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_97
    del _BuildAutomaton_97
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ForwardProxyCapabiity._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ForwardProxyCapabiity._Automaton = _BuildAutomaton_97()




def _BuildAutomaton_98 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_98
    del _BuildAutomaton_98
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ReverseProxyCapabiity._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ReverseProxyCapabiity._Automaton = _BuildAutomaton_98()




PrivacyConfigurationCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Subject'), pyxb.binding.datatypes.string, scope=PrivacyConfigurationCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 456, 5)))

PrivacyConfigurationCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Target'), pyxb.binding.datatypes.string, scope=PrivacyConfigurationCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 457, 5)))

def _BuildAutomaton_99 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_99
    del _BuildAutomaton_99
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 456, 5))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 457, 5))
    counters.add(cc_6)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(PrivacyConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(PrivacyConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(PrivacyConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'statefulCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(PrivacyConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'timeCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(PrivacyConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(PrivacyConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'qosCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(PrivacyConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Subject')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 456, 5))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(PrivacyConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Target')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 457, 5))
    st_7 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    transitions.append(fac.Transition(st_7, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, True) ]))
    st_7._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
PrivacyConfigurationCondition._Automaton = _BuildAutomaton_99()




def _BuildAutomaton_100 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_100
    del _BuildAutomaton_100
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    counters.add(cc_4)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(DataAggregationConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(DataAggregationConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(DataAggregationConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'statefulCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(DataAggregationConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'timeCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(DataAggregationConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(DataAggregationConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'qosCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, True) ]))
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
DataAggregationConfigurationCondition._Automaton = _BuildAutomaton_100()




def _BuildAutomaton_101 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_101
    del _BuildAutomaton_101
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    counters.add(cc_4)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AnonymityConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(AnonymityConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(AnonymityConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'statefulCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(AnonymityConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'timeCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(AnonymityConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(AnonymityConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'qosCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, True) ]))
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AnonymityConfigurationCondition._Automaton = _BuildAutomaton_101()




OnionRoutingTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'exitRelay'), pyxb.binding.datatypes.boolean, scope=OnionRoutingTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 606, 5)))

OnionRoutingTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'IPv6Exit'), pyxb.binding.datatypes.boolean, scope=OnionRoutingTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 607, 5)))

OnionRoutingTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'publicRelay'), pyxb.binding.datatypes.boolean, scope=OnionRoutingTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 608, 5)))

OnionRoutingTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'hiddenServiceDir'), pyxb.binding.datatypes.string, scope=OnionRoutingTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 609, 5)))

OnionRoutingTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'hiddenServicePort'), pyxb.binding.datatypes.string, scope=OnionRoutingTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 610, 5)))

OnionRoutingTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'relayBandwidthRate'), pyxb.binding.datatypes.integer, scope=OnionRoutingTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 611, 5)))

OnionRoutingTechnologyParameter._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'relayBandwidthBurst'), pyxb.binding.datatypes.integer, scope=OnionRoutingTechnologyParameter, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 612, 5)))

def _BuildAutomaton_102 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_102
    del _BuildAutomaton_102
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(OnionRoutingTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'exitRelay')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 606, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(OnionRoutingTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'IPv6Exit')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 607, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(OnionRoutingTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'publicRelay')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 608, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(OnionRoutingTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'hiddenServiceDir')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 609, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(OnionRoutingTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'hiddenServicePort')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 610, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(OnionRoutingTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'relayBandwidthRate')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 611, 5))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(OnionRoutingTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'relayBandwidthBurst')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 612, 5))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
         ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
         ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    st_6._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
OnionRoutingTechnologyParameter._Automaton = _BuildAutomaton_102()




MonitoringConfigurationCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'detectionFilter'), pyxb.binding.datatypes.string, scope=MonitoringConfigurationCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 650, 5)))

MonitoringConfigurationCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'signatureList'), SignatureList, scope=MonitoringConfigurationCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 651, 5)))

def _BuildAutomaton_103 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_103
    del _BuildAutomaton_103
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 650, 5))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 651, 5))
    counters.add(cc_6)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(MonitoringConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(MonitoringConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(MonitoringConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'statefulCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(MonitoringConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'timeCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(MonitoringConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(MonitoringConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'qosCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(MonitoringConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'detectionFilter')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 650, 5))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(MonitoringConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'signatureList')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 651, 5))
    st_7 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    transitions.append(fac.Transition(st_7, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, True) ]))
    st_7._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
MonitoringConfigurationCondition._Automaton = _BuildAutomaton_103()




def _BuildAutomaton_104 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_104
    del _BuildAutomaton_104
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    counters.add(cc_4)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(TrafficDivertConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(TrafficDivertConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(TrafficDivertConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'statefulCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(TrafficDivertConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'timeCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(TrafficDivertConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(TrafficDivertConfigurationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'qosCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, True) ]))
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
TrafficDivertConfigurationCondition._Automaton = _BuildAutomaton_104()




def _BuildAutomaton_105 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_105
    del _BuildAutomaton_105
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(InterfaceSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(InterfaceSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
InterfaceSelectionCondition._Automaton = _BuildAutomaton_105()




def _BuildAutomaton_106 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_106
    del _BuildAutomaton_106
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(DataSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(DataSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
DataSelectionCondition._Automaton = _BuildAutomaton_106()




TrafficDivertEncapsulationAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'EncapsulationParameters'), pyxb.binding.datatypes.string, scope=TrafficDivertEncapsulationAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 989, 5)))

def _BuildAutomaton_107 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_107
    del _BuildAutomaton_107
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TrafficDivertEncapsulationAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'TrafficDivertActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 967, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TrafficDivertEncapsulationAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetDivertAction')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 969, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(TrafficDivertEncapsulationAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'EncapsulationParameters')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 989, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
TrafficDivertEncapsulationAction._Automaton = _BuildAutomaton_107()




RemoveAdvertisementAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'RemoveAdvertisementActionType'), RemoveAdvertisementActionType, scope=RemoveAdvertisementAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1086, 5)))

def _BuildAutomaton_108 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_108
    del _BuildAutomaton_108
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(RemoveAdvertisementAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'RemoveActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1055, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(RemoveAdvertisementAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'RemoveAdvertisementActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1086, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
RemoveAdvertisementAction._Automaton = _BuildAutomaton_108()




RemoveTrackingTechniquesAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'RemoveTrackingTechniquesActionType'), RemoveTrackingTechniquesActionType, scope=RemoveTrackingTechniquesAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1098, 5)))

def _BuildAutomaton_109 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_109
    del _BuildAutomaton_109
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(RemoveTrackingTechniquesAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'RemoveActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1055, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(RemoveTrackingTechniquesAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'RemoveTrackingTechniquesActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1098, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
RemoveTrackingTechniquesAction._Automaton = _BuildAutomaton_109()




ParentalControlAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'pics'), Pics, scope=ParentalControlAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1111, 5)))

def _BuildAutomaton_110 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_110
    del _BuildAutomaton_110
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1111, 5))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ParentalControlAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'EnableActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1010, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(ParentalControlAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'pics')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1111, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ParentalControlAction._Automaton = _BuildAutomaton_110()




AnonimityAction._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'country'), pyxb.binding.datatypes.string, scope=AnonimityAction, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1122, 5)))

def _BuildAutomaton_111 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_111
    del _BuildAutomaton_111
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1122, 5))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AnonimityAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'EnableActionType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1010, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(AnonimityAction._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'country')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1122, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AnonimityAction._Automaton = _BuildAutomaton_111()




AuthorizationCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationSubject'), pyxb.binding.datatypes.string, scope=AuthorizationCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1201, 5)))

AuthorizationCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationTarget'), pyxb.binding.datatypes.string, scope=AuthorizationCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1202, 5)))

def _BuildAutomaton_112 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_112
    del _BuildAutomaton_112
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1201, 5))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1202, 5))
    counters.add(cc_6)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AuthorizationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(AuthorizationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(AuthorizationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'statefulCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(AuthorizationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'timeCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(AuthorizationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(AuthorizationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'qosCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(AuthorizationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationSubject')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1201, 5))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(AuthorizationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'AuthorizationTarget')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1202, 5))
    st_7 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    transitions.append(fac.Transition(st_7, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, True) ]))
    st_7._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AuthorizationCondition._Automaton = _BuildAutomaton_112()




AuthenticationCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationSubject'), pyxb.binding.datatypes.string, scope=AuthenticationCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1242, 5)))

def _BuildAutomaton_113 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_113
    del _BuildAutomaton_113
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    counters.add(cc_4)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AuthenticationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AuthenticationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 763, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AuthenticationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'statefulCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 765, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AuthenticationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'timeCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 767, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AuthenticationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 769, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(AuthenticationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'qosCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 772, 5))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AuthenticationCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'AuthenticationSubject')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1242, 5))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    st_6._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AuthenticationCondition._Automaton = _BuildAutomaton_113()




def _BuildAutomaton_114 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_114
    del _BuildAutomaton_114
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1524, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1525, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1526, 5))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1527, 5))
    counters.add(cc_3)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(DTLSTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'IPsecProtocol')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1524, 5))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(DTLSTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isTunnel')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1525, 5))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(DTLSTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'localEndpoint')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1526, 5))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(DTLSTechnologyParameter._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'remoteEndpoint')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 1527, 5))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
DTLSTechnologyParameter._Automaton = _BuildAutomaton_114()




FilteringCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'stateful'), pyxb.binding.datatypes.boolean, scope=FilteringCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 130, 5)))

FilteringCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerFiltering'), pyxb.binding.datatypes.boolean, scope=FilteringCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 131, 5)))

FilteringCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'httpFiltering'), pyxb.binding.datatypes.boolean, scope=FilteringCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 132, 5)))

FilteringCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'contentInspection'), pyxb.binding.datatypes.boolean, scope=FilteringCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 133, 5)))

def _BuildAutomaton_115 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_115
    del _BuildAutomaton_115
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(FilteringCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(FilteringCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'stateful')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 130, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(FilteringCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'applicationLayerFiltering')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 131, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(FilteringCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'httpFiltering')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 132, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(FilteringCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'contentInspection')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 133, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
FilteringCapability._Automaton = _BuildAutomaton_115()




def _BuildAutomaton_116 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_116
    del _BuildAutomaton_116
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AuthzEnforcementCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AuthzEnforcementCapability._Automaton = _BuildAutomaton_116()




def _BuildAutomaton_117 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_117
    del _BuildAutomaton_117
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(AuthzDecisionCapabiliy._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
AuthzDecisionCapabiliy._Automaton = _BuildAutomaton_117()




MaliciousFileAnalysisCapability._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'fileType'), pyxb.binding.datatypes.string, scope=MaliciousFileAnalysisCapability, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 214, 5)))

def _BuildAutomaton_118 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_118
    del _BuildAutomaton_118
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(MaliciousFileAnalysisCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(MaliciousFileAnalysisCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportOnlineTraficAnalysis')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 185, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(MaliciousFileAnalysisCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'supportOfflineTraficAnalysis')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 186, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(MaliciousFileAnalysisCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'fileType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 214, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
MaliciousFileAnalysisCapability._Automaton = _BuildAutomaton_118()




def _BuildAutomaton_119 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_119
    del _BuildAutomaton_119
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(BotnetDetectorCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(BotnetDetectorCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'resourceType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 236, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
BotnetDetectorCapability._Automaton = _BuildAutomaton_119()




def _BuildAutomaton_120 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_120
    del _BuildAutomaton_120
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(TrafficRecordCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(TrafficRecordCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'resourceType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 236, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
TrafficRecordCapability._Automaton = _BuildAutomaton_120()




def _BuildAutomaton_121 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_121
    del _BuildAutomaton_121
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(NattingCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
NattingCapability._Automaton = _BuildAutomaton_121()




def _BuildAutomaton_122 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_122
    del _BuildAutomaton_122
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(URLRewritingCapability._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'Name')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 110, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
URLRewritingCapability._Automaton = _BuildAutomaton_122()




FileSystemCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'filename'), pyxb.binding.datatypes.string, scope=FileSystemCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 871, 5)))

FileSystemCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'path'), pyxb.binding.datatypes.string, scope=FileSystemCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 872, 5)))

def _BuildAutomaton_123 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_123
    del _BuildAutomaton_123
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 871, 5))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 872, 5))
    counters.add(cc_2)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(FileSystemCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(FileSystemCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(FileSystemCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'filename')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 871, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(FileSystemCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'path')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 872, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
FileSystemCondition._Automaton = _BuildAutomaton_123()




DBDataSelectionCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'tableName'), pyxb.binding.datatypes.string, scope=DBDataSelectionCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 927, 5)))

DBDataSelectionCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'sqlQuery'), pyxb.binding.datatypes.string, scope=DBDataSelectionCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 928, 5)))

DBDataSelectionCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'viewName'), pyxb.binding.datatypes.string, scope=DBDataSelectionCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 929, 5)))

def _BuildAutomaton_124 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_124
    del _BuildAutomaton_124
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DBDataSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DBDataSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DBDataSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'tableName')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 927, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(DBDataSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'sqlQuery')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 928, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(DBDataSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'viewName')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 929, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
DBDataSelectionCondition._Automaton = _BuildAutomaton_124()




XMLDataSelectionCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'xmlDataType'), pyxb.binding.datatypes.string, scope=XMLDataSelectionCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 938, 5)))

XMLDataSelectionCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'xmlNameSpace'), pyxb.binding.datatypes.string, scope=XMLDataSelectionCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 939, 5)))

XMLDataSelectionCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'xmlQueryLanguage'), pyxb.binding.datatypes.string, scope=XMLDataSelectionCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 940, 5)))

XMLDataSelectionCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'xmlQueryLanguageVersion'), pyxb.binding.datatypes.string, scope=XMLDataSelectionCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 941, 5)))

XMLDataSelectionCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'xmlQueryExpression'), pyxb.binding.datatypes.string, scope=XMLDataSelectionCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 943, 5)))

def _BuildAutomaton_125 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_125
    del _BuildAutomaton_125
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(XMLDataSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(XMLDataSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(XMLDataSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'xmlDataType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 938, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(XMLDataSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'xmlNameSpace')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 939, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(XMLDataSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'xmlQueryLanguage')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 940, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(XMLDataSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'xmlQueryLanguageVersion')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 941, 5))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(XMLDataSelectionCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'xmlQueryExpression')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 943, 5))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
         ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
         ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    st_6._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
XMLDataSelectionCondition._Automaton = _BuildAutomaton_125()




WSSecurityCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'content_vs_element'), pyxb.binding.datatypes.boolean, scope=WSSecurityCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 952, 5)))

WSSecurityCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'body'), pyxb.binding.datatypes.string, scope=WSSecurityCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 953, 5)))

WSSecurityCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'attachment'), pyxb.binding.datatypes.string, scope=WSSecurityCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 954, 5)))

WSSecurityCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'headerLocalName'), pyxb.binding.datatypes.string, scope=WSSecurityCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 955, 5)))

WSSecurityCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'headerNameSpace'), pyxb.binding.datatypes.string, scope=WSSecurityCondition, location=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 956, 5)))

def _BuildAutomaton_126 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_126
    del _BuildAutomaton_126
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(WSSecurityCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'isCNF')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 369, 3))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(WSSecurityCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'packetFilterCondition')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 433, 5))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(WSSecurityCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'xmlDataType')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 938, 5))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(WSSecurityCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'xmlNameSpace')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 939, 5))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(WSSecurityCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'xmlQueryLanguage')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 940, 5))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(WSSecurityCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'xmlQueryLanguageVersion')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 941, 5))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(WSSecurityCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'xmlQueryExpression')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 943, 5))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(WSSecurityCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'content_vs_element')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 952, 5))
    st_7 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(WSSecurityCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'body')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 953, 5))
    st_8 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(WSSecurityCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'attachment')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 954, 5))
    st_9 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(WSSecurityCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'headerLocalName')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 955, 5))
    st_10 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_10)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(WSSecurityCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'headerNameSpace')), pyxb.utils.utility.Location('/app/mspl/mspl.xsd', 956, 5))
    st_11 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_11)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
         ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
         ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
         ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
         ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
         ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
         ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_10, [
         ]))
    st_9._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_11, [
         ]))
    st_10._set_transitionSet(transitions)
    transitions = []
    st_11._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
WSSecurityCondition._Automaton = _BuildAutomaton_126()

