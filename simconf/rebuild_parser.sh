#!/bin/bash

#remove all files
rm -rf simconf.py __* raw
#generate the parser
pyxbgen -u simconf.xsd -m simconf --write-for-customization

echo "# -*- coding: utf-8 -*-
from .raw.simconf import *" > simconf.py

# Output: Python for AbsentNamespace0 requires 1 modules is normal