# swagger_client.RTResourceApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**r_t_resource_create**](RTResourceApi.md#r_t_resource_create) | **POST** /RT_resource/ | 
[**r_t_resource_delete**](RTResourceApi.md#r_t_resource_delete) | **DELETE** /RT_resource/{id}/ | 
[**r_t_resource_list**](RTResourceApi.md#r_t_resource_list) | **GET** /RT_resource/ | 
[**r_t_resource_partial_update**](RTResourceApi.md#r_t_resource_partial_update) | **PATCH** /RT_resource/{id}/ | 
[**r_t_resource_read**](RTResourceApi.md#r_t_resource_read) | **GET** /RT_resource/{id}/ | 
[**r_t_resource_update**](RTResourceApi.md#r_t_resource_update) | **PUT** /RT_resource/{id}/ | 


# **r_t_resource_create**
> RTResource r_t_resource_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.RTResourceApi(swagger_client.ApiClient(configuration))
data = swagger_client.RTResource() # RTResource | 

try:
    api_response = api_instance.r_t_resource_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RTResourceApi->r_t_resource_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RTResource**](RTResource.md)|  | 

### Return type

[**RTResource**](RTResource.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **r_t_resource_delete**
> r_t_resource_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.RTResourceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this r t_resource.

try:
    api_instance.r_t_resource_delete(id)
except ApiException as e:
    print("Exception when calling RTResourceApi->r_t_resource_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this r t_resource. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **r_t_resource_list**
> InlineResponse2001 r_t_resource_list(page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.RTResourceApi(swagger_client.ApiClient(configuration))
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.r_t_resource_list(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RTResourceApi->r_t_resource_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **r_t_resource_partial_update**
> RTResource r_t_resource_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.RTResourceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this r t_resource.
data = swagger_client.RTResource() # RTResource | 

try:
    api_response = api_instance.r_t_resource_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RTResourceApi->r_t_resource_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this r t_resource. | 
 **data** | [**RTResource**](RTResource.md)|  | 

### Return type

[**RTResource**](RTResource.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **r_t_resource_read**
> RTResource r_t_resource_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.RTResourceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this r t_resource.

try:
    api_response = api_instance.r_t_resource_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RTResourceApi->r_t_resource_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this r t_resource. | 

### Return type

[**RTResource**](RTResource.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **r_t_resource_update**
> RTResource r_t_resource_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.RTResourceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this r t_resource.
data = swagger_client.RTResource() # RTResource | 

try:
    api_response = api_instance.r_t_resource_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RTResourceApi->r_t_resource_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this r t_resource. | 
 **data** | [**RTResource**](RTResource.md)|  | 

### Return type

[**RTResource**](RTResource.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

