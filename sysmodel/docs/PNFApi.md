# swagger_client.PNFApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**p_nf_create**](PNFApi.md#p_nf_create) | **POST** /PNF/ | 
[**p_nf_delete**](PNFApi.md#p_nf_delete) | **DELETE** /PNF/{id}/ | 
[**p_nf_list**](PNFApi.md#p_nf_list) | **GET** /PNF/ | 
[**p_nf_partial_update**](PNFApi.md#p_nf_partial_update) | **PATCH** /PNF/{id}/ | 
[**p_nf_read**](PNFApi.md#p_nf_read) | **GET** /PNF/{id}/ | 
[**p_nf_update**](PNFApi.md#p_nf_update) | **PUT** /PNF/{id}/ | 


# **p_nf_create**
> PNF p_nf_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PNFApi(swagger_client.ApiClient(configuration))
data = swagger_client.PNF() # PNF | 

try:
    api_response = api_instance.p_nf_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PNFApi->p_nf_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PNF**](PNF.md)|  | 

### Return type

[**PNF**](PNF.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **p_nf_delete**
> p_nf_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PNFApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this pnf.

try:
    api_instance.p_nf_delete(id)
except ApiException as e:
    print("Exception when calling PNFApi->p_nf_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this pnf. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **p_nf_list**
> InlineResponse200 p_nf_list(name=name, page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PNFApi(swagger_client.ApiClient(configuration))
name = 'name_example' # str |  (optional)
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.p_nf_list(name=name, page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PNFApi->p_nf_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**|  | [optional] 
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **p_nf_partial_update**
> PNF p_nf_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PNFApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this pnf.
data = swagger_client.PNF() # PNF | 

try:
    api_response = api_instance.p_nf_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PNFApi->p_nf_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this pnf. | 
 **data** | [**PNF**](PNF.md)|  | 

### Return type

[**PNF**](PNF.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **p_nf_read**
> PNF p_nf_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PNFApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this pnf.

try:
    api_response = api_instance.p_nf_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PNFApi->p_nf_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this pnf. | 

### Return type

[**PNF**](PNF.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **p_nf_update**
> PNF p_nf_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PNFApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this pnf.
data = swagger_client.PNF() # PNF | 

try:
    api_response = api_instance.p_nf_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PNFApi->p_nf_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this pnf. | 
 **data** | [**PNF**](PNF.md)|  | 

### Return type

[**PNF**](PNF.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

