# swagger_client.VnfApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**vnf_create**](VnfApi.md#vnf_create) | **POST** /vnf/ | 
[**vnf_delete**](VnfApi.md#vnf_delete) | **DELETE** /vnf/{id}/ | 
[**vnf_list**](VnfApi.md#vnf_list) | **GET** /vnf/ | 
[**vnf_partial_update**](VnfApi.md#vnf_partial_update) | **PATCH** /vnf/{id}/ | 
[**vnf_read**](VnfApi.md#vnf_read) | **GET** /vnf/{id}/ | 
[**vnf_update**](VnfApi.md#vnf_update) | **PUT** /vnf/{id}/ | 


# **vnf_create**
> VNF vnf_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VnfApi(swagger_client.ApiClient(configuration))
data = swagger_client.VNF() # VNF | 

try:
    api_response = api_instance.vnf_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VnfApi->vnf_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**VNF**](VNF.md)|  | 

### Return type

[**VNF**](VNF.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vnf_delete**
> vnf_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VnfApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this vnf.

try:
    api_instance.vnf_delete(id)
except ApiException as e:
    print("Exception when calling VnfApi->vnf_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this vnf. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vnf_list**
> InlineResponse20016 vnf_list(page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VnfApi(swagger_client.ApiClient(configuration))
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.vnf_list(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VnfApi->vnf_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse20016**](InlineResponse20016.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vnf_partial_update**
> VNF vnf_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VnfApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this vnf.
data = swagger_client.VNF() # VNF | 

try:
    api_response = api_instance.vnf_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VnfApi->vnf_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this vnf. | 
 **data** | [**VNF**](VNF.md)|  | 

### Return type

[**VNF**](VNF.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vnf_read**
> VNFRetrieve vnf_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VnfApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this vnf.

try:
    api_response = api_instance.vnf_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VnfApi->vnf_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this vnf. | 

### Return type

[**VNFRetrieve**](VNFRetrieve.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **vnf_update**
> VNF vnf_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.VnfApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this vnf.
data = swagger_client.VNF() # VNF | 

try:
    api_response = api_instance.vnf_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling VnfApi->vnf_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this vnf. | 
 **data** | [**VNF**](VNF.md)|  | 

### Return type

[**VNF**](VNF.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

