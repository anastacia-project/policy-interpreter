# swagger_client.PskcredentialApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**pskcredential_create**](PskcredentialApi.md#pskcredential_create) | **POST** /pskcredential/ | 
[**pskcredential_delete**](PskcredentialApi.md#pskcredential_delete) | **DELETE** /pskcredential/{id}/ | 
[**pskcredential_list**](PskcredentialApi.md#pskcredential_list) | **GET** /pskcredential/ | 
[**pskcredential_partial_update**](PskcredentialApi.md#pskcredential_partial_update) | **PATCH** /pskcredential/{id}/ | 
[**pskcredential_read**](PskcredentialApi.md#pskcredential_read) | **GET** /pskcredential/{id}/ | 
[**pskcredential_update**](PskcredentialApi.md#pskcredential_update) | **PUT** /pskcredential/{id}/ | 


# **pskcredential_create**
> PSKCredential pskcredential_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PskcredentialApi(swagger_client.ApiClient(configuration))
data = swagger_client.PSKCredential() # PSKCredential | 

try:
    api_response = api_instance.pskcredential_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PskcredentialApi->pskcredential_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PSKCredential**](PSKCredential.md)|  | 

### Return type

[**PSKCredential**](PSKCredential.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pskcredential_delete**
> pskcredential_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PskcredentialApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this psk credential.

try:
    api_instance.pskcredential_delete(id)
except ApiException as e:
    print("Exception when calling PskcredentialApi->pskcredential_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this psk credential. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pskcredential_list**
> InlineResponse20011 pskcredential_list(page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PskcredentialApi(swagger_client.ApiClient(configuration))
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.pskcredential_list(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PskcredentialApi->pskcredential_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pskcredential_partial_update**
> PSKCredential pskcredential_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PskcredentialApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this psk credential.
data = swagger_client.PSKCredential() # PSKCredential | 

try:
    api_response = api_instance.pskcredential_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PskcredentialApi->pskcredential_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this psk credential. | 
 **data** | [**PSKCredential**](PSKCredential.md)|  | 

### Return type

[**PSKCredential**](PSKCredential.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pskcredential_read**
> PSKCredential pskcredential_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PskcredentialApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this psk credential.

try:
    api_response = api_instance.pskcredential_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PskcredentialApi->pskcredential_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this psk credential. | 

### Return type

[**PSKCredential**](PSKCredential.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pskcredential_update**
> PSKCredential pskcredential_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PskcredentialApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this psk credential.
data = swagger_client.PSKCredential() # PSKCredential | 

try:
    api_response = api_instance.pskcredential_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PskcredentialApi->pskcredential_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this psk credential. | 
 **data** | [**PSKCredential**](PSKCredential.md)|  | 

### Return type

[**PSKCredential**](PSKCredential.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

