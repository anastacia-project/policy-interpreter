# swagger_client.SdnruleApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sdnrule_create**](SdnruleApi.md#sdnrule_create) | **POST** /sdnrule/ | 
[**sdnrule_delete**](SdnruleApi.md#sdnrule_delete) | **DELETE** /sdnrule/{id}/ | 
[**sdnrule_list**](SdnruleApi.md#sdnrule_list) | **GET** /sdnrule/ | 
[**sdnrule_partial_update**](SdnruleApi.md#sdnrule_partial_update) | **PATCH** /sdnrule/{id}/ | 
[**sdnrule_read**](SdnruleApi.md#sdnrule_read) | **GET** /sdnrule/{id}/ | 
[**sdnrule_update**](SdnruleApi.md#sdnrule_update) | **PUT** /sdnrule/{id}/ | 


# **sdnrule_create**
> SDNRule sdnrule_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SdnruleApi(swagger_client.ApiClient(configuration))
data = swagger_client.SDNRule() # SDNRule | 

try:
    api_response = api_instance.sdnrule_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SdnruleApi->sdnrule_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**SDNRule**](SDNRule.md)|  | 

### Return type

[**SDNRule**](SDNRule.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sdnrule_delete**
> sdnrule_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SdnruleApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this sdn rule.

try:
    api_instance.sdnrule_delete(id)
except ApiException as e:
    print("Exception when calling SdnruleApi->sdnrule_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this sdn rule. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sdnrule_list**
> InlineResponse20013 sdnrule_list(page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SdnruleApi(swagger_client.ApiClient(configuration))
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.sdnrule_list(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SdnruleApi->sdnrule_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse20013**](InlineResponse20013.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sdnrule_partial_update**
> SDNRule sdnrule_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SdnruleApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this sdn rule.
data = swagger_client.SDNRule() # SDNRule | 

try:
    api_response = api_instance.sdnrule_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SdnruleApi->sdnrule_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this sdn rule. | 
 **data** | [**SDNRule**](SDNRule.md)|  | 

### Return type

[**SDNRule**](SDNRule.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sdnrule_read**
> SDNRule sdnrule_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SdnruleApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this sdn rule.

try:
    api_response = api_instance.sdnrule_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SdnruleApi->sdnrule_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this sdn rule. | 

### Return type

[**SDNRule**](SDNRule.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sdnrule_update**
> SDNRule sdnrule_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SdnruleApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this sdn rule.
data = swagger_client.SDNRule() # SDNRule | 

try:
    api_response = api_instance.sdnrule_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SdnruleApi->sdnrule_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this sdn rule. | 
 **data** | [**SDNRule**](SDNRule.md)|  | 

### Return type

[**SDNRule**](SDNRule.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

