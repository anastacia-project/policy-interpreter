# swagger_client.InterfaceApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**interface_create**](InterfaceApi.md#interface_create) | **POST** /interface/ | 
[**interface_delete**](InterfaceApi.md#interface_delete) | **DELETE** /interface/{id}/ | 
[**interface_list**](InterfaceApi.md#interface_list) | **GET** /interface/ | 
[**interface_partial_update**](InterfaceApi.md#interface_partial_update) | **PATCH** /interface/{id}/ | 
[**interface_read**](InterfaceApi.md#interface_read) | **GET** /interface/{id}/ | 
[**interface_update**](InterfaceApi.md#interface_update) | **PUT** /interface/{id}/ | 


# **interface_create**
> Interface interface_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.InterfaceApi(swagger_client.ApiClient(configuration))
data = swagger_client.Interface() # Interface | 

try:
    api_response = api_instance.interface_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling InterfaceApi->interface_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Interface**](Interface.md)|  | 

### Return type

[**Interface**](Interface.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **interface_delete**
> interface_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.InterfaceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this interface.

try:
    api_instance.interface_delete(id)
except ApiException as e:
    print("Exception when calling InterfaceApi->interface_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this interface. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **interface_list**
> InlineResponse2007 interface_list(page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.InterfaceApi(swagger_client.ApiClient(configuration))
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.interface_list(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling InterfaceApi->interface_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **interface_partial_update**
> Interface interface_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.InterfaceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this interface.
data = swagger_client.Interface() # Interface | 

try:
    api_response = api_instance.interface_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling InterfaceApi->interface_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this interface. | 
 **data** | [**Interface**](Interface.md)|  | 

### Return type

[**Interface**](Interface.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **interface_read**
> Interface interface_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.InterfaceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this interface.

try:
    api_response = api_instance.interface_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling InterfaceApi->interface_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this interface. | 

### Return type

[**Interface**](Interface.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **interface_update**
> Interface interface_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.InterfaceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this interface.
data = swagger_client.Interface() # Interface | 

try:
    api_response = api_instance.interface_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling InterfaceApi->interface_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this interface. | 
 **data** | [**Interface**](Interface.md)|  | 

### Return type

[**Interface**](Interface.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

