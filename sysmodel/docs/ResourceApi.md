# swagger_client.ResourceApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**resource_create**](ResourceApi.md#resource_create) | **POST** /resource/ | 
[**resource_delete**](ResourceApi.md#resource_delete) | **DELETE** /resource/{id}/ | 
[**resource_list**](ResourceApi.md#resource_list) | **GET** /resource/ | 
[**resource_partial_update**](ResourceApi.md#resource_partial_update) | **PATCH** /resource/{id}/ | 
[**resource_read**](ResourceApi.md#resource_read) | **GET** /resource/{id}/ | 
[**resource_update**](ResourceApi.md#resource_update) | **PUT** /resource/{id}/ | 


# **resource_create**
> Resource resource_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.ResourceApi(swagger_client.ApiClient(configuration))
data = swagger_client.Resource() # Resource | 

try:
    api_response = api_instance.resource_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceApi->resource_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Resource**](Resource.md)|  | 

### Return type

[**Resource**](Resource.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **resource_delete**
> resource_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.ResourceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this resource.

try:
    api_instance.resource_delete(id)
except ApiException as e:
    print("Exception when calling ResourceApi->resource_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this resource. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **resource_list**
> InlineResponse20012 resource_list(page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.ResourceApi(swagger_client.ApiClient(configuration))
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.resource_list(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceApi->resource_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse20012**](InlineResponse20012.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **resource_partial_update**
> Resource resource_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.ResourceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this resource.
data = swagger_client.Resource() # Resource | 

try:
    api_response = api_instance.resource_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceApi->resource_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this resource. | 
 **data** | [**Resource**](Resource.md)|  | 

### Return type

[**Resource**](Resource.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **resource_read**
> Resource resource_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.ResourceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this resource.

try:
    api_response = api_instance.resource_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceApi->resource_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this resource. | 

### Return type

[**Resource**](Resource.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **resource_update**
> Resource resource_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.ResourceApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this resource.
data = swagger_client.Resource() # Resource | 

try:
    api_response = api_instance.resource_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ResourceApi->resource_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this resource. | 
 **data** | [**Resource**](Resource.md)|  | 

### Return type

[**Resource**](Resource.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

