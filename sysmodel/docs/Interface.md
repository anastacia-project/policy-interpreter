# Interface

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**interfaceinfos** | [**InterfaceInfo**](InterfaceInfo.md) |  | [optional] 
**virtual** | **bool** |  | [optional] 
**type** | **str** |  | 
**name** | **str** |  | 
**device** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


