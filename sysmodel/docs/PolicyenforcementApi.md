# swagger_client.PolicyenforcementApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**policyenforcement_create**](PolicyenforcementApi.md#policyenforcement_create) | **POST** /policyenforcement/ | 
[**policyenforcement_delete**](PolicyenforcementApi.md#policyenforcement_delete) | **DELETE** /policyenforcement/{id}/ | 
[**policyenforcement_list**](PolicyenforcementApi.md#policyenforcement_list) | **GET** /policyenforcement/ | 
[**policyenforcement_partial_update**](PolicyenforcementApi.md#policyenforcement_partial_update) | **PATCH** /policyenforcement/{id}/ | 
[**policyenforcement_read**](PolicyenforcementApi.md#policyenforcement_read) | **GET** /policyenforcement/{id}/ | 
[**policyenforcement_update**](PolicyenforcementApi.md#policyenforcement_update) | **PUT** /policyenforcement/{id}/ | 


# **policyenforcement_create**
> PolicyEnforcement policyenforcement_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PolicyenforcementApi(swagger_client.ApiClient(configuration))
data = swagger_client.PolicyEnforcement() # PolicyEnforcement | 

try:
    api_response = api_instance.policyenforcement_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PolicyenforcementApi->policyenforcement_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PolicyEnforcement**](PolicyEnforcement.md)|  | 

### Return type

[**PolicyEnforcement**](PolicyEnforcement.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **policyenforcement_delete**
> policyenforcement_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PolicyenforcementApi(swagger_client.ApiClient(configuration))
id = 'id_example' # str | A unique value identifying this policy enforcement.

try:
    api_instance.policyenforcement_delete(id)
except ApiException as e:
    print("Exception when calling PolicyenforcementApi->policyenforcement_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| A unique value identifying this policy enforcement. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **policyenforcement_list**
> InlineResponse20010 policyenforcement_list(page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PolicyenforcementApi(swagger_client.ApiClient(configuration))
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.policyenforcement_list(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PolicyenforcementApi->policyenforcement_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **policyenforcement_partial_update**
> PolicyEnforcement policyenforcement_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PolicyenforcementApi(swagger_client.ApiClient(configuration))
id = 'id_example' # str | A unique value identifying this policy enforcement.
data = swagger_client.PolicyEnforcement() # PolicyEnforcement | 

try:
    api_response = api_instance.policyenforcement_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PolicyenforcementApi->policyenforcement_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| A unique value identifying this policy enforcement. | 
 **data** | [**PolicyEnforcement**](PolicyEnforcement.md)|  | 

### Return type

[**PolicyEnforcement**](PolicyEnforcement.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **policyenforcement_read**
> PolicyEnforcement policyenforcement_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PolicyenforcementApi(swagger_client.ApiClient(configuration))
id = 'id_example' # str | A unique value identifying this policy enforcement.

try:
    api_response = api_instance.policyenforcement_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PolicyenforcementApi->policyenforcement_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| A unique value identifying this policy enforcement. | 

### Return type

[**PolicyEnforcement**](PolicyEnforcement.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **policyenforcement_update**
> PolicyEnforcement policyenforcement_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.PolicyenforcementApi(swagger_client.ApiClient(configuration))
id = 'id_example' # str | A unique value identifying this policy enforcement.
data = swagger_client.PolicyEnforcement() # PolicyEnforcement | 

try:
    api_response = api_instance.policyenforcement_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PolicyenforcementApi->policyenforcement_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**| A unique value identifying this policy enforcement. | 
 **data** | [**PolicyEnforcement**](PolicyEnforcement.md)|  | 

### Return type

[**PolicyEnforcement**](PolicyEnforcement.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

