# swagger_client.SoftwareApi

All URIs are relative to *http://195.148.125.100:9000/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**software_create**](SoftwareApi.md#software_create) | **POST** /software/ | 
[**software_delete**](SoftwareApi.md#software_delete) | **DELETE** /software/{id}/ | 
[**software_list**](SoftwareApi.md#software_list) | **GET** /software/ | 
[**software_partial_update**](SoftwareApi.md#software_partial_update) | **PATCH** /software/{id}/ | 
[**software_read**](SoftwareApi.md#software_read) | **GET** /software/{id}/ | 
[**software_update**](SoftwareApi.md#software_update) | **PUT** /software/{id}/ | 


# **software_create**
> Software software_create(data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SoftwareApi(swagger_client.ApiClient(configuration))
data = swagger_client.Software() # Software | 

try:
    api_response = api_instance.software_create(data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SoftwareApi->software_create: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Software**](Software.md)|  | 

### Return type

[**Software**](Software.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **software_delete**
> software_delete(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SoftwareApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this software.

try:
    api_instance.software_delete(id)
except ApiException as e:
    print("Exception when calling SoftwareApi->software_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this software. | 

### Return type

void (empty response body)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **software_list**
> InlineResponse20015 software_list(page=page)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SoftwareApi(swagger_client.ApiClient(configuration))
page = 56 # int | A page number within the paginated result set. (optional)

try:
    api_response = api_instance.software_list(page=page)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SoftwareApi->software_list: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| A page number within the paginated result set. | [optional] 

### Return type

[**InlineResponse20015**](InlineResponse20015.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **software_partial_update**
> Software software_partial_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SoftwareApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this software.
data = swagger_client.Software() # Software | 

try:
    api_response = api_instance.software_partial_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SoftwareApi->software_partial_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this software. | 
 **data** | [**Software**](Software.md)|  | 

### Return type

[**Software**](Software.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **software_read**
> Software software_read(id)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SoftwareApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this software.

try:
    api_response = api_instance.software_read(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SoftwareApi->software_read: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this software. | 

### Return type

[**Software**](Software.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **software_update**
> Software software_update(id, data)





### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure HTTP basic authorization: Basic
configuration = swagger_client.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = swagger_client.SoftwareApi(swagger_client.ApiClient(configuration))
id = 56 # int | A unique integer value identifying this software.
data = swagger_client.Software() # Software | 

try:
    api_response = api_instance.software_update(id, data)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SoftwareApi->software_update: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| A unique integer value identifying this software. | 
 **data** | [**Software**](Software.md)|  | 

### Return type

[**Software**](Software.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

