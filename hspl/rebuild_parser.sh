#!/bin/bash

#remove all files
rm -rf *.py __* raw
#generate the parser
pyxbgen -u hspl.xsd -m hspl --write-for-customization

rm -rf hspl.py
echo "# -*- coding: utf-8 -*-
from .raw.hspl import *" > __init__.py