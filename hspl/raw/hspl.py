# ./raw/hspl.py
# -*- coding: utf-8 -*-
# PyXB bindings for NM:3be43b5a6e5891dd391c0e36955c69fa565a5cff
# Generated 2019-11-22 10:35:52.179287 by PyXB version 1.2.6 using Python 3.7.1.final.0
# Namespace http://www.example.org/Refinement_Schema

from __future__ import unicode_literals
import pyxb
import pyxb.binding
import pyxb.binding.saxer
import io
import pyxb.utils.utility
import pyxb.utils.domutils
import sys
import pyxb.utils.six as _six
# Unique identifier for bindings created at the same time
_GenerationUID = pyxb.utils.utility.UniqueIdentifier('urn:uuid:7957ecb6-0d0b-11ea-9f0b-0242ac150006')

# Version of PyXB used to generate the bindings
_PyXBVersion = '1.2.6'
# Generated bindings are not compatible across PyXB versions
if pyxb.__version__ != _PyXBVersion:
    raise pyxb.PyXBVersionError(_PyXBVersion)

# A holder for module-level binding classes so we can access them from
# inside class definitions where property names may conflict.
_module_typeBindings = pyxb.utils.utility.Object()

# Import bindings for namespaces imported into schema
import pyxb.binding.datatypes

# NOTE: All namespace declarations are reserved within the binding
Namespace = pyxb.namespace.NamespaceForURI('http://www.example.org/Refinement_Schema', create_if_missing=True)
Namespace.configureCategories(['typeBinding', 'elementBinding'])

def CreateFromDocument (xml_text, default_namespace=None, location_base=None):
    """Parse the given XML and use the document element to create a
    Python instance.

    @param xml_text An XML document.  This should be data (Python 2
    str or Python 3 bytes), or a text (Python 2 unicode or Python 3
    str) in the L{pyxb._InputEncoding} encoding.

    @keyword default_namespace The L{pyxb.Namespace} instance to use as the
    default namespace where there is no default namespace in scope.
    If unspecified or C{None}, the namespace of the module containing
    this function will be used.

    @keyword location_base: An object to be recorded as the base of all
    L{pyxb.utils.utility.Location} instances associated with events and
    objects handled by the parser.  You might pass the URI from which
    the document was obtained.
    """

    if pyxb.XMLStyle_saxer != pyxb._XMLStyle:
        dom = pyxb.utils.domutils.StringToDOM(xml_text)
        return CreateFromDOM(dom.documentElement, default_namespace=default_namespace)
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    saxer = pyxb.binding.saxer.make_parser(fallback_namespace=default_namespace, location_base=location_base)
    handler = saxer.getContentHandler()
    xmld = xml_text
    if isinstance(xmld, _six.text_type):
        xmld = xmld.encode(pyxb._InputEncoding)
    saxer.parse(io.BytesIO(xmld))
    instance = handler.rootObject()
    return instance

def CreateFromDOM (node, default_namespace=None):
    """Create a Python instance from the given DOM node.
    The node tag must correspond to an element declaration in this module.

    @deprecated: Forcing use of DOM interface is unnecessary; use L{CreateFromDocument}."""
    if default_namespace is None:
        default_namespace = Namespace.fallbackNamespace()
    return pyxb.binding.basis.element.AnyCreateFromDOM(node, default_namespace)


# Atomic simple type: {http://www.example.org/Refinement_Schema}action
class action (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'action')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 8, 4)
    _Documentation = None
action._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=action, enum_prefix=None)
action.authorise_access = action._CF_enumeration.addEnumeration(unicode_value='authorise_access', tag='authorise_access')
action.no_authorise_access = action._CF_enumeration.addEnumeration(unicode_value='no_authorise_access', tag='no_authorise_access')
action.enable = action._CF_enumeration.addEnumeration(unicode_value='enable', tag='enable')
action.remove = action._CF_enumeration.addEnumeration(unicode_value='remove', tag='remove')
action.reduce = action._CF_enumeration.addEnumeration(unicode_value='reduce', tag='reduce')
action.check_over = action._CF_enumeration.addEnumeration(unicode_value='check_over', tag='check_over')
action.count = action._CF_enumeration.addEnumeration(unicode_value='count', tag='count')
action.prot_conf = action._CF_enumeration.addEnumeration(unicode_value='prot_conf', tag='prot_conf')
action.prot_integr = action._CF_enumeration.addEnumeration(unicode_value='prot_integr', tag='prot_integr')
action.prot_conf_integr = action._CF_enumeration.addEnumeration(unicode_value='prot_conf_integr', tag='prot_conf_integr')
action.compress = action._CF_enumeration.addEnumeration(unicode_value='compress', tag='compress')
action.config_authentication = action._CF_enumeration.addEnumeration(unicode_value='config_authentication', tag='config_authentication')
action.config_privacy = action._CF_enumeration.addEnumeration(unicode_value='config_privacy', tag='config_privacy')
action.config_monitoring = action._CF_enumeration.addEnumeration(unicode_value='config_monitoring', tag='config_monitoring')
action.config_qos = action._CF_enumeration.addEnumeration(unicode_value='config_qos', tag='config_qos')
action.data_aggregation = action._CF_enumeration.addEnumeration(unicode_value='data_aggregation', tag='data_aggregation')
action.config_network_anonymity = action._CF_enumeration.addEnumeration(unicode_value='config_network_anonymity', tag='config_network_anonymity')
action._InitializeFacetMap(action._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'action', action)
_module_typeBindings.action = action

# Atomic simple type: {http://www.example.org/Refinement_Schema}objectH
class objectH (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'objectH')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 36, 4)
    _Documentation = None
objectH._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=objectH, enum_prefix=None)
objectH.VoIP_traffic = objectH._CF_enumeration.addEnumeration(unicode_value='VoIP_traffic', tag='VoIP_traffic')
objectH.P2P_traffic = objectH._CF_enumeration.addEnumeration(unicode_value='P2P_traffic', tag='P2P_traffic')
objectH.T3G4G_traffic = objectH._CF_enumeration.addEnumeration(unicode_value='T3G4G_traffic', tag='T3G4G_traffic')
objectH.Internet_traffic = objectH._CF_enumeration.addEnumeration(unicode_value='Internet_traffic', tag='Internet_traffic')
objectH.Intranet_traffic = objectH._CF_enumeration.addEnumeration(unicode_value='Intranet_traffic', tag='Intranet_traffic')
objectH.DNS_traffic = objectH._CF_enumeration.addEnumeration(unicode_value='DNS_traffic', tag='DNS_traffic')
objectH.PANA_traffic = objectH._CF_enumeration.addEnumeration(unicode_value='PANA_traffic', tag='PANA_traffic')
objectH.PANA = objectH._CF_enumeration.addEnumeration(unicode_value='PANA', tag='PANA')
objectH.PANA_agent = objectH._CF_enumeration.addEnumeration(unicode_value='PANA_agent', tag='PANA_agent')
objectH.CoAP_traffic = objectH._CF_enumeration.addEnumeration(unicode_value='CoAP_traffic', tag='CoAP_traffic')
objectH.DTLS_traffic = objectH._CF_enumeration.addEnumeration(unicode_value='DTLS_traffic', tag='DTLS_traffic')
objectH.CoAP_EAP_traffic = objectH._CF_enumeration.addEnumeration(unicode_value='CoAP-EAP_traffic', tag='CoAP_EAP_traffic')
objectH.ICMP_traffic = objectH._CF_enumeration.addEnumeration(unicode_value='ICMP_traffic', tag='ICMP_traffic')
objectH.resource = objectH._CF_enumeration.addEnumeration(unicode_value='resource', tag='resource')
objectH.file_scanning = objectH._CF_enumeration.addEnumeration(unicode_value='file_scanning', tag='file_scanning')
objectH.email_scanning = objectH._CF_enumeration.addEnumeration(unicode_value='email_scanning', tag='email_scanning')
objectH.antivirus = objectH._CF_enumeration.addEnumeration(unicode_value='antivirus', tag='antivirus')
objectH.basic_prarental_control = objectH._CF_enumeration.addEnumeration(unicode_value='basic_prarental_control', tag='basic_prarental_control')
objectH.advance_parental_control = objectH._CF_enumeration.addEnumeration(unicode_value='advance_parental_control', tag='advance_parental_control')
objectH.lawful_interception = objectH._CF_enumeration.addEnumeration(unicode_value='lawful_interception', tag='lawful_interception')
objectH.IDS_IPS = objectH._CF_enumeration.addEnumeration(unicode_value='IDS_IPS', tag='IDS_IPS')
objectH.DDos_attack_protection = objectH._CF_enumeration.addEnumeration(unicode_value='DDos_attack_protection', tag='DDos_attack_protection')
objectH.tacking_techniques = objectH._CF_enumeration.addEnumeration(unicode_value='tacking_techniques', tag='tacking_techniques')
objectH.advertisement = objectH._CF_enumeration.addEnumeration(unicode_value='advertisement', tag='advertisement')
objectH.bandwidth = objectH._CF_enumeration.addEnumeration(unicode_value='bandwidth', tag='bandwidth')
objectH.security_status = objectH._CF_enumeration.addEnumeration(unicode_value='security_status', tag='security_status')
objectH.connection = objectH._CF_enumeration.addEnumeration(unicode_value='connection', tag='connection')
objectH.AllTraffic = objectH._CF_enumeration.addEnumeration(unicode_value='AllTraffic', tag='AllTraffic')
objectH.logging = objectH._CF_enumeration.addEnumeration(unicode_value='logging', tag='logging')
objectH.malware_detection = objectH._CF_enumeration.addEnumeration(unicode_value='malware_detection', tag='malware_detection')
objectH.antiPhishing = objectH._CF_enumeration.addEnumeration(unicode_value='antiPhishing', tag='antiPhishing')
objectH.anonimity = objectH._CF_enumeration.addEnumeration(unicode_value='anonimity', tag='anonimity')
objectH._InitializeFacetMap(objectH._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'objectH', objectH)
_module_typeBindings.objectH = objectH

# Atomic simple type: {http://www.example.org/Refinement_Schema}capability
class capability (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'capability')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 377, 4)
    _Documentation = None
capability._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=capability, enum_prefix=None)
capability.Filtering_L4 = capability._CF_enumeration.addEnumeration(unicode_value='Filtering_L4', tag='Filtering_L4')
capability.Filtering_L7 = capability._CF_enumeration.addEnumeration(unicode_value='Filtering_L7', tag='Filtering_L7')
capability.Timing = capability._CF_enumeration.addEnumeration(unicode_value='Timing', tag='Timing')
capability.TrafficInspection_L7 = capability._CF_enumeration.addEnumeration(unicode_value='TrafficInspection_L7', tag='TrafficInspection_L7')
capability.Filtering_3G4G = capability._CF_enumeration.addEnumeration(unicode_value='Filtering_3G4G', tag='Filtering_3G4G')
capability.Filtering_DNS = capability._CF_enumeration.addEnumeration(unicode_value='Filtering_DNS', tag='Filtering_DNS')
capability.Offline_malware_analysis = capability._CF_enumeration.addEnumeration(unicode_value='Offline_malware_analysis', tag='Offline_malware_analysis')
capability.Online_SPAM_analysis = capability._CF_enumeration.addEnumeration(unicode_value='Online_SPAM_analysis', tag='Online_SPAM_analysis')
capability.Online_antivirus_analysis = capability._CF_enumeration.addEnumeration(unicode_value='Online_antivirus_analysis', tag='Online_antivirus_analysis')
capability.Network_traffic_analysis = capability._CF_enumeration.addEnumeration(unicode_value='Network_traffic_analysis', tag='Network_traffic_analysis')
capability.DDos_attack_protection = capability._CF_enumeration.addEnumeration(unicode_value='DDos_attack_protection', tag='DDos_attack_protection')
capability.lawful_interception = capability._CF_enumeration.addEnumeration(unicode_value='lawful_interception', tag='lawful_interception')
capability.Count_L4Connection = capability._CF_enumeration.addEnumeration(unicode_value='Count_L4Connection', tag='Count_L4Connection')
capability.Count_DNS = capability._CF_enumeration.addEnumeration(unicode_value='Count_DNS', tag='Count_DNS')
capability.Protection_confidentiality = capability._CF_enumeration.addEnumeration(unicode_value='Protection_confidentiality', tag='Protection_confidentiality')
capability.Protection_integrity = capability._CF_enumeration.addEnumeration(unicode_value='Protection_integrity', tag='Protection_integrity')
capability.Compress = capability._CF_enumeration.addEnumeration(unicode_value='Compress', tag='Compress')
capability.Logging = capability._CF_enumeration.addEnumeration(unicode_value='Logging', tag='Logging')
capability.AuthoriseAccess_resurce = capability._CF_enumeration.addEnumeration(unicode_value='AuthoriseAccess_resurce', tag='AuthoriseAccess_resurce')
capability.Reduce_bandwidth = capability._CF_enumeration.addEnumeration(unicode_value='Reduce_bandwidth', tag='Reduce_bandwidth')
capability.Online_security_analyzer = capability._CF_enumeration.addEnumeration(unicode_value='Online_security_analyzer', tag='Online_security_analyzer')
capability.Basic_parental_control = capability._CF_enumeration.addEnumeration(unicode_value='Basic_parental_control', tag='Basic_parental_control')
capability.Advanced_parental_control = capability._CF_enumeration.addEnumeration(unicode_value='Advanced_parental_control', tag='Advanced_parental_control')
capability.IPSec_protocol = capability._CF_enumeration.addEnumeration(unicode_value='IPSec_protocol', tag='IPSec_protocol')
capability.TLS_protocol = capability._CF_enumeration.addEnumeration(unicode_value='TLS_protocol', tag='TLS_protocol')
capability.reencrypt = capability._CF_enumeration.addEnumeration(unicode_value='reencrypt', tag='reencrypt')
capability.antiPhishing = capability._CF_enumeration.addEnumeration(unicode_value='antiPhishing', tag='antiPhishing')
capability.anonimity = capability._CF_enumeration.addEnumeration(unicode_value='anonimity', tag='anonimity')
capability._InitializeFacetMap(capability._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'capability', capability)
_module_typeBindings.capability = capability

# Atomic simple type: {http://www.example.org/Refinement_Schema}week_day
class week_day (pyxb.binding.datatypes.string, pyxb.binding.basis.enumeration_mixin):

    """An atomic simple type."""

    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'week_day')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 534, 5)
    _Documentation = None
week_day._CF_enumeration = pyxb.binding.facets.CF_enumeration(value_datatype=week_day, enum_prefix=None)
week_day.Mon = week_day._CF_enumeration.addEnumeration(unicode_value='Mon', tag='Mon')
week_day.Tue = week_day._CF_enumeration.addEnumeration(unicode_value='Tue', tag='Tue')
week_day.Wed = week_day._CF_enumeration.addEnumeration(unicode_value='Wed', tag='Wed')
week_day.Thu = week_day._CF_enumeration.addEnumeration(unicode_value='Thu', tag='Thu')
week_day.Fri = week_day._CF_enumeration.addEnumeration(unicode_value='Fri', tag='Fri')
week_day.Sat = week_day._CF_enumeration.addEnumeration(unicode_value='Sat', tag='Sat')
week_day.Sun = week_day._CF_enumeration.addEnumeration(unicode_value='Sun', tag='Sun')
week_day._InitializeFacetMap(week_day._CF_enumeration)
Namespace.addCategoryObject('typeBinding', 'week_day', week_day)
_module_typeBindings.week_day = week_day

# Complex type {http://www.example.org/Refinement_Schema}purpose with content type ELEMENT_ONLY
class purpose (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}purpose with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'purpose')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 76, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}purpose_property uses Python identifier purpose_property
    __purpose_property = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'purpose_property'), 'purpose_property', '__httpwww_example_orgRefinement_Schema_purpose_httpwww_example_orgRefinement_Schemapurpose_property', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 79, 12), )

    
    purpose_property = property(__purpose_property.value, __purpose_property.set, None, None)

    _ElementMap.update({
        __purpose_property.name() : __purpose_property
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.purpose = purpose
Namespace.addCategoryObject('typeBinding', 'purpose', purpose)


# Complex type {http://www.example.org/Refinement_Schema}time_period with content type ELEMENT_ONLY
class time_period (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}time_period with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'time_period')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 83, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}interval_time uses Python identifier interval_time
    __interval_time = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'interval_time'), 'interval_time', '__httpwww_example_orgRefinement_Schema_time_period_httpwww_example_orgRefinement_Schemainterval_time', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 85, 12), )

    
    interval_time = property(__interval_time.value, __interval_time.set, None, None)

    
    # Attribute time-zone uses Python identifier time_zone
    __time_zone = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'time-zone'), 'time_zone', '__httpwww_example_orgRefinement_Schema_time_period_time_zone', pyxb.binding.datatypes.string)
    __time_zone._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 87, 8)
    __time_zone._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 87, 8)
    
    time_zone = property(__time_zone.value, __time_zone.set, None, None)

    _ElementMap.update({
        __interval_time.name() : __interval_time
    })
    _AttributeMap.update({
        __time_zone.name() : __time_zone
    })
_module_typeBindings.time_period = time_period
Namespace.addCategoryObject('typeBinding', 'time_period', time_period)


# Complex type {http://www.example.org/Refinement_Schema}time_interval with content type ELEMENT_ONLY
class time_interval (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}time_interval with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'time_interval')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 90, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}week_day uses Python identifier week_day
    __week_day = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'week_day'), 'week_day', '__httpwww_example_orgRefinement_Schema_time_interval_httpwww_example_orgRefinement_Schemaweek_day', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 92, 12), )

    
    week_day = property(__week_day.value, __week_day.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}time_hours uses Python identifier time_hours
    __time_hours = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'time_hours'), 'time_hours', '__httpwww_example_orgRefinement_Schema_time_interval_httpwww_example_orgRefinement_Schematime_hours', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 95, 12), )

    
    time_hours = property(__time_hours.value, __time_hours.set, None, None)

    _ElementMap.update({
        __week_day.name() : __week_day,
        __time_hours.name() : __time_hours
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.time_interval = time_interval
Namespace.addCategoryObject('typeBinding', 'time_interval', time_interval)


# Complex type {http://www.example.org/Refinement_Schema}traffic_target with content type ELEMENT_ONLY
class traffic_target (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}traffic_target with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'traffic_target')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 99, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}target_name uses Python identifier target_name
    __target_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'target_name'), 'target_name', '__httpwww_example_orgRefinement_Schema_traffic_target_httpwww_example_orgRefinement_Schematarget_name', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 101, 12), )

    
    target_name = property(__target_name.value, __target_name.set, None, None)

    _ElementMap.update({
        __target_name.name() : __target_name
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.traffic_target = traffic_target
Namespace.addCategoryObject('typeBinding', 'traffic_target', traffic_target)


# Complex type {http://www.example.org/Refinement_Schema}specific_URL with content type ELEMENT_ONLY
class specific_URL (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}specific_URL with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'specific_URL')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 107, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}URL uses Python identifier URL
    __URL = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'URL'), 'URL', '__httpwww_example_orgRefinement_Schema_specific_URL_httpwww_example_orgRefinement_SchemaURL', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 109, 12), )

    
    URL = property(__URL.value, __URL.set, None, None)

    _ElementMap.update({
        __URL.name() : __URL
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.specific_URL = specific_URL
Namespace.addCategoryObject('typeBinding', 'specific_URL', specific_URL)


# Complex type {http://www.example.org/Refinement_Schema}type_Content with content type ELEMENT_ONLY
class type_Content (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}type_Content with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'type_Content')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 113, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}content_name uses Python identifier content_name
    __content_name = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'content_name'), 'content_name', '__httpwww_example_orgRefinement_Schema_type_Content_httpwww_example_orgRefinement_Schemacontent_name', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 115, 12), )

    
    content_name = property(__content_name.value, __content_name.set, None, None)

    _ElementMap.update({
        __content_name.name() : __content_name
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.type_Content = type_Content
Namespace.addCategoryObject('typeBinding', 'type_Content', type_Content)


# Complex type {http://www.example.org/Refinement_Schema}resource with content type ELEMENT_ONLY
class resource (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}resource with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'resource')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 120, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}resource_property uses Python identifier resource_property
    __resource_property = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'resource_property'), 'resource_property', '__httpwww_example_orgRefinement_Schema_resource_httpwww_example_orgRefinement_Schemaresource_property', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 123, 12), )

    
    resource_property = property(__resource_property.value, __resource_property.set, None, None)

    _ElementMap.update({
        __resource_property.name() : __resource_property
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.resource = resource
Namespace.addCategoryObject('typeBinding', 'resource', resource)


# Complex type {http://www.example.org/Refinement_Schema}Property with content type ELEMENT_ONLY
class Property (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}Property with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Property')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 128, 2)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}key uses Python identifier key
    __key = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'key'), 'key', '__httpwww_example_orgRefinement_Schema_Property_httpwww_example_orgRefinement_Schemakey', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 130, 10), )

    
    key = property(__key.value, __key.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}value uses Python identifier value_
    __value = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'value'), 'value_', '__httpwww_example_orgRefinement_Schema_Property_httpwww_example_orgRefinement_Schemavalue', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 131, 10), )

    
    value_ = property(__value.value, __value.set, None, None)

    _ElementMap.update({
        __key.name() : __key,
        __value.name() : __value
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Property = Property
Namespace.addCategoryObject('typeBinding', 'Property', Property)


# Complex type {http://www.example.org/Refinement_Schema}fields with content type ELEMENT_ONLY
class fields (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}fields with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'fields')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 136, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}time_period uses Python identifier time_period
    __time_period = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'time_period'), 'time_period', '__httpwww_example_orgRefinement_Schema_fields_httpwww_example_orgRefinement_Schematime_period', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 138, 12), )

    
    time_period = property(__time_period.value, __time_period.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}traffic_target uses Python identifier traffic_target
    __traffic_target = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'traffic_target'), 'traffic_target', '__httpwww_example_orgRefinement_Schema_fields_httpwww_example_orgRefinement_Schematraffic_target', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 141, 12), )

    
    traffic_target = property(__traffic_target.value, __traffic_target.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}specific_URL uses Python identifier specific_URL
    __specific_URL = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'specific_URL'), 'specific_URL', '__httpwww_example_orgRefinement_Schema_fields_httpwww_example_orgRefinement_Schemaspecific_URL', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 144, 12), )

    
    specific_URL = property(__specific_URL.value, __specific_URL.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}type_content uses Python identifier type_content
    __type_content = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'type_content'), 'type_content', '__httpwww_example_orgRefinement_Schema_fields_httpwww_example_orgRefinement_Schematype_content', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 147, 12), )

    
    type_content = property(__type_content.value, __type_content.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}purpose uses Python identifier purpose
    __purpose = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'purpose'), 'purpose', '__httpwww_example_orgRefinement_Schema_fields_httpwww_example_orgRefinement_Schemapurpose', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 150, 12), )

    
    purpose = property(__purpose.value, __purpose.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}resource uses Python identifier resource
    __resource = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'resource'), 'resource', '__httpwww_example_orgRefinement_Schema_fields_httpwww_example_orgRefinement_Schemaresource', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 153, 12), )

    
    resource = property(__resource.value, __resource.set, None, None)

    
    # Attribute downlink_bandwidth_value uses Python identifier downlink_bandwidth_value
    __downlink_bandwidth_value = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'downlink_bandwidth_value'), 'downlink_bandwidth_value', '__httpwww_example_orgRefinement_Schema_fields_downlink_bandwidth_value', pyxb.binding.datatypes.double)
    __downlink_bandwidth_value._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 157, 8)
    __downlink_bandwidth_value._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 157, 8)
    
    downlink_bandwidth_value = property(__downlink_bandwidth_value.value, __downlink_bandwidth_value.set, None, None)

    
    # Attribute uplink_bandwidth_value uses Python identifier uplink_bandwidth_value
    __uplink_bandwidth_value = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'uplink_bandwidth_value'), 'uplink_bandwidth_value', '__httpwww_example_orgRefinement_Schema_fields_uplink_bandwidth_value', pyxb.binding.datatypes.double)
    __uplink_bandwidth_value._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 158, 8)
    __uplink_bandwidth_value._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 158, 8)
    
    uplink_bandwidth_value = property(__uplink_bandwidth_value.value, __uplink_bandwidth_value.set, None, None)

    
    # Attribute country uses Python identifier country
    __country = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'country'), 'country', '__httpwww_example_orgRefinement_Schema_fields_country', pyxb.binding.datatypes.string)
    __country._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 159, 8)
    __country._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 159, 8)
    
    country = property(__country.value, __country.set, None, None)

    _ElementMap.update({
        __time_period.name() : __time_period,
        __traffic_target.name() : __traffic_target,
        __specific_URL.name() : __specific_URL,
        __type_content.name() : __type_content,
        __purpose.name() : __purpose,
        __resource.name() : __resource
    })
    _AttributeMap.update({
        __downlink_bandwidth_value.name() : __downlink_bandwidth_value,
        __uplink_bandwidth_value.name() : __uplink_bandwidth_value,
        __country.name() : __country
    })
_module_typeBindings.fields = fields
Namespace.addCategoryObject('typeBinding', 'fields', fields)


# Complex type {http://www.example.org/Refinement_Schema}HSPL with content type ELEMENT_ONLY
class HSPL (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}HSPL with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'HSPL')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 163, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}action uses Python identifier action
    __action = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'action'), 'action', '__httpwww_example_orgRefinement_Schema_HSPL_httpwww_example_orgRefinement_Schemaaction', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 165, 12), )

    
    action = property(__action.value, __action.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}objectH uses Python identifier objectH
    __objectH = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'objectH'), 'objectH', '__httpwww_example_orgRefinement_Schema_HSPL_httpwww_example_orgRefinement_SchemaobjectH', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 168, 12), )

    
    objectH = property(__objectH.value, __objectH.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}fields uses Python identifier fields
    __fields = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'fields'), 'fields', '__httpwww_example_orgRefinement_Schema_HSPL_httpwww_example_orgRefinement_Schemafields', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 171, 12), )

    
    fields = property(__fields.value, __fields.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}capabilities uses Python identifier capabilities
    __capabilities = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'capabilities'), 'capabilities', '__httpwww_example_orgRefinement_Schema_HSPL_httpwww_example_orgRefinement_Schemacapabilities', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 174, 12), )

    
    capabilities = property(__capabilities.value, __capabilities.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}candidates uses Python identifier candidates
    __candidates = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'candidates'), 'candidates', '__httpwww_example_orgRefinement_Schema_HSPL_httpwww_example_orgRefinement_Schemacandidates', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 177, 12), )

    
    candidates = property(__candidates.value, __candidates.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}suitableImplementation uses Python identifier suitableImplementation
    __suitableImplementation = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'suitableImplementation'), 'suitableImplementation', '__httpwww_example_orgRefinement_Schema_HSPL_httpwww_example_orgRefinement_SchemasuitableImplementation', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 180, 12), )

    
    suitableImplementation = property(__suitableImplementation.value, __suitableImplementation.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}non-enforzable-Capability uses Python identifier non_enforzable_Capability
    __non_enforzable_Capability = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'non-enforzable-Capability'), 'non_enforzable_Capability', '__httpwww_example_orgRefinement_Schema_HSPL_httpwww_example_orgRefinement_Schemanon_enforzable_Capability', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 183, 12), )

    
    non_enforzable_Capability = property(__non_enforzable_Capability.value, __non_enforzable_Capability.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}implementation uses Python identifier implementation
    __implementation = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'implementation'), 'implementation', '__httpwww_example_orgRefinement_Schema_HSPL_httpwww_example_orgRefinement_Schemaimplementation', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 186, 12), )

    
    implementation = property(__implementation.value, __implementation.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}dependencies uses Python identifier dependencies
    __dependencies = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'dependencies'), 'dependencies', '__httpwww_example_orgRefinement_Schema_HSPL_httpwww_example_orgRefinement_Schemadependencies', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 188, 12), )

    
    dependencies = property(__dependencies.value, __dependencies.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}priority uses Python identifier priority
    __priority = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'priority'), 'priority', '__httpwww_example_orgRefinement_Schema_HSPL_httpwww_example_orgRefinement_Schemapriority', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 189, 12), )

    
    priority = property(__priority.value, __priority.set, None, None)

    
    # Attribute subject uses Python identifier subject
    __subject = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'subject'), 'subject', '__httpwww_example_orgRefinement_Schema_HSPL_subject', pyxb.binding.datatypes.string)
    __subject._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 191, 8)
    __subject._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 191, 8)
    
    subject = property(__subject.value, __subject.set, None, None)

    
    # Attribute enforzability uses Python identifier enforzability
    __enforzability = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'enforzability'), 'enforzability', '__httpwww_example_orgRefinement_Schema_HSPL_enforzability', pyxb.binding.datatypes.boolean, unicode_default='true')
    __enforzability._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 192, 8)
    __enforzability._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 192, 8)
    
    enforzability = property(__enforzability.value, __enforzability.set, None, None)

    
    # Attribute bidirectional uses Python identifier bidirectional
    __bidirectional = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'bidirectional'), 'bidirectional', '__httpwww_example_orgRefinement_Schema_HSPL_bidirectional', pyxb.binding.datatypes.boolean)
    __bidirectional._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 194, 8)
    __bidirectional._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 194, 8)
    
    bidirectional = property(__bidirectional.value, __bidirectional.set, None, None)

    
    # Attribute id uses Python identifier id
    __id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'id'), 'id', '__httpwww_example_orgRefinement_Schema_HSPL_id', pyxb.binding.datatypes.ID, required=True)
    __id._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 195, 8)
    __id._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 195, 8)
    
    id = property(__id.value, __id.set, None, None)

    
    # Attribute orchestrationID uses Python identifier orchestrationID
    __orchestrationID = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'orchestrationID'), 'orchestrationID', '__httpwww_example_orgRefinement_Schema_HSPL_orchestrationID', pyxb.binding.datatypes.string)
    __orchestrationID._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 196, 8)
    __orchestrationID._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 196, 8)
    
    orchestrationID = property(__orchestrationID.value, __orchestrationID.set, None, None)

    _ElementMap.update({
        __action.name() : __action,
        __objectH.name() : __objectH,
        __fields.name() : __fields,
        __capabilities.name() : __capabilities,
        __candidates.name() : __candidates,
        __suitableImplementation.name() : __suitableImplementation,
        __non_enforzable_Capability.name() : __non_enforzable_Capability,
        __implementation.name() : __implementation,
        __dependencies.name() : __dependencies,
        __priority.name() : __priority
    })
    _AttributeMap.update({
        __subject.name() : __subject,
        __enforzability.name() : __enforzability,
        __bidirectional.name() : __bidirectional,
        __id.name() : __id,
        __orchestrationID.name() : __orchestrationID
    })
_module_typeBindings.HSPL = HSPL
Namespace.addCategoryObject('typeBinding', 'HSPL', HSPL)


# Complex type {http://www.example.org/Refinement_Schema}Dependencies with content type ELEMENT_ONLY
class Dependencies (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}Dependencies with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Dependencies')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 199, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}dependency uses Python identifier dependency
    __dependency = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'dependency'), 'dependency', '__httpwww_example_orgRefinement_Schema_Dependencies_httpwww_example_orgRefinement_Schemadependency', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 201, 16), )

    
    dependency = property(__dependency.value, __dependency.set, None, None)

    _ElementMap.update({
        __dependency.name() : __dependency
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Dependencies = Dependencies
Namespace.addCategoryObject('typeBinding', 'Dependencies', Dependencies)


# Complex type {http://www.example.org/Refinement_Schema}Dependency with content type EMPTY
class Dependency (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}Dependency with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Dependency')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 205, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Dependency = Dependency
Namespace.addCategoryObject('typeBinding', 'Dependency', Dependency)


# Complex type {http://www.example.org/Refinement_Schema}DependencyCondition with content type EMPTY
class DependencyCondition (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}DependencyCondition with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'DependencyCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 217, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.DependencyCondition = DependencyCondition
Namespace.addCategoryObject('typeBinding', 'DependencyCondition', DependencyCondition)


# Complex type {http://www.example.org/Refinement_Schema}PSA with content type ELEMENT_ONLY
class PSA (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}PSA with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PSA')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 255, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}PSA_info uses Python identifier PSA_info
    __PSA_info = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'PSA_info'), 'PSA_info', '__httpwww_example_orgRefinement_Schema_PSA_httpwww_example_orgRefinement_SchemaPSA_info', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 257, 12), )

    
    PSA_info = property(__PSA_info.value, __PSA_info.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}PSA_characteristic uses Python identifier PSA_characteristic
    __PSA_characteristic = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'PSA_characteristic'), 'PSA_characteristic', '__httpwww_example_orgRefinement_Schema_PSA_httpwww_example_orgRefinement_SchemaPSA_characteristic', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 260, 12), )

    
    PSA_characteristic = property(__PSA_characteristic.value, __PSA_characteristic.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}capability uses Python identifier capability
    __capability = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'capability'), 'capability', '__httpwww_example_orgRefinement_Schema_PSA_httpwww_example_orgRefinement_Schemacapability', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 263, 12), )

    
    capability = property(__capability.value, __capability.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}MSPL_list uses Python identifier MSPL_list
    __MSPL_list = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'MSPL_list'), 'MSPL_list', '__httpwww_example_orgRefinement_Schema_PSA_httpwww_example_orgRefinement_SchemaMSPL_list', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 264, 12), )

    
    MSPL_list = property(__MSPL_list.value, __MSPL_list.set, None, None)

    
    # Attribute name uses Python identifier name
    __name = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'name'), 'name', '__httpwww_example_orgRefinement_Schema_PSA_name', pyxb.binding.datatypes.string)
    __name._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 266, 8)
    __name._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 266, 8)
    
    name = property(__name.value, __name.set, None, None)

    _ElementMap.update({
        __PSA_info.name() : __PSA_info,
        __PSA_characteristic.name() : __PSA_characteristic,
        __capability.name() : __capability,
        __MSPL_list.name() : __MSPL_list
    })
    _AttributeMap.update({
        __name.name() : __name
    })
_module_typeBindings.PSA = PSA
Namespace.addCategoryObject('typeBinding', 'PSA', PSA)


# Complex type {http://www.example.org/Refinement_Schema}PSA_info with content type EMPTY
class PSA_info (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}PSA_info with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PSA_info')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 269, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PSA_info = PSA_info
Namespace.addCategoryObject('typeBinding', 'PSA_info', PSA_info)


# Complex type {http://www.example.org/Refinement_Schema}PSA_characteristic with content type EMPTY
class PSA_characteristic (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}PSA_characteristic with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PSA_characteristic')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 272, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute cost uses Python identifier cost
    __cost = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'cost'), 'cost', '__httpwww_example_orgRefinement_Schema_PSA_characteristic_cost', pyxb.binding.datatypes.double, required=True)
    __cost._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 273, 8)
    __cost._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 273, 8)
    
    cost = property(__cost.value, __cost.set, None, None)

    
    # Attribute latency uses Python identifier latency
    __latency = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'latency'), 'latency', '__httpwww_example_orgRefinement_Schema_PSA_characteristic_latency', pyxb.binding.datatypes.double, required=True)
    __latency._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 274, 8)
    __latency._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 274, 8)
    
    latency = property(__latency.value, __latency.set, None, None)

    
    # Attribute rating uses Python identifier rating
    __rating = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'rating'), 'rating', '__httpwww_example_orgRefinement_Schema_PSA_characteristic_rating', pyxb.binding.datatypes.double, required=True)
    __rating._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 275, 8)
    __rating._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 275, 8)
    
    rating = property(__rating.value, __rating.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __cost.name() : __cost,
        __latency.name() : __latency,
        __rating.name() : __rating
    })
_module_typeBindings.PSA_characteristic = PSA_characteristic
Namespace.addCategoryObject('typeBinding', 'PSA_characteristic', PSA_characteristic)


# Complex type {http://www.example.org/Refinement_Schema}HSPL_list with content type ELEMENT_ONLY
class HSPL_list (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}HSPL_list with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'HSPL_list')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 279, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}hspl uses Python identifier hspl
    __hspl = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'hspl'), 'hspl', '__httpwww_example_orgRefinement_Schema_HSPL_list_httpwww_example_orgRefinement_Schemahspl', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 281, 12), )

    
    hspl = property(__hspl.value, __hspl.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}dependencies uses Python identifier dependencies
    __dependencies = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'dependencies'), 'dependencies', '__httpwww_example_orgRefinement_Schema_HSPL_list_httpwww_example_orgRefinement_Schemadependencies', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 283, 12), )

    
    dependencies = property(__dependencies.value, __dependencies.set, None, None)

    _ElementMap.update({
        __hspl.name() : __hspl,
        __dependencies.name() : __dependencies
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.HSPL_list = HSPL_list
Namespace.addCategoryObject('typeBinding', 'HSPL_list', HSPL_list)


# Complex type {http://www.example.org/Refinement_Schema}capability_list with content type ELEMENT_ONLY
class capability_list (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}capability_list with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'capability_list')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 297, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}capability_list uses Python identifier capability_list
    __capability_list = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'capability_list'), 'capability_list', '__httpwww_example_orgRefinement_Schema_capability_list_httpwww_example_orgRefinement_Schemacapability_list', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 299, 12), )

    
    capability_list = property(__capability_list.value, __capability_list.set, None, None)

    _ElementMap.update({
        __capability_list.name() : __capability_list
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.capability_list = capability_list
Namespace.addCategoryObject('typeBinding', 'capability_list', capability_list)


# Complex type {http://www.example.org/Refinement_Schema}PSA_list with content type ELEMENT_ONLY
class PSA_list (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}PSA_list with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PSA_list')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 411, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}psa uses Python identifier psa
    __psa = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'psa'), 'psa', '__httpwww_example_orgRefinement_Schema_PSA_list_httpwww_example_orgRefinement_Schemapsa', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 413, 12), )

    
    psa = property(__psa.value, __psa.set, None, None)

    _ElementMap.update({
        __psa.name() : __psa
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PSA_list = PSA_list
Namespace.addCategoryObject('typeBinding', 'PSA_list', PSA_list)


# Complex type {http://www.example.org/Refinement_Schema}MappingType with content type ELEMENT_ONLY
class MappingType (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}MappingType with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MappingType')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 419, 5)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}hspl_list uses Python identifier hspl_list
    __hspl_list = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'hspl_list'), 'hspl_list', '__httpwww_example_orgRefinement_Schema_MappingType_httpwww_example_orgRefinement_Schemahspl_list', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 421, 12), )

    
    hspl_list = property(__hspl_list.value, __hspl_list.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}psa_list uses Python identifier psa_list
    __psa_list = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'psa_list'), 'psa_list', '__httpwww_example_orgRefinement_Schema_MappingType_httpwww_example_orgRefinement_Schemapsa_list', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 424, 12), )

    
    psa_list = property(__psa_list.value, __psa_list.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}solution uses Python identifier solution
    __solution = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'solution'), 'solution', '__httpwww_example_orgRefinement_Schema_MappingType_httpwww_example_orgRefinement_Schemasolution', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 427, 12), )

    
    solution = property(__solution.value, __solution.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}service_graph uses Python identifier service_graph
    __service_graph = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'service_graph'), 'service_graph', '__httpwww_example_orgRefinement_Schema_MappingType_httpwww_example_orgRefinement_Schemaservice_graph', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 430, 12), )

    
    service_graph = property(__service_graph.value, __service_graph.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}mspl_list uses Python identifier mspl_list
    __mspl_list = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'mspl_list'), 'mspl_list', '__httpwww_example_orgRefinement_Schema_MappingType_httpwww_example_orgRefinement_Schemamspl_list', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 433, 12), )

    
    mspl_list = property(__mspl_list.value, __mspl_list.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}remediation uses Python identifier remediation
    __remediation = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'remediation'), 'remediation', '__httpwww_example_orgRefinement_Schema_MappingType_httpwww_example_orgRefinement_Schemaremediation', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 436, 12), )

    
    remediation = property(__remediation.value, __remediation.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}user_psa_list uses Python identifier user_psa_list
    __user_psa_list = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'user_psa_list'), 'user_psa_list', '__httpwww_example_orgRefinement_Schema_MappingType_httpwww_example_orgRefinement_Schemauser_psa_list', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 439, 12), )

    
    user_psa_list = property(__user_psa_list.value, __user_psa_list.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}additional_psa_list uses Python identifier additional_psa_list
    __additional_psa_list = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'additional_psa_list'), 'additional_psa_list', '__httpwww_example_orgRefinement_Schema_MappingType_httpwww_example_orgRefinement_Schemaadditional_psa_list', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 442, 12), )

    
    additional_psa_list = property(__additional_psa_list.value, __additional_psa_list.set, None, None)

    
    # Attribute mix uses Python identifier mix
    __mix = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'mix'), 'mix', '__httpwww_example_orgRefinement_Schema_MappingType_mix', pyxb.binding.datatypes.boolean, unicode_default='false')
    __mix._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 444, 8)
    __mix._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 444, 8)
    
    mix = property(__mix.value, __mix.set, None, None)

    
    # Attribute isEnforciability uses Python identifier isEnforciability
    __isEnforciability = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'isEnforciability'), 'isEnforciability', '__httpwww_example_orgRefinement_Schema_MappingType_isEnforciability', pyxb.binding.datatypes.boolean, unicode_default='true')
    __isEnforciability._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 445, 8)
    __isEnforciability._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 445, 8)
    
    isEnforciability = property(__isEnforciability.value, __isEnforciability.set, None, None)

    _ElementMap.update({
        __hspl_list.name() : __hspl_list,
        __psa_list.name() : __psa_list,
        __solution.name() : __solution,
        __service_graph.name() : __service_graph,
        __mspl_list.name() : __mspl_list,
        __remediation.name() : __remediation,
        __user_psa_list.name() : __user_psa_list,
        __additional_psa_list.name() : __additional_psa_list
    })
    _AttributeMap.update({
        __mix.name() : __mix,
        __isEnforciability.name() : __isEnforciability
    })
_module_typeBindings.MappingType = MappingType
Namespace.addCategoryObject('typeBinding', 'MappingType', MappingType)


# Complex type {http://www.example.org/Refinement_Schema}Candidates with content type ELEMENT_ONLY
class Candidates (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}Candidates with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Candidates')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 450, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}suitable_PSA_list uses Python identifier suitable_PSA_list
    __suitable_PSA_list = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'suitable_PSA_list'), 'suitable_PSA_list', '__httpwww_example_orgRefinement_Schema_Candidates_httpwww_example_orgRefinement_Schemasuitable_PSA_list', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 452, 12), )

    
    suitable_PSA_list = property(__suitable_PSA_list.value, __suitable_PSA_list.set, None, None)

    _ElementMap.update({
        __suitable_PSA_list.name() : __suitable_PSA_list
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Candidates = Candidates
Namespace.addCategoryObject('typeBinding', 'Candidates', Candidates)


# Complex type {http://www.example.org/Refinement_Schema}suitablePSA with content type ELEMENT_ONLY
class suitablePSA (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}suitablePSA with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'suitablePSA')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 456, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}capability uses Python identifier capability
    __capability = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'capability'), 'capability', '__httpwww_example_orgRefinement_Schema_suitablePSA_httpwww_example_orgRefinement_Schemacapability', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 458, 12), )

    
    capability = property(__capability.value, __capability.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}psa_list uses Python identifier psa_list
    __psa_list = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'psa_list'), 'psa_list', '__httpwww_example_orgRefinement_Schema_suitablePSA_httpwww_example_orgRefinement_Schemapsa_list', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 461, 12), )

    
    psa_list = property(__psa_list.value, __psa_list.set, None, None)

    _ElementMap.update({
        __capability.name() : __capability,
        __psa_list.name() : __psa_list
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.suitablePSA = suitablePSA
Namespace.addCategoryObject('typeBinding', 'suitablePSA', suitablePSA)


# Complex type {http://www.example.org/Refinement_Schema}suitableImplementation_list with content type ELEMENT_ONLY
class suitableImplementation_list (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}suitableImplementation_list with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'suitableImplementation_list')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 465, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}subitableImplementation uses Python identifier subitableImplementation
    __subitableImplementation = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'subitableImplementation'), 'subitableImplementation', '__httpwww_example_orgRefinement_Schema_suitableImplementation_list_httpwww_example_orgRefinement_SchemasubitableImplementation', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 467, 12), )

    
    subitableImplementation = property(__subitableImplementation.value, __subitableImplementation.set, None, None)

    
    # Attribute name uses Python identifier name
    __name = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'name'), 'name', '__httpwww_example_orgRefinement_Schema_suitableImplementation_list_name', pyxb.binding.datatypes.string)
    __name._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 471, 8)
    __name._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 471, 8)
    
    name = property(__name.value, __name.set, None, None)

    _ElementMap.update({
        __subitableImplementation.name() : __subitableImplementation
    })
    _AttributeMap.update({
        __name.name() : __name
    })
_module_typeBindings.suitableImplementation_list = suitableImplementation_list
Namespace.addCategoryObject('typeBinding', 'suitableImplementation_list', suitableImplementation_list)


# Complex type {http://www.example.org/Refinement_Schema}solution with content type ELEMENT_ONLY
class solution (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}solution with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'solution')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 474, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}psaList uses Python identifier psaList
    __psaList = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'psaList'), 'psaList', '__httpwww_example_orgRefinement_Schema_solution_httpwww_example_orgRefinement_SchemapsaList', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 476, 12), )

    
    psaList = property(__psaList.value, __psaList.set, None, None)

    
    # Attribute cost uses Python identifier cost
    __cost = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'cost'), 'cost', '__httpwww_example_orgRefinement_Schema_solution_cost', pyxb.binding.datatypes.double)
    __cost._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 478, 8)
    __cost._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 478, 8)
    
    cost = property(__cost.value, __cost.set, None, None)

    
    # Attribute latency uses Python identifier latency
    __latency = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'latency'), 'latency', '__httpwww_example_orgRefinement_Schema_solution_latency', pyxb.binding.datatypes.double)
    __latency._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 479, 8)
    __latency._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 479, 8)
    
    latency = property(__latency.value, __latency.set, None, None)

    
    # Attribute rating uses Python identifier rating
    __rating = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'rating'), 'rating', '__httpwww_example_orgRefinement_Schema_solution_rating', pyxb.binding.datatypes.double)
    __rating._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 480, 8)
    __rating._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 480, 8)
    
    rating = property(__rating.value, __rating.set, None, None)

    _ElementMap.update({
        __psaList.name() : __psaList
    })
    _AttributeMap.update({
        __cost.name() : __cost,
        __latency.name() : __latency,
        __rating.name() : __rating
    })
_module_typeBindings.solution = solution
Namespace.addCategoryObject('typeBinding', 'solution', solution)


# Complex type {http://www.example.org/Refinement_Schema}solutionList with content type ELEMENT_ONLY
class solutionList (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}solutionList with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'solutionList')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 483, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}solutions uses Python identifier solutions
    __solutions = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'solutions'), 'solutions', '__httpwww_example_orgRefinement_Schema_solutionList_httpwww_example_orgRefinement_Schemasolutions', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 485, 12), )

    
    solutions = property(__solutions.value, __solutions.set, None, None)

    _ElementMap.update({
        __solutions.name() : __solutions
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.solutionList = solutionList
Namespace.addCategoryObject('typeBinding', 'solutionList', solutionList)


# Complex type {http://www.example.org/Refinement_Schema}ServiceGraph with content type ELEMENT_ONLY
class ServiceGraph (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}ServiceGraph with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'ServiceGraph')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 489, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}service uses Python identifier service
    __service = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'service'), 'service', '__httpwww_example_orgRefinement_Schema_ServiceGraph_httpwww_example_orgRefinement_Schemaservice', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 491, 12), )

    
    service = property(__service.value, __service.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}rootService uses Python identifier rootService
    __rootService = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'rootService'), 'rootService', '__httpwww_example_orgRefinement_Schema_ServiceGraph_httpwww_example_orgRefinement_SchemarootService', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 494, 12), )

    
    rootService = property(__rootService.value, __rootService.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}endService uses Python identifier endService
    __endService = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'endService'), 'endService', '__httpwww_example_orgRefinement_Schema_ServiceGraph_httpwww_example_orgRefinement_SchemaendService', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 497, 12), )

    
    endService = property(__endService.value, __endService.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}edge uses Python identifier edge
    __edge = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'edge'), 'edge', '__httpwww_example_orgRefinement_Schema_ServiceGraph_httpwww_example_orgRefinement_Schemaedge', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 500, 12), )

    
    edge = property(__edge.value, __edge.set, None, None)

    _ElementMap.update({
        __service.name() : __service,
        __rootService.name() : __rootService,
        __endService.name() : __endService,
        __edge.name() : __edge
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.ServiceGraph = ServiceGraph
Namespace.addCategoryObject('typeBinding', 'ServiceGraph', ServiceGraph)


# Complex type {http://www.example.org/Refinement_Schema}Service with content type ELEMENT_ONLY
class Service (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}Service with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Service')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 504, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}PSA uses Python identifier PSA
    __PSA = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'PSA'), 'PSA', '__httpwww_example_orgRefinement_Schema_Service_httpwww_example_orgRefinement_SchemaPSA', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 506, 12), )

    
    PSA = property(__PSA.value, __PSA.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}capability uses Python identifier capability
    __capability = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'capability'), 'capability', '__httpwww_example_orgRefinement_Schema_Service_httpwww_example_orgRefinement_Schemacapability', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 509, 12), )

    
    capability = property(__capability.value, __capability.set, None, None)

    
    # Attribute serviceID uses Python identifier serviceID
    __serviceID = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'serviceID'), 'serviceID', '__httpwww_example_orgRefinement_Schema_Service_serviceID', pyxb.binding.datatypes.ID)
    __serviceID._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 513, 8)
    __serviceID._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 513, 8)
    
    serviceID = property(__serviceID.value, __serviceID.set, None, None)

    
    # Attribute MSPL_ID uses Python identifier MSPL_ID
    __MSPL_ID = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'MSPL_ID'), 'MSPL_ID', '__httpwww_example_orgRefinement_Schema_Service_MSPL_ID', pyxb.binding.datatypes.string)
    __MSPL_ID._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 514, 8)
    __MSPL_ID._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 514, 8)
    
    MSPL_ID = property(__MSPL_ID.value, __MSPL_ID.set, None, None)

    _ElementMap.update({
        __PSA.name() : __PSA,
        __capability.name() : __capability
    })
    _AttributeMap.update({
        __serviceID.name() : __serviceID,
        __MSPL_ID.name() : __MSPL_ID
    })
_module_typeBindings.Service = Service
Namespace.addCategoryObject('typeBinding', 'Service', Service)


# Complex type {http://www.example.org/Refinement_Schema}edge with content type ELEMENT_ONLY
class edge (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}edge with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'edge')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 520, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}src_Service uses Python identifier src_Service
    __src_Service = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'src_Service'), 'src_Service', '__httpwww_example_orgRefinement_Schema_edge_httpwww_example_orgRefinement_Schemasrc_Service', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 522, 12), )

    
    src_Service = property(__src_Service.value, __src_Service.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}dst_Service uses Python identifier dst_Service
    __dst_Service = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'dst_Service'), 'dst_Service', '__httpwww_example_orgRefinement_Schema_edge_httpwww_example_orgRefinement_Schemadst_Service', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 523, 12), )

    
    dst_Service = property(__dst_Service.value, __dst_Service.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}networkFields uses Python identifier networkFields
    __networkFields = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'networkFields'), 'networkFields', '__httpwww_example_orgRefinement_Schema_edge_httpwww_example_orgRefinement_SchemanetworkFields', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 524, 12), )

    
    networkFields = property(__networkFields.value, __networkFields.set, None, None)

    _ElementMap.update({
        __src_Service.name() : __src_Service,
        __dst_Service.name() : __dst_Service,
        __networkFields.name() : __networkFields
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.edge = edge
Namespace.addCategoryObject('typeBinding', 'edge', edge)


# Complex type {http://www.example.org/Refinement_Schema}time_hour with content type EMPTY
class time_hour (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}time_hour with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'time_hour')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 528, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Attribute start-time uses Python identifier start_time
    __start_time = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'start-time'), 'start_time', '__httpwww_example_orgRefinement_Schema_time_hour_start_time', pyxb.binding.datatypes.string)
    __start_time._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 529, 4)
    __start_time._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 529, 4)
    
    start_time = property(__start_time.value, __start_time.set, None, None)

    
    # Attribute end-time uses Python identifier end_time
    __end_time = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'end-time'), 'end_time', '__httpwww_example_orgRefinement_Schema_time_hour_end_time', pyxb.binding.datatypes.string)
    __end_time._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 530, 8)
    __end_time._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 530, 8)
    
    end_time = property(__end_time.value, __end_time.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __start_time.name() : __start_time,
        __end_time.name() : __end_time
    })
_module_typeBindings.time_hour = time_hour
Namespace.addCategoryObject('typeBinding', 'time_hour', time_hour)


# Complex type {http://www.example.org/Refinement_Schema}networkFields with content type EMPTY
class networkFields (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}networkFields with content type EMPTY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_EMPTY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'networkFields')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 547, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    _ElementMap.update({
        
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.networkFields = networkFields
Namespace.addCategoryObject('typeBinding', 'networkFields', networkFields)


# Complex type {http://www.example.org/Refinement_Schema}MSPL with content type ELEMENT_ONLY
class MSPL (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}MSPL with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MSPL')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 549, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}capabilities uses Python identifier capabilities
    __capabilities = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'capabilities'), 'capabilities', '__httpwww_example_orgRefinement_Schema_MSPL_httpwww_example_orgRefinement_Schemacapabilities', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 551, 12), )

    
    capabilities = property(__capabilities.value, __capabilities.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}candidates uses Python identifier candidates
    __candidates = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'candidates'), 'candidates', '__httpwww_example_orgRefinement_Schema_MSPL_httpwww_example_orgRefinement_Schemacandidates', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 554, 12), )

    
    candidates = property(__candidates.value, __candidates.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}suitableImplementation uses Python identifier suitableImplementation
    __suitableImplementation = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'suitableImplementation'), 'suitableImplementation', '__httpwww_example_orgRefinement_Schema_MSPL_httpwww_example_orgRefinement_SchemasuitableImplementation', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 557, 12), )

    
    suitableImplementation = property(__suitableImplementation.value, __suitableImplementation.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}non-enforzable-Capability uses Python identifier non_enforzable_Capability
    __non_enforzable_Capability = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'non-enforzable-Capability'), 'non_enforzable_Capability', '__httpwww_example_orgRefinement_Schema_MSPL_httpwww_example_orgRefinement_Schemanon_enforzable_Capability', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 560, 12), )

    
    non_enforzable_Capability = property(__non_enforzable_Capability.value, __non_enforzable_Capability.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}implementation uses Python identifier implementation
    __implementation = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'implementation'), 'implementation', '__httpwww_example_orgRefinement_Schema_MSPL_httpwww_example_orgRefinement_Schemaimplementation', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 563, 12), )

    
    implementation = property(__implementation.value, __implementation.set, None, None)

    
    # Attribute id uses Python identifier id
    __id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'id'), 'id', '__httpwww_example_orgRefinement_Schema_MSPL_id', pyxb.binding.datatypes.ID, required=True)
    __id._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 565, 8)
    __id._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 565, 8)
    
    id = property(__id.value, __id.set, None, None)

    
    # Attribute enforzability uses Python identifier enforzability
    __enforzability = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'enforzability'), 'enforzability', '__httpwww_example_orgRefinement_Schema_MSPL_enforzability', pyxb.binding.datatypes.boolean, unicode_default='true')
    __enforzability._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 566, 8)
    __enforzability._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 566, 8)
    
    enforzability = property(__enforzability.value, __enforzability.set, None, None)

    _ElementMap.update({
        __capabilities.name() : __capabilities,
        __candidates.name() : __candidates,
        __suitableImplementation.name() : __suitableImplementation,
        __non_enforzable_Capability.name() : __non_enforzable_Capability,
        __implementation.name() : __implementation
    })
    _AttributeMap.update({
        __id.name() : __id,
        __enforzability.name() : __enforzability
    })
_module_typeBindings.MSPL = MSPL
Namespace.addCategoryObject('typeBinding', 'MSPL', MSPL)


# Complex type {http://www.example.org/Refinement_Schema}MSPL_list with content type ELEMENT_ONLY
class MSPL_list (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}MSPL_list with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'MSPL_list')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 572, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}mspl_list uses Python identifier mspl_list
    __mspl_list = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'mspl_list'), 'mspl_list', '__httpwww_example_orgRefinement_Schema_MSPL_list_httpwww_example_orgRefinement_Schemamspl_list', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 574, 12), )

    
    mspl_list = property(__mspl_list.value, __mspl_list.set, None, None)

    _ElementMap.update({
        __mspl_list.name() : __mspl_list
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.MSPL_list = MSPL_list
Namespace.addCategoryObject('typeBinding', 'MSPL_list', MSPL_list)


# Complex type {http://www.example.org/Refinement_Schema}RemediationList with content type ELEMENT_ONLY
class RemediationList (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}RemediationList with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'RemediationList')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 578, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}solution_repository uses Python identifier solution_repository
    __solution_repository = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'solution_repository'), 'solution_repository', '__httpwww_example_orgRefinement_Schema_RemediationList_httpwww_example_orgRefinement_Schemasolution_repository', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 580, 12), )

    
    solution_repository = property(__solution_repository.value, __solution_repository.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}solution_market uses Python identifier solution_market
    __solution_market = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'solution_market'), 'solution_market', '__httpwww_example_orgRefinement_Schema_RemediationList_httpwww_example_orgRefinement_Schemasolution_market', True, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 581, 12), )

    
    solution_market = property(__solution_market.value, __solution_market.set, None, None)

    _ElementMap.update({
        __solution_repository.name() : __solution_repository,
        __solution_market.name() : __solution_market
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.RemediationList = RemediationList
Namespace.addCategoryObject('typeBinding', 'RemediationList', RemediationList)


# Complex type {http://www.example.org/Refinement_Schema}Remediationt with content type ELEMENT_ONLY
class Remediationt (pyxb.binding.basis.complexTypeDefinition):
    """Complex type {http://www.example.org/Refinement_Schema}Remediationt with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'Remediationt')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 585, 4)
    _ElementMap = {}
    _AttributeMap = {}
    # Base type is pyxb.binding.datatypes.anyType
    
    # Element {http://www.example.org/Refinement_Schema}hspl uses Python identifier hspl
    __hspl = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'hspl'), 'hspl', '__httpwww_example_orgRefinement_Schema_Remediationt_httpwww_example_orgRefinement_Schemahspl', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 588, 12), )

    
    hspl = property(__hspl.value, __hspl.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}mspl uses Python identifier mspl
    __mspl = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'mspl'), 'mspl', '__httpwww_example_orgRefinement_Schema_Remediationt_httpwww_example_orgRefinement_Schemamspl', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 591, 12), )

    
    mspl = property(__mspl.value, __mspl.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}suitablePSA uses Python identifier suitablePSA
    __suitablePSA = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'suitablePSA'), 'suitablePSA', '__httpwww_example_orgRefinement_Schema_Remediationt_httpwww_example_orgRefinement_SchemasuitablePSA', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 594, 12), )

    
    suitablePSA = property(__suitablePSA.value, __suitablePSA.set, None, None)

    _ElementMap.update({
        __hspl.name() : __hspl,
        __mspl.name() : __mspl,
        __suitablePSA.name() : __suitablePSA
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.Remediationt = Remediationt
Namespace.addCategoryObject('typeBinding', 'Remediationt', Remediationt)


# Complex type {http://www.example.org/Refinement_Schema}PolicyDependency with content type ELEMENT_ONLY
class PolicyDependency (Dependency):
    """Complex type {http://www.example.org/Refinement_Schema}PolicyDependency with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PolicyDependency')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 207, 12)
    _ElementMap = Dependency._ElementMap.copy()
    _AttributeMap = Dependency._AttributeMap.copy()
    # Base type is Dependency
    
    # Element {http://www.example.org/Refinement_Schema}dependencyCondition uses Python identifier dependencyCondition
    __dependencyCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'dependencyCondition'), 'dependencyCondition', '__httpwww_example_orgRefinement_Schema_PolicyDependency_httpwww_example_orgRefinement_SchemadependencyCondition', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 211, 10), )

    
    dependencyCondition = property(__dependencyCondition.value, __dependencyCondition.set, None, None)

    _ElementMap.update({
        __dependencyCondition.name() : __dependencyCondition
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PolicyDependency = PolicyDependency
Namespace.addCategoryObject('typeBinding', 'PolicyDependency', PolicyDependency)


# Complex type {http://www.example.org/Refinement_Schema}PolicyDependencyCondition with content type ELEMENT_ONLY
class PolicyDependencyCondition (DependencyCondition):
    """Complex type {http://www.example.org/Refinement_Schema}PolicyDependencyCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'PolicyDependencyCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 219, 2)
    _ElementMap = DependencyCondition._ElementMap.copy()
    _AttributeMap = DependencyCondition._AttributeMap.copy()
    # Base type is DependencyCondition
    
    # Element {http://www.example.org/Refinement_Schema}policyID uses Python identifier policyID
    __policyID = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'policyID'), 'policyID', '__httpwww_example_orgRefinement_Schema_PolicyDependencyCondition_httpwww_example_orgRefinement_SchemapolicyID', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 223, 14), )

    
    policyID = property(__policyID.value, __policyID.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}status uses Python identifier status
    __status = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'status'), 'status', '__httpwww_example_orgRefinement_Schema_PolicyDependencyCondition_httpwww_example_orgRefinement_Schemastatus', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 224, 8), )

    
    status = property(__status.value, __status.set, None, None)

    _ElementMap.update({
        __policyID.name() : __policyID,
        __status.name() : __status
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.PolicyDependencyCondition = PolicyDependencyCondition
Namespace.addCategoryObject('typeBinding', 'PolicyDependencyCondition', PolicyDependencyCondition)


# Complex type {http://www.example.org/Refinement_Schema}EventDependency with content type ELEMENT_ONLY
class EventDependency (Dependency):
    """Complex type {http://www.example.org/Refinement_Schema}EventDependency with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EventDependency')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 230, 4)
    _ElementMap = Dependency._ElementMap.copy()
    _AttributeMap = Dependency._AttributeMap.copy()
    # Base type is Dependency
    
    # Element {http://www.example.org/Refinement_Schema}eventID uses Python identifier eventID
    __eventID = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'eventID'), 'eventID', '__httpwww_example_orgRefinement_Schema_EventDependency_httpwww_example_orgRefinement_SchemaeventID', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 234, 12), )

    
    eventID = property(__eventID.value, __eventID.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}dependencyCondition uses Python identifier dependencyCondition
    __dependencyCondition = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'dependencyCondition'), 'dependencyCondition', '__httpwww_example_orgRefinement_Schema_EventDependency_httpwww_example_orgRefinement_SchemadependencyCondition', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 235, 14), )

    
    dependencyCondition = property(__dependencyCondition.value, __dependencyCondition.set, None, None)

    _ElementMap.update({
        __eventID.name() : __eventID,
        __dependencyCondition.name() : __dependencyCondition
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.EventDependency = EventDependency
Namespace.addCategoryObject('typeBinding', 'EventDependency', EventDependency)


# Complex type {http://www.example.org/Refinement_Schema}EventDependencyCondition with content type ELEMENT_ONLY
class EventDependencyCondition (DependencyCondition):
    """Complex type {http://www.example.org/Refinement_Schema}EventDependencyCondition with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'EventDependencyCondition')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 241, 6)
    _ElementMap = DependencyCondition._ElementMap.copy()
    _AttributeMap = DependencyCondition._AttributeMap.copy()
    # Base type is DependencyCondition
    
    # Element {http://www.example.org/Refinement_Schema}subject uses Python identifier subject
    __subject = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'subject'), 'subject', '__httpwww_example_orgRefinement_Schema_EventDependencyCondition_httpwww_example_orgRefinement_Schemasubject', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 245, 14), )

    
    subject = property(__subject.value, __subject.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}action uses Python identifier action
    __action = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'action'), 'action', '__httpwww_example_orgRefinement_Schema_EventDependencyCondition_httpwww_example_orgRefinement_Schemaaction', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 246, 14), )

    
    action = property(__action.value, __action.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}objectH uses Python identifier objectH
    __objectH = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'objectH'), 'objectH', '__httpwww_example_orgRefinement_Schema_EventDependencyCondition_httpwww_example_orgRefinement_SchemaobjectH', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 247, 14), )

    
    objectH = property(__objectH.value, __objectH.set, None, None)

    
    # Element {http://www.example.org/Refinement_Schema}fields uses Python identifier fields
    __fields = pyxb.binding.content.ElementDeclaration(pyxb.namespace.ExpandedName(Namespace, 'fields'), 'fields', '__httpwww_example_orgRefinement_Schema_EventDependencyCondition_httpwww_example_orgRefinement_Schemafields', False, pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 248, 14), )

    
    fields = property(__fields.value, __fields.set, None, None)

    _ElementMap.update({
        __subject.name() : __subject,
        __action.name() : __action,
        __objectH.name() : __objectH,
        __fields.name() : __fields
    })
    _AttributeMap.update({
        
    })
_module_typeBindings.EventDependencyCondition = EventDependencyCondition
Namespace.addCategoryObject('typeBinding', 'EventDependencyCondition', EventDependencyCondition)


# Complex type {http://www.example.org/Refinement_Schema}HSPL_Orchestration with content type ELEMENT_ONLY
class HSPL_Orchestration (HSPL_list):
    """Complex type {http://www.example.org/Refinement_Schema}HSPL_Orchestration with content type ELEMENT_ONLY"""
    _TypeDefinition = None
    _ContentTypeTag = pyxb.binding.basis.complexTypeDefinition._CT_ELEMENT_ONLY
    _Abstract = False
    _ExpandedName = pyxb.namespace.ExpandedName(Namespace, 'HSPL_Orchestration')
    _XSDLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 288, 6)
    _ElementMap = HSPL_list._ElementMap.copy()
    _AttributeMap = HSPL_list._AttributeMap.copy()
    # Base type is HSPL_list
    
    # Element hspl ({http://www.example.org/Refinement_Schema}hspl) inherited from {http://www.example.org/Refinement_Schema}HSPL_list
    
    # Element dependencies ({http://www.example.org/Refinement_Schema}dependencies) inherited from {http://www.example.org/Refinement_Schema}HSPL_list
    
    # Attribute id uses Python identifier id
    __id = pyxb.binding.content.AttributeUse(pyxb.namespace.ExpandedName(None, 'id'), 'id', '__httpwww_example_orgRefinement_Schema_HSPL_Orchestration_id', pyxb.binding.datatypes.string, required=True)
    __id._DeclarationLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 291, 12)
    __id._UseLocation = pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 291, 12)
    
    id = property(__id.value, __id.set, None, None)

    _ElementMap.update({
        
    })
    _AttributeMap.update({
        __id.name() : __id
    })
_module_typeBindings.HSPL_Orchestration = HSPL_Orchestration
Namespace.addCategoryObject('typeBinding', 'HSPL_Orchestration', HSPL_Orchestration)


Mapping = pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'Mapping'), MappingType, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 417, 4))
Namespace.addCategoryObject('elementBinding', Mapping.name().localName(), Mapping)



purpose._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'purpose_property'), Property, scope=purpose, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 79, 12)))

def _BuildAutomaton ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton
    del _BuildAutomaton
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(purpose._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'purpose_property')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 79, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
purpose._Automaton = _BuildAutomaton()




time_period._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'interval_time'), time_interval, scope=time_period, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 85, 12)))

def _BuildAutomaton_ ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_
    del _BuildAutomaton_
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 85, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(time_period._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'interval_time')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 85, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
time_period._Automaton = _BuildAutomaton_()




time_interval._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'week_day'), week_day, scope=time_interval, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 92, 12)))

time_interval._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'time_hours'), time_hour, scope=time_interval, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 95, 12)))

def _BuildAutomaton_2 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_2
    del _BuildAutomaton_2
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 92, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 95, 12))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(time_interval._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'week_day')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 92, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(time_interval._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'time_hours')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 95, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
time_interval._Automaton = _BuildAutomaton_2()




traffic_target._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'target_name'), pyxb.binding.datatypes.string, scope=traffic_target, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 101, 12)))

def _BuildAutomaton_3 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_3
    del _BuildAutomaton_3
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(traffic_target._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'target_name')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 101, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
traffic_target._Automaton = _BuildAutomaton_3()




specific_URL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'URL'), pyxb.binding.datatypes.string, scope=specific_URL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 109, 12)))

def _BuildAutomaton_4 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_4
    del _BuildAutomaton_4
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(specific_URL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'URL')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 109, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
specific_URL._Automaton = _BuildAutomaton_4()




type_Content._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'content_name'), pyxb.binding.datatypes.string, scope=type_Content, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 115, 12)))

def _BuildAutomaton_5 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_5
    del _BuildAutomaton_5
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(type_Content._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'content_name')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 115, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
type_Content._Automaton = _BuildAutomaton_5()




resource._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'resource_property'), Property, scope=resource, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 123, 12)))

def _BuildAutomaton_6 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_6
    del _BuildAutomaton_6
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(resource._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'resource_property')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 123, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
resource._Automaton = _BuildAutomaton_6()




Property._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'key'), pyxb.binding.datatypes.string, scope=Property, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 130, 10)))

Property._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'value'), pyxb.binding.datatypes.string, scope=Property, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 131, 10)))

def _BuildAutomaton_7 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_7
    del _BuildAutomaton_7
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 131, 10))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(Property._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'key')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 130, 10))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(Property._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'value')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 131, 10))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
Property._Automaton = _BuildAutomaton_7()




fields._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'time_period'), time_period, scope=fields, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 138, 12)))

fields._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'traffic_target'), traffic_target, scope=fields, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 141, 12)))

fields._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'specific_URL'), specific_URL, scope=fields, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 144, 12)))

fields._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'type_content'), type_Content, scope=fields, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 147, 12)))

fields._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'purpose'), purpose, scope=fields, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 150, 12)))

fields._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'resource'), resource, scope=fields, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 153, 12)))

def _BuildAutomaton_8 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_8
    del _BuildAutomaton_8
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 138, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 141, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 144, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 147, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 150, 12))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 153, 12))
    counters.add(cc_5)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(fields._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'time_period')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 138, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(fields._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'traffic_target')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 141, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(fields._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'specific_URL')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 144, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(fields._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'type_content')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 147, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(fields._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'purpose')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 150, 12))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(fields._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'resource')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 153, 12))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    st_5._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
fields._Automaton = _BuildAutomaton_8()




HSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'action'), action, scope=HSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 165, 12)))

HSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'objectH'), objectH, scope=HSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 168, 12)))

HSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'fields'), fields, scope=HSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 171, 12)))

HSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'capabilities'), capability_list, scope=HSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 174, 12)))

HSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'candidates'), Candidates, scope=HSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 177, 12)))

HSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'suitableImplementation'), suitableImplementation_list, scope=HSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 180, 12)))

HSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'non-enforzable-Capability'), capability_list, scope=HSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 183, 12)))

HSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'implementation'), PSA_list, scope=HSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 186, 12)))

HSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'dependencies'), Dependencies, scope=HSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 188, 12)))

HSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'priority'), pyxb.binding.datatypes.integer, scope=HSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 189, 12)))

def _BuildAutomaton_9 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_9
    del _BuildAutomaton_9
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 171, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 174, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 177, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 180, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 183, 12))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 186, 12))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 188, 12))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 189, 12))
    counters.add(cc_7)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(HSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'action')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 165, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(HSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'objectH')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 168, 12))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(HSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'fields')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 171, 12))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(HSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'capabilities')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 174, 12))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(HSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'candidates')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 177, 12))
    st_4 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(HSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'suitableImplementation')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 180, 12))
    st_5 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(HSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'non-enforzable-Capability')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 183, 12))
    st_6 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(HSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'implementation')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 186, 12))
    st_7 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(HSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dependencies')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 188, 12))
    st_8 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_8)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(HSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'priority')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 189, 12))
    st_9 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_9)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    transitions.append(fac.Transition(st_4, [
         ]))
    transitions.append(fac.Transition(st_5, [
         ]))
    transitions.append(fac.Transition(st_6, [
         ]))
    transitions.append(fac.Transition(st_7, [
         ]))
    transitions.append(fac.Transition(st_8, [
         ]))
    transitions.append(fac.Transition(st_9, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_7._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_8, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_8._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_9, [
        fac.UpdateInstruction(cc_7, True) ]))
    st_9._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
HSPL._Automaton = _BuildAutomaton_9()




Dependencies._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'dependency'), Dependency, scope=Dependencies, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 201, 16)))

def _BuildAutomaton_10 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_10
    del _BuildAutomaton_10
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(Dependencies._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dependency')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 201, 16))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
Dependencies._Automaton = _BuildAutomaton_10()




PSA._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'PSA_info'), PSA_info, scope=PSA, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 257, 12)))

PSA._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'PSA_characteristic'), PSA_characteristic, scope=PSA, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 260, 12)))

PSA._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'capability'), capability_list, scope=PSA, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 263, 12)))

PSA._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'MSPL_list'), MSPL_list, scope=PSA, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 264, 12)))

def _BuildAutomaton_11 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_11
    del _BuildAutomaton_11
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 257, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 264, 12))
    counters.add(cc_1)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(PSA._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'PSA_info')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 257, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(PSA._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'PSA_characteristic')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 260, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(PSA._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'capability')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 263, 12))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(PSA._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'MSPL_list')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 264, 12))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
PSA._Automaton = _BuildAutomaton_11()




HSPL_list._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'hspl'), HSPL, scope=HSPL_list, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 281, 12)))

HSPL_list._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'dependencies'), Dependencies, scope=HSPL_list, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 283, 12)))

def _BuildAutomaton_12 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_12
    del _BuildAutomaton_12
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 283, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(HSPL_list._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'hspl')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 281, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(HSPL_list._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dependencies')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 283, 12))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
HSPL_list._Automaton = _BuildAutomaton_12()




capability_list._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'capability_list'), capability, scope=capability_list, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 299, 12)))

def _BuildAutomaton_13 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_13
    del _BuildAutomaton_13
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 299, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(capability_list._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'capability_list')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 299, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
capability_list._Automaton = _BuildAutomaton_13()




PSA_list._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'psa'), PSA, scope=PSA_list, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 413, 12)))

def _BuildAutomaton_14 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_14
    del _BuildAutomaton_14
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 413, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(PSA_list._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'psa')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 413, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
PSA_list._Automaton = _BuildAutomaton_14()




MappingType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'hspl_list'), HSPL_list, scope=MappingType, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 421, 12)))

MappingType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'psa_list'), PSA_list, scope=MappingType, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 424, 12)))

MappingType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'solution'), solutionList, scope=MappingType, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 427, 12)))

MappingType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'service_graph'), ServiceGraph, scope=MappingType, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 430, 12)))

MappingType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'mspl_list'), MSPL_list, scope=MappingType, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 433, 12)))

MappingType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'remediation'), RemediationList, scope=MappingType, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 436, 12)))

MappingType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'user_psa_list'), PSA_list, scope=MappingType, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 439, 12)))

MappingType._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'additional_psa_list'), PSA_list, scope=MappingType, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 442, 12)))

def _BuildAutomaton_15 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_15
    del _BuildAutomaton_15
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 421, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 424, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 427, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 430, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 433, 12))
    counters.add(cc_4)
    cc_5 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 436, 12))
    counters.add(cc_5)
    cc_6 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 439, 12))
    counters.add(cc_6)
    cc_7 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 442, 12))
    counters.add(cc_7)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(MappingType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'hspl_list')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 421, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(MappingType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'psa_list')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 424, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(MappingType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'solution')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 427, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(MappingType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'service_graph')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 430, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(MappingType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'mspl_list')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 433, 12))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_5, False))
    symbol = pyxb.binding.content.ElementUse(MappingType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'remediation')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 436, 12))
    st_5 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_5)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_6, False))
    symbol = pyxb.binding.content.ElementUse(MappingType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'user_psa_list')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 439, 12))
    st_6 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_6)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_7, False))
    symbol = pyxb.binding.content.ElementUse(MappingType._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'additional_psa_list')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 442, 12))
    st_7 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_7)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_3, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_4, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_4, False) ]))
    st_4._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_5, [
        fac.UpdateInstruction(cc_5, True) ]))
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_5, False) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_5, False) ]))
    st_5._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_6, [
        fac.UpdateInstruction(cc_6, True) ]))
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_6, False) ]))
    st_6._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_7, [
        fac.UpdateInstruction(cc_7, True) ]))
    st_7._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
MappingType._Automaton = _BuildAutomaton_15()




Candidates._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'suitable_PSA_list'), suitablePSA, scope=Candidates, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 452, 12)))

def _BuildAutomaton_16 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_16
    del _BuildAutomaton_16
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 452, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(Candidates._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'suitable_PSA_list')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 452, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
Candidates._Automaton = _BuildAutomaton_16()




suitablePSA._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'capability'), capability, scope=suitablePSA, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 458, 12)))

suitablePSA._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'psa_list'), PSA_list, scope=suitablePSA, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 461, 12)))

def _BuildAutomaton_17 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_17
    del _BuildAutomaton_17
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(suitablePSA._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'capability')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 458, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(suitablePSA._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'psa_list')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 461, 12))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
suitablePSA._Automaton = _BuildAutomaton_17()




suitableImplementation_list._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'subitableImplementation'), PSA_list, scope=suitableImplementation_list, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 467, 12)))

def _BuildAutomaton_18 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_18
    del _BuildAutomaton_18
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 467, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(suitableImplementation_list._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'subitableImplementation')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 467, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
suitableImplementation_list._Automaton = _BuildAutomaton_18()




solution._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'psaList'), PSA_list, scope=solution, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 476, 12)))

def _BuildAutomaton_19 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_19
    del _BuildAutomaton_19
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(solution._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'psaList')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 476, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
solution._Automaton = _BuildAutomaton_19()




solutionList._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'solutions'), solution, scope=solutionList, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 485, 12)))

def _BuildAutomaton_20 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_20
    del _BuildAutomaton_20
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 485, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(solutionList._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'solutions')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 485, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
solutionList._Automaton = _BuildAutomaton_20()




ServiceGraph._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'service'), Service, scope=ServiceGraph, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 491, 12)))

ServiceGraph._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'rootService'), pyxb.binding.datatypes.IDREF, scope=ServiceGraph, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 494, 12)))

ServiceGraph._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'endService'), pyxb.binding.datatypes.IDREF, scope=ServiceGraph, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 497, 12)))

ServiceGraph._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'edge'), edge, scope=ServiceGraph, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 500, 12)))

def _BuildAutomaton_21 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_21
    del _BuildAutomaton_21
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 491, 12))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ServiceGraph._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'service')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 491, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ServiceGraph._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'rootService')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 494, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(ServiceGraph._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'endService')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 497, 12))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(ServiceGraph._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'edge')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 500, 12))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
         ]))
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
ServiceGraph._Automaton = _BuildAutomaton_21()




Service._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'PSA'), PSA, scope=Service, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 506, 12)))

Service._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'capability'), capability, scope=Service, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 509, 12)))

def _BuildAutomaton_22 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_22
    del _BuildAutomaton_22
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 506, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 509, 12))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(Service._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'PSA')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 506, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(Service._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'capability')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 509, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
Service._Automaton = _BuildAutomaton_22()




edge._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'src_Service'), pyxb.binding.datatypes.IDREF, scope=edge, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 522, 12)))

edge._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'dst_Service'), pyxb.binding.datatypes.IDREF, scope=edge, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 523, 12)))

edge._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'networkFields'), networkFields, scope=edge, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 524, 12)))

def _BuildAutomaton_23 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_23
    del _BuildAutomaton_23
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 524, 12))
    counters.add(cc_0)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(edge._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'src_Service')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 522, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(edge._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dst_Service')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 523, 12))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(edge._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'networkFields')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 524, 12))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
         ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
edge._Automaton = _BuildAutomaton_23()




MSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'capabilities'), capability_list, scope=MSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 551, 12)))

MSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'candidates'), Candidates, scope=MSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 554, 12)))

MSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'suitableImplementation'), suitableImplementation_list, scope=MSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 557, 12)))

MSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'non-enforzable-Capability'), capability_list, scope=MSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 560, 12)))

MSPL._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'implementation'), PSA_list, scope=MSPL, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 563, 12)))

def _BuildAutomaton_24 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_24
    del _BuildAutomaton_24
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 551, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 554, 12))
    counters.add(cc_1)
    cc_2 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 557, 12))
    counters.add(cc_2)
    cc_3 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 560, 12))
    counters.add(cc_3)
    cc_4 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 563, 12))
    counters.add(cc_4)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(MSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'capabilities')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 551, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(MSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'candidates')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 554, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_2, False))
    symbol = pyxb.binding.content.ElementUse(MSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'suitableImplementation')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 557, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_3, False))
    symbol = pyxb.binding.content.ElementUse(MSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'non-enforzable-Capability')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 560, 12))
    st_3 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_4, False))
    symbol = pyxb.binding.content.ElementUse(MSPL._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'implementation')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 563, 12))
    st_4 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_4)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_2, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_2, False) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_2, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_3, True) ]))
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_3, False) ]))
    st_3._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_4, [
        fac.UpdateInstruction(cc_4, True) ]))
    st_4._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
MSPL._Automaton = _BuildAutomaton_24()




MSPL_list._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'mspl_list'), MSPL, scope=MSPL_list, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 574, 12)))

def _BuildAutomaton_25 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_25
    del _BuildAutomaton_25
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(MSPL_list._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'mspl_list')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 574, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
MSPL_list._Automaton = _BuildAutomaton_25()




RemediationList._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'solution_repository'), Remediationt, scope=RemediationList, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 580, 12)))

RemediationList._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'solution_market'), Remediationt, scope=RemediationList, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 581, 12)))

def _BuildAutomaton_26 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_26
    del _BuildAutomaton_26
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 580, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=None, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 581, 12))
    counters.add(cc_1)
    states = []
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(RemediationList._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'solution_repository')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 580, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_1, False))
    symbol = pyxb.binding.content.ElementUse(RemediationList._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'solution_market')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 581, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, True, containing_state=None)
RemediationList._Automaton = _BuildAutomaton_26()




Remediationt._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'hspl'), HSPL, scope=Remediationt, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 588, 12)))

Remediationt._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'mspl'), MSPL, scope=Remediationt, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 591, 12)))

Remediationt._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'suitablePSA'), suitablePSA, scope=Remediationt, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 594, 12)))

def _BuildAutomaton_27 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_27
    del _BuildAutomaton_27
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 588, 12))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 591, 12))
    counters.add(cc_1)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(Remediationt._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'hspl')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 588, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(Remediationt._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'mspl')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 591, 12))
    st_1 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(Remediationt._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'suitablePSA')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 594, 12))
    st_2 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    transitions = []
    transitions.append(fac.Transition(st_0, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    st_2._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
Remediationt._Automaton = _BuildAutomaton_27()




PolicyDependency._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'dependencyCondition'), PolicyDependencyCondition, scope=PolicyDependency, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 211, 10)))

def _BuildAutomaton_28 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_28
    del _BuildAutomaton_28
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(PolicyDependency._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dependencyCondition')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 211, 10))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    transitions = []
    st_0._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
PolicyDependency._Automaton = _BuildAutomaton_28()




PolicyDependencyCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'policyID'), pyxb.binding.datatypes.string, scope=PolicyDependencyCondition, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 223, 14)))

PolicyDependencyCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'status'), pyxb.binding.datatypes.string, scope=PolicyDependencyCondition, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 224, 8)))

def _BuildAutomaton_29 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_29
    del _BuildAutomaton_29
    import pyxb.utils.fac as fac

    counters = set()
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(PolicyDependencyCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'policyID')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 223, 14))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(PolicyDependencyCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'status')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 224, 8))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
PolicyDependencyCondition._Automaton = _BuildAutomaton_29()




EventDependency._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'eventID'), pyxb.binding.datatypes.string, scope=EventDependency, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 234, 12)))

EventDependency._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'dependencyCondition'), DependencyCondition, scope=EventDependency, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 235, 14)))

def _BuildAutomaton_30 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_30
    del _BuildAutomaton_30
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 235, 14))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(EventDependency._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'eventID')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 234, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(EventDependency._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dependencyCondition')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 235, 14))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
EventDependency._Automaton = _BuildAutomaton_30()




EventDependencyCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'subject'), pyxb.binding.datatypes.string, scope=EventDependencyCondition, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 245, 14)))

EventDependencyCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'action'), action, scope=EventDependencyCondition, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 246, 14)))

EventDependencyCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'objectH'), objectH, scope=EventDependencyCondition, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 247, 14)))

EventDependencyCondition._AddElement(pyxb.binding.basis.element(pyxb.namespace.ExpandedName(Namespace, 'fields'), fields, scope=EventDependencyCondition, location=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 248, 14)))

def _BuildAutomaton_31 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_31
    del _BuildAutomaton_31
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 246, 14))
    counters.add(cc_0)
    cc_1 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 247, 14))
    counters.add(cc_1)
    states = []
    final_update = None
    symbol = pyxb.binding.content.ElementUse(EventDependencyCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'subject')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 245, 14))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(EventDependencyCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'action')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 246, 14))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    final_update = None
    symbol = pyxb.binding.content.ElementUse(EventDependencyCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'objectH')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 247, 14))
    st_2 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_2)
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(EventDependencyCondition._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'fields')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 248, 14))
    st_3 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_3)
    transitions = []
    transitions.append(fac.Transition(st_1, [
         ]))
    transitions.append(fac.Transition(st_2, [
         ]))
    transitions.append(fac.Transition(st_3, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_0, False) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_0, False) ]))
    st_1._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_2, [
        fac.UpdateInstruction(cc_1, True) ]))
    transitions.append(fac.Transition(st_3, [
        fac.UpdateInstruction(cc_1, False) ]))
    st_2._set_transitionSet(transitions)
    transitions = []
    st_3._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
EventDependencyCondition._Automaton = _BuildAutomaton_31()




def _BuildAutomaton_32 ():
    # Remove this helper function from the namespace after it is invoked
    global _BuildAutomaton_32
    del _BuildAutomaton_32
    import pyxb.utils.fac as fac

    counters = set()
    cc_0 = fac.CounterCondition(min=0, max=1, metadata=pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 283, 12))
    counters.add(cc_0)
    states = []
    final_update = set()
    symbol = pyxb.binding.content.ElementUse(HSPL_Orchestration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'hspl')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 281, 12))
    st_0 = fac.State(symbol, is_initial=True, final_update=final_update, is_unordered_catenation=False)
    states.append(st_0)
    final_update = set()
    final_update.add(fac.UpdateInstruction(cc_0, False))
    symbol = pyxb.binding.content.ElementUse(HSPL_Orchestration._UseForTag(pyxb.namespace.ExpandedName(Namespace, 'dependencies')), pyxb.utils.utility.Location('/app/hspl/hspl.xsd', 283, 12))
    st_1 = fac.State(symbol, is_initial=False, final_update=final_update, is_unordered_catenation=False)
    states.append(st_1)
    transitions = []
    transitions.append(fac.Transition(st_0, [
         ]))
    transitions.append(fac.Transition(st_1, [
         ]))
    st_0._set_transitionSet(transitions)
    transitions = []
    transitions.append(fac.Transition(st_1, [
        fac.UpdateInstruction(cc_0, True) ]))
    st_1._set_transitionSet(transitions)
    return fac.Automaton(states, counters, False, containing_state=None)
HSPL_Orchestration._Automaton = _BuildAutomaton_32()

