# # -*- coding: utf-8 -*-
# statics.py
"""
This python module contains the required static values for the policy interpreter
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

# Imports

import sys,os
# Insert the parent path into the sys.path in order to import the mspl
parentPath = os.path.abspath("..")
if parentPath not in sys.path:
	sys.path.insert(0, parentPath)
# Logging 
from logger.handlers import KafkaHandler
import logging
logging.basicConfig(level=logging.INFO)

# remote logging
remote_logger = logging.getLogger("remote_logger")
remote_logger.setLevel(logging.INFO)
remote_logger.propagate = False

# Set remote logging handler
#REMOTE_LOGGING_URL = ['172.17.0.1:9092']
REMOTE_LOGGING_URL = ["{}:{}".format(os.environ["RL_ADDR"],os.environ["RL_PORT"])]
TOPIC = 'anastacia-log'
try:
	kh = KafkaHandler(REMOTE_LOGGING_URL, TOPIC)
	remote_logger.addHandler(kh)
except Exception as e:
	logging.info(e)
"""
msg = {
 "timestamp": 1533311972,
 "from_module": "REACTION",
 "from_component": "SAS",
 "to_module": "SEAL_MANAGEMENT",
 "to_component": "DSPS",
 "incoming": False,
 "method": "RabbitMQ",
 "data": {"foo": 1, "bar": 2},
 "notes": "Sample data"
}
remote_logger.info(msg)
"""

# Static values
#SECURITY_ORCHESTRATOR_URL = "http://{}:{}/meservice".format(os.environ["SO_ADDR"],os.environ["SO_PORT"])
SECURITY_ORCHESTRATOR_URL = "http://{}:{}/enforce/".format(os.environ["SO_ADDR"],os.environ["SO_PORT"])
CONFLICT_DETECTOR_URL = "http://{}:{}/mcdtservice".format(os.environ["PI_CDT_ADDR"],os.environ["PI_CDT_PORT"])
SEP_GET_PLUGIN = "/get_plugin?name="
PLUGIN_FOLDER = "m2l_plugins"
PLUGIN_PREFIX = "mspl_"
POLICY_REPOSITORY_SERVICE_URL = "http://{}:{}/api/v1/".format(os.environ["PR_ADDR"],os.environ["PR_PORT"])
POLICY_ENFORCEMENT = "policy-enforcements/"
SET_TRANSLATION_URL = "set-policy-translation/"
SET_REFINEMENT_URL = "set-policy-refinement/"
#SECURITY_ENABLER_PROVIDER_URL = "http://195.146.125.100:8001"
LOCAL_SECURITY_ENABLER_PROVIDER_URL = "http://{}:{}".format(os.environ["SEP_ADDR"],os.environ["SEP_PORT"])
SECURITY_ENABLER_PROVIDER_URL = LOCAL_SECURITY_ENABLER_PROVIDER_URL
SEP_TIMEOUT = 1


#H2M Refinement

							# Action             # objectH        # refine method
CAPABILITY_MATCHING = {"no_authorise_access":{"internet_traffic":["Filtering_L4"],
												"alltraffic":["Filtering_L4"],
												"dtls_traffic":["Filtering_L4"],
												"pana_traffic":["Filtering_L4"],
												"resource":["AuthoriseAccess_resurce"]},
						"require_authentication": {"pana_agent":["Authentication"]},
						"authorise_access": {"pana_traffic":["Traffic_Divert"],
											"dtls_traffic":["Traffic_Divert"],
											"coap_traffic":["Traffic_Divert"],
											"alltraffic": ["Traffic_Divert"],
											"resource":["AuthoriseAccess_resurce"]},
						"enable": {"resource":["IoT_control"]},
						"prot_conf_integr": {"alltraffic":["Protection_confidentiality"]},
						"config_authentication": {"pana":["Authentication"]}
						}

ACTION_MATCHING = {"no_authorise_access":"DENY"}

# TODO: Get these values from the system model service
SYS_MODEL = {"sensor-1":{"address":"2001:720:1710:4::5001/128","prot_conf_type":"DTLS_protocol"},
			"sensor-2":{"address":"2001:720:1710:4::5002/128","prot_conf_type":"DTLS_protocol"},
			"alltraffic":{"address":"::/0"},
			"udp_traffic":{"protocol":"UDP"},
			"peana": {"interface":"of:000b1c98ec7f9b00/7"},
			"peana-1": {"interface":"of:000b1c98ec7f9b00/3"},
			"peana-2": {"interface":"of:000b1c98ec7f9b00/7"},
			"peana-0": {"interface":"of:000b1c98ec7f9b00/7","prot_conf_type":"DTLS_protocol","nodes":1},
			"peana-100": {"interface":"of:000b1c98ec7f9b00/7","prot_conf_type":"DTLS_protocol","nodes":100},
			"peana-200": {"interface":"of:000b1c98ec7f9b00/7","prot_conf_type":"DTLS_protocol","nodes":200},
			"peana-300": {"interface":"of:000b1c98ec7f9b00/7","prot_conf_type":"DTLS_protocol","nodes":300},
			"peana-400": {"interface":"of:000b1c98ec7f9b00/7","prot_conf_type":"DTLS_protocol","nodes":400},
			"peana-500": {"interface":"of:000b1c98ec7f9b00/7","prot_conf_type":"DTLS_protocol","nodes":500},
			"peana-600": {"interface":"of:000b1c98ec7f9b00/7","prot_conf_type":"DTLS_protocol","nodes":600},
			"peana-700": {"interface":"of:000b1c98ec7f9b00/7","prot_conf_type":"DTLS_protocol","nodes":700},
			"peana-800": {"interface":"of:000b1c98ec7f9b00/7","prot_conf_type":"DTLS_protocol","nodes":800},
			"peana-900": {"interface":"of:000b1c98ec7f9b00/7","prot_conf_type":"DTLS_protocol","nodes":900},
			"peana-1000": {"interface":"of:000b1c98ec7f9b00/7","prot_conf_type":"DTLS_protocol","nodes":1000},
			"iot_controller": {"address":"2001:720:1710:4:f816:3eff:fe16:27a3/128"}, 
			"iot_broker": {"address":"2001:720:1710:4:5054:ff:feed:fb46/128"},
			"pana_traffic":{"protocol":"UDP","port":"716"},
			"coap_traffic":{"protocol":"UDP","port":"5683"},
			"dtls_traffic":{"protocol":"UDP","port":"5684"},
			"pana_agent":{"address":"2001:720:1710:4:5054:ff:feed:fb46/128"},
			"power_off":{"action":"PowerMgmtAction","resource":"60000/0/5","port":"5683"},
			"temperature":{"action":"EnableAction","resource":"60000/0/4","port":"5683"},
			"humidity":{"action":"EnableAction","resource":"60000/0/3","port":"5683"}}


SM_USER = os.environ["SM_USER"]
SM_PASS = os.environ["SM_PASS"]
