#!/bin/bash

 USAGE="USAGE examples:
  --Enforcement example--
 ./test_anastacia_orchestration_mspl_enforcement.sh anastacia/proactive/proactive4.xml
 "

if [ $# -eq 0 ]
  then
    printf "$USAGE"
    exit 1
fi

curl -H "Cache-Control: no-cache" -H "Content-Type: application/xml" -X POST -d @$1 http://10.79.7.175:5000/enforce/
printf '\n'