#!/bin/bash

 USAGE="USAGE examples:
  --Id conflict--
 ./test_anastacia_orchestration_mspl_conflicts.sh cdtservice/id_conflict/id_conflict_input.xml
 --Same behavour conflict--
 ./test_anastacia_orchestration_mspl_conflicts.sh cdtservice/same_behaviour_conflict/same_behaviour_conflict_input.xml
  --Duties conflict--
 ./test_anastacia_orchestration_mspl_conflicts.sh cdtservice/duties_conflict/duties_conflict_input.xml
 --Managers Conflict--
 ./test_anastacia_orchestration_mspl_conflicts.sh cdtservice/managers_conflict/managers_conflict_input.xml
  --Override conflict--
 ./test_anastacia_orchestration_mspl_conflicts.sh cdtservice/override_conflict/override_conflict_input.xml
  --Priority dependency conflict--
 ./test_anastacia_orchestration_mspl_conflicts.sh cdtservice/priority_dependency_conflict/priority_dependency_conflict_input.xml
  --aaa dependencies example--
  ./test_anastacia_orchestration_mspl_conflicts.sh m2lservice/aaa/aaa.xml
 "

if [ $# -eq 0 ]
  then
    printf "$USAGE"
    exit 1
fi

curl -H "Cache-Control: no-cache" -H "Content-Type: application/xml" -X POST -d @$1 http://anastacia-framework:8010/mcdtservice
printf '\n'