#!/bin/bash

 USAGE="USAGE examples:
  --Authorization Orchestration example--
 ./test_anastacia_orchestration_mspl_translation.sh m2lservice/authZ/authZ.xml
  --Authentication Orchestration example--
 ./test_anastacia_orchestration_mspl_translation.sh m2lservice/authN/authN.xml
  --Channel Protection example--
 ./test_anastacia_orchestration_mspl_translation.sh m2lservice/DTLS/dtls.xml
 --Filtering/Forwarding example--
 ./test_anastacia_orchestration_mspl_translation.sh m2lservice/filtering/filtering_allow.xml
 ./test_anastacia_orchestration_mspl_translation.sh m2lservice/filtering/filtering_deny.xml
 ./test_anastacia_orchestration_mspl_translation.sh m2lservice/filtering/filtering_room_deny.xml
 --IoT Management example--
 ./test_anastacia_orchestration_mspl_translation.sh m2lservice/iot_management/power_off.xml
 --aaa example--
 ./test_anastacia_orchestration_mspl_translation.sh m2lservice/aaa/aaa.xml
  --data privacy example--
 ./test_anastacia_orchestration_mspl_translation.sh m2lservice/data_privacy/data_privacy.xml
  ****** Conflicts examples ******
  --Id conflict--
 ./test_anastacia_orchestration_mspl_translation.sh cdtservice/id_conflict/id_conflict_input.xml
 --Same behavour conflict--
 ./test_anastacia_orchestration_mspl_translation.sh cdtservice/same_behaviour_conflict/same_behaviour_conflict_input.xml
  --Duties conflict--
 ./test_anastacia_orchestration_mspl_translation.sh cdtservice/duties_conflict/duties_conflict_input.xml
 --Managers Conflict--
 ./test_anastacia_orchestration_mspl_translation.sh cdtservice/managers_conflict/managers_conflict_input.xml
  --Override conflict--
 ./test_anastacia_orchestration_mspl_translation.sh cdtservice/override_conflict/override_conflict_input.xml
  --Priority dependency conflict--
 ./test_anastacia_orchestration_mspl_translation.sh cdtservice/priority_dependency_conflict/priority_dependency_conflict_input.xml
 "

if [ $# -eq 0 ]
  then
    printf "$USAGE"
    exit 1
fi

curl -H "Cache-Control: no-cache" -H "Content-Type: application/xml" -X POST -d @$1 http://anastacia-framework:8000/m2lservice
printf '\n'