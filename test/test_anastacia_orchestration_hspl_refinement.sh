#!/bin/bash

 USAGE="USAGE examples:
  --Authentication Orchestration example--
 ./test_anastacia_orchestration_hspl_refinement.sh h2mservice/authN/authN.xml
   --Authorization Orchestration example--
 ./test_anastacia_orchestration_hspl_refinement.sh h2mservice/authZ/authZ.xml
   --Channel protection example--
 ./test_anastacia_orchestration_hspl_refinement.sh h2mservice/DTLS/dtls.xml
   --Filtering/Forwarding example--
 ./test_anastacia_orchestration_hspl_refinement.sh h2mservice/filtering/filtering_deny.xml
 ./test_anastacia_orchestration_hspl_refinement.sh h2mservice/filtering/filtering_allow.xml
   --IoT Management example--
 ./test_anastacia_orchestration_hspl_refinement.sh h2mservice/iot_management/power_off.xml
 "

if [ $# -eq 0 ]
  then
    printf "$USAGE"
    exit 1
fi

curl -H "Cache-Control: no-cache" -H "Content-Type: application/xml" -X POST -d @$1 http://anastacia-framework:8009/h2mservice