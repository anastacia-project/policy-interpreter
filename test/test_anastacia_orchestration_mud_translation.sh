#!/bin/bash

 USAGE="USAGE examples:
  --Basic example--
 ./test_anastacia_orchestration_mud_translation.sh mud2mservice/iot-controller-coap.json
 ./test_anastacia_orchestration_mud_translation.sh mud2mservice/same-manufacturer-coap.json
 "

if [ $# -eq 0 ]
  then
    printf "$USAGE"
    exit 1
fi

curl -v -H "Cache-Control: no-cache" -H "Content-Type: application/json" -X POST -d @$1 http://anastacia-framework:8000/mud2mservice
printf '\n'