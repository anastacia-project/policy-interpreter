# Use an official Python runtime as a parent image
FROM python:3

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install any needed packages specified in requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt
# Install the rule engine
RUN apt update
RUN apt install unzip -y
RUN mkdir libs && cd libs && wget https://netcologne.dl.sourceforge.net/project/pyke/pyke/1.1.1/pyke3-1.1.1.zip && \
unzip pyke3-1.1.1.zip && \
cd pyke-1.1.1 && python setup.py build && python setup.py install

# Make port 80 available to the world outside this container
EXPOSE 8000


# Run app.py when the container launches
CMD ["gunicorn", "-b", "0.0.0.0:8000", "--workers", "2", "policy_interpreter:app", "--timeout", "3600"] 