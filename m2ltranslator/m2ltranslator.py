# # -*- coding: utf-8 -*-
"""
This python module implements a MSPL->Low-level translator inside the ANASTACIA European Project,
using/extending the HSPL/MSPL languages defined in SECURED project.
How to use:
	python3 m2ltranslator.py [MSPL_FILE.xml]

"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"


import json
import requests
import uuid
from settings import logging, LOCAL_SECURITY_ENABLER_PROVIDER_URL, SEP_GET_PLUGIN, SEP_TIMEOUT, PLUGIN_FOLDER, PLUGIN_PREFIX
logger = logging.getLogger(__name__)
# mspl models
import mspl

"""
	Output example:
	{"omspl_translation": {"mspl_id":ID, "mspl": omspl_text,"mspl_translations":mspl_translations}}
"""

class PolicyIDGenerator():
	@staticmethod
	def generate_ID(policy_object,prefix):
		if not policy_object.id:
			policy_object.id = "{}_{}".format(prefix,uuid.uuid4().hex)

class PolicyTextGenerator():
	@staticmethod
	def to_text(policy_object,element_name):
		policy_text = policy_object.toxml(element_name=element_name)
		#mspl_text = mspl_object.toDOM(element_name=element_name).toprettyxml(encoding="utf-8").decode("utf-8")
		# pyxb introduces by default ns1 namespace in text generation and it is not parser correctly later
		policy_text = policy_text.replace(":ns1","").replace("ns1:","").replace('xsi:type="ITResourceType"',"")
		return policy_text


class ITResourceOrchestrationTypeCustom(mspl.ITResourceOrchestrationType):
		
	def generate_ID(self):
		"""Generates an ID if it is not already provided."""
		PolicyIDGenerator.generate_ID(self,"omspl")

	def to_text(self):
		return PolicyTextGenerator.to_text(self, "ITResourceOrchestration")
		#return MSPLTextGenerator.to_text(self, "ITResourceOrchestration")	
		#logger.debug(dir(self))


	def load(self):
		"""Load the MSPLs plaint text into MSPL objects"""
		self.generate_ID()
		#mspl_objects = []
		for mspl_object in self.ITResource:
			mspl_object.generate_ID()
			mspl_object.link_to_orchestration(self.id)
			mspl_object.mspl_text = mspl_object.to_text()
		self.mspl_text = self.to_text()
			# perform the translation
			#mspl_objects.append(mspl_object)
		#logger.debug(self.toDOM(element_name="ITResourceOrchestration").toprettyxml(encoding="utf-8").decode("utf-8"))

	def translate(self):
		#logger.debug("IN ITResourceOrchestration TRANSLATE METHOD")
		#logger.debug(self.toDOM(element_name="ITResourceOrchestration").toprettyxml(encoding="utf-8").decode("utf-8"))
		omspl_text = self.to_text()
		mspl_translations = []
		for mspl_object in self.ITResource:
			# perform the translation
			enabler_name = mspl_object.enablerCandidates.enabler
			#mspl_text = mspl_object.toDOM(element_name="ITResourceOrchestration").toprettyxml(encoding="utf-8").decode("utf-8").replace(":ns1","").replace("ns1:","")
			mspl_translations.append(mspl_object.translate())
		return {"omspl_translation": {"mspl_id":self.id, "mspl": omspl_text,"mspl_translations":mspl_translations}}

class ITResourceTypeCustom(mspl.ITResourceType):

	def generate_ID(self):
		"""Generates an ID if it is not already provided."""
		PolicyIDGenerator.generate_ID(self,"mspl")

	def to_text(self):
		#logger.debug(type(self))
		return PolicyTextGenerator.to_text(self, "ITResource")

	def link_to_orchestration(self, orchestration_id):
		"""Link the MSPL with an orchestration MSPL if required"""
		if not self.orchestrationID:
			self.orchestrationID = orchestration_id

	def load(self):
		"""Load the MSPL plaint text into MSPL object"""
		self.generate_ID()
		self.mspl_text = self.to_text()

	def translate(self):
		# configurations{"enabler_name":[{"mspl_id":ID,"enabler_conf":CONF}]}
		# Get enabler name from mspl xml we always select the first one
		enabler_name = self.enablerCandidates.enabler[0]
		#logger.debug("Selected Enabler: {}".format(enabler_name))
		# Get the plugin from the Security Enabler Provider
		#try:
			##logger.debug("Using REMOTE Security Enabler Provider")
			#response = requests.get("{}{}{}".format(SECURITY_ENABLER_PROVIDER_URL,self.SEP_GET_PLUGIN,security_enabler),timeout=SEP_TIMEOUT)
			#logger.debug("Error conecting to REMOTE SEP, using LOCAL Security Enabler Provider")
		#	response = requests.get("{}{}{}".format(LOCAL_SECURITY_ENABLER_PROVIDER_URL, SEP_GET_PLUGIN,enabler_name),timeout=SEP_TIMEOUT)
		#except:
			#logger.debug("Error conecting to REMOTE SEP, using LOCAL Security Enabler Provider")
		response = requests.get("{}{}{}".format(LOCAL_SECURITY_ENABLER_PROVIDER_URL, SEP_GET_PLUGIN,enabler_name),timeout=SEP_TIMEOUT)

		#logger.debug("Security Enabler provider response {}".format(response))
		# Decode the JSON response
		logger.debug(response.text)
		json_response = json.loads(response.text)
		# The JSON response must be at this spec: {'plugin':'PLUGIN_CODE','plugin_file_name':PLUGIN_FILE_NAME}
		plugin_code = json_response["plugin"]
		plugin_file_name = json_response["plugin_file_name"]
		
		# Write the plugin code in the specified plugin folder with the received plugin file name
		plugin_path = "{}/{}".format(PLUGIN_FOLDER,plugin_file_name)
		plugin_file = open(plugin_path,"w")
		plugin_file.write(json_response["plugin"])
		plugin_file.close()
		
		#logger.debug("Loading dinamically the module {}".format(plugin_path))
		# Load dinamically the plugin

		plugin_module = __import__("{}.{}{}".format(PLUGIN_FOLDER,PLUGIN_PREFIX,enabler_name), globals(), locals(), ['M2LPlugin'], 0)

		# Executes the get configuration method on the dinamyc loaded plugin
		
		m2lplugin = plugin_module.M2LPlugin()
		# BE CAREFUL! THIS CALL MODIFIES THE IMPORTS, LATER self.to_text() will not work properly
		# This is the reason why we store the mspl text in load methods.
		#logger.debug("mspl: {}".format(self.mspl_text))
		enabler_conf = m2lplugin.get_configuration(self.mspl_text)
		
		#logger.debug("enabler_conf: {}".format(enabler_conf))

		return {"mspl_id":self.id,"mspl":self.mspl_text,"enabler":enabler_name,"enabler_conf":enabler_conf}
		#return {"mspl_id":self.ID,"mspl":mspl_text,"enabler":enabler_name,"enabler_conf":""}


class M2LTranslator:
	'Class in charge to translate MSPL to low-level configurations'

	def translate(self, mspl_data):
		"""Translate the mspl policies in the mspl list"""
		if not isinstance(mspl_data, list):
			return mspl_data.translate()
		
		mspl_translations = []
		for mspl_object in mspl_data:
			mspl_translations.append(mspl_object.translate())
		return {"mspl_translations":mspl_translations}
		
	
	#@staticmethod
	def load(self, mspl_source):
		'Return the MSPL python object from the  JSON or XML MSPL source'
		mspl.ITResourceOrchestrationType._SetSupersedingClass(ITResourceOrchestrationTypeCustom)
		mspl.ITResourceType._SetSupersedingClass(ITResourceTypeCustom)
		mspl_object = mspl.CreateFromDocument(mspl_source)
		#logger.debug("MANUAL")
		#logger.debug(mspl_object.toDOM(element_name="ITResourceOrchestration").toprettyxml(encoding="utf-8").decode("utf-8"))
		#logger.debug("USING TO_TEXT")
		#mspl_object.to_text()
		mspl_object.load()
		return mspl_object

"""
if __name__ == "__main__":
	xml_file = sys.argv[1] 
	m2ltranslator = M2LTranslator()
	#logger.debug("Reading mspl file...")
	xml_source = open(xml_file).read()
	mspl_policy = m2ltranslator.load_mspl(xml_source)
	# Pretty print
	separator = "*"
	separator_group = separator*5
	title = "MSPL POLICY"
	print("\n{}{}{}".format(separator_group,title,separator_group))
	print(mspl_policy)
	print("{}{}{}\n".format(separator_group,separator*len(title),separator_group))
"""	