# # -*- coding: utf-8 -*-
# policy_interpreter.py
"""
This python module implements the HSPL/MSPL policy interpreter inside the ANASTACIA European Project,
extending the HSPL/MSPL language defined in secured project.
How to use:
	Run the server: gunicorn policy_interpreter:app
	Request for HSPL/MSPL translation: curl localhost:8000/h2mservice -d @[hspl_policy.json]
	Request for /Lower translation: curl localhost:8000/m2lservice -d @[mspl_policy.json]
"""
__author__ = "Alejandro Molina Zarca"
__copyright__ = "Copyright 2018, ANASTACIA H2020"
__credits__ = ["Antonio Skarmeta", "Jorge Bernal Bernabé", "Alejandro Molina Zarca"]
__license__ = "GPL"
__version__ = "0.0.1"
__maintainer__ = "Alejandro Molina Zarca"
__email__ = "alejandro.mzarca@um.es"
__status__ = "Development"

# Let's get this party started!
import falcon
import json
import requests
from datetime import datetime
from h2mrefiner import H2MRefiner
from m2ltranslator import M2LTranslator
from mud2mtranslator import MUD2MTranslator
#from utils import timing
from settings import logging, remote_logger, POLICY_REPOSITORY_SERVICE_URL,CONFLICT_DETECTOR_URL, SET_TRANSLATION_URL, SECURITY_ORCHESTRATOR_URL
logger = logging.getLogger(__name__)
import mspl

class M2EService(object):

    #SECURITY_ORCHESTRATOR_URL = "http://orchestrator:8002/meservice"
    #SECURITY_ORCHESTRATOR_URL = "http://orchestrator:8002/meservice"
    POLICY_REPOSITORY_STATUS_URL = "{}set-policy-enforcement/".format(POLICY_REPOSITORY_SERVICE_URL)

    """ High to enforcement service"""
    def on_get(self, req, resp):
        """ GET Method is not allowed """
        raise falcon.HTTPMethodNotAllowed('GET method not supported',
                                        'Please, uses POST in order to provide the HSPL.')

    def on_post(self, req, resp):
        """Handles POST requests"""
        body = req.stream.read().decode("utf-8")
        if not body:
            raise falcon.HTTPBadRequest('Empty request body',
                                        'A valid JSON document is required.')

        logger.debug("m2eservice")
        logger.debug(body)

        #json_mspl_policies = json.loads(body)  
        #if isinstance(json_mspl_policies, str):
        #    json_mspl_policies = json.loads(json_mspl_policies) 

        """
        TODO: PROBABLY AT THIS POINT IS REQUIRED LOAD THE MSPL
        mspl_status = []
        status = "P"
        for mspl_policy_enablers in json_mspl_policies["mspl_list"]:
            mspl_policy = mspl_policy_enablers["mspl"]
            try:
                mspl_id = mspl_policy.split("ID='")[1].split("'")[0]
            except IndexError:
                mspl_id = mspl_policy.split('ID="')[1].split('"')[0]

            mspl_status.append({"mspl_id":mspl_id,"status":status})
        requests.post("{}".format(self.POLICY_REPOSITORY_STATUS_URL),json={"mspl_status":mspl_status})
        """

        # Request the enforcement to orchestrator
        #response = requests.post("{}".format(self.SECURITY_ORCHESTRATOR_URL),json=json_mspl_policies)

        logger.info("logging the message:")

        logging_message = {
         "timestamp": datetime.timestamp(datetime.now()),
         "from_module": "POLICY_MANAGER",
         "from_component": "POLICY_EDITOR_TOOL",
         "to_module": "ORCHESTRATOR",
         "to_component": "ORCHESTRATOR",
         "incoming": False,
         "method": "REST",
         "data": body,
         "notes": "MSPL-OP enforcement request"
        }
        logger.info(logging_message)
        """
        logging_message = {
         "timestamp": 1533311972,
         "from_module": "REACTION",
         "from_component": "SAS",
         "to_module": "SEAL_MANAGEMENT",
         "to_component": "DSPS",
         "incoming": False,
         "method": "RabbitMQ",
         "data": {"foo": 1, "bar": 2},
         "notes": "Sample data"
        }
        """
        remote_logger.info(logging_message)

        logger.info("Sending to {}".format(SECURITY_ORCHESTRATOR_URL))
        logger.info(body)
        response = requests.post("{}".format(SECURITY_ORCHESTRATOR_URL),data=body, timeout=7)
        logger.debug("Security Orchestrator response {}".format(response.text))
        resp.status = falcon.HTTP_200 
        resp.body = response.text

        return resp.body
        # TODO: manage connection errors

    def on_delete(self, req, resp):
        """Handles POST requests"""
        body = req.stream.read().decode("utf-8")
        if not body:
            raise falcon.HTTPBadRequest('Empty request body',
                                        'A valid JSON document is required.')

        json_mspl_policies = json.loads(body) 
        

        # Request the enforcement to orchestrator
        response = requests.delete("{}".format(SECURITY_ORCHESTRATOR_URL),json=json_mspl_policies)
        #logger.debug("Security Orchestrator response {}".format(response.text))
        resp.status = falcon.HTTP_200 
        resp.body = response.text
        return resp.body
        # TODO: manage connection errors


class M2LService(object):
    """Implementation of Medium-to-Low service"""

    def to_json(self,mspl_translations,conflicts_dependencies):
        """Codify into a common json m2lservice output the configuration."""
        return json.dumps({"translations":mspl_translations,**conflicts_dependencies})

    def on_get(self, req, resp):
        """GET Method is not allowed"""
        raise falcon.HTTPMethodNotAllowed('GET method not supported',
                                        'Please, uses POST in order to provide the MSPL.')

    def generate_ID(self, mspl_object):
        """Generates an ID if it is not already provided."""
        if not mspl_object.ID:
            mspl_object.ID = "mspl_{}".format(uuid.uuid4().hex)


    def link_to_orchestration(self,mspl_object, orchestration_id):
        """Link the MSPL with an orchestration MSPL if required"""
        if not mspl_object.orchestrationID:
            mspl_object.orchestrationID = orchestration_id

    def parse_xml_input(self, data):
        """Parse to translator input format"""
        m2ltranslator = M2LTranslator()
        return m2ltranslator.load(data)

    def parse_json_input(self, data):
        """Load the MSPL policy and include the security enabler provided in json"""
        #The input is {"mspl_list": [{"mspl": <MSPL>,"security_enabler":"test"}]
        mspl_list = []
        json_input = json.loads(data)  
        global mspl
        for mspl_enabler in json_input["mspl_list"]:
            # Load the MSPL
            mspl_object = M2LTranslator.load(mspl_enabler["mspl"])
            # Include the enabler in the MSPL
            if not mspl_object.enablerCandidates:
                mspl_object.enablerCandidates = mspl.EnablerCandidates()
                mspl_object.enablerCandidates.append(mspl_enabler["security_enabler"])
            # Add to list
            #mspl_policies.append(mspl_policy)
            #logger.debug(mspl_policy.toDOM(element_name="ITResource").toprettyxml(encoding="utf-8").decode("utf-8").replace(":ns1","").replace("ns1:","").replace(":ns2","").replace("ns2:",""))
            #self.generate_ID(mspl_object)
            #mspl_enabler["mspl"] = mspl_object
            mspl_list.append(mspl_object)
            #mspl_policy = mspl_policy.toDOM(element_name="ITResource").toprettyxml(encoding="utf-8").decode("utf-8").replace(":ns1","").replace("ns1:","").replace(":ns2","").replace("ns2:","")
        #logger.debug(mspl_list)
        return mspl_list
        
    #@timing("M2LService")
    def on_post(self, req, resp):
        """Handles POST requests for MSPL translation

            Output example: 
                If policy for orchestration
                    {"omspl_translation": {"mspl_id": MSPL_ID,"mspl": MSPL_TEXT,"mspl_translations":[{"mspl_id":MSPL_ID,
                                    "mspl":MSPL_TEXT,"enabler":ENABLER_NAME,"enabler_conf":ENABLER_CONFIGURATION}]}}
                If standard policy/es
                    {"mspl_translations":[{"mspl_id":MSPL_ID,
                                    "mspl":MSPL_TEXT,"enabler":ENABLER_NAME,"enabler_conf":ENABLER_CONFIGURATION}]}}}
        """

        # Content-type verification
        try:
            input_type = req.content_type.split("/")[1]
            parser_input_function = getattr(self, "parse_{}_input".format(input_type))
        except Exception as e:
            raise falcon.HTTPInvalidHeader('Content-Type ERROR',
                                        'It is expected application/json or application/xml')

        # Not empty data verification
        data = req.stream.read().decode("utf-8")
        if not data:
            raise falcon.HTTPInvalidParam('Empty request body',
                                        'A valid JSON/XML document is required.')
        # Input parser (old input is provided as {"mspl_list": [{"mspl":"MSPL_XML", "security_enabler": "enabler_name"}]})
        # Regular input provides an xml which includes the enabler in the enablerCandidates
        logger.debug("M2L Received: {}".format(data))

        #TODO: Remote logging
        logging_message = {
         "timestamp": datetime.timestamp(datetime.now()),
         "from_module": "ORCHESTRATOR",
         "from_component": "ORCHESTRATOR",
         "to_module": "POLICY_MANAGER",
         "to_component": "POLICY_INTERPRETER",
         "incoming": True,
         "method": "REST",
         "data": data,
         "notes": "M2L translation request"
        }
        """
        logging_message = {
         "timestamp": 1533311972,
         "from_module": "REACTION",
         "from_component": "SAS",
         "to_module": "SEAL_MANAGEMENT",
         "to_component": "DSPS",
         "incoming": False,
         "method": "RabbitMQ",
         "data": {"foo": 1, "bar": 2},
         "notes": "Sample data"
        }
        """
        remote_logger.info(logging_message)

        try:
            mspl_data = parser_input_function(data)
        except Exception as e:
            logger.exception(e)
            raise falcon.HTTPInvalidParam('Error parsing MSPL', e)

        # Policy conclict detection
        #detector = MSPLConflictDetector()
        #conflicts_dependencies = detector.detect(mspl_data)


        # Call conflict detection API
        response = requests.post(CONFLICT_DETECTOR_URL,data=data)
        logger.info(response)
        conflicts_dependencies = json.loads(response.text)


        # Policies translation
        try:
            m2ltranslator = M2LTranslator()
            mspl_translations = m2ltranslator.translate(mspl_data)
        except Exception as e:
            logger.exception(e)
            raise falcon.HTTPInvalidParam('Error translating MSPL', e)

        # Post the result in the policy repository
        try:
            requests.post("{}{}".format(POLICY_REPOSITORY_SERVICE_URL,SET_TRANSLATION_URL),json=mspl_translations)
        except Exception as e:
            logger.exception(e)
            raise falcon.HTTPInternalServerError("Error registering the translation in Policy Repository", e)
        
        resp.body = self.to_json(mspl_translations, conflicts_dependencies)

        
        logging_message = {
         "timestamp": datetime.timestamp(datetime.now()),
         "from_module": "POLICY_MANAGER",
         "from_component": "POLICY_INTERPRETER",
         "to_module": "ORCHESTRATOR",
         "to_component": "ORCHESTRATOR",
         "incoming": False,
         "method": "REST",
         "data": {"translations":mspl_translations,**conflicts_dependencies},
         "notes": "M2L translation result"
        }
        
        remote_logger.info(logging_message)

       
class MUD2MSPLService(object):
    """Implementation of MUD to MSPL service"""
       
    #@timing("MUD2MSPLService")
    def on_post(self, req, resp):
        """Handles POST requests for MUD to MSPL translation."""
        # Load the mud data
        device_mud = req.stream.read().decode("utf-8")
        if not device_mud:
            raise falcon.HTTPInvalidParam('Empty request body',
                                        'A valid MUD-file document is required.')
        # MUD translation
        mud2m_translator = MUD2MTranslator()
        resp.body = mud2m_translator.translate(device_mud)


class HealthService(object):

    def on_get(self, req, resp):
        resp.body = json.dumps({"status":"OK"})


# falcon.API instances are callable WSGI apps
app = falcon.API()

# Resources are represented by long-lived class instances
m2lservice = M2LService()
m2eservice = M2EService()
mud2mservice = MUD2MSPLService()
health_service = HealthService()

# things will handle all requests to the '/things' URL path
app.add_route('/m2lservice', m2lservice)
app.add_route('/m2eservice', m2eservice)
app.add_route('/mud2mservice', mud2mservice)
app.add_route('/health', health_service)

